var GameComponent = require('../GameComponent')
var CollisionHelper = require('./CollisionHelper')

function CircleCollider(gameObject) {
	GameComponent.call(this, gameObject)
	this.radius = null
}

CircleCollider.prototype.constructor = CircleCollider

CircleCollider.prototype.checkCollision = function(collider) {
	if (!collider) return CollisionHelper.noCollision()

	switch (collider.constructor) {
		case CircleCollider:	
			return CollisionHelper.circleToCircle(this, collider)
		case LineCollider:
			return CollisionHelper.circleToLine(this, collider)
		default:
			//throw('CircleCollder collision against unknown collider')
			//return CollisionHelper.noCollision()
	}
}



// Testing...
CircleCollider.prototype.onCollision = function(otherCollider) {

	//TODO: if this collider has a tag that overlaps with the other collider
	// then we have collided
}

module.exports = CircleCollider