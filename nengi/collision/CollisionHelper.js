
//static
var CollisionHelper = {}


CollisionHelper.noCollision = function() {
	return { isCollision: false }
}


CollisionHelper.circleToCircleExplicit = function(cAx, cAy, cAr, cBx, cBy, cBr) {
	
	//console.log('ctce', 'a', cAx, cAy, cAr,'b', cBx, cBy, cBr)
	// equation: (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2
	var dx = (cBx - cAx)
	var dy = (cAy - cBy)

	var rr = cAr + cBr

	return { isCollision: (dx * dx) + (dy * dy) <= (rr * rr) }
}

CollisionHelper.circleToCircle = function(circleA, circleB) {
	
	return CollisionHelper.circleToCircleExplicit(
		circleA.x, 
		circleA.y, 
		circleA.radius,
		circleB.x, 
		circleB.y, 
		circleB.radius
	)
}

/*
* Circle collision check agianst line 
* Returns bool along w/ entry and exit points
*/
CollisionHelper.circleToLine = function(circle, line) {

	var dx = line.x2 - line.x1
	var dy = line.y2 - line.y1

	var fx = line.x1 - circle.x
	var fy = line.y1 - circle.y

	var a = MathEx.dot(dx, dy, dx, dy)//MathEx.dot(line.x1, line.y1, line.x2, line.y2)//d.Dot( d ) 
	var b = 2 * MathEx.dot(fx, fy, dx, dy)//2*f.Dot( d ) 
	var c = MathEx.dot(fx, fy, fx, fy) - (circle.radius * circle.radius)//f.Dot( f ) - r*r 

	var discriminant = b*b-4*a*c

	if( discriminant < 0 )	{
	  // no intersection
	}	else	{
	  // ray didn't totally miss sphere,
	  // so there is a solution to
	  // the equation.

	  discriminant = Math.sqrt(discriminant)

	  // either solution may be on or off the ray so need to test both
	  // t1 is always the smaller value, because BOTH discriminant and
	  // a are nonnegative.
	  var t1 = (-b - discriminant)/(2*a)
	  var t2 = (-b + discriminant)/(2*a)

	  // 3x HIT cases:
	  //          -o->             --|-->  |            |  --|->
	  // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

	  // 3x MISS cases:
	  //       ->  o                     o ->              | -> |
	  // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

	  var t1Hit = false
	  if( t1 >= 0 && t1 <= 1 )
	  {
	    // t1 is the intersection, and it's closer than t2
	    // (since t1 uses -b - discriminant)
	    // Impale, Poke
	    t1Hit = true
	  }

	  // here t1 didn't intersect so we are either started
	  // inside the sphere or completely past it
	  var t2Hit = false
	  if( t2 >= 0 && t2 <= 1 )
	  {
	  	t2Hit = true
	    // ExitWound
	  }
	  if (t1Hit || t2Hit) {
	  	//return true
	  }
	  // no intn: FallShort, Past, CompletelyInside

	  return { isCollision: (t1Hit || t2Hit), t1: t1, t2: t2 }
	}
	return { isCollision: false }
}


module.exports = CollisionHelper