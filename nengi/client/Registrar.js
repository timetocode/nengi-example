var Snapshot = require('./Snapshot')
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')
/*
* A record of game state sent from the server
*/
function Registrar(common) {
	this.common = common
	this.snapshots = {}
	this.mostRecentTick = -1
	this.lastConfirmedTick = -1
	this.ping = 0



    this.entities = []
    this.entitiesRef = {}

    this.gameEvents = []
    this.gameEventsRef = {}
    //this.newGameEvents = []

    this.directMessages = []
    this.directMessagesRef = {}

	// debug representation of serverside quadtrees+aabbs
	this.aabbs = []
}


Registrar.prototype.createEntity = function(proxy) {
	var ctor = this.common.entityConstructors.getConstructor(proxy[1])
	var entity = new ctor()
	entity.id = proxy[0]
	entity.n_type = proxy[1]
	entity.n_constuctorName = ctor.name
	entity.x = proxy[2]
	entity.y = proxy[3]

	BinarySerializer.deproxify(proxy, entity)

    entity.instance = this
    this.entities.push(entity)
    this.entitiesRef[entity.id] = entity
    return entity
}





Registrar.prototype.addMessage = function(proxy) {
	this.snapshots[this.mostRecentTick].addMessage(proxy)
}

Registrar.prototype.addGameObjectProxy = function(proxy) {
	console.log('add whole proxy')
	this.snapshots[this.mostRecentTick].addGameObjectProxy(proxy)
}

Registrar.prototype.addGameEventProxy = function(proxy) {
	//console.log('add event')
	this.snapshots[this.mostRecentTick].addGameEventProxy(proxy)
}

Registrar.prototype.updateGameObjectProxy = function(id, prop, value) {
	this.snapshots[this.mostRecentTick].updateGameObjectProxy(id, prop, value)
}

Registrar.prototype.updateProxySimple = function(id, index, value) {
	this.snapshots[this.mostRecentTick].updateProxySimple(id, index, value)
}

Registrar.prototype.updateProxyDeltaSimple = function(id, prop, value) {
	this.snapshots[this.mostRecentTick].updateProxyDeltaSimple(id, prop, value)
}

Registrar.prototype.updateGameObjectProxyDelta = function(id, prop, value) {
	this.snapshots[this.mostRecentTick].updateGameObjectProxyDelta(id, prop, value)
}

Registrar.prototype.forget = function(id) {
	this.snapshots[this.mostRecentTick].forget(id)
}

Registrar.prototype.getGameObjectProxy = function(id, tick) {

	if (this.snapshots[tick]) {
		var snapshot = this.snapshots[tick]

		if (snapshot.gameObjectsProxiesRef[id]) 
			return snapshot.gameObjectsProxiesRef[id]
	}
	return null
}



Registrar.prototype.getGameEventProxy = function(id, tick) {
	if (this.snapshots[tick]) {
		var snapshot = this.snapshots[tick]

		if (snapshot.gameEventProxiesRef[id]) 
			return snapshot.gameEventProxiesRef[id]
	}
	return null
}


Registrar.prototype.getRecentGameEvents = function() {
	if (this.snapshots[this.mostRecentTick]) {
		var snapshot = this.snapshots[this.mostRecentTick] 
		return snapshot.gameEventProxies
	}
	return null
}

Registrar.prototype.getSnapshot = function(tick) {
	if (this.snapshots[tick]) {
		return this.snapshots[tick]
	}
	return null
}

Registrar.prototype.markNew = function(id) {
	this.snapshots[this.mostRecentTick].markNew(id)
}

Registrar.prototype.begin = function(tick) {
	this.mostRecentTick = tick
	var now = Date.now()

	var tickToClone = tick - 1
	if (tickToClone === -1) {
		tickToClone = 255
	}

	if (this.snapshots[tickToClone]) {
		var previous = this.snapshots[tickToClone]
		var clone = previous.clone()
		this.snapshots[tick] = clone
	} else {
		this.snapshots[tick] = new Snapshot()
	}
	
	this.snapshots[tick].tick = tick
	this.snapshots[tick].timestamp = now
}

Registrar.prototype.setLastConfirmedTick = function(lastConfirmedTick) {
	this.snapshots[this.mostRecentTick].confirmed = lastConfirmedTick
	this.lastConfirmedTick = lastConfirmedTick
}

module.exports = Registrar