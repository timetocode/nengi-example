var createSchema = require('../binary/BinarySerializer').createSchema
var BinaryType = require('../binary/BinaryType')

function InputFrame() {
	this.id = -1
	this.tick = -1
}

InputFrame.prototype.constructor = InputFrame

// specify which properties should be networked, and their value types
InputFrame.prototype.netSchema = createSchema({
	'id': BinaryType.UInt8,
	'type': BinaryType.UInt8,
	'tick': BinaryType.UInt8
})

module.exports = InputFrame

