//var Shared = require('../core/Shared')

function Snapshot() {
	this.tick = -1
	this.confirmed = -1
	this.gameObjectsProxiesRef = {}
	this.gameObjectProxies = []
	
	//this.eventsRef = {}
	this.events = []

	this.messages = []
	//this.messagesRef = []

	this.newly = []
	this.forgets = []
	//this.events = {}

	this.loaded = false
}

Snapshot.prototype.clone = function() {
	var clonedSnapshop = new Snapshot()
	clonedSnapshop.tick = this.tick
	for (var i = 0; i < this.gameObjectProxies.length; i++) {
		var proxy = this.gameObjectProxies[i]
		var clonedProxy = proxy.slice()
        clonedSnapshop.addGameObjectProxy(clonedProxy)
	}
	return clonedSnapshop
}

Snapshot.prototype.forget = function(id) {
	//console.log('forgetting', id)
	this.forgets.push(id)


	delete this.gameObjectsProxiesRef[id]

	var index = -1
	for (var i = 0; i < this.gameObjectProxies.length; i++) {
		if (this.gameObjectProxies[i][0] === id) {
			index = i
			break
		}
	}

	if (index !== -1) {
		this.gameObjectProxies.splice(index, 1)
	}
}

Snapshot.prototype.addMessage = function(proxy) {
	this.messages.push(proxy)	
}


Snapshot.prototype.addGameObjectProxy = function(proxy) {
	var id = proxy[0]
	if (!this.gameObjectsProxiesRef[id]) {
		this.gameObjectsProxiesRef[id] = proxy
		this.gameObjectProxies.push(proxy)
	}
}

Snapshot.prototype.addGameEventProxy = function(proxy) {
	this.events.push(proxy)
}

Snapshot.prototype.markNew = function(id) {
	this.newly.push(id)
}

/*
Snapshot.prototype.updateGameObjectProxy = function(id, prop, value) {
	var netSchema = Shared.gameObjectCtors.getNetSchema(this.gameObjectsProxiesRef[id][1])
	var index = netSchema.properties[prop].key
	this.gameObjectsProxiesRef[id][index] = value
}
*/

Snapshot.prototype.updateProxySimple = function(id, index, value) {
	this.gameObjectsProxiesRef[id][index] = value
}

Snapshot.prototype.updateProxyDeltaSimple = function(id, index, value) {
	//console.log('updateProxySimple', id, index, value, this.tick, this.gameObjectsProxiesRef)
	this.gameObjectsProxiesRef[id][index] -= value
}

/*
Snapshot.prototype.updateGameObjectProxyDelta = function(id, prop, deltaValue) {
	var netSchema = Shared.gameObjectCtors.getNetSchema(this.gameObjectsProxiesRef[id][1])
	var index = netSchema.properties[prop].key
	this.gameObjectsProxiesRef[id][index] -= deltaValue
}
*/

Snapshot.prototype.getGameObjectProxy = function(id) {
	return this.gameObjectsProxiesRef[id]
}

module.exports = Snapshot