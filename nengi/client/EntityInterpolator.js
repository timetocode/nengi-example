var MathEx = require('../MathEx')
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

function EntityInterpolator(emitter, registrar, common) {
	this.emitter = emitter
	this.registrar = registrar
	this.common = common
	this.lastProcessed = 0
	this.isEnabled = true


}

/**
* Finds two snapshots on either side of renderTime
* there are a few common outcomes of this procedure:
*  1) there are no shapshots yet, because the client has just connected
*  2) there is snapshotA but no snapshotB because the client experienced a
*     lag spike (consider extrapolation)
*  3) both snapshotA and snapshotB exist, so interpolation will be possible
*
* @param {Registrar} registrar The source of snapshot data
* @param {Integer} renderTime A time in milliseconds in the recent past
* @return {Object} Returns { snapshotA: snapshotA, snapshotB: snapshotB }
*/
EntityInterpolator.prototype.findSnapshots = function(renderTime) {
	var snapshotA = null
	var snapshotB = null

	for (var i = 0; i < 10; i++) {
		// begin looking 0 snapshots ago (most recent)
		var snapshotId = this.registrar.mostRecentTick - i

		// wrap id from 0-255
		if (snapshotId < 0) {
			//snapshotId = 256 + snapshotId
		}
		var temp = this.registrar.getSnapshot(snapshotId)
		// does this snapshop qualify? (it must be older than renderTime)
		// it is possible for no snapshotA to exist if a client is freshly 
		// connected
		if (!snapshotA && temp && temp.timestamp < renderTime) {
			// we have found snapshotA
			snapshotA = temp

			// look for snapshotB, the snapshot that followed snapshotA
			var snapshotId2 = snapshotA.tick + 1
			// wrap id from 0-255
			if (snapshotId2 < 0) {
				//snapshotId2 = 256 + snapshotId2
			}
			if (snapshotId2 > 255) {
				//snapshotId2 = snapshotId2 - 256
			}

			var temp2 = this.registrar.getSnapshot(snapshotId2)
			// due to lag and whatshot it is possible to have a snapshotA but
			// no snapshotB, check if snapshotB exists
			if (temp2 && temp2.timestamp > renderTime) {
				snapshotB = temp2
				// end the for loop if snapshots A & B have been found already
				continue 
			}
		}
	}

	return { snapshotA: snapshotA, snapshotB: snapshotB }
}

EntityInterpolator.prototype.handleCreatesAndDeletes = function(snapshot) {


	//this.lastProcessed = snapshot.tick


	// createEntity
	for (var i = 0; i < snapshot.newly.length; i++) {
		var id = snapshot.newly[i]

		// look up the object's constructor and return a populated instance
		var proxy = snapshot.getGameObjectProxy(id)	
		var entity = this.registrar.createEntity(proxy)
		//console.log('emitting createEntity')
		this.emitter.emit('createEntity', entity)		
	}


	// deleteEntity
	for (var i = 0; i < snapshot.forgets.length; i++) {
		var id = snapshot.forgets[i]
		this.emitter.emit('deleteEntity', id)		
	}


}


EntityInterpolator.prototype.createEvents = function(snapshot) {
	// createEvent
	for (var i = 0; i < snapshot.events.length; i++) {
		//console.log('trying to create event', snapshot.gameEventProxies[i])
		//var id = snapshot.gameEventProxies[i]
		var proxy = snapshot.events[i]//snapshot.gameEventProxiesRef[id]
	
		var ctor = this.common.gameEventConstructors.getConstructor(proxy[0])
		var newEvent = new ctor()
		//newEvent.id = proxy[0]
		newEvent.n_type = ctor.name
		//newEvent.x = proxy[2]
		//newEvent.y = proxy[3]
		newEvent.targetId = proxy[3]

		this.emitter.emit('createEvent', newEvent)
	}
}

EntityInterpolator.prototype.createMessages = function(snapshot) {
	// createEvent
	for (var i = 0; i < snapshot.messages.length; i++) {
		var proxy = snapshot.messages[i]
	
		var ctor = this.common.messageConstructors.getConstructor(proxy[0])
		var message = new ctor()
		//newEvent.id = proxy[0]
		message.n_type = ctor.name
		//newEvent.x = proxy[2]
		//newEvent.y = proxy[3]
		//console.log('deproxify', proxy, message.netSchema)
		BinarySerializer.deproxify2(proxy, message)
		//console.log('resulting message', message)

		//message.targetId = proxy[3]

		this.emitter.emit('createMessage', message)
	}
}


/**
* Interpolates a position between snapshotA and snapshotB for every entity based
* on renderTime
* Returns nothing, emits 'updateEntity' { id: id, x: interpX, y: interpY }
*
* @param {Object} snapshotA The snapshot of entities before renderTime
* @param {Object} snapshotB The snapshot of entities after renderTime
* @param {Integer} renderTime A time in milliseconds inbetween snapshots A and B
* @emit {Object} updateEntity Emits 'updateEntity', { 
*	id: {Integer} id The id of the entity interpolated, 
*   x: {Number} x A new x position, 
*   y: {Number} Y A new y position
* }
*/
EntityInterpolator.prototype.interpolateSnapshots = function(snapshotA, snapshotB, renderTime) {
	// length in milliseconds of the time between snapshotA and snapshotB
	// this number is usually exactly the length of a tick, but can vary minorly
	var total = snapshotB.timestamp - snapshotA.timestamp

	// how long our target renderTime occured after snapshotA in milliseconds
	var portion = renderTime - snapshotA.timestamp
	
	// relative distance between snapshots represented by our renderTime (e.g.
    // 0.5 would mean halfway between snapshotA and snapshotB)
	var ratio = portion / total

	for (var i = 0; i < snapshotA.gameObjectProxies.length; i++) {
		var id = snapshotA.gameObjectProxies[i][0]
		var entityA = snapshotA.getGameObjectProxy(id)
		var entityB = snapshotB.getGameObjectProxy(id)
		
		// can only interpolate for entities that exist in both snapshots, 
		// this will exclude entities freshly created or deleted 
		if (entityA && entityB) {
			// lerp each position

			var netSchema = this.common.entityConstructors.getNetSchema(entityA[1])
			var interpResult = this.interpolateEntity(entityA, entityB, ratio)
			//console.log('interpResult', interpResult)
		    this.emitter.emit('updateEntity', id, interpResult)
		}
	}
}


EntityInterpolator.prototype.interpolateEntity = function(stateA, stateB, ratio) {
	var netSchema = this.common.entityConstructors.getNetSchema(stateA[1])


	var interpolatedArr = []

	//var interpolated = {}
	//interpolated['id'] = stateA[0]

	// skip i = 0 (id), skip i = 1 (type)
	for (var i = 2; i < netSchema.keys.length; i++) {


		// the name of the property
		var key = netSchema.keys[i]
		var propertyData = netSchema.properties[key]



		// values of the property at two positions in time
		var valueA = stateA[i]
		var valueB = stateB[i]

		// for the properties that skip interpolation
		var interpEnabledForThisProperty = propertyData.interp
		if (!interpEnabledForThisProperty) {
			interpValue = valueA
			interpolatedArr.push({ property: key, value: interpValue })
			// skip the rest of this iteration
			continue
		}
		//console.log('key', key, valueA)
		//console.log('interpolating', key, valueA, valueB)
		// skip interpolation if the values are the same
		if (valueA === valueB) {
			// skip the rest of this iteration
			continue
		}

		var binaryTypeSpec = Binary[propertyData.binaryType]
		//console.log('binaryTypeSpec', binaryTypeSpec)

		var interpValue = valueA

		if (binaryTypeSpec.interp && binaryTypeSpec.interp === 'lerp') {
			interpValue = MathEx.lerp(valueA, valueB, ratio)
		}

		if (binaryTypeSpec.interp && binaryTypeSpec.interp === 'byteRotationLerp') {

			interpValue = MathEx.interpolateRotation(valueA, valueB, ratio)
			//console.log('interp rotation', valueA, valueB, ratio, 'result', interpValue)
		}


		interpolatedArr.push({ property: key, value: interpValue })


	}
	return interpolatedArr
}

EntityInterpolator.prototype.update = function() {
	if (!this.isEnabled) {
		this.skipInterpolation()
		return
	}

	var now = Date.now()
	var renderTime = now - 100

	var snapshots = this.findSnapshots(renderTime)

	var snapshotA = snapshots.snapshotA
	var snapshotB = snapshots.snapshotB

	// if there was at least a snapshotA, emit all of its create/delete events
	if (snapshotA) {
		//console.log('processing snapshotA', snapshotA.tick, this.lastProcessed)
		if (snapshotA.tick === this.lastProcessed) {
			return
		} else if (snapshotA.tick > this.lastProcessed) {
			var diff = snapshotA.tick - this.lastProcessed
			//console.log('idff', diff)
			if (diff === 1) {

				if (!snapshotA.isLoaded) {
					this.handleCreatesAndDeletes(this.registrar.snapshots[snapshotA.tick])

					
					snapshotA.isLoaded = true
				}

				//this.lastProcessed = snapshotA.tick
			} else {
				//console.log('BEHIND', diff, 'catch up!')

				for (var i = diff-1; i >= 0; i--) {
					var tick = snapshotA.tick - i
					//console.log('catching up on', tick)
					if (!this.registrar.snapshots[tick].isLoaded) {
						this.handleCreatesAndDeletes(this.registrar.snapshots[tick])
						this.registrar.snapshots[tick].isLoaded = true
						this.lastProcessed = tick
					}

				}
				//console.log('CAUGHT UP')
			}
			

			/*
			for (var i = diff; i > 0; i++) {
				var tick = this.lastProcessed - i




				console.log('processing', tick)
				if (this.registrar.snapshots[tick])
				this.handleCreatesAndDeletes(this.registrar.snapshots[tick])
			}
			console.log('CAUGHT UP')
			*/
		} else {

		}


	}

	// if there both snapshots exist, it is interpolation time!
	if (snapshotA && snapshotB) {
		this.interpolateSnapshots(snapshotA, snapshotB, renderTime)
	} else {
		//console.log('could not interpolate')
		// there is where extrapolation code could go
	}

	var snapshot = this.registrar.snapshots[this.registrar.mostRecentTick]
	if (snapshot && !snapshot.loaded) {
		this.createEvents(snapshot)
		this.createMessages(snapshot)
		snapshot.loaded = true
	}
}


EntityInterpolator.prototype.skipInterpolation = function() {
	var snapshot = this.registrar.snapshots[this.registrar.mostRecentTick]

	if (snapshot) {
		this.handleCreatesAndDeletes(snapshot)
		this.createEvents(snapshot)
		this.createMessages(snapshot)
		snapshot.loaded = true

		for (var i = 0; i < snapshot.gameObjectProxies.length; i++) {
			var proxy = snapshot.gameObjectProxies[i]

			this.emitter.emit('updateEntity', {
				id: proxy[0],
				x: proxy[2],
				y: proxy[3]
			})
		}
	}
}

module.exports = EntityInterpolator