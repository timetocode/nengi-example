
var Registrar = require('./Registrar')
var GameStateReader = require('../network/GameStateReader')
var PlayerInputNetworker = require('../network/PlayerInputNetworker')
var BitBuffer = require('../binary/BitBuffer')
var BinarySerializer = require('../binary/BinarySerializer')
var EntityInterpolator = require('./EntityInterpolator')
var EventEmitter = require('../external/EventEmitter')
var InputFrame = require('./InputFrame')

/**
* Clientside (browser) entry point for nengi
*
*/

var latency = 0// add fake lag with this number
function withSimulatedLatency(fn) {
	setTimeout(fn, latency)
}

function Application(common) {
	EventEmitter.call(this)

	
	// a counter for game state snapshots sent FROM the server
	this.snapshotTick = 0
	// a counter for input state snapshots sent TO the server
	this.inputTick = 0

	// inputs to be sent
	this.inputBuffer = []

	this.websocket = null
	this.registrar = new Registrar(common)
	this.prediction = new Prediction(this)
	this.common = common
	this.gameStateReader = new GameStateReader(this.registrar, common)
	this.playerInputNetworker = new PlayerInputNetworker(common)
	this.entityInterpolator = new EntityInterpolator(this, this.registrar, common)
}

Application.prototype = Object.create(EventEmitter.prototype)
Application.prototype.constructor = Application


function Prediction(app) {
	this.app = app
	this.context = null
}

Prediction.prototype.createPredictionContext = function(id, acceptsInputs) {
	// TODO n_key isnt set, setup n_type and n_typename or something like that
	//var constructor = this.common.entityConstructors.getConstructor(serverEntity.n_type)

	//var clientEntity = new constructor()
	//clientEntity.x = serverEntity.x
	//clientEntity.y = serverEntity.y
	console.log('createPredictionContext', id, acceptsInputs)
	this.context = new PredictionContext()
	this.context.id = id
	this.context.acceptsInputs = acceptsInputs

	console.log('PREDICTION CONTEXT', this.context)
	//throw('k')
}

function PredictionContext() {
	this.id = -1
	this.isBound = false
	this.acceptsInputs = false
	this.serverEntity = null
	this.predictedEntity = null
	this.lastConfirmedInput = -1
	this.pending = []
}

PredictionContext.prototype.constructor = PredictionContext

PredictionContext.prototype.resolve = function(app) {
	//console.log('resolve')
	var obj = app.registrar.getGameObjectProxy(this.id, app.registrar.mostRecentTick)
	if (obj) {
		console.log('OBJ FOUND', obj)

		var constructor = app.common.entityConstructors.getConstructor(obj[1])
		//var serverEntity = 

		this.serverEntity = new constructor()
		BinarySerializer.deproxify(obj, this.serverEntity)

		this.predictedEntity = new constructor()
		BinarySerializer.deproxify(obj, this.predictedEntity)

		var original = this.predictedEntity.update

		
		this.predictedEntity.update = function(delta) {			
			original.apply(this, [delta])
			//console.log('!!', this.rotation)


		}
		

		this.isBound = true
	}
}
	

Prediction.prototype.update = function(inputs, delta) {
	var ctx = this.context
	if (!ctx) return

	if (inputs.length < 1) return 


	//ctx.pending[inputs[0].tick] = inputs
	ctx.pending.push(inputs)



	//console.log('inputs', inputs)
	//console.log('CONFIRM', this.app.registrar.lastConfirmedTick)

	var num = this.app.registrar.lastConfirmedTick
	var num2 = inputs[0].tick

	//console.log('diff', num, num2)

	var diff = 0

	if (num > num2) {
		diff = num2 + 256 - num
	} else {
		diff = num2 - num
	}

	while (ctx.pending.length > diff) {
		ctx.pending.shift()
	}


	if (ctx && !ctx.isBound) {
		//console.log('unbound')
		ctx.resolve(this.app)
		//console.log(ctx)
		
	} else if (ctx && ctx.isBound) {
		//console.log('bound, upating') 
		var obj = this.app.registrar.getGameObjectProxy(ctx.id, this.app.registrar.mostRecentTick)
		//console.log()

		if (!obj) return


		ctx.serverEntity.x = obj[2]
		ctx.serverEntity.y = obj[3]

		ctx.predictedEntity.x = ctx.serverEntity.x
		ctx.predictedEntity.y = ctx.serverEntity.y
		ctx.predictedEntity.rotation = ctx.serverEntity.rotation

		var debugString = 0
		/*
		for (var tick in ctx.pending) {
			//console.log('INPUT TICK', tick)
			var inputs = ctx.pending[tick]
			debugString++ //+= tick + ' '

			for (var i = 0; i < inputs.length; i++) {
				ctx.predictedEntity.applyInput(inputs[i])
			}
			ctx.predictedEntity._update(1/1000)

		}*/

		for (var i = 0; i < ctx.pending.length; i++) {
			var inputs = ctx.pending[i]

			for (var j = 0; j < inputs.length; j++) {
				ctx.predictedEntity.applyInput(inputs[j])
			}
			ctx.predictedEntity._update(0.001)
		}

		//console.log('debug', debugString)
		this.app.emit('prediction', ctx.predictedEntity)
	}


}





Application.prototype.connect = function() {
	var self = this
	this.websocket = new WebSocket('ws://' + this.common.config.IP + ':' + this.common.config.PORT, this.common.config.PROTOCOL)
	this.websocket.binaryType = 'arraybuffer'	

	this.websocket.onopen = function() {
		console.log('WebSocket connection open')
	}

	this.websocket.onerror = function(err) {
		console.log('WebSocket error', err)
	}

	this.websocket.onclose = function() {
		//throw('stop')
	}

	this.websocket.onmessage = function(message) {
		if (message.data instanceof ArrayBuffer) {		
			withSimulatedLatency(function() {
				self.snapshotTick++
				self.registrar.begin(self.snapshotTick)
				self.gameStateReader.readBinary(message.data, self.websocket)
			})
		} else if (typeof message.data === 'string') {
			console.log('received json from server, ignoring')
		} else {
			console.log('unknown websocket data type')
		}
	}
	this.emit('connect')

}

Application.prototype.disconnect = function() {
	//TODO
}

Application.prototype.send = function(input) {
	if (this.inputBuffer.length === 0) {
		var inputFrame = new InputFrame()
		inputFrame.tick = this.inputTick
		this.inputBuffer.push(inputFrame)
	}
	this.inputBuffer.push(input)
}

Application.prototype.sendInputs = function(playerInputs) {
	// TODO: calculate the real size of the buffer instead of just picking a big enough number
	var bitBuffer = new BitBuffer(256)
	var offset = 0
	var self = this
	if (this.websocket && this.websocket.readyState === 1) {
		for (var i = 0; i < playerInputs.length; i++) {
			var input = playerInputs[i]
			input.type = this.common.inputConstructors.getConstructorKeyByName(input.constructor.name)
		}

		offset = this.playerInputNetworker.write(bitBuffer, offset, playerInputs)			
		withSimulatedLatency(function() {self.websocket.send(bitBuffer.byteArray)})
	}
}


Application.prototype.update = function(delta) {
	//console.log(this.inputTick, this.snapshotTick)
	this.inputTick++
	if (this.inputTick > 255) {
		this.inputTick = 0
	}
	this.entityInterpolator.update(delta)

	this.sendInputs(this.inputBuffer)
	this.prediction.update(this.inputBuffer, delta)

	this.inputBuffer = []
}

module.exports = Application