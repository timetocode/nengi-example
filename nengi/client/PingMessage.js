var createSchema = require('../binary/BinarySerializer').createSchema
var BinaryType = require('../binary/BinaryType')
var Message = require('../game/Message')

// should this be in a differnet folder?
function PingMessage() {
	Message.call(this)
	this.ping = -1
}

PingMessage.prototype = Object.create(Message.prototype)
PingMessage.prototype.constructor = PingMessage

// specify which properties should be networked, and their value types
PingMessage.prototype.netSchema = createSchema({
	'id': BinaryType.UInt16, // this type should come from the config
	'type': BinaryType.UInt8,
	'ping': BinaryType.UInt16
})

module.exports = PingMessage

