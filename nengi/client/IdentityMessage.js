var createSchema = require('../binary/BinarySerializer').createSchema
var BinaryType = require('../binary/BinaryType')
var Message = require('../game/Message')

// should this be in a differnet folder?
function IdentityMessage() {
	Message.call(this)
	this.identity = -1
}

IdentityMessage.prototype = Object.create(Message.prototype)
IdentityMessage.prototype.constructor = IdentityMessage

// specify which properties should be networked, and their value types
IdentityMessage.prototype.netSchema = createSchema({
	'id': BinaryType.UInt16, // this type should come from the config
	'type': BinaryType.UInt8,
	'identity': BinaryType.EntityId
})

module.exports = IdentityMessage

