var Binary = require('../binary/Binary')

var MessageType = require('../MessageType')

function PlayerInputNetworker(common) {
	this.common = common
}

PlayerInputNetworker.prototype.write = function(bitBuffer, offset, playerInputs) {
	
    var length = playerInputs.length

    bitBuffer.writeUInt8(MessageType.PlayerInputs, offset)
    offset += 8

    bitBuffer.writeUInt8(length, offset)
    offset += 8

	for (var j = 0; j < playerInputs.length; j++) {

		var playerInput = playerInputs[j]
		var netSchema = playerInput.netSchema

		for (var i = 0; i < netSchema.keys.length; i++) {
		    var propName = netSchema.keys[i]
		    var propData = netSchema.properties[propName]
		    var value = playerInput[propName]

		    //console.log('writing', propName, value)
	        bitBuffer[Binary[propData.binaryType].write](value, offset)
        	offset += Binary[propData.binaryType].bits
		}
	}
	return offset
}

PlayerInputNetworker.prototype.read = function(bitBuffer, offset, inputSnapshot) {
	//console.log('PlayerInputNetworker.prototype.read')
	var length = bitBuffer.readUInt8(offset)
	offset += 8
	//console.log('length', length)
	for (var i = 0; i < length; i++) {
		var serialized = []

        var id = bitBuffer.readUInt8(offset)
        offset += 8
        var type = bitBuffer.readUInt8(offset)
        offset += 8

        //console.log('reading input', id, type)
		var netSchema = this.common.inputConstructors.getNetSchema(type)
		var ctor = this.common.inputConstructors.getConstructor(type)

		var playerInput = new ctor()
		playerInput.id = id
		playerInput.type = type

        for (var j = 2; j < netSchema.keys.length; j++) {
            var propName = netSchema.keys[j]
            var propData = netSchema.properties[propName]

            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            playerInput[propName] = value
        }
        //client.instance.emit('playerInput', client, playerInput)
        //client.queueInputFrame(playerInput)

        if (ctor.name === 'InputFrame') {
        	inputSnapshot.tick = playerInput.tick

        	//console.log('INPUTFRAME', playerInput)
        } else {
        	//console.log('REGULARINPUT', playerInput)
        }

        inputSnapshot.add(playerInput)
	}
}

module.exports = PlayerInputNetworker