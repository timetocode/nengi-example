
var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')
var MessageType = require('../../MessageType')
var MathEx = require('../../MathEx')

/**
* Writes partial game objects to the outgoing network buffer
*/
function PartialGameObjectNetworker(common) {
    this.common = common

}

PartialGameObjectNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    //bits += 8

    for (var i = 0; i < ids.length; i++) {

        var id = ids[i].id
        var diffs = ids[i].diffs

        //if (client.isKnownGameObjectId(ids[i])) {
        var netObject = netObjectCache.getEntity(id, tick)

        for (var j = 0; j < diffs.length; j++) {
            // object id
            bits += this.common.config.ID_BINARYTYPE.bits
            //var key = netObject.partial[j].propIndex
            //var value = netObject.partial[j].propValue
            //var propName = netObject.netSchema.keys[key]
            var propData = netObject.netSchema.properties[diffs[j]]

            //if (propName === 'x' || propName === 'y') {
                //continue
            //}
            // propIndex
            bits += 8  
            // propValue
            bits += Binary[propData.binaryType].bits
        }
        //}
    }
    //console.log('pTally', bits)
    return bits
}

PartialGameObjectNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset



    bitBuffer.writeUInt8(MessageType.PartialGameObjects, offset)
    offset += 8
    // write the number of whole proxies to be sent

    var length = 0
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i].id
        var netObject = netObjectCache.getEntity(id, tick)
        length += netObject.partial.length
    }

    //bitBuffer.writeUInt8(length, offset)
    //offset += 8

    for (var i = 0; i < ids.length; i++) {
        var id = ids[i].id
        //bitBuffer.writeUInt16(offset, ids[i])
       // offset += 16
        var netObject = netObjectCache.getEntity(id, tick)
        offset = this.writePartialProxy(bitBuffer, offset, netObject, ids[i].diffs, client)
    

       //client.clientState.gameObjects[ids[i]] = netObject.proxy
    }
    return offset
}

PartialGameObjectNetworker.prototype.writePartialProxy = function(bitBuffer, offset, netObject, diffs, client) {

    var aliasId = client.getAliasId(netObject.id)
    //console.log('partial id', aliasId)
    var cachedObject = client.clientState.gameObjects[netObject.id]

    for (var i = 0; i < diffs.length; i++) {

        
        // write id
        bitBuffer[this.common.config.ID_BINARYTYPE.write](aliasId, offset)        
        offset += this.common.config.ID_BINARYTYPE.bits
        //var key = netObject.partial[i].propIndex
        //var value = netObject.partial[i].propValue
        var propName = diffs[i]
        var propData = netObject.netSchema.properties[propName]
        var key = propData.key
        var value = netObject.proxy[key]

        if (propData.binaryType === BinaryType.Rotation) {
            //console.log('WRITING A ROTATION')
            value = MathEx.radiansToUInt8(value)
        }


        // write the property key (e.g. the key for 'x', 'y', etc)
        bitBuffer.writeUInt8(key, offset)
        offset += 8  
        // write the actual value
        bitBuffer[Binary[propData.binaryType].write](Math.round(value), offset)
        offset += Binary[propData.binaryType].bits


        cachedObject[key] = Math.round(value)

       // console.log(netObject.id, key, Math.round(value))
    }
    
   return offset
}


PartialGameObjectNetworker.prototype.read = function(bitBuffer, offset, registrar) {  
   

    var count = 0

    while (offset < bitBuffer.bitLength && bitBuffer.bitLength - offset >= this.common.config.ID_BINARYTYPE.bits) {
        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)

        var proxyObject = registrar.getGameObjectProxy(id, registrar.mostRecentTick)
        var netSchema = this.common.entityConstructors.getNetSchema(proxyObject[1])

        //console.log('netSchema found', netSchema)
       // var netSchema = 
        //console.log('partial id', id)
        offset += this.common.config.ID_BINARYTYPE.bits
        var key = bitBuffer.readUInt8(offset)
        //console.log('key', key)
        offset += 8
        var propName = netSchema.keys[key]      
        var propData = netSchema.properties[propName]

        var value = bitBuffer[Binary[propData.binaryType].read](offset)
        //console.log('value', value)
        offset += Binary[propData.binaryType].bits
        //console.log('p', id, key, value)

        if (propData.binaryType === BinaryType.Rotation) {
            //console.log('WRITING A ROTATION')
            value = MathEx.UInt8ToRadians(value)
        }

        registrar.updateProxySimple(id, key, value)
        //registrar.updateGameObjectProxy(id, propName, value)

        count++
    }

    return offset
}

module.exports = PartialGameObjectNetworker