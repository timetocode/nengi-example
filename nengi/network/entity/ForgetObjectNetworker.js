
var MessageType = require('../../MessageType')
/**
* Writes the ids of objects to forget outgoing network buffer (these are objects
* that have died or moved too far away to still be relevant to a client/player)
*/
function ForgetObjectNetworker(common) {
    this.common = common
}

ForgetObjectNetworker.prototype.countBits = function(ids) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    for (var i = 0; i < ids.length; i++) {
        bits += this.common.config.ID_BINARYTYPE.bits
    }
    return bits
}


ForgetObjectNetworker.prototype.write = function(bitBuffer, offset, ids, client) {
    if (ids.length === 0) return offset

    var length = ids.length
    bitBuffer.writeUInt8(MessageType.ForgetObjects, offset)
    offset += 8
    // write the number of whole proxies to be sent
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    //console.log('ids', ids)
    for (var i = 0; i < length; i++) {
        //if (client.isKnownGameObjectId(ids[i])) {
        var alias = client.getAliasId(ids[i])
        bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
        //console.log('hey, forget about', alias)
        offset += this.common.config.ID_BINARYTYPE.bits
        client.releaseAliasId(alias)
        delete client.clientState.gameObjects[ids[i]]
        //}
    }
    return offset
}


ForgetObjectNetworker.prototype.read = function(bitBuffer, offset, registrar) {

    var count = bitBuffer.readUInt8(offset)
    offset += 8
    //console.log(count, 'readForgets')

    for (var j = 0; j < count; j++) {
        var serialized = []
        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)
        offset += this.common.config.ID_BINARYTYPE.bits
        registrar.forget(id)
    }

    return offset
}

module.exports = ForgetObjectNetworker