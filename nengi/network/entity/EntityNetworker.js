var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')
var MessageType = require('../../MessageType')
var MathEx = require('../../MathEx')
//var Shared = require('../../core/Shared')

/**
* Writes whole game objects to the outgoing network buffer
*/
function EntityNetworker(common) {
    this.common = common
}

EntityNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    var length = ids.length 
    for (var i = 0; i < length; i++) {

        var netObject = netObjectCache.getEntity(ids[i], tick)
        for (var j = 0; j < netObject.proxy.length; j++) {
            var propName = netObject.netSchema.keys[j]

            if (propName === 'id') {
                bits += this.common.config.ID_BINARYTYPE.bits
            } else {
                var propData = netObject.netSchema.properties[propName]
                bits += Binary[propData.binaryType].bits
            }
        }

    }

    return bits
}


/**
* Writes proxy data (a shallow copy) of game objects to the bitBuffer. The game 
* object's data is taken from the netObjectCache at the specified tick. After
* the data is written to the buffer it is also copied to each client.clientState
*
* @method
* @param {BitBuffer} bitBuffer - BitBuffer to write to
* @param {Integer} offset - position within the BitBuffer to begin writing
* @param {Array} ids - the ids of the gameObjects whose data will be written
* @param {Integer} tick - the step of the game simulation being represented
* @param {Client} client - the client to whom this data will be sent
* @param {NetObjectCache} netObjectCache - a cache of proxy data
* @returns {Integer} Returns the offset incremented by however many bits were written 
*
*/
EntityNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

    var length = ids.length

    // write the message type for GameObjects
    bitBuffer.writeUInt8(MessageType.GameObjects, offset)
    offset += 8

    // write the number of game objects to be transmitted
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) {
       
        // get the game object's data from the netObjectCache @ tick
        var netObject = netObjectCache.getEntity(ids[i], tick) 
        // write the game object (well, its proxy, a shallow copy) to the buffer           
        offset = this.writeWholeProxy(bitBuffer, offset, netObject.proxy, netObject.netSchema, client)        

        // copy the proxy
        var newProxy = []
        for (var j = 0; j < netObject.proxy.length; j++) {
            newProxy.push(netObject.proxy[j])
        }
        // store a copy of the proxy in the clientState
        client.clientState.gameObjects[ids[i]] = newProxy
    }    
    return offset
}


/**
* Writes a single game object in proxy form to the bitBuffer.
*
* @method
* @param {BitBuffer} bitBuffer - BitBuffer to write to
* @param {Integer} offset - position within the BitBuffer to begin writing
* @param {Array} proxy - the game object data as an array
* @param {NetSchema} netSchema - the game object's network schema
* @param {Client} client - the client to whom this game object will be sent
* @returns {Integer} Returns the offset incremented by however many bits were written 
*/
EntityNetworker.prototype.writeWholeProxy = function(bitBuffer, offset, proxy, netSchema, client) {

    // proxy is fundamentally an array of numbers that represents a shallow
    // copy of a game object. At this point we write the proxy to the bitBuffer
   // console.log('writeWholeProxy', proxy)
    for (var i = 0; i < proxy.length; i++) {
        // property name corresponding to this position in the array
        var propName = netSchema.keys[i]
        // network/binary information for this property
        var propData = netSchema.properties[propName]

 
        var value = proxy[i]
        if (typeof value === 'undefined')
            value = 0

        if (propData.binaryType === BinaryType.Rotation) {
            
            value = MathEx.radiansToUInt8(value)
            //console.log('WRITING A ROTATION', value)
        }
        
        if (propName === 'id'){
            // special case if we're writing an id to the buffer
            // id is aliased, to optmize binary size and provide security
            var alias = client.createAliasId(value)
            // write the id to the buffer and increment the offset
            bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
            offset += this.common.config.ID_BINARYTYPE.bits
        } else {
            // normal case, writing data other than an id
            // write the value to the buffer
            bitBuffer[Binary[propData.binaryType].write](value, offset)
            offset += Binary[propData.binaryType].bits
        }

    }
    return offset
}


 EntityNetworker.prototype.read = function(bitBuffer, offset, registrar) {
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    for (var j = 0; j < count; j++) {
        var serialized = []

        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)
        offset += this.common.config.ID_BINARYTYPE.bits
        serialized.push(id)

        var type = bitBuffer.readUInt8(offset)
        offset += 8
        serialized.push(type)

        //var constructor = this.common.gameObjectCtors.getConstructor(type)
        //var newObject = new constructor()
        //console.log('type', type)         

        var netSchema = this.common.entityConstructors.getNetSchema(type)//newObject.netSchema
        for (var i = 2; i < netSchema.keys.length; i++) {
            var propName = netSchema.keys[i]
            //console.log(propName)
            var propData = netSchema.properties[propName]


            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            if (propData.binaryType === BinaryType.Rotation) {
                
                value = MathEx.UInt8ToRadians(value)
                ///console.log('WRITING A ROTATION', value)
            }

            serialized.push(value)
        }
        //console.log(serialized)
        registrar.addGameObjectProxy(serialized)
        registrar.markNew(id)
    }

    return offset
}

module.exports = EntityNetworker