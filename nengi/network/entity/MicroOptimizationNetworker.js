var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')
var MessageType = require('../../MessageType')

function MicroOptimizationNetworker(common) {
	this.common = common
}

MicroOptimizationNetworker.prototype.countBits = function(ids, tick, clients, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8    

    for (var i = 0; i < ids.length; i++) {
        var netObject = netObjectCache.getEntity(ids[i], tick)
        bits += this.common.config.ID_BINARYTYPE.bits

        for (var j = 0; j < netObject.netSchema.optimizations.length; j++) {
            var optimization = netObject.netSchema.optimizations[j]
            bits += Binary[optimization.binaryType].bits
        }   
    }
    return bits 
}

MicroOptimizationNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

    var length = ids.length

    bitBuffer.writeUInt8(MessageType.GameObjectOptimizations, offset)
    offset += 8

    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) { 
        var netObject = netObjectCache.getEntity(ids[i], tick)
        //console.log('id', ids[i])
        offset = this.writeOptimizedMovement(bitBuffer, offset, netObject, netObject.netSchema, client)
        //console.log('speed', this.getGameObjectById(netObject.id).ai.speed)

    }
    return offset
}

MicroOptimizationNetworker.prototype.writeOptimizedMovement = function(bitBuffer, offset, netObject, netSchema, client) {
    var alias = client.getAliasId(netObject.id)
    bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
    offset += this.common.config.ID_BINARYTYPE.bits

    var cachedObject = client.clientState.gameObjects[netObject.id]

    for (var i = 0; i < netSchema.optimizations.length; i++) {
        var optimization = netSchema.optimizations[i]
        var value = 0
        if (optimization.delta) {
            // delta! calculate delta
            var prop = optimization.prop
            var realValue = netObject.proxy[netSchema.properties[prop].key]
            var cachedValue = cachedObject[netSchema.properties[prop].key]
            var deltaValue = Math.round(cachedValue - realValue)
            // update cache
            cachedObject[netSchema.properties[prop].key] -= deltaValue
            // update client
            bitBuffer[Binary[optimization.binaryType].write](deltaValue, offset)
            offset += Binary[optimization.binaryType].bits
        } else {
            // no delta, just write the value
            var prop = optimization.prop
            var realValue = netObject.proxy[netSchema.properties[prop].key]
            var value = Math.round(realValue)
            // update cache
            cachedObject[netSchema.properties[prop].key] -= value
            // update client
            bitBuffer[Binary[optimization.binaryType].write](value, offset)
            offset += Binary[optimization.binaryType].bits
        }
    }

    return offset
}


MicroOptimizationNetworker.prototype.read = function(bitBuffer, offset, registrar) {
    // TODO: this is hardcoded for x,y movement, make it generic
    var go = null
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    //console.log(count, 'readOptimizedMovementProxies')
    for (var j = 0; j < count; j++) {
        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)
        offset += this.common.config.ID_BINARYTYPE.bits
        var dx = bitBuffer[Binary[BinaryType.Int8].read](offset)
        offset += 8
        var dy = bitBuffer[Binary[BinaryType.Int8].read](offset)
        offset += 8
        //console.log('id', id, 'dx', dx, 'dy', dy)

        registrar.updateProxyDeltaSimple(id, 2, dx)
        registrar.updateProxyDeltaSimple(id, 3, dy)
        //registrar.updateGameObjectProxyDelta(id, 'x', dx)
        //registrar.updateGameObjectProxyDelta(id, 'y', dy)
    }
    return offset
}

module.exports = MicroOptimizationNetworker