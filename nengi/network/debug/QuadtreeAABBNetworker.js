//var config = require('../../config')
var Binary = require('../../binary/Binary')
var MessageType = require('../../MessageType')

/**
* For debug purposes only!
* Writes quadtree aabbs to outgoing network buffer (bandwidth intensive!)
*/
function QuadtreeAABBNetworker() {

}

QuadtreeAABBNetworker.prototype.countBits = function(aabbs) {
    return (aabbs.length * 4 * 32) + 8 + 32
}

QuadtreeAABBNetworker.prototype.write = function (bitBuffer, offset, aabbs) {
    var count = aabbs.length

    bitBuffer.writeUInt8(MessageType.Quadtrees, offset)
    offset += 8
    bitBuffer.writeUInt32(count, offset)
    offset += 32

    for (var i = 0; i < count; i++) {
        offset = aabbToMessage(bitBuffer, offset, aabbs[i])
    }
    return offset
}

function aabbToMessage(bitBuffer, offset, aabb) {
    bitBuffer.writeInt32(aabb.x, offset)
    offset += 32
    bitBuffer.writeInt32(aabb.y, offset)
    offset += 32
    bitBuffer.writeInt32(aabb.halfWidth, offset)
    offset += 32
    bitBuffer.writeInt32(aabb.halfHeight, offset)
    offset += 32

    return offset
}


QuadtreeAABBNetworker.prototype.read = function(bitBuffer, offset, registrar) {
    var count = bitBuffer.readInt32(offset)
    offset += 32

    var aabbs = []
    for (var i = 0; i < count; i++) {
        var aabb = {}
        aabb.x = bitBuffer.readInt32(offset)
        offset += 32
        aabb.y = bitBuffer.readInt32(offset)
        offset += 32
        aabb.halfWidth = bitBuffer.readInt32(offset)
        offset += 32
        aabb.halfHeight = bitBuffer.readInt32(offset)
        offset += 32
        aabbs.push(aabb)
    }

    registrar.aabbs = aabbs
    return offset
    
}

module.exports = QuadtreeAABBNetworker