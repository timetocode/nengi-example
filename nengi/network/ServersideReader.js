
var BitBuffer = require('../binary/BitBuffer')
var MessageType = require('../MessageType')
var PlayerInputNetworker = require('./PlayerInputNetworker')

var PingCheckNetworker = require('./PingCheckNetworker')

var InputSnapshot = require('./InputSnapshot')

function ServersideReader(shared) {
	this.shared = shared

	this.playerInputNetworker = new PlayerInputNetworker(shared)
	this.pingCheckNetworker = new PingCheckNetworker()
}

ServersideReader.prototype.read = function(data, client, instance) {

	var bitBuffer = new BitBuffer(data)
	var offset = 0

	var inputSnapshot = new InputSnapshot()
	inputSnapshot.client = client

	while (offset < bitBuffer.bitLength && bitBuffer.bitLength - offset >= 8) {		
		var msgType = bitBuffer.readUInt8(offset)
		//console.log('message type', msgType)
		offset += 8

		if (msgType === MessageType.Pong) {
			offset = this.pingCheckNetworker.readPong(bitBuffer, offset, client)
		}

		if (msgType === MessageType.PlayerInputs) {
			offset = this.playerInputNetworker.read(bitBuffer, offset, inputSnapshot)
		}	
	}

	client.instance.addInputSnapshot(inputSnapshot)
}

module.exports = ServersideReader