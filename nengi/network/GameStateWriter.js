var BitBuffer = require('../binary/BitBuffer')
var Binary = require('../binary/Binary')
var EntityNetworker = require('./entity/EntityNetworker')
var PartialGameObjectNetworker = require('./entity/PartialGameObjectNetworker')
var MicroOptimizationNetworker = require('./entity/MicroOptimizationNetworker')
var ForgetObjectNetworker = require('./entity/ForgetObjectNetworker')
var GameEventNetworker = require('./event/GameEventNetworker')
var QuadtreeAABBNetworker = require('./debug/QuadtreeAABBNetworker')
var PingCheckNetworker = require('./PingCheckNetworker')
var DirectMessageNetworker = require('./message/DirectMessageNetworker')

function GameStateWriter(netObjectCache, common) {
	this.netObjectCache = netObjectCache
    this.entityNetworker = new EntityNetworker(common)
    this.partialEntityNetworker = new PartialGameObjectNetworker(common)
    this.microOptimizationNetworker = new MicroOptimizationNetworker(common)
    this.forgetObjectNetworker = new ForgetObjectNetworker(common)
    this.gameEventNetworker = new GameEventNetworker(common)
    this.quadtreeAABBNetworker = new QuadtreeAABBNetworker(common)
    this.pingCheckNetworker = new PingCheckNetworker(common)
    this.directMessageNetworker = new DirectMessageNetworker(common)
    this.debugShowQuadtree = false
}


/**
* Creates a buffer containing a snapshot of the game state
*
* @method
* @param {Client} client The client receiving this data
* @param {Object} visibilty Visibility changes for this client
* @param {Integer} tick The tick of the game simulation
* @param {Array} aabbs An array of AABBs showing the server side quadtrees for debug purposes
* @returns {Buffer}
*/
GameStateWriter.prototype.writeBuffer = function(client, visibility, tick, aabbs) {
	var forget = visibility.noLongerVisible
    var whole = visibility.newlyVisible
    var opt = []
    var partial = []

    // using visibility data sort the entity/event data into 4 categories:
    //  forget: ids that the this client should delete
    //  whole: whole game objects to send to this client
    //  opt: microoptmized changes to game objects already tracked by the client
    //  partial: small changes to game objects already tracked yb the client
    this.chooseOptOrPartial(client, visibility, whole, opt, partial, tick)

    // events are simply sent in their entirety, note that visible events are
    // events that have positional significant (occured near the player)
    var events = visibility.events

    var messageIds = []
    for (var i = 0; i < client.messages.length; i++) {
        messageIds.push(client.messages[i].id)
    }
    client.messages = []


    var isTimeToCheckPing = client.needsPingCheck()

    var arr = []
    // build and send network data
    var bitBuffers = []

    /* COUNTING THE BUFFER LENGTH IN BITS! */
    var quadtreeBits = 0
    if (this.debugShowQuadtree) {
    	quadtreeBits = this.quadtreeAABBNetworker.countBits(aabbs)  
    }

    var pingCheckBits = (isTimeToCheckPing) 
        ? this.pingCheckNetworker.countBits() : 0
      

    var forgetIdsBits = this.forgetObjectNetworker.countBits(
        visibility.noLongerVisible
    )

    var wholeProxyBits = this.entityNetworker.countBits(
        visibility.newlyVisible, 
        tick, 
        this.netObjectCache
    )

    var gameEventBits = this.gameEventNetworker.countBits(
        events, 
        tick,
        this.netObjectCache
    )

    
    var directMessageBits = this.directMessageNetworker.countBits(
        messageIds,
        tick,
        this.netObjectCache
    )


    var optimizedMovementBits = this.microOptimizationNetworker.countBits(
        opt, //visibility.stillVisible, 
        tick, 
        client,
        this.netObjectCache
    )

    var partialProxyBits = this.partialEntityNetworker.countBits(
        partial, 
        tick, 
        this.netObjectCache
    )

    var offset = 0

    // create a bitBuffer whose length is the bit total
    var bitBuffer = new BitBuffer(
        8 +
        pingCheckBits +
        directMessageBits +
        quadtreeBits +
        forgetIdsBits +
        wholeProxyBits +
        gameEventBits +
        optimizedMovementBits +
        partialProxyBits
    )
    
    
    
    // for debug purposes, log visibilty / bit data 
    /*
    console.log(
        'f>>', forgetIdsBits, 
        'w>>', wholeProxyBits, 
        'p>>', partialProxyBits, 
        'o>>', optimizedMovementBits,
        'total:', forgetIdsBits + wholeProxyBits + partialProxyBits + optimizedMovementBits,
        'forget', visibility.noLongerVisible.length, 
        'new', visibility.newlyVisible.length, 
        'still', visibility.stillVisible.length,
        'part', partial.length,
        'opt', opt.length
    )
*/
    
    
    /* ACTUAL WRITING TO THE BUFFER */
    //console.log('client.lastInputProcessed', client.lastInputProcessed)
    bitBuffer.writeUInt8(client.lastInputProcessed, offset)
    offset += 8

    if (this.debugShowQuadtree) {
	    offset = this.quadtreeAABBNetworker.write(bitBuffer, offset, aabbs)
	}

    
    if (isTimeToCheckPing) {
        offset = this.pingCheckNetworker.write(
            bitBuffer,
            offset,
            client
        )
        client.pingCheckSent()
    }

    


    offset = this.forgetObjectNetworker.write(
        bitBuffer, 
        offset, 
        visibility.noLongerVisible, 
        client
    )

    offset = this.entityNetworker.write(
        bitBuffer, 
        offset, 
        visibility.newlyVisible, 
        tick, 
        client,
        this.netObjectCache
    )

    offset = this.directMessageNetworker.write( 
        bitBuffer,
        offset,
        messageIds,
        tick,
        client,
        this.netObjectCache
    )
    
    offset = this.gameEventNetworker.write(
        bitBuffer,
        offset,
        events,
        tick,
        client,
        this.netObjectCache
    )       

    offset = this.microOptimizationNetworker.write(
        bitBuffer, 
        offset, 
        opt, //visibility.stillVisible,
        tick, 
        client,
        this.netObjectCache
    )

    offset = this.partialEntityNetworker.write(
        bitBuffer, 
        offset, 
        partial, 
        tick, 
        client,
        this.netObjectCache
    )


    //console.log('new', visibility.newlyVisible, 'cont', visibility.stillVisible, 'gone', visibility.noLongerVisible)
    return bitBuffer.toBuffer()
}

GameStateWriter.prototype.processDiffs = function(proxy, cachedProxy, netSchema) {
    var diffs = []
    // start at 2, skipping id and type (which are always the same)
    for (var j = 2; j < proxy.length; j++) {
        var propName = netSchema.keys[j]
        var propData = netSchema.properties[propName]
 
        var value = proxy[j]
        //if (typeof value === 'undefined')
        //    value = 0


        var realValue = proxy[netSchema.properties[propName].key]
        var cachedValue = cachedProxy[netSchema.properties[propName].key]

        var deltaValue = Math.round(cachedValue - realValue)

        if (deltaValue !== 0) {
            //console.log(propName, 'changed', deltaValue)

            diffs.push(propName)
        }
    }
    return diffs
}

GameStateWriter.prototype.countOptimizations = function(proxy, cachedProxy, netSchema) {
    var optimizableCount = 0
    var length = netSchema.optimizations.length
    for (var j = 0; j < length; j++) {
        var optimization = netSchema.optimizations[j]
        var binary = Binary[optimization.binaryType]
        var realValue = proxy[netSchema.properties[optimization.prop].key]
        var cachedValue = cachedProxy[netSchema.properties[optimization.prop].key]

        var deltaValue = Math.round(cachedValue - realValue)

        if (deltaValue >= binary.min && deltaValue <= binary.max) {
            // this particular prop can be optimized
            optimizableCount++
        }
    }
    return optimizableCount
}

GameStateWriter.prototype.partialDiffs = function(optimizableCount, diffs, length, netSchema, opt, id) {
    var remainingDiffs = []
    //if (noChangeCount !== length) {
    if (optimizableCount === length) {
        // all optimizations were valid for this gameObject
        for (var j = 0; j < diffs.length; j++) {                             
            var prop = diffs[j]

            // scan through the list of diffs, and remove any diff
            // that was covered by an optimization
            var wasOpt = false
            for (var k = 0; k < length; k++) {
                var optimization = netSchema.optimizations[k]
                if (optimization.prop === prop) {
                    wasOpt = true
                }
            }

            // the remaining diffs are any changes that were not
            // already part of an optimization
            if (!wasOpt) {
                remainingDiffs.push(prop)
            }
        }
        opt.push(id)
    } else {
        //console.log('nothing changed for gameobject', id)
        remainingDiffs = diffs
    }
    return remainingDiffs
}

/**
* Sorts entities into categories that affect the type of update sent by nengi
* This function and partialDiffs constitute the core algorithm which attempts to
* optimize the representation of an entity. The gist of the algo is:
*  microoptimizations are evaluated first, if an entity's changes only occured to
*    the properties that are 'microoptimized', then the microoptimization
*    representation is chosen, and passed out via opt
*  partial representation of an entity's state are evalulated second, and consist
*    of any properties that are not microoptimized
*
*  It is possible for this analysis to produce both a microoptimized and a partial
*  representation for an object, such as would commonly occur if an entity experienced
*  changes in x,y, and hp (x,y would be microoptimized, hp would be sent seperate)
*   
* @method
*
*/
GameStateWriter.prototype.chooseOptOrPartial = function(client, visibility, whole, opt, partial, tick) {
    for (var i = 0; i < visibility.stillVisible.length; i++) {
        var id = visibility.stillVisible[i]
        // seralized state of the object
        var netObject = this.netObjectCache.getEntity(id, tick)
        // last known state of this particular object known to the client      
        var cachedObject = client.clientState.gameObjects[netObject.id]
        // schema
        var netSchema = netObject.netSchema

        if (netSchema.hasOptimizations) {
            //console.log('herr')
            //var optimizableCount = 0
            var noChangeCount = 0
            var length = netSchema.optimizations.length

            var proxy = netObject.proxy

            var diffs = this.processDiffs(proxy, cachedObject, netSchema)
            var optimizableCount = this.countOptimizations(proxy, cachedObject, netSchema)


            var remainingDiffs = this.partialDiffs(optimizableCount, diffs, length, netSchema, opt, id)

            if (remainingDiffs.length > 0) {
                // if any differences remain that were not part of an opt                                        //}
                partial.push({id:id, diffs: remainingDiffs})

            }

            //console.log('still different', remainingDiffs)
        }
    }
}

module.exports = GameStateWriter