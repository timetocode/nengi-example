
var MessageType = require('../MessageType')
var MathEx = require('../MathEx')
var BitBuffer = require('../binary/BitBuffer')
/**
* Writes the ids of objects to forget outgoing network buffer (these are objects
* that have died or moved too far away to still be relevant to a client/player)
*/
function PingCheckNetworker(common) {
    this.common = common
}

PingCheckNetworker.prototype.countBits = function() {
    var bits = 0
    // msgtype
    bits += 8
    // the random pingVerificationNumber
    bits += 32
    // the client's average ping
    bits += 16
    return bits
}


/**
* writes a ping check into buffer, records timestamp and verfication into client
*/
PingCheckNetworker.prototype.write = function(bitBuffer, offset, client) {
    //console.log('write ping check')
    bitBuffer.writeUInt8(MessageType.Ping, offset)
    offset += 8
    // write the number of whole proxies to be sent

    // the actual random 32-bit Integer is not particularly important
    // all that matters is that the client can't spoof its ping, so any random
    // number would probably suffice
    var random = MathEx.random(0, 2147483647)
    bitBuffer.writeUInt32(random, offset)
    offset += 32

    bitBuffer.writeUInt16(client.ping, offset)
    offset += 16

   
    // in truth the ping isn't sent yet, but it'll probaby be sent within the
    // same millisecond as this code
    client.pingTimestamp = Date.now()
    client.pingVerificationNumber = random
    return offset
}


/**
* Clientside reading of ping message, replies immediately
*
*/
PingCheckNetworker.prototype.readPingAndReplyWithPong = function(bitBuffer, offset, websocket, registrar) {
    
   
    var pingVerificationNumber = bitBuffer.readUInt32(offset)
    offset += 32
    var pingResult = bitBuffer.readUInt16(offset)
    offset += 16

    registrar.ping = pingResult
    //console.log('readPingAndReplyWithPong', pingVerificationNumber)
    var responseBuffer = new BitBuffer(56)
    responseBuffer.writeUInt8(MessageType.Pong, 0)
    responseBuffer.writeUInt32(pingVerificationNumber, 8)

    if (websocket && websocket.readyState === 1) { 
        //console.log('WEBSOCKET')        
        websocket.send(responseBuffer.byteArray)
    }

    return offset
}

/**
* Serverside reading of pong, performs ping calculation and stores in client
*/
PingCheckNetworker.prototype.readPong = function(bitBuffer, offset, client) {
    //console.log('readPong')
    var pingVerificationNumber = bitBuffer.readUInt32(offset)
    offset += 32

    if (pingVerificationNumber === client.pingVerificationNumber) {
        var diff = Date.now() - client.pingTimestamp
        //client.ping = diff
        client.setPingResult(diff)
       // console.log('ping calculated:', client.ping)
    } else {
        //console.log('client attempted to spoof ping/pong')
        client.ping = 0
    }
    return offset 

}

module.exports = PingCheckNetworker