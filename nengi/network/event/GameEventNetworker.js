
var Binary = require('../../binary/Binary')
var MessageType = require('../../MessageType')
function GameEventNetworker(common) {
    this.common = common
}

GameEventNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    for (var i = 0; i < ids.length; i++) {
        var id = ids[i]
        //bits += 8
        var netObject = netObjectCache.getEvent(ids[i], tick)
        for (var j = 0; j < netObject.proxy.length; j++) {
            var propName = netObject.netSchema.keys[j]

            // skip id and type
            if (propName === 'id') {
                //bits += this.common.config.ID_BINARYTYPE.bits
            } else {
                var propData = netObject.netSchema.properties[propName]
                bits += Binary[propData.binaryType].bits
            }
        }
    }
    return bits
}


GameEventNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

    //console.log('writeGameEvents', ids.length)
    //console.log('WP')
    var length = ids.length //(ids.length > client.maxVisibleObjects) ? client.maxVisibleObjects : ids.length
    //console.log('writing', 66, '@', offset)
    bitBuffer.writeUInt8(MessageType.GameEvents, offset)
    offset += 8
    // write the number of whole proxies to be sent
    //console.log('writing', length, '@', offset)
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) {
        //if (client.isKnownGameObjectId(ids[i])) {
        var netObject = netObjectCache.getEvent(ids[i], tick)
            //console.log('id', ids[i])
        offset = writeGameEvent(bitBuffer, offset, netObject.proxy, netObject.netSchema, client)
        

        var newProxy = []
        for (var j = 0; j < netObject.proxy.length; j++) {
            newProxy.push(netObject.proxy[j])
        }

        //client.clientState.gameObjects[ids[i]] = newProxy// netObject.proxy
        //}
    }    
    return offset
}

var writeGameEvent = function(bitBuffer, offset, proxy, netSchema, client) {
   // console.log(proxy)
    var debugOutput = []

    // writing type
    bitBuffer[this.common.config.ID_BINARYTYPE.write](0, offset)
    debugOutput.push(0)
    offset += this.common.config.ID_BINARYTYPE.bits

    for (var i = 2; i < proxy.length; i++) {
        var propName = netSchema.keys[i]
        var propData = netSchema.properties[propName]      

        var value = proxy[i]
        if (typeof value === 'undefined')
            value = 0

        if (propName === 'id') {
            //var alias = client.createAliasId(value)
            //var alias = client.createAliasId(value)
            //bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
            //offset += this.common.config.ID_BINARYTYPE.bits
        } else {
            bitBuffer[Binary[propData.binaryType].write](value, offset)
            offset += Binary[propData.binaryType].bits

            debugOutput.push(value)
        }
    }
   // console.log('writeGameEvent', debugOutput, proxy)

    return offset
}

GameEventNetworker.prototype.read = function(bitBuffer, offset, netSchema, registrar) {
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    console.log('read events:', count)
    for (var j = 0; j < count; j++) {
        var serialized = []

        //var id = bitBuffer[nengi.ID_BINARYTYPE.read](offset)
        //console.log('read16 bits from', offset, 'result', id)
        //offset += nengi.ID_BINARYTYPE.bits
        //serialized.push(id)

        var type = bitBuffer.readUInt8(offset)
        offset += 8


        var constructor = this.shared.eventConstructors.getConstructor(type)
        var newEvent = new constructor()

        serialized.push(type)
        var netSchema = newEvent.netSchema


        for (var i = 2; i < netSchema.keys.length; i++) {
            var propName = netSchema.keys[i]
            //console.log(propName)
            var propData = netSchema.properties[propName]

            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            serialized.push(value)
        }
        // TODO register this data
        registrar.addGameEventProxy(serialized)

    }
    return offset
}

module.exports = GameEventNetworker