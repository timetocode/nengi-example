
var BitBuffer = require('../binary/BitBuffer')
var MessageType = require('../MessageType')
var EntityNetworker = require('./entity/EntityNetworker')
var ForgetObjectNetworker = require('./entity/ForgetObjectNetworker')
var MicroOptimizationNetworker = require('./entity/MicroOptimizationNetworker')
var PartialGameObjectNetworker = require('./entity/PartialGameObjectNetworker')
var GameEventNetworker = require('./event/GameEventNetworker')
var DirectMessageNetworker = require('./message/DirectMessageNetworker')
var QuadtreeAABBNetworker = require('./debug/QuadtreeAABBNetworker')
var PingCheckNetworker = require('./PingCheckNetworker')

var Snapshot = require('../client/Snapshot')

function GameStateReader(registrar, common) {
	this.registrar = registrar
	this.common = common

	this.quadtreeAABBNetworker = new QuadtreeAABBNetworker(common)
	this.entityNetworker = new EntityNetworker(common)
	this.gameEventNetworker = new GameEventNetworker(common)
	this.forgetObjectNetworker = new ForgetObjectNetworker(common)
	this.microOptimizationNetworker = new MicroOptimizationNetworker(common)
	this.partialGameObjectNetworker = new PartialGameObjectNetworker(common)
	this.directMessageNetworker = new DirectMessageNetworker(common)
	this.pingCheckNetworker = new PingCheckNetworker(common)
}

GameStateReader.prototype.readBinary = function(data, websocket) {
	var registrar = this.registrar
	var bitBuffer = new BitBuffer(data)
	var offset = 0

	//console.log('DATA', data)

	var sv_lastProcessedTick = bitBuffer.readUInt8(offset)
	offset += 8

	registrar.setLastConfirmedTick(sv_lastProcessedTick)

	//var snapshot = new Snapshot()
	//snapshot.confirmed = sv_lastProcessedTick

	if (bitBuffer.bitLength >= 8) {
		var msgType = bitBuffer.readUInt8(offset)
	}

	while (offset < bitBuffer.bitLength && bitBuffer.bitLength - offset >= 8) {		
		var msgType = bitBuffer.readUInt8(offset)
		offset += 8

		if (msgType === MessageType.Quadtrees) {
			offset = this.quadtreeAABBNetworker.read(bitBuffer, offset, registrar)
		}
		
		if (msgType === MessageType.Ping) {
			offset = this.pingCheckNetworker.readPingAndReplyWithPong(bitBuffer, offset, websocket, registrar)
		}
		
		if (msgType === MessageType.DirectMessages) {
			offset = this.directMessageNetworker.read(bitBuffer, offset, null, registrar)
		}		

		if (msgType === MessageType.ForgetObjects) {
			offset = this.forgetObjectNetworker.read(bitBuffer, offset, registrar)
		}

		if (msgType === MessageType.GameObjects) {
			offset = this.entityNetworker.read(bitBuffer, offset, registrar)
		}

		if (msgType === MessageType.GameEvents) {
			offset = this.gameEventNetworker.read(bitBuffer, offset, null, registrar)
		}

		if (msgType === MessageType.GameObjectOptimizations) {
			offset = this.microOptimizationNetworker.read(bitBuffer, offset, registrar)
		}

		if (msgType === MessageType.PartialGameObjects) {
			offset = this.partialGameObjectNetworker.read(bitBuffer, offset, registrar)
		}	
	}
}

module.exports = GameStateReader