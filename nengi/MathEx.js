
/**
* MathEx, aka MathExtensions
* not to be confused with javascript's Math
* contains some normal game dev math
*  distance, vectors, ranges, lerps
*/

/**
* Scales a number from a source range to its value in a destination range
* e.g. n: 127.5 from range [0, 255] would become n: 0.0 in range [-1, 1]
* @method scale
* @param {Number} n The number to scale
* @param {Number} a The lower limit of the source range
* @param {Number} b The upper limit of the source range
* @param {Number} c The lower limit of the destination range
* @param {Number} d The lower limit of the destination range
* @return {Number} Returns a value for n relative to the destination range
*/
var scale = function(n, a, b, c, d) {
  return (d - c) * (n - a) / (b - a) + c
}
module.exports.scale = scale


module.exports.radiansToUInt8 = function(rad) {
  return Math.floor(scale(rad, -Math.PI, Math.PI, 0, 255) % 256)
}

module.exports.UInt8ToRadians = function(uint8) {
  return uint8 * ((2 * Math.PI) / 255)
}

module.exports.radiansToDegrees = function(rad) {
  return rad * (180 / Math.PI)
}

module.exports.degreesToRadians = function(degrees) {
  return degrees * (Math.PI / 180)
}

module.exports.degreesToUInt8 = function(degrees) {
  if (degrees > 360 || degrees < 0)
    throw('degreesToUInt8 overflow')
  return Math.round(scale(degrees, 0, 360, 0, 255))
}

module.exports.UInt8ToDegrees = function(uint8) {
  return Math.round(scale(uint8, 0, 255, 0, 360))
}

module.exports.dot = function(a1, a2, b1, b2) {
  return (a1*b1) + (a2*b2)
}

/*
* Distance between two points, pythagorean
* @method distance
* @param {Number} x1 X component of the first point
* @param {Number} y1 Y component of the first point
* @param {Number} x2 X component of the second point
* @param {Number} y2 Y component of the second point
* @return {Number} Returns distance
*/
module.exports.distance = function(x1, y1, x2, y2) {
    var x = x2 - x1
    var y = y2 - y1
    return Math.sqrt((x*x) + (y*y))
}

/*
* Turns two points into a vector
* @method vectorize
* @param {Number} x1 X component of the first point
* @param {Number} y1 Y component of the first point
* @param {Number} x2 X component of the second point
* @param {Number} y2 Y component of the second point
* @return {Object} Returns a vector with x and y components
*/
module.exports.vectorize = function(x1, y1, x2, y2) {
  return {
    x: x2 - x1,
    y: y2 - y1
  }
}

/*
* Calculates length of a vector ##not sure if this is actually used anywhere
* @method vectorLength
* @param {Object} vector A vector with an x and y component
* @return {Object} Returns a vector with x and y components
*/
module.exports.vectorLength = function(vector) {
  return Math.sqrt((vector.x * vector.x) + (vector.y * vector.y))
}


/*
* Calculates a unit vector (vector of length 1)
* @method normalizeVector
* @param {Object} vector A vector with an x and y component
* @return {Object} Returns a unit vector with x and y components
*/
var normalizeVector = function(vector) {
  if (vector.x === 0 & vector.y === 0) {
    return { x: 0, y: 0 }
  }

  var length = Math.sqrt((vector.x * vector.x) + (vector.y * vector.y))
  return {
    x: vector.x / length,
    y: vector.y / length
  }
}
module.exports.normalizeVector = normalizeVector

/*
* Liner interpolation
* e.g. a: 50, b: 100, portion: 0.5, result: 75 (75 is halfway from 50 to 100)
* @method lerp
* @param {Number} a The first number in the range
* @param {Number} b The second number in the range
* @param {Number} portion Partial distance usually between 0.0 and 1.0
* @return {Object} Returns a value at 'portion' distance between a and b
*/
var lerp = function(a, b, portion) {
  return a + ((b - a) * portion)
}
module.exports.lerp = lerp

/*
* Liner interpolation between two colors (for blending/gradients)
* @method lerpColor
* @param {Number} colorA The first color
* @param {Number} colorB The second color
* @param {Number} portion How far between the colors (0.0 is colorA, 1.0 is colorB)
* @return {Object} Returns a color blend between a and b
*/
module.exports.lerpColor = function(colorA, colorB, portion) {
    return {
        r: lerp(colorA.r, colorB.r, portion),
        g: lerp(colorA.g, colorB.g, portion),
        b: lerp(colorA.b, colorB.b, portion),
        a: lerp(colorA.a, colorB.a, portion)
    }
}


/*
* Random integer from min to max
* @method random
* @param {Integer} min The minimum of the random range
* @param {Integer} max The maximum of the random range
* @return {Integer} Returns a random integer within the specified range
*/
var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
module.exports.random = random

/*
* Random heading as a unit vector
*/
module.exports.randomDirection = function() {
  return normalizeVector({x: random(-1000,1000), y: random(-1000, 1000)})
}

/*
* Random float from min to max
* @method random
* @param {Number} min The minimum of the random range
* @param {Number} max The maximum of the random range
* @return {Number} Returns a random float within the specified range
*/
module.exports.randomFloat = function(min, max) {
  return Math.random() * (max - min) + min
}

/*
* Interpolates UInt8 (0-255) angles as a rotation around a circle, carefully
* wraps around 0 and 255 choosing the intuitive direction to turn
* @method interpolateRotationUInt8random
* @param {UInt8} a First angle as a byte
* @param {UInt8} b Second angle as a byte
* @param {Number} ratio Amount to interpolate (0 -> a, 1 -> b, 0.5 -> halfway)
* @return {Number} Returns the new angle
*/
module.exports.interpolateRotationUInt8 = function(a, b, ratio) {
  if (a < 63 && b > 191) {
    return interpRot = lerp(a + 255, b, ratio) - 255
  } else if (a > 191 && b < 63) {
    return interpRot = lerp(a, b + 255, ratio) - 255
  } else {
    return interpRot = lerp(a, b, ratio)
  }    
}

/*
* Interpolates radians as a rotation around a circle, carefully
* wraps around 0 and 255 choosing the intuitive direction to turn
* @method interpolateRotationUInt8random
* @param {UInt8} a First angle as a byte
* @param {UInt8} b Second angle as a byte
* @param {Number} ratio Amount to interpolate (0 -> a, 1 -> b, 0.5 -> halfway)
* @return {Number} Returns the new angle
*/
module.exports.interpolateRotation = function(a, b, ratio) {
//return interpRot = lerp(a, b, ratio)
  var PI = Math.PI
  var whole = 2 * PI
  var quarter = PI / 2
  var threeQuarters = 3* PI / 2

  if (a < quarter && b > threeQuarters) {
    return interpRot = lerp(a + whole, b, ratio) - whole
  } else if (a > threeQuarters && b < quarter) {
    return interpRot = lerp(a, b + whole, ratio) - whole
  } else {
    return interpRot = lerp(a, b, ratio)
  }    
}


module.exports.isApproximately = function(a, b, threshold) {
  return (a < b + threshold && a > b - threshold)
}
