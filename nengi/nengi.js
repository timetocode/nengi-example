/**
* Please see the NENGI END USER LICENSE available in the LICENSE.txt file
* By downloading or using this software or related content you are agreeing to 
* be bounds by the terms of the NENGI END USER LICENSE Agreement. If you do not 
* or cannot agree to the terms of the Agreement please do not download or use 
* this software.
*/
var nengi = {}

//nengi.config = require('./config')
/* Rather than add nengi to all of the classes, they are just wired together 
* here. This saves the trouble of typing nengi.something from within the lib
* itself. External usage should always be nengi.something */

var BinaryType = require('./binary/BinaryType')
// shortcuts for less typing
nengi.Boolean   = BinaryType.Boolean
nengi.Int2      = BinaryType.Int2
nengi.UInt2     = BinaryType.UInt2
nengi.Int3      = BinaryType.Int3
nengi.UInt3     = BinaryType.UInt3
nengi.Int4      = BinaryType.Int4
nengi.UInt4     = BinaryType.UInt4
nengi.Int6      = BinaryType.Int6
nengi.UInt6     = BinaryType.UInt6
nengi.Int8      = BinaryType.Int8
nengi.UInt8     = BinaryType.UInt8
nengi.Int10     = BinaryType.Int10
nengi.UInt10    = BinaryType.UInt10
nengi.Int12     = BinaryType.Int12
nengi.UInt12    = BinaryType.UInt12
nengi.Int16     = BinaryType.Int16
nengi.UInt16    = BinaryType.UInt16
nengi.Int32     = BinaryType.Int32
nengi.UInt32    = BinaryType.UInt32
nengi.String    = BinaryType.String
nengi.EntityId  = BinaryType.EntityId
nengi.Rotation  = BinaryType.Rotation

//nengi.ID_BINARYTYPE = nengi.config.ID_BINARYTYPE
//nengi.TYPE_BINARYTYPE = nengi.config.TYPE_BINARYTYPE

nengi.Constructors = require('./game/Constructors')

nengi.MessageType = require('./MessageType')

nengi.Entity = require('./game/Entity')
nengi.Component = require('./game/Component')
nengi.Event = require('./game/Event')
nengi.Message = require('./game/Message')
nengi.Binary = require('./binary/Binary')
nengi.createEntitySchema = require('./binary/BinarySerializer').createEntitySchema
nengi.createSchema = require('./binary/BinarySerializer').createSchema
//nengi.NetSchema = require('./NetSchema')

nengi.Vector2 = require('./Vector2')
nengi.MathEx = require('./MathEx')

nengi.IdentityMessage = require('./client/IdentityMessage')
nengi.PingMessage = require('./client/PingMessage')
nengi.InputFrame = require('./client/InputFrame')

//nengi.Historian = require('./Historian')
//nengi.Quadtree = require('./Quadtree')
//nengi.AABB = require('./AABB')

//nengi.NetObject = require('./NetObject')
//nengi.NetObjectCache = require('./NetObjectCache')

nengi.Instance = require('./server/Instance')
nengi.Client = require('./server/Client')

//nengi.IdentityEvent = require('./network/IdentityEvent')

nengi.Application = require('./client/Application')




// hacky way of excluding node.js specific code from the being built into the
// browser version of nengi
if (typeof window === 'undefined') {
    nengi.NodeLoop = require('./NodeLoop')
}


module.exports = nengi