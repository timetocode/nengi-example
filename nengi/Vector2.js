/*
* Vector2, a 2D vector and useful maths
* adapated from THREE.js (MIT License)
* license attached: https://github.com/mrdoob/three.js/blob/master/LICENSE
*/
function Vector2 (x, y) {
	this.x = x || 0
	this.y = y || 0
}

Vector2.prototype.constructor = Vector2

Vector2.prototype.add = function(vector) {
	this.x += vector.x
	this.y += vector.y
}

Vector2.prototype.addScalar = function(scalar) {
	this.x += scalar
	this.y += scalar
}

Vector2.prototype.subtract = function(vector) {
	this.x -= vector.x
	this.y -= vector.y
}

Vector2.prototype.multiply = function(vector) {
	this.x *= vector.x
	this.y *= vector.y
}

Vector2.prototype.multiplyScalar = function(scalar) {
	this.x *= scalar
	this.y *= scalar
}

Vector2.prototype.divide = function(vector) {
	this.x /= vector.x
	this.y /= vector.y
}

Vector2.prototype.divideScalar = function(scalar) {
	this.x /= scalar
	this.y /= scalar
}

Vector2.prototype.dotProduct = function(vector) {
	return this.x * vector.x + this.y * vector.y
}

Vector2.prototype.lengthSquared = function() {
	return this.x * this.x + this.y * this.y
}

Object.defineProperty(Vector2.prototype, 'length', {
	get: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y)
	},
	set: function(value) {
		var oldLength = this.length
		if (oldLength !== 0 && value !== oldLength) {
			this.multiplyScalar(value/oldLength)
		}
	}
})

Vector2.prototype.normalize = function() {
	return this.divideScalar(this.length)
}

Vector2.prototype.lerp = function(vector, alpha) {
	this.x += (vector.x - this.x) * alpha
	this.y += (vector.y - this.y) * alpha
}

Vector2.prototype.equals = function(vector) {
	return (this.x === vector.x && this.y === vector.y)
}

//TODO obey object pool creation rules, no 'new'
Vector2.prototype.clone = function() {
	return new Vector2(this.x, this.y)
}

module.exports = Vector2

/*
// some THREE.js functionality of potential interest to include
THREE.Vector2.prototype = {
	copy: function ( v ) {

		this.x = v.x;
		this.y = v.y;

		return this;
	},

	addVectors: function ( a, b ) {

		this.x = a.x + b.x;
		this.y = a.y + b.y;

		return this;

	},
	subVectors: function ( a, b ) {

		this.x = a.x - b.x;
		this.y = a.y - b.y;

		return this;

	},

	distanceTo: function ( v ) {
		return Math.sqrt( this.distanceToSquared( v ) );
	},
	distanceToSquared: function ( v ) {
		var dx = this.x - v.x, dy = this.y - v.y;
		return dx * dx + dy * dy;
	},
};
*/