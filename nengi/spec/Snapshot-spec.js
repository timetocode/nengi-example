var Snapshot = require('../Snapshot')

describe('Snapshot', function() {

	var snapshot = new Snapshot()

	it('can have a proxy added', function() {
		var id = 16
		var proxy = [id, 2, 255, 230]
		snapshot.addGameObjectProxy(proxy)
		expect(snapshot.gameObjectProxies[0] === proxy)
		expect(snapshot.gameObjectsProxiesRef[id]).toEqual(proxy)
	})


	it('can return a proxy by id', function() {
		var id = 16
		var proxy = [id, 2, 255, 230]
		snapshot.addGameObjectProxy(proxy)
		expect(snapshot.getGameObjectProxy(id)).toEqual( proxy)
	})


	it('can forget a proxy', function() {
		var id = 16
		var proxy = [id, 2, 255, 230]
		snapshot.addGameObjectProxy(proxy)
		snapshot.forget(id)
		expect(snapshot.getGameObjectProxy(id)).toBe(undefined)
		expect(snapshot.forgets[0]).toBe(id)
	})

	it('can forget a proxy #2', function() {
		var id = 16
		var proxy0 = [234, 2, 255, 230]
		var proxy1 = [id, 3, 121, 5530]
		var proxy2 = [123, 5, 123, 3120]
		snapshot.addGameObjectProxy(proxy0)
		snapshot.addGameObjectProxy(proxy1)
		snapshot.addGameObjectProxy(proxy2)
		snapshot.forget(id)
		expect(snapshot.getGameObjectProxy(id)).toBe(undefined)

	})

	it('can clone itself', function() {
		var proxy = [1, 2, 3, 5, 12]
		snapshot.addGameObjectProxy(proxy)

		var clone = snapshot.clone()

		expect(clone).not.toEqual(snapshot)
		// expect a value from the clone to be the same as a value from the original
		expect(clone.gameObjectProxies[0][0]).toEqual(snapshot.gameObjectProxies[0][0])
		
		// confirming that the clone is duplicate and does not reference the same underlying objects, changing both
		clone.gameObjectProxies[0][0] = 5
		snapshot.gameObjectProxies[0][0] = 6
		expect(clone.gameObjectProxies[0][0]).not.toEqual(snapshot.gameObjectProxies[0][0])
	})

})
