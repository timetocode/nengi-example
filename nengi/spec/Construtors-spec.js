var Constructors = require('../Constructors')

describe('Constructors', function() {

	function SomeClass() {}
	function SomeOtherClass() {}

	var constructors = new Constructors([SomeClass, SomeOtherClass])

	it('internally will have constructors that were added', function() {
		expect(constructors.ctors[0]).toEqual(SomeClass)
		expect(constructors.ctors[1]).toEqual(SomeOtherClass)
	})

	it('can access constructors via keys', function() {
		expect(constructors.getConstructor(0)).toEqual(SomeClass)
		expect(constructors.getConstructor(1)).toEqual(SomeOtherClass)
	})

	it('can access constructors keys via string names', function() {
		expect(constructors.getConstructorKeyByName('SomeClass')).toEqual(0)
		expect(constructors.getConstructorKeyByName('SomeOtherClass')).toEqual(1)
	})
	
})
