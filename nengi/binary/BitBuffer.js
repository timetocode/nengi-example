(function (root) {


var BitBuffer = function(sourceOrLength) {
	this.bitLength = null // length in bits (can be less than the bytes/8)
	this.byteLength = null // length in bytes (atleast enough to hold the bits)
	this.byteArray = null // Uint8Array holding the underlying bits and bytes

	//console.log('BitBuffer ctor', sourceOrLength, typeof sourceOrLength)
	if (typeof sourceOrLength === 'number') {
		// create a bitBuffer with *length* bits
		this.bitLength = sourceOrLength
		this.byteLength = Math.ceil(sourceOrLength / 8)
		if (typeof Buffer !== 'undefined') {
			this.byteArray = new Buffer(this.byteLength)
		} else {
			this.byteArray = new Uint8Array(this.byteLength)
		}
		
	} else if (sourceOrLength instanceof ArrayBuffer) {	
		// create a bitBuffer from an ArrayBuffer (Uint8Array, etc)
		this.bitLength = sourceOrLength.byteLength * 8
		this.byteLength = sourceOrLength.byteLength
		this.byteArray = new Uint8Array(sourceOrLength)
	} else if (typeof Buffer !== 'undefined' && sourceOrLength instanceof Buffer) {
		// create a bitBuffer from a node Buffer
		this.bitLength = sourceOrLength.length * 8
		this.byteLength = sourceOrLength.length
		this.byteArray = new Uint8Array(sourceOrLength)
	} else {
		throw 'Unable to create BitBuffer, expected length (in bits), ArrayBuffer, or Buffer'
	}	
}

// Used to massage fp values so we can operate on them
// at the bit level.
BitBuffer._scratch = new DataView(new ArrayBuffer(8));

BitBuffer.concat = function(bitViews) {
	var bitLength = 0
	for (var i = 0; i < bitViews.length; i++) {	
		bitLength += bitViews[i].bitLength	
	}
	var bitView = new BitBuffer(new Buffer(Math.ceil(bitLength/8)))
	var offset = 0
	for (var i = 0; i < bitViews.length; i++) {
		for (var j = 0; j < bitViews[i].bitLength; j++) {
			bitView._setBit(bitViews[i]._getBit(j), offset)
			offset++
		}
	}
	return bitView
}

BitBuffer.prototype.toBuffer = function() {
	return this.byteArray //new Buffer(this.byteArray, this.byteLength)
}


/*
Object.defineProperty(BitBuffer.prototype, 'buffer', {
	get: function () { return this._buffer; },
	enumerable: true,
	configurable: false
});
*/

/*
Object.defineProperty(BitBuffer.prototype, 'byteLength', {
	get: function () { return this.byteArray.length; },
	enumerable: true,
	configurable: false
});
*/

BitBuffer.prototype._getBit = function (offset) {
	return this.byteArray[offset >> 3] >> (offset & 7) & 0x1;
};

BitBuffer.prototype._setBit = function (on, offset) {
	if (on) {
		this.byteArray[offset >> 3] |= 1 << (offset & 7);
	} else {
		this.byteArray[offset >> 3] &= ~(1 << (offset & 7));
	}
};

BitBuffer.prototype.getBits = function (offset, bits, signed) {
	var available = (this.byteArray.length * 8 - offset);

	if (bits > available) {
		throw new Error('Cannot get ' + bits + ' bit(s) from offset ' + offset + ', ' + available + ' available');
	}

	var value = 0;
	for (var i = 0; i < bits;) {
		var read;

		// Read an entire byte if we can.
		if ((bits - i) >= 8 && ((offset & 7) === 0)) {
			value |= (this.byteArray[offset >> 3] << i);
			read = 8;
		} else {
			value |= (this._getBit(offset) << i);
			read = 1;
		}

		offset += read;
		i += read;
	}

	if (signed) {
		// If we're not working with a full 32 bits, check the
		// imaginary MSB for this bit count and convert to a
		// valid 32-bit signed value if set.
		if (bits !== 32 && value & (1 << (bits - 1))) {
			value |= -1 ^ ((1 << bits) - 1);
		}

		return value;
	}

	return value >>> 0;
};

BitBuffer.prototype.setBits = function (value, offset, bits) {
	var available = (this.byteArray.length * 8 - offset);

	if (bits > available) {
		throw new Error('Cannot set ' + bits + ' bit(s) from offset ' + offset + ', ' + available + ' available');
	}

	for (var i = 0; i < bits;) {
		var wrote;

		// Write an entire byte if we can.
		if ((bits - i) >= 8 && ((offset & 7) === 0)) {
			this.byteArray[offset >> 3] = value & 0xFF;
			wrote = 8;
		} else {
			this._setBit(value & 0x1, offset);
			wrote = 1;
		}

		value = (value >> wrote);

		offset += wrote;
		i += wrote;
	}
};

// true, false
BitBuffer.prototype.readBoolean = function (offset) {
	return this.getBits(offset, 1, false) !== 0;
};

// -4 to 3
BitBuffer.prototype.readInt3 = function (offset) {
	return this.getBits(offset, 3, true);
};
// 0 to 7
BitBuffer.prototype.readUInt3 = function (offset) {
	return this.getBits(offset, 3, false);
};
// -8 to 7
BitBuffer.prototype.readInt4 = function (offset) {
	return this.getBits(offset, 4, true);
};
// 0 to 15
BitBuffer.prototype.readUInt4 = function (offset) {
	return this.getBits(offset, 4, false);
};
// -32 to 31
BitBuffer.prototype.readInt6 = function (offset) {
	return this.getBits(offset, 6, true);
};
// 0 to 63
BitBuffer.prototype.readUInt6 = function (offset) {
	return this.getBits(offset, 6, false);
};
// -128 to 127
BitBuffer.prototype.readInt8 = function (offset) {
	return this.getBits(offset, 8, true);
};
// 0 to 255
BitBuffer.prototype.readUInt8 = function (offset) {
	return this.getBits(offset, 8, false);
};
// -512 to 511
BitBuffer.prototype.readInt10 = function (offset) {
	return this.getBits(offset, 10, true);
};
// 0 to 1023
BitBuffer.prototype.readUInt10 = function (offset) {
	return this.getBits(offset, 10, false);
};
// -2048 to 2047
BitBuffer.prototype.readInt12 = function (offset) {
	return this.getBits(offset, 12, true);
};
// 0 to 4095
BitBuffer.prototype.readUInt12 = function (offset) {
	return this.getBits(offset, 12, false);
};
// -32768 to 32767
BitBuffer.prototype.readInt16 = function (offset) {
	return this.getBits(offset, 16, true);
};
// 0 to 65535
BitBuffer.prototype.readUInt16 = function (offset) {
	return this.getBits(offset, 16, false);
};
// -2147483648 to 2147483647
BitBuffer.prototype.readInt32 = function (offset) {
	return this.getBits(offset, 32, true);
};
// 0 to 4294967295
BitBuffer.prototype.readUInt32 = function (offset) {
	return this.getBits(offset, 32, false);
};
BitBuffer.prototype.readFloat32 = function (offset) {
	BitBuffer._scratch.setUint32(0, this.readUInt32(offset));
	return BitBuffer._scratch.getFloat32(0);
};
BitBuffer.prototype.readFloat64 = function (offset) {
	BitBuffer._scratch.setUint32(0, this.readUInt32(offset));
	// DataView offset is in bytes.
	BitBuffer._scratch.setUint32(4, this.readUInt32(offset+32));
	return BitBuffer._scratch.getFloat64(0);
};

BitBuffer.prototype.writeBoolean = function (value, offset) {
	this.setBits(value ? 1 : 0, offset, 1);
};
BitBuffer.prototype.writeInt3  =
BitBuffer.prototype.writeUInt3 = function (value, offset) {
	this.setBits(value, offset, 3);
};
BitBuffer.prototype.writeInt4  =
BitBuffer.prototype.writeUInt4 = function (value, offset) {
	this.setBits(value, offset, 4);
};
BitBuffer.prototype.writeInt6  =
BitBuffer.prototype.writeUInt6 = function (value, offset) {
	this.setBits(value, offset, 6);
};
BitBuffer.prototype.writeInt8  =
BitBuffer.prototype.writeUInt8 = function (value, offset) {
	this.setBits(value, offset, 8);
};
BitBuffer.prototype.writeInt10  =
BitBuffer.prototype.writeUInt10 = function (value, offset) {
	this.setBits(value, offset, 10);
};
BitBuffer.prototype.writeInt12  =
BitBuffer.prototype.writeUInt12 = function (value, offset) {
	this.setBits(value, offset, 12);
};
BitBuffer.prototype.writeInt16  =
BitBuffer.prototype.writeUInt16 = function (value, offset) {
	this.setBits(value, offset, 16);
};
BitBuffer.prototype.writeInt32  =
BitBuffer.prototype.writeUInt32 = function (value, offset) {
	this.setBits(value, offset, 32);
};
BitBuffer.prototype.writeFloat32 = function (value, offset) {
	BitBuffer._scratch.setFloat32(0, value);
	this.setBits(BitBuffer._scratch.getUint32(0), offset, 32);
};
BitBuffer.prototype.writeFloat64 = function (value, offset) {
	BitBuffer._scratch.setFloat64(0, value);
	this.setBits(BitBuffer._scratch.getUint32(0), offset, 32);
	this.setBits(BitBuffer._scratch.getUint32(4), offset+32, 32);
};

/**********************************************************
 *
 * BitStream
 *
 * Small wrapper for a BitBuffer to maintain your position,
 * as well as to handle reading / writing of string data
 * to the underlying buffer.
 *
 **********************************************************/
var reader = function (name, size) {
	return function () {
		var val = this.byteArray[name](this._index);
		this._index += size;
		return val;
	};
};

var writer = function (name, size) {
	return function (value) {
		this.byteArray[name](this._index, value);
		this._index += size;
	};
};

function readASCIIString(stream, bytes) {
	var i = 0;
	var chars = [];
	var append = true;

	// Read while we still have space available, or until we've
	// hit the fixed byte length passed in.
	while (!bytes || (bytes && i < bytes)) {
		var c = stream.readUInt8();

		// Stop appending chars once we hit 0x00
		if (c === 0x00) {
			append = false;

			// If we don't have a fixed length to read, break out now.
			if (!bytes) {
				break;
			}
		}

		if (append) {
			chars.push(c);
		}

		i++;
	}

	// Convert char code array back to string.
	return chars.map(function (x) {
		return String.fromCharCode(x);
	}).join('');
}

function writeASCIIString(stream, string, bytes) {
	var length = bytes || string.length + 1;  // + 1 for NULL

	for (var i = 0; i < length; i++) {
		stream.writeUInt8(i < string.length ? string.charCodeAt(i) : 0x00);
	}
}

var BitStream = function (source, byteOffset, byteLength) {
	var isBuffer = source instanceof ArrayBuffer ||
		(typeof Buffer !== 'undefined' && source instanceof Buffer);

	if (!(source instanceof BitBuffer) && !isBuffer) {
		throw new Error('Must specify a valid BitBuffer, ArrayBuffer or Buffer');
	}

	if (isBuffer) {
		this.byteArray = new BitBuffer(source, byteOffset, byteLength);
	} else {
		this.byteArray = source;
	}

	this._index = 0;
};

Object.defineProperty(BitStream.prototype, 'byteIndex', {
	// Ceil the returned value, over compensating for the amount of
	// bits written to the stream.
	get: function () { return Math.ceil(this._index / 8); },
	set: function (val) { this._index = val * 8; },
	enumerable: true,
	configurable: true
});

Object.defineProperty(BitStream.prototype, 'buffer', {
	get: function () { return this.byteArray.buffer; },
	enumerable: true,
	configurable: false
});

Object.defineProperty(BitStream.prototype, 'view', {
	get: function () { return this.byteArray; },
	enumerable: true,
	configurable: false
});

BitStream.prototype.getBits = function (bits, signed) {
	var val = this.byteArray.getBits(this._index, bits, signed);
	this._index += bits;
	return val;
};

BitStream.prototype.setBits = function (value, bits) {
	this.byteArray.setBits(this._index, value, bits);
	this._index += bits;
};

BitStream.prototype.readInt8 = reader('getInt8', 8);
BitStream.prototype.readUInt8 = reader('getUInt8', 8);
BitStream.prototype.readInt16 = reader('getInt16', 16);
BitStream.prototype.readUInt16 = reader('getUInt16', 16);
BitStream.prototype.readInt32 = reader('getInt32', 32);
BitStream.prototype.readUInt32 = reader('getUInt32', 32);
BitStream.prototype.readFloat32 = reader('getFloat32', 32);
BitStream.prototype.readFloat64 = reader('getFloat64', 64);

BitStream.prototype.writeInt8 = writer('setInt8', 8);
BitStream.prototype.writeUInt8 = writer('setUInt8', 8);
BitStream.prototype.writeInt16 = writer('setInt16', 16);
BitStream.prototype.writeUInt16 = writer('setUInt16', 16);
BitStream.prototype.writeInt32 = writer('setInt32', 32);
BitStream.prototype.writeUInt32 = writer('setUInt32', 32);
BitStream.prototype.writeFloat32 = writer('setFloat32', 32);
BitStream.prototype.writeFloat64 = writer('setFloat64', 64);

BitStream.prototype.readASCIIString = function (bytes) {
	return readASCIIString(this, bytes);
};

BitStream.prototype.writeASCIIString = function (string, bytes) {
	writeASCIIString(this, string, bytes);
};

// AMD / RequireJS
if (typeof define !== 'undefined' && define.amd) {
	define(function () {
		return {
			BitBuffer: BitBuffer,
			BitStream: BitStream
		};
	});
}
// Node.js
else if (typeof module !== 'undefined' && module.exports) {
	module.exports = BitBuffer
}

}(this));