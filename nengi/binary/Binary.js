
var BinaryType = require('./BinaryType')
/*
*
*
*
*
*/
var Binary = {}


/*
* Relatively straight forward binary types
*/
Binary[BinaryType.Boolean] = {
    'min': 0,
    'max': 1,
    'bits': 1,
    'write': 'writeBoolean',
    'read': 'readBoolean',
    'interp': 'never'
}

Binary[BinaryType.Int4] = {
    'min': -8,
    'max': 7,
    'bits': 4,
    'write': 'writeInt4',
    'read': 'readInt4',
    'interp': 'lerp'
}

Binary[BinaryType.UInt4] = {
    'min': 0,
    'max': 15,
    'bits': 4,
    'write': 'writeUInt4',
    'read': 'readUInt4',
    'interp': 'lerp'
}

Binary[BinaryType.Int6] = {
    'min': -32,
    'max': 31,
    'bits': 6,
    'write': 'writeInt6',
    'read': 'readInt6',
    'interp': 'lerp'
}

Binary[BinaryType.UInt6] = {
    'min': 0,
    'max': 63,
    'bits': 6,
    'write': 'writeUInt6',
    'read': 'readUInt6',
    'interp': 'lerp'
}

Binary[BinaryType.Int8] = {
    'min': -128,
    'max': 127,
    'bits': 8,
    'write': 'writeInt8',
    'read': 'readInt8',
    'interp': 'lerp'
}

Binary[BinaryType.UInt8] = {
    'min': 0,
    'max': 255,
    'bits': 8,
    'write': 'writeUInt8',
    'read': 'readUInt8',
    'interp': 'lerp'
}

Binary[BinaryType.Int10] = {
    'min': -512,
    'max': 511,
    'bits': 10,
    'write': 'writeInt10',
    'read': 'readInt10',
    'interp': 'lerp'
}

Binary[BinaryType.UInt10] = {
    'min': 0,
    'max': 1023,
    'bits': 10,
    'write': 'writeUInt10',
    'read': 'readUInt10',
    'interp': 'lerp'
}

Binary[BinaryType.Int16] = {
    'min': -32768,
    'max': 32767,
    'bits': 16,
    'write': 'writeInt16',
    'read': 'readInt16',
    'interp': 'lerp'
}

Binary[BinaryType.UInt16] = {
    'min': 0,
    'max': 65535,
    'bits': 16,
    'write': 'writeUInt16',
    'read': 'readUInt16',
    'interp': 'lerp'
}

// ## untested/unused so far
Binary[BinaryType.Int32] = {
    'min': -2147483648,
    'max': 2147483647,
    'bits': 32,
    'write': 'writeInt32',
    'read': 'readInt32',
    'interp': 'lerp'
}

// ## untested/unused so far
Binary[BinaryType.UInt32] = {
    'min': 0,
    'max': 4294967295,
    'bits': 32,
    'write': 'writeUInt32',
    'read': 'readUInt32',
    'interp': 'lerp'
}

/*
* Specially customized binary types
*/

// denotes that the number is an id
Binary[BinaryType.EntityId] = {
    'min': 0,
    'max': 255,
    'bits': 8,
    'write': 'writeUInt8',
    'read': 'readUInt8',
    'interp': 'never'
}

// storing a rotation as a byte
// should be translated to radians or degrees before being used meaningfully
Binary[BinaryType.Rotation] = {
    'min': 0,
    'max': 255,
    'bits': 8,
    'write': 'writeUInt8',
    'read': 'readUInt8',
    'interp': 'byteRotationLerp'
}

Binary[BinaryType.String] = {
    // todo
}


module.exports = Binary