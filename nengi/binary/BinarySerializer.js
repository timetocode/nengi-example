
var Binary = require('./Binary')
var BinaryType = require('./BinaryType')
var BitBuffer = require('./BitBuffer')

function BinarySerializer() {
	
}

BinarySerializer.prototype.checkProxies = function(proxyA, proxyB) {
	var dirty = []
	for (var i = 0; i < proxyA.length; i++) {
		if (proxyA[i] !== proxyB[i])
		dirty.push(i)		
	}
	return dirty
}


BinarySerializer.prototype.serializeFulls = function(bytes, serialized, netSchema) {
    //console.log(serialized)
    var bitBuffer = new BitBuffer(bytes * 8)
    var offset = 0
    for (var i = 0; i < serialized.length; i++) {
        var propName = netSchema.keys[i]
        var propData = netSchema.properties[propName]
       // bTypes.push(propData.binaryType)
        //var bytes = Binary[propData.binaryType].bytes
        var value = serialized[i]
        if (typeof value === 'undefined')
            value = 0

        //console.log('bits', Binary[propData.binaryType].bits, '/', bytes * 8, offset)

        bitBuffer[Binary[propData.binaryType].write](offset, value, true)
        offset += Binary[propData.binaryType].bits
    }
    return bitBuffer//.toBuffer()
}




BinarySerializer.prototype.serializePartials = function(bytes, serialized, netSchema) {
    //console.log('bar', serialized)
    var bitBuffer = new BitBuffer(bytes * 8)
    var offset = 0
    for (var key in serialized) {
    	//console.log('bitBuffer writing', key)
        //buffer[Binary[BinaryType.UInt8].write](key, offset, true)
        //buffer.writeUInt8(key, offset, true)
        //console.log('buffer->',bitBuffer[Binary[BinaryType.UInt8].svReadFn](offset))
        bitBuffer[Binary[BinaryType.UInt8].write](offset, key)
        //console.log('bitview->',bitBuffer[Binary[BinaryType.UInt8].svReadFn](offset))
        offset += 8
        
        var propName = netSchema.keys[key]
        //console.log('o', offset, key, 'key:'+propName)
        var propData = netSchema.properties[propName]
        var value = serialized[key]
        //console.log(propName, value, propData)
       // console.log('bitview writing', value)
        bitBuffer[Binary[propData.binaryType].write](offset, Math.round(value))
        //console.log('bitview->',bitBuffer[Binary[propData.binaryType].svReadFn](offset))
        offset += Binary[propData.binaryType].bits
        //console.log('o', offset, propName+':'+value)

    }
    return bitBuffer//.toBuffer()
}


/*
* Creates a proxy of anything with a netSchema
*
* example:
*	id: 1           ->    0: 1
*	type: SomeType  ->    1: ? (0-255 representation of type)
*	x: 55           ->    2: 55
*	y: 102          ->    3: 102
* the exact keys, values, and relevant properties come from a netSchema
*
* @method proxify
* @param {GameObject} gameObject
* @return {Object}
*/
BinarySerializer.prototype.proxify = function(gameObject) {
	//var bytes = 0
	var proxy = []

	//console.log('PROXY', gameObject)

	var schema = gameObject.netSchema
	for (var i = 0; i < schema.keys.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[i]]

		if (prop.path) {
			var path = prop.path
			//bytes += Binary[prop.binaryType].bytes
			if (path.length === 1) {
				value = gameObject[path[0]]
			} else if (path.length === 2) {
				value = gameObject[path[0]][path[1]]
			} else if (path.length === 3) {
				value = gameObject[path[0]][path[1]][path[2]]
			}
		}

		proxy.push(value)
	}

	return proxy
}

BinarySerializer.prototype.proxifyPartials = function(keys, gameObject) {
	var result = []
	var schema = gameObject.netSchema

	//console.log(keys, gameObject.netSchema)

	for (var i = 0; i < keys.length; i++) {
		var key = keys[i]
		var propAlias = schema.keys[key]

		var value = null
		var prop = schema.properties[propAlias]
		console.log('checking',propAlias, schema.keys[i], prop )
		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				value = gameObject[path[0]]
			} else if (path.length === 2) {
				value = gameObject[path[0]][path[1]]
			} else if (path.length === 3) {
				value = gameObject[path[0]][path[1]][path[2]]
			}
		}

		result.push({ key: key, value: value })
	}
	return result
}

/*
* Deserializes and copies data into a gameObject
*
* @method deproxify
* @param {Object} serializedGameObject The serialized data
* @param {GameObject} gameObject the recipient of the serialized data
*/
BinarySerializer.prototype.deproxify = function(serializedGameObject, gameObject) {	
	var schema = gameObject.netSchema
	for (var i = 0; i < schema.keys.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[i]]

		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				gameObject[path[0]] = serializedGameObject[i]
			} else if (path.length === 2) {
				gameObject[path[0]][path[1]] = serializedGameObject[i]
			} else if (path.length === 3) {
				gameObject[path[0]][path[1]][path[2]] = serializedGameObject[i]
			}
		}
	}
}


// for deproxifying net messages
BinarySerializer.prototype.deproxify2 = function(serializedGameObject, gameObject) {
	//console.log('deproxify2', serializedGameObject)	
	var schema = gameObject.netSchema
	for (var i = 2; i < schema.keys.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[i]]
		//console.log('checking', prop,prop.path)

		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				//console.log('herehere', path[0], 'set to', serializedGameObject[i-1])
				gameObject[path[0]] = serializedGameObject[i-1]
			} else if (path.length === 2) {
				gameObject[path[0]][path[1]] = serializedGameObject[i-1]
			} else if (path.length === 3) {
				gameObject[path[0]][path[1]][path[2]] = serializedGameObject[i-1]
			}
		}
	}
}

BinarySerializer.prototype.foo = function(serializedGameObject, gameObject) {
	var schema = gameObject.netSchema

	for (var i = 0; i < serializedGameObject.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[serializedGameObject[i].key]]

		console.log('prop', prop)
		
		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				gameObject[path[0]] = serializedGameObject[i].value
			} else if (path.length === 2) {
				gameObject[path[0]][path[1]] = serializedGameObject[i].value
			} else if (path.length === 3) {
				gameObject[path[0]][path[1]][path[2]] = serializedGameObject[i].value
			}
		}
	}
}

/*
* Creates a netSchema based on the properties and types listed in config. Only
* the properties listed in this schema will be networked from server to client.
* Any other properties are considered private to the server. IMMUTABLE
*
* example config (object):
*  'id': BinaryType.UInt16
*  'type': BinaryType.UInt8
*  'x': BinaryType.Int16
*  'y': BinaryType.Int16
*  'physics.mass' : BinaryType.UInt16
*
* example resulting netSchema (array):
*   0: { key: 0, binaryType: 4, path: ['id'] }
*   1: { key: 1, binaryType: 1, path: ['type'] }
*   2: { key: 2, binaryType: 3, path: ['x'] }
*   3: { key: 3, binaryType: 3, path: ['y'] }
*   4: { key: 4, binaryType: 4, path: ['physics', 'mass'] }
*
* the netSchema can be used to serialize gameObject data for transmission
* 'type' represents a BinaryType (see BinaryType defintion)
* 'key' is a 0-255 key for the property name
* 'path' is a path to a value if the value is nested within a gameComponent
* rather than being in the root gameObject
*
* @constructor NetSchema
* @param {Object} config
* @return {Object}
*/
 BinarySerializer.prototype.createEntitySchema = function(config, optimizations) {
	// the list of properties
	var schema = {}

	schema.properties = {}
	schema.totalBits = 0
	// the byte representation of each property, e.g. 'id' is 0, 'type' is 1
	schema.keys = []
	var index = 0
	for (var prop in config) {

		var binaryType = ''
		var interp = true
		if (typeof config[prop] === 'object') {
			//console.log('prop was object', config[prop])
			binaryType = config[prop].binaryType
			interp = config[prop].interp
		} else {
			//console.log('prop was not object', config[prop], typeof config[prop])
			binaryType = config[prop]
		}
		// RULE: first property must be the gameObject id
		// RULE: second property must be the gameObject type
		if (index === 0 && prop !== 'id') {
			throw 'netSchema first property must be id'
		} else if (index === 1 && prop !== 'type') {
			throw 'netSchema second property must be type'
		}


		schema.properties[prop] = { key: index, binaryType: binaryType, interp: interp }
		schema.totalBits += Binary[binaryType].bits
		schema.keys.push(prop)
		// if the property was not in the root gameObject...
		if (prop.indexOf('.') !== -1) {
			// then create an array called path with the property path
			// e.g. 'physics.mass' becomes path['physics', 'mass']
			schema.properties[prop].path = prop.split('.')
			if (schema.properties[prop].path.length > 3) {
				throw new Exception('createSchema error: can only create schemas of gameObjects with 3 or fewer layers of nested components, e.g. gameObject.foo.bar.baz')
			}
		} else {
			schema.properties[prop].path = [prop]
		}
		index++
	}
	if (index > 255) {
		throw 'netSchema cannot be created with more than 255 properties'
	}

	if (typeof optimizations !== 'undefined') {
		schema.hasOptimizations = true		
		schema.optimizations = []

		for (var prop in optimizations) {
			var opt = optimizations[prop]
			opt.prop = prop
			schema.optimizations.push(optimizations[prop])
		}
		/*
		schema.optimization.properties = {}
		schema.optimization.keys =[]

		for (var prop in optimization) {
			var index = schema.properties[prop].key
			schema.optimization.properties[prop] =  { key: index, binaryType: optimization[prop] }
			schema.optimization.keys.push(prop)
		}
		*/
		//schema.keys[optimization]
	} else {
		schema.hasOptimizations = false
	}
	// return an immutable schema
	//console.log('SCHEMA', schema)
	return Object.freeze(schema)
}

 BinarySerializer.prototype.createSchema = function(config) {
	// the list of properties
	var schema = {}
	schema.properties = {}
	schema.totalBits = 0
	// the byte representation of each property, e.g. 'id' is 0, 'type' is 1
	schema.keys = []
	var index = 0
	for (var prop in config) {
		schema.properties[prop] = { key: index, binaryType: config[prop] }
		schema.totalBits += Binary[config[prop]].bits
		schema.keys.push(prop)
		// if the property was not in the root gameObject...
		if (prop.indexOf('.') !== -1) {
			// then create an array called path with the property path
			// e.g. 'physics.mass' becomes path['physics', 'mass']
			schema.properties[prop].path = prop.split('.')
			if (schema.properties[prop].path.length > 3) {
				throw new Exception('createSchema error: can only create schemas of gameObjects with 3 or fewer layers of nested components, e.g. gameObject.foo.bar.baz')
			}
		} else {
			schema.properties[prop].path = [prop]
		}
		index++
	}
	if (index > 255) {
		throw 'netSchema cannot be created with more than 255 properties'
	}
	schema.hasOptimizations = false

	// return an immutable schema
	//console.log('SCHEMA', schema)
	return Object.freeze(schema)
}

var singleton = new BinarySerializer()
module.exports = singleton