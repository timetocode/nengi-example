

var BinaryType = {
	// the 'value' of these enums is irrelevant.
	// usage:   something.binaryType = BinaryType.Int16
	// for convenience these are aliased in nengi's root e.g. nengi.UInt16
	Null: 0, // never used
	Boolean: 1,
	Int2: 2,
	UInt2: 3,
	Int3: 4,
	UInt3: 5,
	Int4: 6,
	UInt4: 7,
	Int6: 33,
	UInt6: 34,	
	Int8: 8,
	UInt8: 9,
	Int10: 10,
	UInt10: 11,
	Int12: 12,
	UInt12: 13,
	Int16: 14,
	UInt16: 15,
	Int32: 16,
	UInt32: 17,
	Float32: 18, // untested
	Float64: 19, // untested
	String: 20, // untested

	// Customized even more...

	EntityId: 21,
	Rotation: 22
}



module.exports = BinaryType