
function Constructors(ctors) {
	this.ctors = []
	this.ctorsRef = {}
	this.netSchemas = []
	this.register(ctors)

	this.enumValue = 0
	//Object.freeze(this)
}

Constructors.prototype.register = function(ctors) {
	for (var i = 0; i < ctors.length; i++) {
		var ctor = ctors[i]

		// new
		//ctor.prototype.type = this.enumValue++


		this.ctors.push(ctor)
		this.ctorsRef[ctor.name] = i
		var obj = new ctor()
		this.netSchemas.push(obj.netSchema)
	}
}

Constructors.prototype.getConstructor = function(key) {
	return this.ctors[key]
}

Constructors.prototype.getNetSchema = function(key) {
	return this.netSchemas[key]
}

Constructors.prototype.getConstructorKeyByName = function(ctorName) {
	return this.ctorsRef[ctorName]
}

module.exports = Constructors