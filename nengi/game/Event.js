var Vector2 = require('../Vector2')


function Event(now) {
	this.initialize(now)

	//if (config) {
	//	this.configure(config)
	//}
}

Event.prototype.constructor = Event

Event.prototype.initialize = function(now) {
	this.created = now
	this.position = new Vector2(0, 0)
	this.state = null
}

/*
* Shorthand access to gameEvent.position.x via gameEvent.x
*/
Object.defineProperty(Event.prototype, 'x', {
	get: function() { return this.position.x },
	set: function(value) { this.position.x = value }
})

/*
* Shorthand access to gameEvent.position.y via gameEvent.y
*/
Object.defineProperty(Event.prototype, 'y', {
	get: function() { return this.position.y },
	set: function(value) { this.position.y = value }
})

Event.prototype.update = function(delta, tick, now) {
	// to be overriden by descendents
}

module.exports = Event