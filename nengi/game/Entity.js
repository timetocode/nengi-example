var Vector2 = require('../Vector2')
//var shared = require('../../core/Shared')

function Entity(config) {
	this.initialize()

	if (config) {
		this.configure(config)
	}


	//this.type = this.constructor.prototype.type
	//console.log('ENTITY TYPE', this.type, this.constructor.name)
}

Entity.prototype.constructor = Entity

Entity.prototype.initialize = function() {
	this.id = null

	this.position = new Vector2(0, 0)

	this.state = null

	/* components for iteration */
	this.components = []
	/* components are also added to the gameObject directly as properties
	* e.g.
	* this.bodyCollider = colliderComponent
	*/


}

/*
* Uses an object to set values within the gameObject
* Accepts x,y without an explicit position. An explicit position ref
* is allowed as well ( pass in config.position )
* @method configure
* @param {Object} config Key-value-pair to copy into gameObject
*/
Entity.prototype.configure = function(config) {
	for (var prop in config) {		
		if (prop === 'x' || prop === 'y') {
			// adding: x, y 
			this.position[prop] = config[prop]
		} else if (prop === 'position') {
			// adding: predefined position
			this['position'] = config['position']
		} else {
			// adding: component
			this.addComponent(config[prop])
		}
	}
}

/*
* Adds a component to the gameObject. The gameObject may only have one component
* with a given name. To add two of the same type of component to the gameObject 
* one of the components must be renamed, e.g. collider.name = 'specialCollider'
* @method addComponent
* @param {GameComponent} component
*/
Entity.prototype.addComponent = function(name, component) {
	if (!this[name]) {
		this[name] = component
		this.components.push(component)
	} else {
		throw 'Cannot add same component to gameObject twice.'
	}
}

/*
* Removes a component from the gameObject by name
* @method removeComponent
* @param {String} name
*/
Entity.prototype.removeComponent = function(name) {
	if (this[component.name]) {
		var removeIndex = -1
		for (var i = 0; i < this.components.length; i++) {
			if (this.components[i].name === component.name) {
				removeIndex = i
			}
		}
		if (removeIndex !== -1) {
			this.components.splice(removeIndex, 1)
			delete this[component.name]
		} else {
			throw 'Failed to remove component.'
		}
	}
}

/*
* Returns a component by name
* @method getComponent
* @param {String} name
* @return {GameComponent}
*/
Entity.prototype.getComponent = function(name) {
	if (this[name]) {
		return this[name]
	} else {
		throw 'Could not get component.'
	}
}
/* not implemented nor used: changeComponent */

/*
* Shorthand access to gameObject.position.x via gameObject.x
*/
Object.defineProperty(Entity.prototype, 'x', {
	get: function() { return this.position.x },
	set: function(value) { this.position.x = value }
})

/*
* Shorthand access to gameObject.position.y via gameObject.y
*/
Object.defineProperty(Entity.prototype, 'y', {
	get: function() { return this.position.y },
	set: function(value) { this.position.y = value }
})

/*
* Shorthand access to gameObject.position.rotation via gameObject.rotation
*/
Object.defineProperty(Entity.prototype, 'rotation', {
	get: function() { return this.position.rotation },
	set: function(value) { this.position.rotation = value }
})

Entity.prototype._update = function(delta, tick, now) {
	if (this.update) {
		this.update(delta, tick, now)
	}
	for (var i = 0; i < this.components.length; i++) {
		var component = this.components[i]
		if (component.update) {
			component.update(delta, tick, now)
		}
	}
}

module.exports = Entity