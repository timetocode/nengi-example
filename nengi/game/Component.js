
function Component(gameObject, config) {
	this.gameObject = gameObject
	this.position = gameObject.position

	this.initialize()

	if (config) {
		this.configure(config)
	}
}

Component.prototype.constructor = Component


Component.prototype.setup = function(gameObject) {
	this.gameObject = gameObject
	this.position = gameObject.position
}


Component.prototype.initialize = function() {

}

/*
* Uses an object to set values within the gameObject
* Accepts x,y,rotation without an explicit position. An explicit position ref
* is allowed as well ( config.position )
* @method configure
* @param {Object} config Key-value-pair to copy into gameObject
*/
Component.prototype.configure = function(config) {
	for (var prop in config) {
		this[prop] = config[prop]
	}
}

/*
* Fetches a fellow component from the parent gameObject. Merely a shortcut for 
* access to other components within component scope.
* @method getComponent
* @return {Component} component
*/
Component.prototype.getComponent = function(name) {
	return this.gameObject.getComponent(name)
}

/*
* Shorthand access to gameObject.position.x via component.x
*/
Object.defineProperty(Component.prototype, 'x', {
	get: function() { return this.position.x },
	set: function(value) { this.position.x = value }
})

/*
* Shorthand access to gameObject.position.y via component.y
*/
Object.defineProperty(Component.prototype, 'y', {
	get: function() { return this.position.y },
	set: function(value) { this.position.y = value }
})

/*
* Shorthand access to gameObject.position.rotation via component.rotation
*/
Object.defineProperty(Component.prototype, 'rotation', {
	get: function() { return this.position.rotation },
	set: function(value) { this.position.rotation = value }
})

module.exports = Component