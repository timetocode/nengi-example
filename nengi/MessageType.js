var MessageType = {
	Ping: 8,
	Pong: 9,
	PartialGameObjects: 10,
	GameObjects: 11,
	GameObjectOptimizations: 12,
	GameEvents: 13,
	ForgetObjects: 14,
	Quadtrees: 15,
	PlayerInputs: 16,
	DirectMessages: 17
}

module.exports = MessageType