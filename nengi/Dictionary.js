function Dictionary() {
	this._objects = {}
	this._iterable = []
}

Dictionary.prototype.get = function(id) {
	return this._objects[id]
}

Dictionary.prototype.forEach = function(iterator) {
	for (var i = 0; i < this._iterable.length; i++) {
		iterator(this._iterable[i])
	}
}

// a copy of the underlying array
Dictionary.prototype.toArray = function() {
    return this._iterable.slice()
}

Dictionary.prototype.add = function(obj) {
	this._objects[obj.id] = obj
    this._iterable.push(obj)
}

//in place filter: 
/*
var j = 0; 
for (var i = 0; i < arr.length; ++i) { 
	if (shouldKeep(arr[i])) { 
		arr[j++] = arr[i]; 
	} 
} arr.length = j;
shouldKeep is the filter, apply any change to the items
*/

//remove: 
// arr[i] = arr[arr.length-1];
// arr.pop();
Dictionary.prototype.remove = function(id) {
    var index = -1
    for (var i = 0; i < this._iterable.length; i++) {
        if (this._iterable[i].id === id) {
            index = i
            break
        }
    }
    if (index !== -1) {
        this._iterable.splice(index, 1)
    }

    delete this._objects[id]
}


module.exports = Dictionary