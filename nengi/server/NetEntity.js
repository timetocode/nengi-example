var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

/*
* Holds a gameObject's networked values
*/
function NetEntity() {
    this.id = null

    this.tickSerialized = null
    // <Object> serialized object (via a netSchema)
    this.proxy = null
    // <Array> props changed since last tick, use to create partial
    this.dirty = []
    // <Buffer> full data for the game object as a buffer
    this.full = null
    // byte count of the full data
    this.fullBytes = 0
    // <Buffer> partial data for the game object, prop changes since last tick
    this.partial = null
    // byte count of the partial data
    this.partialBytes = 0

    this.dx = 0
    this.dy = 0
    this.dxr = 0
    this.dyr = 0

    this.netSchema = null
}

NetEntity.prototype.constructor = NetEntity
//todo
NetEntity.prototype.copy = function() {
    var netObject = new NetEntity()
    netObject.id = this.id
    netObject.tickSerialized = this.tickSerialized
    netObject.proxy = this.proxy.copy()
    netObject.dirty = this.dirty.slice()

    netObject.full = this.full
    netObject.fullBytes = this.fullBytes

    netObject.partial = this.partial
    netObject.partialBytes = this.partialBytes

    netObject.dx = this.dx
    netObject.dy = this.dy
    netObject.netSchema = this.netSchema
}

/*
* Serializes a gameObject and stores it in this netObject
* sets values for proxy, full, fullBytes
*/
NetEntity.prototype.serialize = function(gameObject, tick) {
    var netSchema = gameObject.netSchema
    this.id = gameObject.id
    this.proxy = BinarySerializer.proxify(gameObject)
    this.netSchema = gameObject.netSchema
    this.tickSerialized = tick
}

/*
* Reserializes a gameObject, tracking the differences
* sets values for proxy, full, fullbytes, partial, partialBytes
*/
NetEntity.prototype.reserialize = function(gameObject, tick) {
    var netSchema = gameObject.netSchema


    var oldX = this.proxy[netSchema.properties['x'].key]
    var oldY = this.proxy[netSchema.properties['y'].key]
    //console.log('old', oldX, oldY, netSchema.properties)

    var proxy = BinarySerializer.proxify(gameObject)
    

    // gameObject already cached, find diffs between the new state and old state
    var diffs = []
    for (var j = 0; j < this.proxy.length; j++) {
        if (this.proxy[j] !== proxy[j]) {
            diffs.push(j)
        }
    }

    if (diffs.length > 0) {
        var diffArr = []

        for (var j = 0; j < diffs.length; j++) {
            var index = diffs[j]
            var propName = netSchema.keys[index]
            var prop = netSchema.properties[propName]
            var value = this.proxy[index]
            diffArr.push({ propIndex: index, propValue: value })

            if (propName === 'x') {
                this.dx = this.proxy[index] - proxy[index]
                this.dxr = (this.proxy[index] - proxy[index]) % 1
            } else if (propName === 'y') {
                this.dy = this.proxy[index] - proxy[index]
                this.dyr = (this.proxy[index] - proxy[index]) % 1
            }
        }
        this.partial = diffArr
    }
    //console.log('dxr', this.dxr, 'dyr', this.dyr)
    this.dirty = diffs
    this.proxy = proxy
    this.netSchema = gameObject.netSchema
    this.tickSerialized = tick    
}



module.exports = NetEntity