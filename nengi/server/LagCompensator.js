var MathEx = require('../MathEx')

// TODO: consider just merging this to historian
function LagCompensator(historian, config) {
	this.historian = historian
	this.config = config
}

/**
* Looks into the past of area, returning the past positions of all entities therein.
*
* @param area {AABB} Axis-aligned bounding box specifying the area
* @param latencyMs {Integer} Latency of client in milliseconds
* @param interpolationMs {Integer} Length of the interpolation window in the client in millseconds
* @param clientTickRate {Integer} Tick rate, aka fps of the client
* @return {Array} An array of past position objects [{ id: Integer, x: Number, y: Number },etc.]
*/
// TODO: client should have its own latency, interp, tickrate variables,
// which could simplify these params to area, currentTick, client
// or perhaps only client, currenTick, and assume area as the client.viewArea
LagCompensator.prototype.compensateArea = function(client, area, tick) {
	var currentTick = tick
	var latencyMs = client.ping // or shoudl this be ping/2?
	var interpolationMs = 100 //client.interp
	var clientTickRate = client.tickRate

	var TICKS_PER_SECOND = this.config.UPDATE_RATE

	// how far in the past the player's entities are relative to the server in milliseconds
	// e.g. a player with 250 ping and default 100 ms interpolation, running at 60 fps 
	// has an approximate total delay of 366 ms
	var delay = latencyMs + interpolationMs + (1000 / clientTickRate)

	//console.log('compensating with delay', delay)

	//console.log('compensation delay', delay)
	// how long a tick is on the server in milliseconds
	// e.g. a server at 20 fps will have a ticklength of 50 ms
	var tickLength = 1000 / TICKS_PER_SECOND

	// how many ticks ago to rewind the server's entities
	// e.g. 366/50 means we need to look 7 ticks into the past to get entity positions
	var ticksAgo = Math.floor(delay / tickLength)

	// how far between one tick and the next are we?
	// e.g. 366/50 is actually 7.32, the tick portion is 0.32, 
	// roughly 1/3 the way from tick 7 to tick 8
	// TODO: while this seems to produce good results (perhaps because it is *close*), 
	// is itactually right? maybe 1.0 - 0.32 is more accurate. because we woudl be lerping
	// from 8 to 7, not 7 to 8...
	var tickPortion = (delay % tickLength) / tickLength

	// two timeslices from the historian that flank the time in the past we are compensating to
	// e.g. if our delay requires us to go back in time 7.32 ticks, then
	// timesliceA is the positional data from 8 ticks ago
	// timesliceB is the positional data from 7 ticks ago
	// and we intend to interpolate postions 0.32 of the way between these postions
	// TODO: or should we interpolate 1 - 0.32 = 0.68, in this example
	var timesliceA = this.historian.getTimesliceQuadtree(currentTick - ticksAgo - 1)
	var timesliceB = this.historian.getTimesliceQuadtree(currentTick - ticksAgo)

	// get all the NPCs from the quadtree in the relevant area for both time slices
	// NOTE: fast moving objects might have existed in one slice but not another, if
	// these still need compensated, then one must look in a larger area
	var positionsA = []
	timesliceA.queryRange(area, positionsA)
	var positionsB = []
	timesliceB.queryRange(area, positionsB)

	// copy the entity position data into a dictionary instead of an array
	// TODO: evaluate if this could be more elegant
	var entitiesA = {}
	var entitiesB = {}
	for (var i = 0; i < positionsA.length; i++) {
	    entitiesA[positionsA[i].id] = positionsA[i]
	}
	for (var i = 0; i < positionsB.length; i++) {
	    entitiesB[positionsB[i].id] = positionsB[i]
	}

	var pastPositions = []

	for (var entityId in entitiesA) {
        // if it appears in both timeslices...
        if (entitiesB[entityId]) {

            // lerp between the position in timesliceA and timesliceB
            var entityA = entitiesA[entityId]
            var entityB = entitiesB[entityId]

            // calculate the x, y position the object would've been in, making the
            // assumption that it moved linearly between these past two positions
            var x = MathEx.lerp(entityA.x, entityB.x, tickPortion)
            var y = MathEx.lerp(entityA.y, entityB.y, tickPortion)
            
            var pastPosition = {
            	id: entityId,
            	x: x,
            	y: y
            }
            pastPositions.push(pastPosition)
		}
    }
    return pastPositions
}

module.exports = LagCompensator