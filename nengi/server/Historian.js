var AABB = require('./AABB')
var Quadtree = require('./Quadtree')
var LagCompensator = require('./LagCompensator')
/** 
Stores snapshots of the gameObject quadtree, offers the ability to see where
entities were in the recent past

@class Historian
@constructor
*/
function Historian(config) {
	this.history = {}
  this.mostRecentTick = -1
  this.lagCompensator = new LagCompensator(this, config)
}


/**
Returns a historical Quadtree

@method getTimesliceQuadtree
@param {Integer} tick The simulation step
@return {Object} Returns a historical Quadtree
*/
Historian.prototype.getTimesliceQuadtree = function(tick) {
  if (this.history[tick]) {
    return this.history[tick]
  } else {
    return Quadtree.create(AABB.create(0,0,500,500))
  }
}


/**
* enum private to Historian
*/
var ProxyType = {
  GameObject: 1,
  GameEvent: 2
}

/**
For any given tick of the simulation, creates and saves a quadtree holding all
of the entities. Given that all collisions involve these quadtrees, the 
Historian is required to have recored at least one tick before any collisions 
may occur.

@method recordHistory
@param {Integer} tick The simulation step
@param {Array} gameObjects Array of gameObjects to insert
@param {Array} gameEvents Array of gameEvents to insert
@param {AABB} boundary An AABB of the maximum area of the quadtree
*/
Historian.prototype.recordHistory = function(tick, gameObjects, gameEvents, boundary) {
  var quadtree = Quadtree.create(boundary.clone())
  for (var i = 0; i < gameObjects.length; i++) {
    var gameObject = gameObjects[i]
    quadtree.insert({
      id: gameObject.id,
      x: gameObject.x, 
      y: gameObject.y,  
      type: ProxyType.GameObject
    })
  }

  for (var i = 0; i < gameEvents.length; i++) {
    var gameEvent = gameEvents[i]
    quadtree.insert({
      id: gameEvent.id,
      x: gameEvent.x, 
      y: gameEvent.y,    
      type: ProxyType.GameEvent
    })
  }

  this.history[tick] = quadtree

  if (tick > this.mostRecentTick) {
    this.mostRecentTick = tick
  }

  //Quadtree.logStats()
  //AABB.logStats()
  if (this.history[tick-20]) {
    this.history[tick-20].release()
    delete this.history[tick-20]
  }
}

Historian.prototype.getRecentState = function(view) {
  var quadtree = this.getTimesliceQuadtree(this.mostRecentTick)
  quadtree.selectIds(view)


}

Historian.prototype.getRecentEvents = function() {
  var quadtree = this.getTimesliceQuadtree(this.mostRecentTick)
}

Historian.prototype.getRecentQuadtree = function() {
  return this.getTimesliceQuadtree(this.mostRecentTick)
}

module.exports = Historian