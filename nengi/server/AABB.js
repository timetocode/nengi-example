
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
// axis-aligned bounding box
/* PARAMS: (Point)center, (Point)half */
function AABB(x, y, halfWidth, halfHeight) {
	this.initialize(x, y, halfWidth, halfHeight)
}

AABB.pool = []
AABB.instanceCount = 0
AABB.inUseCount = 0
AABB.logStats = function() {
	console.log(
		'AABBs', 
		'total', AABB.instanceCount, 
		'avail', AABB.instanceCount - AABB.inUseCount, 
		'in-use', AABB.inUseCount
	)
}

AABB.create = function(x, y, halfWidth, halfHeight) {
	//return new AABB(x, y, halfWidth, halfHeight)
	var instance

	if (AABB.pool.length === 0) {        
		instance = new AABB(x, y, halfWidth, halfHeight)
		AABB.instanceCount++
    } else {
        instance = AABB.pool.pop()
        instance.initialize(x, y, halfWidth, halfHeight)        
    }

    AABB.inUseCount++
	return instance
}

AABB.prototype.release = function() {
	AABB.pool.push(this)
	AABB.inUseCount--
}

AABB.prototype.initialize = function(x, y, halfWidth, halfHeight) {
	this.x = x
	this.y = y
	this.halfWidth = halfWidth
	this.halfHeight = halfHeight
}

AABB.prototype.containsPoint = function(x, y) {
	// inclusive on the lower bound, exclusivive on the upper bound for quadtree
	// compatibilty, so that a point exactly on a line belongs to only one node
	return (x >= this.x - this.halfWidth 
		&& y >= this.y - this.halfHeight
		&& x < this.x + this.halfWidth
		&& y < this.y + this.halfHeight)
}

AABB.prototype.intersects = function(aabb) {
	return (Math.abs(this.x - aabb.x) * 2 < (this.halfWidth  * 2 + aabb.halfWidth  * 2))
		&& (Math.abs(this.y - aabb.y) * 2 < (this.halfHeight * 2 + aabb.halfHeight * 2))
}

AABB.prototype.clone = function() {
	return AABB.create(this.x, this.y, this.halfWidth, this.halfHeight)
}


/*
// only used for debugging quadtrees across a network
AABB.prototype.netSchema = BinarySerializer.createSchema({
	'x': BinaryType.Int32,
	'y': BinaryType.Int32,
	'halfWidth': BinaryType.Int32,
	'halfHeight': BinaryType.Int32
})
*/

module.exports = AABB