var AABB = require('./AABB')
var IdAliases = require('./IdAliases')
var ClientState = require('./ClientState')
//var EventEmitter = require('./external/EventEmitter')

/*
* (Server-side) holds information about a connected client, and what the client
* can see
*/
function Client(common) {
	//EventEmitter.call(this)
	this.id = null
	// <WebSocketServerConnection>
	this.connection = null
	this.common = common

	this.entity = null
	// <AABB> visible area
	this.view = AABB.create(250, 250, 900, 900)
	this.clientState = new ClientState()
	this.interp = 100
	this.tickRate = this.common.config.DRAW_RATE

	// <Array> gameObjectIds visible last time updatedateVisibilty was called
	this.previousVisibleIds = []
	this.maxVisibleObjects = this.common.config.ID_BINARYTYPE.max
	// <IdAliases> provides alias identifers for game objects from 0-255
	this.idAliases = new IdAliases(this.maxVisibleObjects)

	this.messages = []

	// instance to which this client is connected
	this.instance = null

	this.totalVisible = 0

	this.lastInputProcessed = -1

	this.inputFrameBuffer = []


	this.lastPingCheckTimestamp = Date.now()
	this.ping = 0
	this.pingResults = []
}

//Client.prototype = Object.create(EventEmitter.prototype)
Client.prototype.constructor = Client

Client.prototype.setPingResult = function(ping) {
	this.pingResults.push(ping)
	while (this.pingResults.length > 5) {
		this.pingResults.shift()
	}

	var tempTotal = 0
	for (var i = 0; i < this.pingResults.length; i++) {
		tempTotal += this.pingResults[i]
	}
	this.ping = tempTotal / this.pingResults.length

	if (this.ping > 1024) {
		this.ping = 1024
	}
	//console.log('AVERAGE PING', this.ping)
}

Client.prototype.needsPingCheck = function() {
	return Date.now() - this.lastPingCheckTimestamp > this.common.config.PING_INTERVAL
}

Client.prototype.pingCheckSent = function() {
	this.lastPingCheckTimestamp = Date.now()
}


/*
* The client uses a system of Alias Ids, shortened Ids that map to full game 
* objects e.g. gameObject 120390 could map to an alias of 15, allowing for less 
* data to be sent, as well as providing some security because the client does 
* not know the true ids of the game objects it can see
*/
Client.prototype.getAliasId = function(gameObjectId) {
	//console.log('getAliasId', gameObjectId, this.idAliases.getAliasId(gameObjectId))
	return this.idAliases.getAliasId(gameObjectId)	
}

Client.prototype.createAliasId = function(gameObjectId) {

	return this.idAliases.registerId(gameObjectId)
}

Client.prototype.releaseAliasId = function(aliasId) {

	return this.idAliases.unregisterId(aliasId)
}


Client.prototype.queueMessage = function(message) {
	this.messages.push(message)
}

Client.prototype.queueInputFrame = function(inputFrame) {
	this.inputFrameBuffer.push(inputFrame)
}



/*
* Updates the visibility category of gameObjects
* gameObjects may be one in one of three categories
*   newlyVisible: gameObject has just become visible, client needs its full data
*   stillVisible: gameObject was already visible, client needs only changes updated
*   noLongerVisible: gameObject has just left visibility, client should remove it
*/
Client.prototype.categorizeEntityVisibility = function(quadtree) {

	var stuffs = quadtree.selectObjectAndEventIds(this.view)
	var allVisibleIds = stuffs.objectIds//quadtree.selectIds(this.view)

	var visibleIds = []
	var max = allVisibleIds.length
	if (allVisibleIds.length > this.maxVisibleObjects) {
		max = this.maxVisibleObjects
	}
	for (var i = 0; i < max; i++) {
		visibleIds.push(allVisibleIds[i])
	}

	//console.log('TOTAL VISIBLE', visibleIds.length, '/', allVisibleIds.length, 'max:', this.maxVisibleObjects)
	//console.log('VISIBLE', visibleIds)
	//console.log('PREVIOUS',this.previousVisibleIds)

	var categories = arrayDiffs(visibleIds, this.previousVisibleIds)
	// update the visible ids, for the next call
	this.previousVisibleIds = visibleIds

	this.noLongerVisible = categories.right
	this.newlyVisible = categories.left
	this.stillVisible = categories.both

	//this.noLongerVisibleCache = this.noLongerVisible

	//console.log(this.noLongerVisible)





	var ret = { 
		noLongerVisible: this.noLongerVisible, 
		newlyVisible: this.newlyVisible, 
		stillVisible: this.stillVisible,
		events: stuffs.eventIds
	}

	
	return ret
}


Client.prototype.skipVisibilityCheck = function() {
	return { 
		noLongerVisible: [], 
		newlyVisible: [],
		stillVisible: this.previousVisibleIds
	}
}

function compareNumbers(a, b) {
  return a - b
}

function arrayDiffs(a, b) {
	a.sort(compareNumbers)
	b.sort(compareNumbers)

	//console.log('A', a)
	//console.log('B', b)

	var left = []
	var both = []
	var right = []

	var i = 0
	var j = 0

	while (i < a.length && j < b.length) {
	    if (a[i] < b[j]) {
        	left.push(a[i])
	        ++i
	    } else if (b[j] < a[i]) {
	        right.push(b[j])
	        ++j
	    } else {
	        both.push(a[i])
	        ++i
	        ++j
	    }
	}
	while (i < a.length) {
	   left.push(a[i])
	    ++i
	}
	while (j < b.length) {
	    right.push(b[j])
	    ++j
	}

	return { left: left, both: both, right: right }
}

module.exports = Client