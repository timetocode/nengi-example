/*
* Serverside record of a client's state
*/
function ClientState() {
	// <Object> key: gameObject.id, value: gameObject
	this.gameObjects = {}
}

ClientState.prototype.constructor = ClientState

module.exports = ClientState