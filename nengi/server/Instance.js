var Historian = require('./Historian')
var AABB = require('./AABB')
var NetObjectCache = require('./NetObjectCache')
var BandwidthMonitor = require('./BandwidthMonitor')
//var BinaryType = require('./BinaryType')
var EventEmitter = require('../external/EventEmitter')
var Dictionary = require('../Dictionary')

var QuadtreeAABBNetworker = require('../network/debug/QuadtreeAABBNetworker')
var GameStateWriter = require('../network/GameStateWriter')

var PingMessage = require('../client/PingMessage')

function Instance(common) {
    EventEmitter.call(this)
	this.boundary = AABB.create(0, 0, 10000, 10000)
	this.historian = new Historian(common.config)

    //this.entities = new Dictionary()
    this.entities = []
    this.entitiesRef = {}

    // positional, most events related to the game
    // these events are added to the quadtree like entitys, and are selected
    // based on vicinity
    // e.g. fired a laser, healed someone, gaining a level (if everyone can see)
    this.gameEvents = []
    this.gameEventsRef = {}
    this.newGameEvents = []

    // private, for a player only
    // per player queue
    // e.g. a private message to a player, achievement unlocked (if private)
    // format { clientId, gameEvent }
    this.directMessages = []
    this.directMessagesRef = {}

    // global, for everyone
    // queue, cleared at end of tick
    // e.g. messages for all players, server shutting down, boss killed,
    this.instanceEvents = []
    this.instanceMessages = []  

    //this.messages = new Dictionary()
    this.messages = []
    this.messagesRef = []

	this.netObjectCache = new NetObjectCache(this)
    this.gameStateWriter = new GameStateWriter(this.netObjectCache, common)
    this.bandwidthMonitor = new BandwidthMonitor(common.config)

    this.common = common
    //console.log('CTORS', this.common)

   // this.clientsRef = {}
	//this.clients = []

    this.clients = []
    this.clientsRef = {}

    //this.clients = new Dictionary()
    this.inputSnapshots = []
}

Instance.prototype = Object.create(EventEmitter.prototype)
Instance.prototype.constructor = Instance

Instance.prototype.addInputSnapshot = function(inputSnapshot) {
    this.inputSnapshots.push(inputSnapshot)
}

var eventId = 0
Instance.prototype.addGameEvent = function(gameEvent) {
    //gameEvent.instance = this // TODO do events use this?
    gameEvent.id = eventId++
    this.gameEvents.push(gameEvent)
    this.newGameEvents.push(gameEvent)
    this.gameEventsRef[gameEvent.id] = gameEvent
    //console.log('EVENT-->', gameEvent)
   // this.entitysRef[entity.id] = entity
}

Instance.prototype.createEntity = function(type, config) {
    //TODO
    //instantiate by type
    //copy config into object
    //this.addEntity(entity)
}

var messageId = 0
Instance.prototype.addMessage = function(message, client) {
    //gameMessage
    console.log('message added to instance', message, message.constructor.name)
    message.id = messageId++
    message.type = this.common.messageConstructors.getConstructorKeyByName(message.constructor.name)
    this.messages.push(message)
    this.messagesRef[message.id] = message
    client.queueMessage(message)
}

var clientId = 0
Instance.prototype.addClient = function(client) {
    client.id = clientId++
    client.instance = this
    this.clients.push(client)
    this.clientsRef[client.id] = client
}
Instance.prototype.removeClient = function(client) {
    var id = client.id
    var index = -1
    for (var i = 0; i < this.clients.length; i++) {
        if (this.clients[i].id === id) {
            index = i
            break
        }
    }
    if (index !== -1) {
        this.clients.splice(index, 1)
    }
    delete this.clientsRef[id]
}

var id = 0
Instance.prototype.addEntity = function(entity) {
    entity.instance = this
    entity.id = id++

    // TODO consider decoupling the constructor code from this area
    entity.type = this.common.entityConstructors.getConstructorKeyByName(entity.constructor.name)
    

    //this.entities.add(entity)

    this.entities.push(entity)
    this.entitiesRef[entity.id] = entity
    //console.log(entity)
}

Instance.prototype.getEntity = function(id) {
   // return this.entities.get(id)
    return this.entitiesRef[id]
}

Instance.prototype.getMessage = function(id) {
    return this.messagesRef[id]
}

Instance.prototype.getEventObjectById = function(id) {
    return this.gameEventsRef[id]
}

// unused & untested
Instance.prototype.removeEntity = function(entity) {

    //return this.entities.remove(id)

    var id = entity.id
    var index = -1
    for (var i = 0; i < this.entities.length; i++) {
        if (this.entities[i].id === id) {
            index = i
            break
        }
    }
    if (index !== -1) {
        this.entities.splice(index, 1)
    }
    //this.entitiesRef[id].instance = null
    delete this.entitiesRef[id]
    
}


Instance.prototype.addPrivateEvent = function(client, evt) {
    //this.privateEvents.push({ clientId: clientId, gameEvent: ev})

    client.directMessages.push(evt)
}



Instance.prototype.n_update = function(delta, tick, now) {

    //console.log('instance', 'c', this.clients.length, 'e', this.entities.length)
    if (this.update) {
        this.update(delta, tick, now)
    }
    this.bandwidthMonitor.update(delta, tick, now)


    while (this.inputSnapshots.length > 0) {
        var inputSnapshot = this.inputSnapshots.shift()

        while (inputSnapshot.inputs.length > 0) {
            var input = inputSnapshot.inputs.shift()
            inputSnapshot.client.lastInputProcessed = inputSnapshot.tick
            this.emit('inputContext', { 
                tick: inputSnapshot.tick, 
                client: inputSnapshot.client, 
                input: input 
            })
        }
        
    }
    

    // update all entitys

	for (var i = 0; i < this.entities.length; i++) {
		var entity = this.entities[i]
		entity._update(delta, tick, now)				
	}
    

    //this.entities.forEach(function(entity) {
    //    entity._update(delta, tick, now)
    //})

    for (var i = 0; i < this.gameEvents.length; i++) {
        var gameEvent = this.gameEvents[i]
        gameEvent._update(delta, tick, now)
    }


    //console.log('adding events to history:', this.newGameEvents.length)
    this.historian.recordHistory(
        tick, 
        this.entities,
        this.newGameEvents, 
        this.boundary
    )
    this.newGameEvents = []

    // get most recent entity positions for the entire instance
	var quadtree = this.historian.getRecentQuadtree()

    var aabbs = quadtree.getAllAABBs()
    var self = this
	for (var m = 0; m < this.clients.length; m++) {
		var client = this.clients[m]

        //console.log('client ping', client.ping)
        //var pingMessage = new PingMessage()
        //pingMessage.ping = client.ping
       // this.addMessage(pingMessage, client)

        // determine visibility of client
        //console.log('attempting to send to client', client)

		var visibility = null

        //var temp = m+tick
        //console.log(j, 'j+tick', j+tick)
        //if (temp % 1 === 0) {
        visibility = client.categorizeEntityVisibility(quadtree)

        var buffer = self.gameStateWriter.writeBuffer(client, visibility, tick, aabbs)
        //console.log('+=== ===+')
        self.bandwidthMonitor.addOutgoing(buffer.length)

        //console.log('sending', finalBuffer.length, 'to', client.id)
        //var test = new BitBuffer(8)
        //test.writeUInt8(0, 15)
        //if (finalBuffer.length > 0)

        

        client.connection.sendBytes(buffer)

        //console.log(client.clientState)

        //client.flush()

       // console.log('SENT', bitBuffer.readUInt8(0))
    }
}



module.exports = Instance