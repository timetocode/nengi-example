var IdPool = require('./IdPool')

/**
* IdAliases provides bi-directional dictionary-like access between aliasIds and
* actual gameObjectIds. Internally IdAliases uses an IdPool. Using an alias for
* an id allows for smaller numbers (e.g. gameObject.id 12039490 could map to 
* aliasId 12). This allows for a level of security, as the cilent only knows
* alias ids, and does not know the actual ids. This also allows for optimization
* of the data type used to represent an id (e.g. maxAliasId of 255 is 8 bits, a 
* max of 65535 is 16 bits, etc).
*
* @class IdAliases
* @constructor
* @param {Integer} maxAliasId The upper end of the desired alias id range
*/
function IdAliases(maxAliasId) {
	// key: gameObjectId, value: aliasId
	this.gameObjectIds = {}
	// key: aliasId, value: gameObjectId
	this.aliasIds = {}
	// generator for the alias ids 
	this.aliasIdPool = new IdPool(maxAliasId)
}


/**
* Returns the gameObject id for a given alias id.
*
* @method getGameObjectId
* @param {Integer} aliasId 
* @return {Integer} The real gameObjectId, or -1 if nothing found
*/
IdAliases.prototype.getGameObjectId = function(aliasId) {
	//return aliasId
	
	var gameObjectId = this.aliasIds[aliasId]
	if (typeof gameObjectId !== 'undefined') {
		return gameObjectId
	} else {
		return -1
		//throw new Error('No gameObject id for alias ' + aliasId)
	}
}


/**
* Returns the alias id for a given gameObject id.
*
* @method getAliasId
* @param {Integer} gameObjectId
* @return {Integer} The aliasId for the gameObjectId, or -1 if nothing found
*/
IdAliases.prototype.getAliasId = function(gameObjectId) {
	//return gameObjectId
	
	var aliasId = this.gameObjectIds[gameObjectId]
	if (typeof aliasId !== 'undefined') {
		return aliasId
	} else {
		return -1
		//throw new Error('No alias id for gameObject ' + gameObjectId)
	}	
}


/**
* Registers a gameObjectId, assigning it an aliasId
*
* @method registerId
* @param {Integer} gameObjectId
* @return {Integer} The aliasId for the gameObjectId
*/
IdAliases.prototype.registerId = function(gameObjectId) {
	//return gameObjectId
	
	var aliasId = this.aliasIdPool.nextId()

	if (aliasId !== -1) {
		this.gameObjectIds[gameObjectId] = aliasId
		this.aliasIds[aliasId] = gameObjectId
	} else {
		console.log('no alias available for', gameObjectId)
	}
	return aliasId	
}


/**
* Unregisters a aliasId, freeing this aliasId for future use
*
* @method unregisterId
* @param {Integer} aliasId
*/
IdAliases.prototype.unregisterId = function(aliasId) {
	//return aliasId
	
	var gameObjectId = this.getGameObjectId(aliasId)
	delete this.gameObjectIds[gameObjectId]
	delete this.aliasIds[aliasId]
	this.aliasIdPool.releaseId(aliasId)	
}

module.exports = IdAliases