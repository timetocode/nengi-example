var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

/*
* Holds a message's networked values
*/
function NetMessage() {
    this.id = null

    this.tickSerialized = null
    // <Object> serialized object (via a netSchema)
    this.proxy = null
    // <Buffer> full data for the game object as a buffer
    this.full = null
    // byte count of the full data
    this.fullBytes = 0
    // <Buffer> partial data for the game object, prop changes since last tick

    this.netSchema = null
}

NetMessage.prototype.constructor = NetMessage


NetMessage.prototype.copy = function() {
    var netObject = new NetMessage()
    netObject.id = this.id
    netObject.tickSerialized = this.tickSerialized
    netObject.proxy = this.proxy.copy()
    netObject.full = this.full
    netObject.fullBytes = this.fullBytes
}

/*
* Serializes a message and stores it in this netObject
* sets values for proxy, full, fullBytes
*/
NetMessage.prototype.serialize = function(message, tick) {
    var netSchema = message.netSchema
    this.id = message.id
    this.proxy = BinarySerializer.proxify(message)
    this.netSchema = message.netSchema
    this.tickSerialized = tick
}


module.exports = NetMessage