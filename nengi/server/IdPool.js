/**
* IdPool provides a set of continuous integer (e.g. 0-255) that can be borrowed
* from the pool and returned to the pool.
*
* @class IdPool
* @constructor
* @param {Integer} count The (max) size of the IdPool
*/
function IdPool(count) {
	this.ids = []
	for (var i = 0; i < count; i++) {
		this.ids.push(i)
	}
}


/**
* Obtains an availity id from the pool. Will return -1 if no ids are available.
*
* @method nextId
* @return {Integer} An available id
*/
IdPool.prototype.nextId = function() {
	if (this.ids.length > 1) {
		var id = this.ids.shift()
		return id
	} else {
		console.log('IdPool overflow')
		return -1
		//throw new Error('IdPool overflow. No ids left to distribute.')
	}	
}

/**
* Returns an id to the pool
*
* @method releaseId
* @param {Integer} id
*/
IdPool.prototype.releaseId = function(id) {
	this.ids.push(id)
}

module.exports = IdPool