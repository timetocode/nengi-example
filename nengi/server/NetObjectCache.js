var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')
var NetEntity = require('./NetEntity')
var NetEvent = require('./NetEvent')
var NetMessage = require('./NetMessage')

/*
* Holds NetObjects
*/
function NetObjectCache(instance) {
    // cached netproxy of entities
    this.netEntities = {}
    // cached netproxy of positional events
    this.netEvents = {}
    // cached netproxy of messages
    this.netMessages = {}
    this.instance = instance
}

/*
* Returns a netEntity (a representation of a entity, see: NetObject)
* if this object was not already in the cache, it is created and cached
* if this object was already in the cache, it is reserialized
* reserialization checks the object for changes and updates the netEntity
* objects will only undergo [re]serialization once per tick, subsequent calls to
* get() will return a cached copy
*/
NetObjectCache.prototype.getEntity = function(id, tick) {
	var entity = this.instance.getEntity(id)

	// update the cache, if needed
    if (!this.netEntities[id]) {
    	// not in the cache? serialize and add it
    	var netEntity = new NetEntity()
    	netEntity.serialize(entity, tick)
    	this.netEntities[id] = netEntity
    	return netEntity
    } else {
    	// already in cache? reserialize (only once per tick)
    	var netEntity = this.netEntities[id]
    	if (netEntity.tickSerialized !== tick) {
    		netEntity.reserialize(entity, tick)
    	} 
    	// return cached copy
    	return netEntity
    }
}


NetObjectCache.prototype.getEvent = function(id, tick) {
    var gameEvent = this.instance.getEventObjectById(id)

    // update the cache, if needed
    if (!this.netEvents[id]) {
        // not in the cache? serialize and add it
        var netEvent = new NetEvent()
        netEvent.serialize(gameEvent, tick)

        //console.log('seralized netevent', netEvent)
        this.netEvents[id] = netEvent
        return netEvent
    } else {
        // return cached copy
        var netEvent = this.netEvents[id]
        return netEvent
    }   
}

NetObjectCache.prototype.getMessage = function(id, tick) {
    var message = this.instance.getMessage(id)

    // update the cache, if needed
    if (!this.netMessages[id]) {
        // not in the cache? serialize and add it
        var netMessage = new NetMessage()
        netMessage.serialize(message, tick)
        this.netMessages[id] = netMessage
        return netMessage
    } else {
        // return cached copy
        var netMessage = this.netMessages[id]
        return netMessage
    }   
}


module.exports = NetObjectCache