

function BandwidthMonitor(config) {
	this.bytesSent = 0
	this.recentTimestamp = 0
	this.started = 0
	this.bytesSentRecently = 0
	this.bytesReceived = 0
	this.sampleWindow = 5000 // milliseconds 

	this.outgoingBytesPerSecondTotal = 0
	this.outgoingBytesPerSecondRecent = 0
	this.config = config

	//this.incomingBytesPerSecondTotal = 0
	//this.incomingBytesPerSecondRecent = 0
}

BandwidthMonitor.prototype.constructor = BandwidthMonitor

BandwidthMonitor.prototype.update = function(delta, tick, now) {

	if (this.recentTimestamp === 0) {
		this.recentTimestamp = now
		this.started = now
	}
	this.recalculate(now)

	if (now > this.recentTimestamp + this.sampleWindow) {
		console.log(
			'recent:', bytesToString(this.outgoingBytesPerSecondRecent),
			'total:', bytesToString(this.outgoingBytesPerSecondTotal)
		)

		this.bytesSentRecently = 0
		this.recentTimestamp = now
	}
}

BandwidthMonitor.prototype.addOutgoing = function(bytes) {
	this.bytesSent += bytes
	this.bytesSentRecently += bytes
}

BandwidthMonitor.prototype.recalculate = function(now) {
	var totalTime = (now - this.started)/1000

	this.outgoingBytesPerSecondRecent = this.bytesSentRecently / (this.sampleWindow / 1000)
	this.outgoingBytesPerSecondTotal = this.bytesSent / totalTime
}

var bytesToString = function(bytes) {
	// following the base 10 rules for bytes (1000 instead of 1024 bytes in a kB)
	if (bytes < 1000) {
		return bytes.toFixed(2) + ' B/s'
	} else if (bytes >= 1000000) {
		return (bytes/1000000).toFixed(2) + ' MB/s'
	} else if (bytes >= 1000) {
		return (bytes/1000).toFixed(2)+ ' KB/s'
	}
}

module.exports = BandwidthMonitor