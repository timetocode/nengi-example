var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

/*
* Holds a gameEvent's networked values
*/
function NetEvent() {
    this.id = null

    this.tickSerialized = null
    // <Object> serialized object (via a netSchema)
    this.proxy = null
    // <Buffer> full data for the game object as a buffer
    this.full = null
    // byte count of the full data
    this.fullBytes = 0
    // <Buffer> partial data for the game object, prop changes since last tick

    this.netSchema = null
}

NetEvent.prototype.constructor = NetEvent

//todo
NetEvent.prototype.copy = function() {
    var netObject = new NetEvent()
    netObject.id = this.id
    netObject.tickSerialized = this.tickSerialized
    netObject.proxy = this.proxy.copy()
    netObject.full = this.full
    netObject.fullBytes = this.fullBytes
}

/*
* Serializes a gameEvent and stores it in this netObject
* sets values for proxy, full, fullBytes
*/
NetEvent.prototype.serialize = function(gameEvent, tick) {

    //console.log('NetEvent.serialize', gameEvent, tick)
    //console.log('schema', gameEvent.netSchema)
    var netSchema = gameEvent.netSchema
    this.id = gameEvent.id
    this.proxy = BinarySerializer.proxify(gameEvent)
    this.netSchema = gameEvent.netSchema
    this.tickSerialized = tick
}


module.exports = NetEvent