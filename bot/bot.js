/**
* This is a bot version of the client. Run it from a command prompt using node.
* The bot client can be used to connect large numbers of clients to nengi for
* testing and benchmarking purposes. You may need to import some of your game 
* client to run tests, usually stuff like the Input commands that players would
* send to the server. In this case I messily hacked just enough functionality
* out of nengi and the game demo to make the bots constantly wiggle their
* controls.
*/

var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
var common = require('../core/common')
var nengi = require('../nengi/nengi')
var WebSocketClient = require('websocket').client
var BitBuffer = require('../nengi/binary/BitBuffer')
var PlayerInputNetworker = require('../nengi/network/PlayerInputNetworker')

var playerInputNetworker = new PlayerInputNetworker(common)
// importing a little bit of game logic so that the bots can send commands
var Move = require('../core/model/input/Move')

function Bot() {
	WebSocketClient.call(this)

	this.inputBuffer = []
	this.inputTick = 0

	this.connection = null

	//this.app = new nengi.Application(common)

	this.on('connectFailed', function(error) {
    	console.log('Connect Error: ' + error.toString())
	})

	var self = this
	this.on('connect', function(connection) {
	    console.log('WebSocket Client Connected')

	    self.connection = connection
	    connection.on('error', function(error) {
	        console.log("Connection Error: " + error.toString())
	    })

	    connection.on('close', function() {
	        console.log('echo-protocol Connection Closed')
	    })

	    connection.on('message', function(message) {
	        if (message.type === 'utf8') {
	            console.log("Received: '" + message.utf8Data + "'")
	        }
	    })
	})
}
Bot.prototype = Object.create(WebSocketClient.prototype)
Bot.prototype.constructor = Bot

Bot.prototype.update = function() {

	// simulate hitting random keys (should cause them to wiggle)
	var move = new Move()
	move.W = random(0,1)
	move.A = random(0,1)
	move.S = random(0,1)
	move.D = random(0,1)
	this.send(move)
	this.sendInputs(this.inputBuffer)
	this.inputBuffer = [] // clear the buffer


	this.inputTick++
}


var HOW_MANY_DO_YOU_WANT = 60
var DELAY_BETWEEN_CONNECTIONS_MILLISECONDS = 100
var bots = []

Bot.prototype.send = function(input) {
	if (this.inputBuffer.length === 0) {
		var inputFrame = new nengi.InputFrame()
		inputFrame.tick = this.inputTick
		this.inputBuffer.push(inputFrame)
	}
	this.inputBuffer.push(input)
}

Bot.prototype.sendInputs = function(playerInputs) {
	// TODO: calculate the real size of the buffer instead of just picking a big enough number
	var bitBuffer = new BitBuffer(100)
	var offset = 0
	var self = this

	for (var i = 0; i < playerInputs.length; i++) {
		var input = playerInputs[i]
		input.type = common.inputConstructors.getConstructorKeyByName(input.constructor.name)
	}

	offset = playerInputNetworker.write(bitBuffer, offset, playerInputs)
	if (this.connection) {
		//console.log('sending')
		this.connection.send(bitBuffer.byteArray)
	}

}



for (var i = 0; i < HOW_MANY_DO_YOU_WANT; i++) {
	var temp = i
	setTimeout(function() {		
		var bot = new Bot()
		bot.connect(
			'ws://' + common.config.IP + ':' + common.config.PORT,
			common.config.PROTOCOL
		)
		bots.push(bot)
	}, DELAY_BETWEEN_CONNECTIONS_MILLISECONDS * temp)
}

var main = function() {
	for (var i = 0; i < bots.length; i++) {
		var bot = bots[i]
		bot.update()
	}

	console.log('bot count', bots.length)
}


nengi.NodeLoop.setFPS(60)
nengi.NodeLoop.setMain(main)
nengi.NodeLoop.begin()
