// main.js
var nengi = require('../../../nengi/nengi')
var MyGameClient = require('../../../core/MyGameClient')

var isWindowLoaded = false
var isAssetsLoaded = false
var isConnected = false

/*
* Three things need to be loaded:
*   window: allows access to dom (for pixi webgl) and websockets
*   assets: loading the game images and sounds (just images for now)
*   connection: nengi confirming that the websocket connection has been made
*/
var checkReady = function(myGameClient) {
	return (isWindowLoaded && isAssetsLoaded && isConnected)
}
PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST
PIXI.loader.add('badstar', '/images/badstar-spritesheet.json')
PIXI.loader.load(function(loader, resources) {

	isAssetsLoaded = true
})

var myGameClient = new MyGameClient()
myGameClient.app.on('connect', function() {
	console.log('connected to nengi instance')
	isConnected = true
})


var last = Date.now()
window.onload = function() {
	console.log('window loaded')
	isWindowLoaded = true

	myGameClient.setup()

	var loop = function() {
		var now = Date.now()
		var delta = (now - last) / 1000
		last = now

		if (!checkReady()) {
			// loop until readyCheck passes
			console.log('not ready yet')
			window.requestAnimationFrame(loop)
			return // return here, don't go to the other code until we're ready
		}		

		myGameClient.update(delta)

		if (myGameClient.app.websocket && myGameClient.app.websocket.readyState > 1) {
			throw('disconnected, stopping client to make console readable')
		}	

		window.requestAnimationFrame(loop)	
	}

	loop()
}