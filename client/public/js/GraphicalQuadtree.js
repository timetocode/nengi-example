
function GraphicalQuadtree() {	
	PIXI.Container.call(this)

	this.aabbs = []
}

GraphicalQuadtree.prototype = Object.create(PIXI.Container.prototype)
GraphicalQuadtree.prototype.constructor = GraphicalQuadtree

GraphicalQuadtree.prototype.refresh = function() {

	for (var i = 0; i < this.children.length; i++) {
		this.removeChild(this.children[i])
	}

	for (var i = 0; i < this.aabbs.length; i++) {

		var aabb = this.aabbs[i]
		var graphics = new PIXI.Graphics()

		//graphics.beginFill(0xff0000) 

		graphics.lineStyle(1, 0xff0000)

		graphics.drawRect(
			aabb.x - aabb.halfWidth, 
			aabb.y - aabb.halfHeight, 
			aabb.halfWidth * 2, 
			aabb.halfHeight * 2
		)

		graphics.alpha = 0.2

		this.addChild(graphics)
	}
	
}

module.exports = GraphicalQuadtree