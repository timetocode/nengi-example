(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */
/* eslint-disable no-proto */

var base64 = require('base64-js')
var ieee754 = require('ieee754')
var isArray = require('is-array')

exports.Buffer = Buffer
exports.SlowBuffer = SlowBuffer
exports.INSPECT_MAX_BYTES = 50
Buffer.poolSize = 8192 // not used by this implementation

var rootParent = {}

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Safari 5-7 lacks support for changing the `Object.prototype.constructor` property
 *     on objects.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined
  ? global.TYPED_ARRAY_SUPPORT
  : typedArraySupport()

function typedArraySupport () {
  function Bar () {}
  try {
    var arr = new Uint8Array(1)
    arr.foo = function () { return 42 }
    arr.constructor = Bar
    return arr.foo() === 42 && // typed array instances can be augmented
        arr.constructor === Bar && // constructor can be set
        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
        arr.subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
  } catch (e) {
    return false
  }
}

function kMaxLength () {
  return Buffer.TYPED_ARRAY_SUPPORT
    ? 0x7fffffff
    : 0x3fffffff
}

/**
 * Class: Buffer
 * =============
 *
 * The Buffer constructor returns instances of `Uint8Array` that are augmented
 * with function properties for all the node `Buffer` API functions. We use
 * `Uint8Array` so that square bracket notation works as expected -- it returns
 * a single octet.
 *
 * By augmenting the instances, we can avoid modifying the `Uint8Array`
 * prototype.
 */
function Buffer (arg) {
  if (!(this instanceof Buffer)) {
    // Avoid going through an ArgumentsAdaptorTrampoline in the common case.
    if (arguments.length > 1) return new Buffer(arg, arguments[1])
    return new Buffer(arg)
  }

  this.length = 0
  this.parent = undefined

  // Common case.
  if (typeof arg === 'number') {
    return fromNumber(this, arg)
  }

  // Slightly less common case.
  if (typeof arg === 'string') {
    return fromString(this, arg, arguments.length > 1 ? arguments[1] : 'utf8')
  }

  // Unusual.
  return fromObject(this, arg)
}

function fromNumber (that, length) {
  that = allocate(that, length < 0 ? 0 : checked(length) | 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < length; i++) {
      that[i] = 0
    }
  }
  return that
}

function fromString (that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') encoding = 'utf8'

  // Assumption: byteLength() return value is always < kMaxLength.
  var length = byteLength(string, encoding) | 0
  that = allocate(that, length)

  that.write(string, encoding)
  return that
}

function fromObject (that, object) {
  if (Buffer.isBuffer(object)) return fromBuffer(that, object)

  if (isArray(object)) return fromArray(that, object)

  if (object == null) {
    throw new TypeError('must start with number, buffer, array or string')
  }

  if (typeof ArrayBuffer !== 'undefined') {
    if (object.buffer instanceof ArrayBuffer) {
      return fromTypedArray(that, object)
    }
    if (object instanceof ArrayBuffer) {
      return fromArrayBuffer(that, object)
    }
  }

  if (object.length) return fromArrayLike(that, object)

  return fromJsonObject(that, object)
}

function fromBuffer (that, buffer) {
  var length = checked(buffer.length) | 0
  that = allocate(that, length)
  buffer.copy(that, 0, 0, length)
  return that
}

function fromArray (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

// Duplicate of fromArray() to keep fromArray() monomorphic.
function fromTypedArray (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  // Truncating the elements is probably not what people expect from typed
  // arrays with BYTES_PER_ELEMENT > 1 but it's compatible with the behavior
  // of the old Buffer constructor.
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

function fromArrayBuffer (that, array) {
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    array.byteLength
    that = Buffer._augment(new Uint8Array(array))
  } else {
    // Fallback: Return an object instance of the Buffer class
    that = fromTypedArray(that, new Uint8Array(array))
  }
  return that
}

function fromArrayLike (that, array) {
  var length = checked(array.length) | 0
  that = allocate(that, length)
  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

// Deserialize { type: 'Buffer', data: [1,2,3,...] } into a Buffer object.
// Returns a zero-length buffer for inputs that don't conform to the spec.
function fromJsonObject (that, object) {
  var array
  var length = 0

  if (object.type === 'Buffer' && isArray(object.data)) {
    array = object.data
    length = checked(array.length) | 0
  }
  that = allocate(that, length)

  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255
  }
  return that
}

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype
  Buffer.__proto__ = Uint8Array
}

function allocate (that, length) {
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = Buffer._augment(new Uint8Array(length))
    that.__proto__ = Buffer.prototype
  } else {
    // Fallback: Return an object instance of the Buffer class
    that.length = length
    that._isBuffer = true
  }

  var fromPool = length !== 0 && length <= Buffer.poolSize >>> 1
  if (fromPool) that.parent = rootParent

  return that
}

function checked (length) {
  // Note: cannot use `length < kMaxLength` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
                         'size: 0x' + kMaxLength().toString(16) + ' bytes')
  }
  return length | 0
}

function SlowBuffer (subject, encoding) {
  if (!(this instanceof SlowBuffer)) return new SlowBuffer(subject, encoding)

  var buf = new Buffer(subject, encoding)
  delete buf.parent
  return buf
}

Buffer.isBuffer = function isBuffer (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function compare (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers')
  }

  if (a === b) return 0

  var x = a.length
  var y = b.length

  var i = 0
  var len = Math.min(x, y)
  while (i < len) {
    if (a[i] !== b[i]) break

    ++i
  }

  if (i !== len) {
    x = a[i]
    y = b[i]
  }

  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function isEncoding (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'binary':
    case 'base64':
    case 'raw':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function concat (list, length) {
  if (!isArray(list)) throw new TypeError('list argument must be an Array of Buffers.')

  if (list.length === 0) {
    return new Buffer(0)
  }

  var i
  if (length === undefined) {
    length = 0
    for (i = 0; i < list.length; i++) {
      length += list[i].length
    }
  }

  var buf = new Buffer(length)
  var pos = 0
  for (i = 0; i < list.length; i++) {
    var item = list[i]
    item.copy(buf, pos)
    pos += item.length
  }
  return buf
}

function byteLength (string, encoding) {
  if (typeof string !== 'string') string = '' + string

  var len = string.length
  if (len === 0) return 0

  // Use a for loop to avoid recursion
  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'binary':
      // Deprecated
      case 'raw':
      case 'raws':
        return len
      case 'utf8':
      case 'utf-8':
        return utf8ToBytes(string).length
      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2
      case 'hex':
        return len >>> 1
      case 'base64':
        return base64ToBytes(string).length
      default:
        if (loweredCase) return utf8ToBytes(string).length // assume utf8
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}
Buffer.byteLength = byteLength

// pre-set for values that may exist in the future
Buffer.prototype.length = undefined
Buffer.prototype.parent = undefined

function slowToString (encoding, start, end) {
  var loweredCase = false

  start = start | 0
  end = end === undefined || end === Infinity ? this.length : end | 0

  if (!encoding) encoding = 'utf8'
  if (start < 0) start = 0
  if (end > this.length) end = this.length
  if (end <= start) return ''

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'binary':
        return binarySlice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toString = function toString () {
  var length = this.length | 0
  if (length === 0) return ''
  if (arguments.length === 0) return utf8Slice(this, 0, length)
  return slowToString.apply(this, arguments)
}

Buffer.prototype.equals = function equals (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return true
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function inspect () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max) str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function compare (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  if (this === b) return 0
  return Buffer.compare(this, b)
}

Buffer.prototype.indexOf = function indexOf (val, byteOffset) {
  if (byteOffset > 0x7fffffff) byteOffset = 0x7fffffff
  else if (byteOffset < -0x80000000) byteOffset = -0x80000000
  byteOffset >>= 0

  if (this.length === 0) return -1
  if (byteOffset >= this.length) return -1

  // Negative offsets start from the end of the buffer
  if (byteOffset < 0) byteOffset = Math.max(this.length + byteOffset, 0)

  if (typeof val === 'string') {
    if (val.length === 0) return -1 // special case: looking for empty string always fails
    return String.prototype.indexOf.call(this, val, byteOffset)
  }
  if (Buffer.isBuffer(val)) {
    return arrayIndexOf(this, val, byteOffset)
  }
  if (typeof val === 'number') {
    if (Buffer.TYPED_ARRAY_SUPPORT && Uint8Array.prototype.indexOf === 'function') {
      return Uint8Array.prototype.indexOf.call(this, val, byteOffset)
    }
    return arrayIndexOf(this, [ val ], byteOffset)
  }

  function arrayIndexOf (arr, val, byteOffset) {
    var foundIndex = -1
    for (var i = 0; byteOffset + i < arr.length; i++) {
      if (arr[byteOffset + i] === val[foundIndex === -1 ? 0 : i - foundIndex]) {
        if (foundIndex === -1) foundIndex = i
        if (i - foundIndex + 1 === val.length) return byteOffset + foundIndex
      } else {
        foundIndex = -1
      }
    }
    return -1
  }

  throw new TypeError('val must be string, number or Buffer')
}

// `get` is deprecated
Buffer.prototype.get = function get (offset) {
  console.log('.get() is deprecated. Access using array indexes instead.')
  return this.readUInt8(offset)
}

// `set` is deprecated
Buffer.prototype.set = function set (v, offset) {
  console.log('.set() is deprecated. Access using array indexes instead.')
  return this.writeUInt8(v, offset)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  // must be an even number of digits
  var strLen = string.length
  if (strLen % 2 !== 0) throw new Error('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; i++) {
    var parsed = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(parsed)) throw new Error('Invalid hex string')
    buf[offset + i] = parsed
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length)
}

function asciiWrite (buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length)
}

function binaryWrite (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length)
}

function ucs2Write (buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length)
}

Buffer.prototype.write = function write (string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8'
    length = this.length
    offset = 0
  // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset
    length = this.length
    offset = 0
  // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset | 0
    if (isFinite(length)) {
      length = length | 0
      if (encoding === undefined) encoding = 'utf8'
    } else {
      encoding = length
      length = undefined
    }
  // legacy write(string, encoding, offset, length) - remove in v0.13
  } else {
    var swap = encoding
    encoding = offset
    offset = length | 0
    length = swap
  }

  var remaining = this.length - offset
  if (length === undefined || length > remaining) length = remaining

  if ((string.length > 0 && (length < 0 || offset < 0)) || offset > this.length) {
    throw new RangeError('attempt to write outside buffer bounds')
  }

  if (!encoding) encoding = 'utf8'

  var loweredCase = false
  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length)

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length)

      case 'ascii':
        return asciiWrite(this, string, offset, length)

      case 'binary':
        return binaryWrite(this, string, offset, length)

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length)

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding)
        encoding = ('' + encoding).toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.toJSON = function toJSON () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  end = Math.min(buf.length, end)
  var res = []

  var i = start
  while (i < end) {
    var firstByte = buf[i]
    var codePoint = null
    var bytesPerSequence = (firstByte > 0xEF) ? 4
      : (firstByte > 0xDF) ? 3
      : (firstByte > 0xBF) ? 2
      : 1

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte
          }
          break
        case 2:
          secondByte = buf[i + 1]
          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | (secondByte & 0x3F)
            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint
            }
          }
          break
        case 3:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | (thirdByte & 0x3F)
            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint
            }
          }
          break
        case 4:
          secondByte = buf[i + 1]
          thirdByte = buf[i + 2]
          fourthByte = buf[i + 3]
          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | (fourthByte & 0x3F)
            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint
            }
          }
      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD
      bytesPerSequence = 1
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000
      res.push(codePoint >>> 10 & 0x3FF | 0xD800)
      codePoint = 0xDC00 | codePoint & 0x3FF
    }

    res.push(codePoint)
    i += bytesPerSequence
  }

  return decodeCodePointsArray(res)
}

// Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety
var MAX_ARGUMENTS_LENGTH = 0x1000

function decodeCodePointsArray (codePoints) {
  var len = codePoints.length
  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints) // avoid extra slice()
  }

  // Decode in chunks to avoid "call stack size exceeded".
  var res = ''
  var i = 0
  while (i < len) {
    res += String.fromCharCode.apply(
      String,
      codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH)
    )
  }
  return res
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i] & 0x7F)
  }
  return ret
}

function binarySlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; i++) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function slice (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len
    if (start < 0) start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0) end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start) end = start

  var newBuf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = Buffer._augment(this.subarray(start, end))
  } else {
    var sliceLen = end - start
    newBuf = new Buffer(sliceLen, undefined)
    for (var i = 0; i < sliceLen; i++) {
      newBuf[i] = this[i + start]
    }
  }

  if (newBuf.length) newBuf.parent = this.parent || this

  return newBuf
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0) throw new RangeError('offset is not uint')
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUIntLE = function readUIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }

  return val
}

Buffer.prototype.readUIntBE = function readUIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) {
    checkOffset(offset, byteLength, this.length)
  }

  var val = this[offset + --byteLength]
  var mul = 1
  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul
  }

  return val
}

Buffer.prototype.readUInt8 = function readUInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function readUInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function readUInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function readUInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function readUInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
    ((this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    this[offset + 3])
}

Buffer.prototype.readIntLE = function readIntLE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var val = this[offset]
  var mul = 1
  var i = 0
  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readIntBE = function readIntBE (offset, byteLength, noAssert) {
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkOffset(offset, byteLength, this.length)

  var i = byteLength
  var mul = 1
  var val = this[offset + --i]
  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul
  }
  mul *= 0x80

  if (val >= mul) val -= Math.pow(2, 8 * byteLength)

  return val
}

Buffer.prototype.readInt8 = function readInt8 (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80)) return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function readInt16LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function readInt16BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function readInt32LE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset]) |
    (this[offset + 1] << 8) |
    (this[offset + 2] << 16) |
    (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function readInt32BE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
    (this[offset + 1] << 16) |
    (this[offset + 2] << 8) |
    (this[offset + 3])
}

Buffer.prototype.readFloatLE = function readFloatLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function readFloatBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function readDoubleLE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function readDoubleBE (offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('buffer must be a Buffer instance')
  if (value > max || value < min) throw new RangeError('value is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('index out of range')
}

Buffer.prototype.writeUIntLE = function writeUIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0)

  var mul = 1
  var i = 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUIntBE = function writeUIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  byteLength = byteLength | 0
  if (!noAssert) checkInt(this, value, offset, byteLength, Math.pow(2, 8 * byteLength), 0)

  var i = byteLength - 1
  var mul = 1
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = (value / mul) & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeUInt8 = function writeUInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = (value & 0xff)
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; i++) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function writeUInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; i++) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function writeUInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

Buffer.prototype.writeIntLE = function writeIntLE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = 0
  var mul = 1
  var sub = value < 0 ? 1 : 0
  this[offset] = value & 0xFF
  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeIntBE = function writeIntBE (value, offset, byteLength, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1)

    checkInt(this, value, offset, byteLength, limit - 1, -limit)
  }

  var i = byteLength - 1
  var mul = 1
  var sub = value < 0 ? 1 : 0
  this[offset + i] = value & 0xFF
  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = ((value / mul) >> 0) - sub & 0xFF
  }

  return offset + byteLength
}

Buffer.prototype.writeInt8 = function writeInt8 (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = (value & 0xff)
  return offset + 1
}

Buffer.prototype.writeInt16LE = function writeInt16LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
  } else {
    objectWriteUInt16(this, value, offset, true)
  }
  return offset + 2
}

Buffer.prototype.writeInt16BE = function writeInt16BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = (value & 0xff)
  } else {
    objectWriteUInt16(this, value, offset, false)
  }
  return offset + 2
}

Buffer.prototype.writeInt32LE = function writeInt32LE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value & 0xff)
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else {
    objectWriteUInt32(this, value, offset, true)
  }
  return offset + 4
}

Buffer.prototype.writeInt32BE = function writeInt32BE (value, offset, noAssert) {
  value = +value
  offset = offset | 0
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = (value & 0xff)
  } else {
    objectWriteUInt32(this, value, offset, false)
  }
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (value > max || value < min) throw new RangeError('value is out of bounds')
  if (offset + ext > buf.length) throw new RangeError('index out of range')
  if (offset < 0) throw new RangeError('index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  }
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function writeFloatLE (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function writeFloatBE (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  }
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function writeDoubleBE (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function copy (target, targetStart, start, end) {
  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (targetStart >= target.length) targetStart = target.length
  if (!targetStart) targetStart = 0
  if (end > 0 && end < start) end = start

  // Copy 0 bytes; we're done
  if (end === start) return 0
  if (target.length === 0 || this.length === 0) return 0

  // Fatal error conditions
  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds')
  }
  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds')
  if (end < 0) throw new RangeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length) end = this.length
  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start
  }

  var len = end - start
  var i

  if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (i = len - 1; i >= 0; i--) {
      target[i + targetStart] = this[i + start]
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    // ascending copy from start
    for (i = 0; i < len; i++) {
      target[i + targetStart] = this[i + start]
    }
  } else {
    target._set(this.subarray(start, start + len), targetStart)
  }

  return len
}

// fill(value, start=0, end=buffer.length)
Buffer.prototype.fill = function fill (value, start, end) {
  if (!value) value = 0
  if (!start) start = 0
  if (!end) end = this.length

  if (end < start) throw new RangeError('end < start')

  // Fill 0 bytes; we're done
  if (end === start) return
  if (this.length === 0) return

  if (start < 0 || start >= this.length) throw new RangeError('start out of bounds')
  if (end < 0 || end > this.length) throw new RangeError('end out of bounds')

  var i
  if (typeof value === 'number') {
    for (i = start; i < end; i++) {
      this[i] = value
    }
  } else {
    var bytes = utf8ToBytes(value.toString())
    var len = bytes.length
    for (i = start; i < end; i++) {
      this[i] = bytes[i % len]
    }
  }

  return this
}

/**
 * Creates a new `ArrayBuffer` with the *copied* memory of the buffer instance.
 * Added in Node 0.12. Only available in browsers that support ArrayBuffer.
 */
Buffer.prototype.toArrayBuffer = function toArrayBuffer () {
  if (typeof Uint8Array !== 'undefined') {
    if (Buffer.TYPED_ARRAY_SUPPORT) {
      return (new Buffer(this)).buffer
    } else {
      var buf = new Uint8Array(this.length)
      for (var i = 0, len = buf.length; i < len; i += 1) {
        buf[i] = this[i]
      }
      return buf.buffer
    }
  } else {
    throw new TypeError('Buffer.toArrayBuffer not supported in this browser')
  }
}

// HELPER FUNCTIONS
// ================

var BP = Buffer.prototype

/**
 * Augment a Uint8Array *instance* (not the Uint8Array class!) with Buffer methods
 */
Buffer._augment = function _augment (arr) {
  arr.constructor = Buffer
  arr._isBuffer = true

  // save reference to original Uint8Array set method before overwriting
  arr._set = arr.set

  // deprecated
  arr.get = BP.get
  arr.set = BP.set

  arr.write = BP.write
  arr.toString = BP.toString
  arr.toLocaleString = BP.toString
  arr.toJSON = BP.toJSON
  arr.equals = BP.equals
  arr.compare = BP.compare
  arr.indexOf = BP.indexOf
  arr.copy = BP.copy
  arr.slice = BP.slice
  arr.readUIntLE = BP.readUIntLE
  arr.readUIntBE = BP.readUIntBE
  arr.readUInt8 = BP.readUInt8
  arr.readUInt16LE = BP.readUInt16LE
  arr.readUInt16BE = BP.readUInt16BE
  arr.readUInt32LE = BP.readUInt32LE
  arr.readUInt32BE = BP.readUInt32BE
  arr.readIntLE = BP.readIntLE
  arr.readIntBE = BP.readIntBE
  arr.readInt8 = BP.readInt8
  arr.readInt16LE = BP.readInt16LE
  arr.readInt16BE = BP.readInt16BE
  arr.readInt32LE = BP.readInt32LE
  arr.readInt32BE = BP.readInt32BE
  arr.readFloatLE = BP.readFloatLE
  arr.readFloatBE = BP.readFloatBE
  arr.readDoubleLE = BP.readDoubleLE
  arr.readDoubleBE = BP.readDoubleBE
  arr.writeUInt8 = BP.writeUInt8
  arr.writeUIntLE = BP.writeUIntLE
  arr.writeUIntBE = BP.writeUIntBE
  arr.writeUInt16LE = BP.writeUInt16LE
  arr.writeUInt16BE = BP.writeUInt16BE
  arr.writeUInt32LE = BP.writeUInt32LE
  arr.writeUInt32BE = BP.writeUInt32BE
  arr.writeIntLE = BP.writeIntLE
  arr.writeIntBE = BP.writeIntBE
  arr.writeInt8 = BP.writeInt8
  arr.writeInt16LE = BP.writeInt16LE
  arr.writeInt16BE = BP.writeInt16BE
  arr.writeInt32LE = BP.writeInt32LE
  arr.writeInt32BE = BP.writeInt32BE
  arr.writeFloatLE = BP.writeFloatLE
  arr.writeFloatBE = BP.writeFloatBE
  arr.writeDoubleLE = BP.writeDoubleLE
  arr.writeDoubleBE = BP.writeDoubleBE
  arr.fill = BP.fill
  arr.inspect = BP.inspect
  arr.toArrayBuffer = BP.toArrayBuffer

  return arr
}

var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g

function base64clean (str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  // Node converts strings with length < 2 to ''
  if (str.length < 2) return ''
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (string, units) {
  units = units || Infinity
  var codePoint
  var length = string.length
  var leadSurrogate = null
  var bytes = []

  for (var i = 0; i < length; i++) {
    codePoint = string.charCodeAt(i)

    // is surrogate component
    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
          continue
        }

        // valid lead
        leadSurrogate = codePoint

        continue
      }

      // 2 leads in a row
      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
        leadSurrogate = codePoint
        continue
      }

      // valid surrogate pair
      codePoint = leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00 | 0x10000
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD)
    }

    leadSurrogate = null

    // encode utf8
    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break
      bytes.push(codePoint)
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break
      bytes.push(
        codePoint >> 0x6 | 0xC0,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break
      bytes.push(
        codePoint >> 0xC | 0xE0,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break
      bytes.push(
        codePoint >> 0x12 | 0xF0,
        codePoint >> 0xC & 0x3F | 0x80,
        codePoint >> 0x6 & 0x3F | 0x80,
        codePoint & 0x3F | 0x80
      )
    } else {
      throw new Error('Invalid code point')
    }
  }

  return bytes
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str, units) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    if ((units -= 2) < 0) break

    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(base64clean(str))
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; i++) {
    if ((i + offset >= dst.length) || (i >= src.length)) break
    dst[i + offset] = src[i]
  }
  return i
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"base64-js":2,"ieee754":3,"is-array":4}],2:[function(require,module,exports){
var lookup = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

;(function (exports) {
	'use strict';

  var Arr = (typeof Uint8Array !== 'undefined')
    ? Uint8Array
    : Array

	var PLUS   = '+'.charCodeAt(0)
	var SLASH  = '/'.charCodeAt(0)
	var NUMBER = '0'.charCodeAt(0)
	var LOWER  = 'a'.charCodeAt(0)
	var UPPER  = 'A'.charCodeAt(0)
	var PLUS_URL_SAFE = '-'.charCodeAt(0)
	var SLASH_URL_SAFE = '_'.charCodeAt(0)

	function decode (elt) {
		var code = elt.charCodeAt(0)
		if (code === PLUS ||
		    code === PLUS_URL_SAFE)
			return 62 // '+'
		if (code === SLASH ||
		    code === SLASH_URL_SAFE)
			return 63 // '/'
		if (code < NUMBER)
			return -1 //no match
		if (code < NUMBER + 10)
			return code - NUMBER + 26 + 26
		if (code < UPPER + 26)
			return code - UPPER
		if (code < LOWER + 26)
			return code - LOWER + 26
	}

	function b64ToByteArray (b64) {
		var i, j, l, tmp, placeHolders, arr

		if (b64.length % 4 > 0) {
			throw new Error('Invalid string. Length must be a multiple of 4')
		}

		// the number of equal signs (place holders)
		// if there are two placeholders, than the two characters before it
		// represent one byte
		// if there is only one, then the three characters before it represent 2 bytes
		// this is just a cheap hack to not do indexOf twice
		var len = b64.length
		placeHolders = '=' === b64.charAt(len - 2) ? 2 : '=' === b64.charAt(len - 1) ? 1 : 0

		// base64 is 4/3 + up to two characters of the original data
		arr = new Arr(b64.length * 3 / 4 - placeHolders)

		// if there are placeholders, only get up to the last complete 4 chars
		l = placeHolders > 0 ? b64.length - 4 : b64.length

		var L = 0

		function push (v) {
			arr[L++] = v
		}

		for (i = 0, j = 0; i < l; i += 4, j += 3) {
			tmp = (decode(b64.charAt(i)) << 18) | (decode(b64.charAt(i + 1)) << 12) | (decode(b64.charAt(i + 2)) << 6) | decode(b64.charAt(i + 3))
			push((tmp & 0xFF0000) >> 16)
			push((tmp & 0xFF00) >> 8)
			push(tmp & 0xFF)
		}

		if (placeHolders === 2) {
			tmp = (decode(b64.charAt(i)) << 2) | (decode(b64.charAt(i + 1)) >> 4)
			push(tmp & 0xFF)
		} else if (placeHolders === 1) {
			tmp = (decode(b64.charAt(i)) << 10) | (decode(b64.charAt(i + 1)) << 4) | (decode(b64.charAt(i + 2)) >> 2)
			push((tmp >> 8) & 0xFF)
			push(tmp & 0xFF)
		}

		return arr
	}

	function uint8ToBase64 (uint8) {
		var i,
			extraBytes = uint8.length % 3, // if we have 1 byte left, pad 2 bytes
			output = "",
			temp, length

		function encode (num) {
			return lookup.charAt(num)
		}

		function tripletToBase64 (num) {
			return encode(num >> 18 & 0x3F) + encode(num >> 12 & 0x3F) + encode(num >> 6 & 0x3F) + encode(num & 0x3F)
		}

		// go through the array every three bytes, we'll deal with trailing stuff later
		for (i = 0, length = uint8.length - extraBytes; i < length; i += 3) {
			temp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
			output += tripletToBase64(temp)
		}

		// pad the end with zeros, but make sure to not forget the extra bytes
		switch (extraBytes) {
			case 1:
				temp = uint8[uint8.length - 1]
				output += encode(temp >> 2)
				output += encode((temp << 4) & 0x3F)
				output += '=='
				break
			case 2:
				temp = (uint8[uint8.length - 2] << 8) + (uint8[uint8.length - 1])
				output += encode(temp >> 10)
				output += encode((temp >> 4) & 0x3F)
				output += encode((temp << 2) & 0x3F)
				output += '='
				break
		}

		return output
	}

	exports.toByteArray = b64ToByteArray
	exports.fromByteArray = uint8ToBase64
}(typeof exports === 'undefined' ? (this.base64js = {}) : exports))

},{}],3:[function(require,module,exports){
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var nBits = -7
  var i = isLE ? (nBytes - 1) : 0
  var d = isLE ? -1 : 1
  var s = buffer[offset + i]

  i += d

  e = s & ((1 << (-nBits)) - 1)
  s >>= (-nBits)
  nBits += eLen
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & ((1 << (-nBits)) - 1)
  e >>= (-nBits)
  nBits += mLen
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity)
  } else {
    m = m + Math.pow(2, mLen)
    e = e - eBias
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen)
}

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c
  var eLen = nBytes * 8 - mLen - 1
  var eMax = (1 << eLen) - 1
  var eBias = eMax >> 1
  var rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0)
  var i = isLE ? 0 : (nBytes - 1)
  var d = isLE ? 1 : -1
  var s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0

  value = Math.abs(value)

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0
    e = eMax
  } else {
    e = Math.floor(Math.log(value) / Math.LN2)
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--
      c *= 2
    }
    if (e + eBias >= 1) {
      value += rt / c
    } else {
      value += rt * Math.pow(2, 1 - eBias)
    }
    if (value * c >= 2) {
      e++
      c /= 2
    }

    if (e + eBias >= eMax) {
      m = 0
      e = eMax
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen)
      e = e + eBias
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen)
      e = 0
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = (e << mLen) | m
  eLen += mLen
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128
}

},{}],4:[function(require,module,exports){

/**
 * isArray
 */

var isArray = Array.isArray;

/**
 * toString
 */

var str = Object.prototype.toString;

/**
 * Whether or not the given `val`
 * is an array.
 *
 * example:
 *
 *        isArray([]);
 *        // > true
 *        isArray(arguments);
 *        // > false
 *        isArray('');
 *        // > false
 *
 * @param {mixed} val
 * @return {bool}
 */

module.exports = isArray || function (val) {
  return !! val && '[object Array]' == str.call(val);
};

},{}],5:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],6:[function(require,module,exports){
// main.js
var nengi = require('../../../nengi/nengi')
var MyGameClient = require('../../../core/MyGameClient')

var isWindowLoaded = false
var isAssetsLoaded = false
var isConnected = false

/*
* Three things need to be loaded:
*   window: allows access to dom (for pixi webgl) and websockets
*   assets: loading the game images and sounds (just images for now)
*   connection: nengi confirming that the websocket connection has been made
*/
var checkReady = function(myGameClient) {
	return (isWindowLoaded && isAssetsLoaded && isConnected)
}
PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST
PIXI.loader.add('badstar', '/images/badstar-spritesheet.json')
PIXI.loader.load(function(loader, resources) {

	isAssetsLoaded = true
})

var myGameClient = new MyGameClient()
myGameClient.app.on('connect', function() {
	console.log('connected to nengi instance')
	isConnected = true
})


var last = Date.now()
window.onload = function() {
	console.log('window loaded')
	isWindowLoaded = true

	myGameClient.setup()

	var loop = function() {
		var now = Date.now()
		var delta = (now - last) / 1000
		last = now

		if (!checkReady()) {
			// loop until readyCheck passes
			console.log('not ready yet')
			window.requestAnimationFrame(loop)
			return // return here, don't go to the other code until we're ready
		}		

		myGameClient.update(delta)

		if (myGameClient.app.websocket && myGameClient.app.websocket.readyState > 1) {
			throw('disconnected, stopping client to make console readable')
		}	

		window.requestAnimationFrame(loop)	
	}

	loop()
}
},{"../../../core/MyGameClient":8,"../../../nengi/nengi":50}],7:[function(require,module,exports){

/**
* Stores keystrokes for any given tick of the client engine
* During any given tick, a key is counted as either havng been 
* pressed for the entire tick, or not pressed at all -- there is 
* no such thing as pressing a key for a portion of a tick. This
* allows for a deterministic simulation.
* @class InputManager
* @constructor
*/

var EventEmitter = require('../nengi/external/EventEmitter')
function InputManager() {
  EventEmitter.call(this)
  this.buffer = {} 
  // current tick # of the client engine
  this.currentTick = 0 
  // mouse position
  this.mx = 0
  this.my = 0
  this.mouseDown = false
  this.Space = false
  this.camera = null
  this.lastSent = -1
  this.unsentIds = []
  this.cursor = null
  this.display = null

  this.mouseDownTimestamp = Date.now()
}

InputManager.prototype = Object.create(EventEmitter.prototype)
InputManager.prototype.constructor = InputManager

InputManager.prototype.readUnsent = function() {
  var unsent = []
  while (this.unsentIds.length > 0) {
    var id = this.unsentIds.pop()
    unsent.unshift(this.getInput(id))
  }
  return unsent
}

InputManager.prototype.addAttackCommand = function(id) {
  console.log('attack command', id)
  this.buffer[this.currentTick].attackId = id

  ee.emit('attack', id)
}

InputManager.prototype.addMoveCommand = function(x, y) {
  //console.log('move command [raw]', x, y)


  //var relativeX = this.display.convertToRelativeX(x)
  //var relativeY = this.display.convertToRelativeY(y)

  

  //ee.emit('move', { x: relativeX, y: relativeY })

  //ee.emit('mov')
}


//hackz
InputManager.prototype.draw = function(delta, tick) {
  this.update(delta, tick)
}

/**
* Creates a new tick for the InputManager
* All keystrokes are set to false for self tick, unless
* the key is still being held down
* @method update
* @param {Integer} tick A tick number from the client engine
*/
InputManager.prototype.update = function(delta, tick) {
  //console.log('input tick', tick)
  this.currentTick = tick

  var input
  if (!this.buffer[tick]) {
    //console.log('created new object')
    this.buffer[tick] = {}
  } else {
    //console.log('recycling existing object') 
  }

  input = this.buffer[tick]
  // TODO: set any key states to their default state
 // input.confirmedByServer = false
  //input.predictedX = null
  //input.predictedY = null

  input.tick = tick



  // predicted yet? change to true after applying input locally
  input.isPredicted = false
  //input.sent = false
  input.move = null
  input.attackId = null

  input.releaseStrength = 0

  // on any given frame W A S D may be registered at most one time
  input.W = false
  input.A = false
  input.S = false
  input.D = false
  
  input.One = false
  input.Two = false
  input.Three = false
  input.Four = false
  input.Five = false

  input.Space = false

  input.mouseDown = false

  if (this.mouseDown) { 
    input.mouseDown = true 
    //input.mx = this.mx
    //input.my = this.my
  }

  input.mx = this.mx
  input.my = this.my

    if (this.mouseDown) {
      //this.addMoveCommand(this.mx, this.my)
      input.mouseClickDown = { x: this.mx, y: this.my }
    }


  //input.attack = false
  //input.attackX = null
  //input.attackY = null
  //input.One = false

  // W A S D are still held down from a previous frame
  if (this.W) { input.W = true }
  if (this.A) { input.A = true }
  if (this.S) { input.S = true }
  if (this.D) { input.D = true }

  if (this.One) { input.One = true }
  if (this.Two) { input.Two = true }
  if (this.Three) { input.Three = true }
  if (this.Four) { input.Four = true }
  if (this.Five) { input.Five = true }

  if (this.Space) { input.Space = true }



  this.unsentIds.push(tick)
  //console.log(input)

  // Old inputs are never deleted! They are overwritten via the tick cycle 0-99
  //this.deleteInput(tick-30)
}

/**
* Returns the inputs for any given tick
* @method getInput
* @param {Integer} tick
*/
InputManager.prototype.getInput = function(tick) {
  return this.buffer[tick]
}

/**
* Deletes the inputs for any given tick, for example after they've been confirmed
* by the server
* @method deleteInput
* @param {Integer} tick
*/
InputManager.prototype.deleteInput = function(tick) {
  delete this.buffer[tick]
}

/**
* Begins listening for W A S D keystrokes
* @method beginCapture
* @param {DOselfElement} canvas The canvas or DOselfElement which will listen for keystrokes
*/
InputManager.prototype.beginCapture = function(canvas) {
  var self = this

  document.addEventListener('keydown', function(event) {
    //if($('#input').is(':focus') || $('#name').is(':focus')) return
    // keep track of which keys are up/down
    // and register any movement immediately even if the 
    // player lets go of the key
   //console.log('keydown:' + event.keyCode)


    // W A S D
    if (event.keyCode === 87 || event.keyCode === 38) { 
      self.W = true
      self.buffer[self.currentTick].W = true
    } 
    if (event.keyCode === 65 || event.keyCode === 37) { 
      self.A = true
      self.buffer[self.currentTick].A = true
    }
    if (event.keyCode === 83 || event.keyCode === 40) { 
      self.S = true 
      self.buffer[self.currentTick].S = true
    }
    if (event.keyCode === 68 || event.keyCode === 39) { 
      self.D = true
      self.buffer[self.currentTick].D = true
    }

    // 1 through 5
    if (event.keyCode === 49) {
      self.One = true
      self.buffer[self.currentTick].One = true
    }
    if (event.keyCode === 50) {
      self.Two = true
      self.buffer[self.currentTick].Two = true
    }
    if (event.keyCode === 51) {
      self.Three = true
      self.buffer[self.currentTick].Three = true
    }
    if (event.keyCode === 52) {
      self.Four = true
      self.buffer[self.currentTick].Four = true
    }
    if (event.keyCode === 53) {
      self.Five = true
      self.buffer[self.currentTick].Five = true
    }

    // spacebar
    if (event.keyCode === 32) {
      self.Space = true
      self.buffer[self.currentTick].Space = true
    }

    //if (event.keyCode === 49) { 
      //self.D = true
    //  self.buffer[self.currentTick].One = true
    //} 
  })

  document.addEventListener('keyup', function(event) {
    // don't capture game input if user is chatting / changing their name
    //if($('#input').is(':focus') || $('#name').is(':focus')) return

    // W or up arrow
    if (event.keyCode === 87 || event.keyCode === 38) { self.W = false }
    // A or left arrow
    if (event.keyCode === 65 || event.keyCode === 37) { self.A = false }
    // S or down arrow
    if (event.keyCode === 83 || event.keyCode === 40) { self.S = false }
    // D or right arrow
    if (event.keyCode === 68 || event.keyCode === 39) { self.D = false }

    // 1 through 5
    if (event.keyCode === 49) { self.One = false }
    if (event.keyCode === 50) { self.Two = false }
    if (event.keyCode === 51) { self.Three = false }
    if (event.keyCode === 52) { self.Four = false }
    if (event.keyCode === 53) { self.Five = false }

    // spacebar
    if (event.keyCode === 32) { self.Space = false }
  })

  document.addEventListener('mousemove', function(e) {
    if (e.offsetX !== undefined && e.offsetY !== undefined) { 
      self.mx = e.offsetX
      self.my = e.offsetY
    } else {
      self.mx = e.layerX
      self.my = e.layerY
    }

    if (self.cursor) {
      self.cursor.x = self.mx
      self.cursor.y = self.my
    }



    //console.log(self.mx, self.my)
  })

  // disable right click context menu
  /*
  canvas.oncontextmenu = function(e) {
    e.preventDefault()
  }
  */


  document.addEventListener('mousedown', function(e) {
  
    var rightclick
    if (!e) var e = window.event
    if (e.which) rightclick = (e.which == 3)
    else if (e.button) rightclick = (e.button == 2)

    self.buffer[self.currentTick].mouseDown = true
    self.mouseDown = true
    self.buffer[self.currentTick].mx = self.mx
    self.buffer[self.currentTick].my = self.my

    console.log('mouse clicked', 
      self.buffer[self.currentTick].mx,
      self.buffer[self.currentTick].my
    )

    if (rightclick) {
      console.log('right click')
     // event.preventDefault()
      //
    } else {
      //if (self.cursor.state === 1) {
      self.buffer[self.currentTick].mouseClickDown = { x: self.mx, y: self.my }
        //..self.addMoveCommand(self.mx, self.my)
      self.mouseDownTimestamp = Date.now()

      self.emit('mouseDown', self.mx, self.my)
      //}
    }
  })

  document.addEventListener('mouseup', function(event) {
    self.mouseDown = false
    self.emit('mouseUp', self.mx, self.my)

    //self.buffer[self.currentTick].releaseStrength = Date.now() - self.mouseDownTimestamp
  })

  // EXPANDABLE: could listen for other keys or mouse actions
}

module.exports = InputManager

},{"../nengi/external/EventEmitter":44}],8:[function(require,module,exports){
var nengi = require('../nengi/nengi')

var Move = require('./model/input/Move')
var ChargeWeapon = require('./model/input/ChargeWeapon')
var DischargeWeapon = require('./model/input/DischargeWeapon')
var SumoShipView = require('./view/SumoShipView')
var SumoShip = require('./model/SumoShip')
var ChevronView = require('./view/ChevronView')
var AsteroidView = require('./view/AsteroidView')
var SumoRingBoundary = require('./model/SumoRingBoundary')
var SumoRingBoundaryView = require('./view/SumoRingBoundaryView')
var InputManager = require('./InputManager')
var common = require('./common')


function MyGameClient() {
	// TODO: make these things clientside components
	this.app = new nengi.Application(common)
	this.inputManager = new InputManager()

	this.queuedAttacks = []
	// even the pixi stuff could be a component
	this.stage = null
	this.background = null
	this.foreground = null
	this.renderer = null

	this.graphics = []
	this.graphicsRef = {}

	this.meGraphic = null
}


var predictionContext = {
	id: -1,
	last: -1
}


MyGameClient.prototype.update = function(delta) {
	this.app.update(delta)



	//console.log('ping', this.app.registrar.ping)

	// clientside objects running update (usually to run their own rendering stuff)
	for (var i = 0; i < this.graphics.length; i++) {

		var obj = this.graphics[i]
		if (typeof obj.update !== 'undefined') {
			obj.update(delta)
		}
	}

	// send all of the inputs that were buffered during the last frame
	var unsent = this.inputManager.readUnsent()
	// any movement (all buffered by inputManager)
	for (var i = 0; i < unsent.length; i++) {
		var rawInput = unsent[i]

		var move = new Move()
		move.W = rawInput.W
		move.A = rawInput.A
		move.S = rawInput.S
		move.D = rawInput.D
		move.mx = rawInput.mx
		move.my = rawInput.my
		//console.log(move)
		this.app.send(move)

		if (predictionContext.id > -1) {
			predictionContext.last = this.app.registrar.lastConfirmedTick
			//console.log(predictionContext.last)
			predictionContext.model.move(move)

			//console.log(predictionContext.model.physics.velocity, predictionContext.model.position)

			//console.log('PHYS?', predictionContext.model.physics)
			//console.log(predictionContext.model)
			predictionContext.model.physics.update(delta)
			//console.log(predictionContext.model.position)
			predictionContext.cl.x = predictionContext.model.x
			predictionContext.cl.y = predictionContext.model.y

			//predictionContext.cl.x++


		}
	}



	this.renderer.render(this.stage)
	this.inputManager.update()


}

MyGameClient.prototype.setup = function() {
	//app.entityInterpolator.isEnabled = false
	var self = this

	// setup the pixi stuff
	this.stage = new PIXI.Container()
	this.stage.scale.x = this.stage.scale.y = 1
	this.background = new PIXI.Container()
	this.foreground = new PIXI.Container()
	this.stage.addChild(this.background)
	this.stage.addChild(this.foreground)

	this.renderer = PIXI.autoDetectRenderer(
		window.innerWidth,
		window.innerHeight,
		{ antialiasing: false, transparent: false, resolution: 1 }
	)
	document.body.appendChild(this.renderer.view)

	setTimeout(function() {
		// setup the sumo ring and background (no backgound yet)
		var sumoRing = new SumoRingBoundary()
		var sumoRingView = new SumoRingBoundaryView(
			sumoRing.x, 
			sumoRing.y, 
			sumoRing.radius
		)
		self.background.addChild(sumoRingView)
	}, 1000)


	// connects nengi
	this.app.connect()

	// input bindings...
	this.inputManager.on('mouseUp', function(x, y) {
		var discharge = new DischargeWeapon()
		discharge.x = x
		discharge.y = y
		self.app.send(discharge)
	})

	this.inputManager.on('mouseDown', function(x, y) {
		var charge = new ChargeWeapon()
		charge.x = x
		charge.y = y
		self.app.send(charge)
	})

	this.inputManager.beginCapture()

	/*
	this.app.on('connect', function() {		
	})
	*/

	// receive nengi messages
	this.app.on('createMessage', function(message) {
		// Doesn't really matter yet, but will be used for prediction later


		console.log('message', message)
		if (message.n_type === 'IdentityMessage') {
			//return

				
				//self.myId = message.identity
				//predictionContext.id = message.identity

				//var auth = self.graphicsRef[message.identity]
				//predictionContext.sv = auth

				self.app.prediction.createPredictionContext(message.identity, true)

				setTimeout(function() {
					self.graphicsRef[message.identity].isMine = true
				}, 1000)
				

				/*
				var model = new SumoShip()
				predictionContext.model = model
				model.x = auth.x
				model.y = auth.y

				console.log('ID', message.identity, auth)

				predictionContext.cl = new SumoShipView(predictionContext.sv)
				self.foreground.addChild(predictionContext.cl)
				*/

			//var graphics = n	
		}
	})

	this.app.on('prediction', function(entity) {
		//console.log('prediction', entity)

		if (!self.meGraphic) {
			self.meGraphic = new SumoShipView(entity)
			self.foreground.addChild(self.meGraphic)
		} else {
			self.meGraphic.x = entity.x
			self.meGraphic.y = entity.y
			self.meGraphic.rotation = entity.rotation + (Math.PI / 180) * 180
			self.meGraphic.isPrediction = true
			self.meGraphic.hull.tint = 0xFFFF88
			self.meGraphic.update(60/1000)
		}
	})


	// receive nengi entities
	this.app.on('createEntity', function(entity) {
		// TODO: clean this up
		//console.log('createEntity', entity)
		var graphics = null
		if (entity.constructor.name === 'SumoShip') {
			graphics = new SumoShipView(entity)			
		} else if (entity.constructor.name === 'Projectile') {
			graphics = new ChevronView(entity)
		} else if (entity.constructor.name === 'Asteroid') {
			graphics = new AsteroidView(entity)

		}
		graphics.originalEntity = entity

		

		self.foreground.addChild(graphics)
		self.graphics.push(graphics)
		self.graphicsRef[entity.id] = graphics
		console.log('createEntity', entity.id)		
	})

	// receive nengi entity updates
	this.app.on('updateEntity', function(id, changes) {
		//console.log('update received for', id, changes)
		var graphics = self.graphicsRef[id]
		//TODO: make a helper method that copies changes into a ViewEntity
		for (var i = 0; i < changes.length; i++) {
			var change = changes[i]
			graphics[change.property] = change.value
		}
	})

	// receive nengi entity deletes
	this.app.on('deleteEntity', function(id) {
		var graphics = self.graphicsRef[id]
		self.foreground.removeChild(graphics)

	    var index = -1
	    for (var i = 0; i < self.graphics.length; i++) {
	        if (self.graphics[i].id === id) {
	            index = i
	            break
	        }
	    }
	    if (index !== -1) {
	        self.graphics.splice(index, 1)
	    }
	    delete self.graphicsRef[id]
	})

}

module.exports = MyGameClient
},{"../nengi/nengi":50,"./InputManager":7,"./common":9,"./model/SumoRingBoundary":17,"./model/SumoShip":18,"./model/input/ChargeWeapon":19,"./model/input/DischargeWeapon":20,"./model/input/Move":21,"./view/AsteroidView":23,"./view/ChevronView":24,"./view/SumoRingBoundaryView":25,"./view/SumoShipView":26}],9:[function(require,module,exports){

var nengi = require('../nengi/nengi')
var config = require('./config')

module.exports = {
	config: config,

	entityConstructors: new nengi.Constructors([
		require('./model/SumoShip'), 
		require('./model/Asteroid'),
		require('./model/Projectile')
	]),

	eventConstructors: new nengi.Constructors([
		//require('./model/MouseOverEvent') // not used
	]),

	messageConstructors: new nengi.Constructors([
		nengi.IdentityMessage // leave this line
	]),

	inputConstructors: new nengi.Constructors([
		nengi.InputFrame, // leave this line
		require('./model/input/Move'),
		require('./model/input/ChargeWeapon'),
		require('./model/input/DischargeWeapon'),
		require('./model/input/UseThruster'),
	])

}


},{"../nengi/nengi":50,"./config":10,"./model/Asteroid":12,"./model/Projectile":16,"./model/SumoShip":18,"./model/input/ChargeWeapon":19,"./model/input/DischargeWeapon":20,"./model/input/Move":21,"./model/input/UseThruster":22}],10:[function(require,module,exports){
var BinaryType = require('../nengi/binary/BinaryType')
var Binary = require('../nengi/binary/Binary')

module.exports = {
	// the tick rate of the game simulation
	UPDATE_RATE: 20,

	// the tick rate of rendering and input processing on client
	DRAW_RATE: 60,

	// server ip
	IP: '127.0.0.1',

	// server port
	PORT: '8080',
	
	// protocol name for websocket connection handshake
	PROTOCOL: 'nengi-protocol',

	// the number of bits used to represent 'ids' in this app
	ID_BINARYTYPE: Binary[BinaryType.UInt8],

	// the number of bits used to represent 'types' in this app
	TYPE_BINARYTYPE: Binary[BinaryType.UInt8],

	// time between ping/pong messages, in milliseconds
	// this won't occur faster than 1000/UPDATE_RATE (aka every tick)
	PING_INTERVAL: 500,

	OPTIMIZE_XY: true, // not used
	version: 123456789.0 // not used
}

},{"../nengi/binary/Binary":33,"../nengi/binary/BinaryType":35}],11:[function(require,module,exports){
// Version 0.5.0 - Copyright 2012 - 2015 -  Jim Riecken <jimr@jimr.ca>
//
// Released under the MIT License - https://github.com/jriecken/sat-js
//
// A simple library for determining intersections of circles and
// polygons using the Separating Axis Theorem.
/** @preserve SAT.js - Version 0.5.0 - Copyright 2012 - 2015 - Jim Riecken <jimr@jimr.ca> - released under the MIT License. https://github.com/jriecken/sat-js */

/*global define: false, module: false*/
/*jshint shadow:true, sub:true, forin:true, noarg:true, noempty:true, 
  eqeqeq:true, bitwise:true, strict:true, undef:true, 
  curly:true, browser:true */

// Create a UMD wrapper for SAT. Works in:
//
//  - Plain browser via global SAT variable
//  - AMD loader (like require.js)
//  - Node.js
//
// The quoted properties all over the place are used so that the Closure Compiler
// does not mangle the exposed API in advanced mode.
/**
 * @param {*} root - The global scope
 * @param {Function} factory - Factory that creates SAT module
 */
(function (root, factory) {
  "use strict";
  if (typeof define === 'function' && define['amd']) {
    define(factory);
  } else if (typeof exports === 'object') {
    module['exports'] = factory();
  } else {
    root['SAT'] = factory();
  }
}(this, function () {
  "use strict";

  var SAT = {};

  //
  // ## Vector
  //
  // Represents a vector in two dimensions with `x` and `y` properties.


  // Create a new Vector, optionally passing in the `x` and `y` coordinates. If
  // a coordinate is not specified, it will be set to `0`
  /** 
   * @param {?number=} x The x position.
   * @param {?number=} y The y position.
   * @constructor
   */
  function Vector(x, y) {
    this['x'] = x || 0;
    this['y'] = y || 0;
  }
  SAT['Vector'] = Vector;
  // Alias `Vector` as `V`
  SAT['V'] = Vector;


  // Copy the values of another Vector into this one.
  /**
   * @param {Vector} other The other Vector.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['copy'] = Vector.prototype.copy = function(other) {
    this['x'] = other['x'];
    this['y'] = other['y'];
    return this;
  };

  // Create a new vector with the same coordinates as this on.
  /**
   * @return {Vector} The new cloned vector
   */
  Vector.prototype['clone'] = Vector.prototype.clone = function() {
    return new Vector(this['x'], this['y']);
  };

  // Change this vector to be perpendicular to what it was before. (Effectively
  // roatates it 90 degrees in a clockwise direction)
  /**
   * @return {Vector} This for chaining.
   */
  Vector.prototype['perp'] = Vector.prototype.perp = function() {
    var x = this['x'];
    this['x'] = this['y'];
    this['y'] = -x;
    return this;
  };

  // Rotate this vector (counter-clockwise) by the specified angle (in radians).
  /**
   * @param {number} angle The angle to rotate (in radians)
   * @return {Vector} This for chaining.
   */
  Vector.prototype['rotate'] = Vector.prototype.rotate = function (angle) {
    var x = this['x'];
    var y = this['y'];
    this['x'] = x * Math.cos(angle) - y * Math.sin(angle);
    this['y'] = x * Math.sin(angle) + y * Math.cos(angle);
    return this;
  };

  // Reverse this vector.
  /**
   * @return {Vector} This for chaining.
   */
  Vector.prototype['reverse'] = Vector.prototype.reverse = function() {
    this['x'] = -this['x'];
    this['y'] = -this['y'];
    return this;
  };
  

  // Normalize this vector.  (make it have length of `1`)
  /**
   * @return {Vector} This for chaining.
   */
  Vector.prototype['normalize'] = Vector.prototype.normalize = function() {
    var d = this.len();
    if(d > 0) {
      this['x'] = this['x'] / d;
      this['y'] = this['y'] / d;
    }
    return this;
  };
  
  // Add another vector to this one.
  /**
   * @param {Vector} other The other Vector.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['add'] = Vector.prototype.add = function(other) {
    this['x'] += other['x'];
    this['y'] += other['y'];
    return this;
  };
  
  // Subtract another vector from this one.
  /**
   * @param {Vector} other The other Vector.
   * @return {Vector} This for chaiing.
   */
  Vector.prototype['sub'] = Vector.prototype.sub = function(other) {
    this['x'] -= other['x'];
    this['y'] -= other['y'];
    return this;
  };
  
  // Scale this vector. An independant scaling factor can be provided
  // for each axis, or a single scaling factor that will scale both `x` and `y`.
  /**
   * @param {number} x The scaling factor in the x direction.
   * @param {?number=} y The scaling factor in the y direction.  If this
   *   is not specified, the x scaling factor will be used.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['scale'] = Vector.prototype.scale = function(x,y) {
    this['x'] *= x;
    this['y'] *= y || x;
    return this; 
  };
  
  // Project this vector on to another vector.
  /**
   * @param {Vector} other The vector to project onto.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['project'] = Vector.prototype.project = function(other) {
    var amt = this.dot(other) / other.len2();
    this['x'] = amt * other['x'];
    this['y'] = amt * other['y'];
    return this;
  };
  
  // Project this vector onto a vector of unit length. This is slightly more efficient
  // than `project` when dealing with unit vectors.
  /**
   * @param {Vector} other The unit vector to project onto.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['projectN'] = Vector.prototype.projectN = function(other) {
    var amt = this.dot(other);
    this['x'] = amt * other['x'];
    this['y'] = amt * other['y'];
    return this;
  };
  
  // Reflect this vector on an arbitrary axis.
  /**
   * @param {Vector} axis The vector representing the axis.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['reflect'] = Vector.prototype.reflect = function(axis) {
    var x = this['x'];
    var y = this['y'];
    this.project(axis).scale(2);
    this['x'] -= x;
    this['y'] -= y;
    return this;
  };
  
  // Reflect this vector on an arbitrary axis (represented by a unit vector). This is
  // slightly more efficient than `reflect` when dealing with an axis that is a unit vector.
  /**
   * @param {Vector} axis The unit vector representing the axis.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['reflectN'] = Vector.prototype.reflectN = function(axis) {
    var x = this['x'];
    var y = this['y'];
    this.projectN(axis).scale(2);
    this['x'] -= x;
    this['y'] -= y;
    return this;
  };
  
  // Get the dot product of this vector and another.
  /**
   * @param {Vector}  other The vector to dot this one against.
   * @return {number} The dot product.
   */
  Vector.prototype['dot'] = Vector.prototype.dot = function(other) {
    return this['x'] * other['x'] + this['y'] * other['y'];
  };
  
  // Get the squared length of this vector.
  /**
   * @return {number} The length^2 of this vector.
   */
  Vector.prototype['len2'] = Vector.prototype.len2 = function() {
    return this.dot(this);
  };
  
  // Get the length of this vector.
  /**
   * @return {number} The length of this vector.
   */
  Vector.prototype['len'] = Vector.prototype.len = function() {
    return Math.sqrt(this.len2());
  };
  
  // ## Circle
  //
  // Represents a circle with a position and a radius.

  // Create a new circle, optionally passing in a position and/or radius. If no position
  // is given, the circle will be at `(0,0)`. If no radius is provided, the circle will
  // have a radius of `0`.
  /**
   * @param {Vector=} pos A vector representing the position of the center of the circle
   * @param {?number=} r The radius of the circle
   * @constructor
   */
  function Circle(pos, r) {
    this['pos'] = pos || new Vector();
    this['r'] = r || 0;
  }
  SAT['Circle'] = Circle;
  
  // Compute the axis-aligned bounding box (AABB) of this Circle.
  //
  // Note: Returns a _new_ `Polygon` each time you call this.
  /**
   * @return {Polygon} The AABB
   */
  Circle.prototype['getAABB'] = Circle.prototype.getAABB = function() {
    var r = this['r'];
    var corner = this["pos"].clone().sub(new Vector(r, r));
    return new Box(corner, r*2, r*2).toPolygon();
  };

  // ## Polygon
  //
  // Represents a *convex* polygon with any number of points (specified in counter-clockwise order)
  //
  // Note: Do _not_ manually change the `points`, `angle`, or `offset` properties. Use the
  // provided setters. Otherwise the calculated properties will not be updated correctly.
  //
  // `pos` can be changed directly.

  // Create a new polygon, passing in a position vector, and an array of points (represented
  // by vectors relative to the position vector). If no position is passed in, the position
  // of the polygon will be `(0,0)`.
  /**
   * @param {Vector=} pos A vector representing the origin of the polygon. (all other
   *   points are relative to this one)
   * @param {Array.<Vector>=} points An array of vectors representing the points in the polygon,
   *   in counter-clockwise order.
   * @constructor
   */
  function Polygon(pos, points) {
    this['pos'] = pos || new Vector();
    this['angle'] = 0;
    this['offset'] = new Vector();
    this.setPoints(points || []);
  }
  SAT['Polygon'] = Polygon;
  
  // Set the points of the polygon.
  //
  // Note: The points are counter-clockwise *with respect to the coordinate system*.
  // If you directly draw the points on a screen that has the origin at the top-left corner
  // it will _appear_ visually that the points are being specified clockwise. This is just
  // because of the inversion of the Y-axis when being displayed.
  /**
   * @param {Array.<Vector>=} points An array of vectors representing the points in the polygon,
   *   in counter-clockwise order.
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['setPoints'] = Polygon.prototype.setPoints = function(points) {
    // Only re-allocate if this is a new polygon or the number of points has changed.
    var lengthChanged = !this['points'] || this['points'].length !== points.length;
    if (lengthChanged) {
      var i;
      var calcPoints = this['calcPoints'] = [];
      var edges = this['edges'] = [];
      var normals = this['normals'] = [];
      // Allocate the vector arrays for the calculated properties
      for (i = 0; i < points.length; i++) {
        calcPoints.push(new Vector());
        edges.push(new Vector());
        normals.push(new Vector());
      }
    }
    this['points'] = points;
    this._recalc();
    return this;
  };

  // Set the current rotation angle of the polygon.
  /**
   * @param {number} angle The current rotation angle (in radians).
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['setAngle'] = Polygon.prototype.setAngle = function(angle) {
    this['angle'] = angle;
    this._recalc();
    return this;
  };

  // Set the current offset to apply to the `points` before applying the `angle` rotation.
  /**
   * @param {Vector} offset The new offset vector.
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['setOffset'] = Polygon.prototype.setOffset = function(offset) {
    this['offset'] = offset;
    this._recalc();
    return this;
  };

  // Rotates this polygon counter-clockwise around the origin of *its local coordinate system* (i.e. `pos`).
  //
  // Note: This changes the **original** points (so any `angle` will be applied on top of this rotation).
  /**
   * @param {number} angle The angle to rotate (in radians)
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['rotate'] = Polygon.prototype.rotate = function(angle) {
    var points = this['points'];
    var len = points.length;
    for (var i = 0; i < len; i++) {
      points[i].rotate(angle);
    }
    this._recalc();
    return this;
  };

  // Translates the points of this polygon by a specified amount relative to the origin of *its own coordinate
  // system* (i.e. `pos`).
  //
  // This is most useful to change the "center point" of a polygon. If you just want to move the whole polygon, change
  // the coordinates of `pos`.
  //
  // Note: This changes the **original** points (so any `offset` will be applied on top of this translation)
  /**
   * @param {number} x The horizontal amount to translate.
   * @param {number} y The vertical amount to translate.
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['translate'] = Polygon.prototype.translate = function (x, y) {
    var points = this['points'];
    var len = points.length;
    for (var i = 0; i < len; i++) {
      points[i].x += x;
      points[i].y += y;
    }
    this._recalc();
    return this;
  };


  // Computes the calculated collision polygon. Applies the `angle` and `offset` to the original points then recalculates the
  // edges and normals of the collision polygon.
  /**
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype._recalc = function() {
    // Calculated points - this is what is used for underlying collisions and takes into account
    // the angle/offset set on the polygon.
    var calcPoints = this['calcPoints'];
    // The edges here are the direction of the `n`th edge of the polygon, relative to
    // the `n`th point. If you want to draw a given edge from the edge value, you must
    // first translate to the position of the starting point.
    var edges = this['edges'];
    // The normals here are the direction of the normal for the `n`th edge of the polygon, relative
    // to the position of the `n`th point. If you want to draw an edge normal, you must first
    // translate to the position of the starting point.
    var normals = this['normals'];
    // Copy the original points array and apply the offset/angle
    var points = this['points'];
    var offset = this['offset'];
    var angle = this['angle'];
    var len = points.length;
    var i;
    for (i = 0; i < len; i++) {
      var calcPoint = calcPoints[i].copy(points[i]);
      calcPoint.x += offset.x;
      calcPoint.y += offset.y;
      if (angle !== 0) {
        calcPoint.rotate(angle);
      }
    }
    // Calculate the edges/normals
    for (i = 0; i < len; i++) {
      var p1 = calcPoints[i];
      var p2 = i < len - 1 ? calcPoints[i + 1] : calcPoints[0];
      var e = edges[i].copy(p2).sub(p1);
      normals[i].copy(e).perp().normalize();
    }
    return this;
  };
  
  
  // Compute the axis-aligned bounding box. Any current state
  // (translations/rotations) will be applied before constructing the AABB.
  //
  // Note: Returns a _new_ `Polygon` each time you call this.
  /**
   * @return {Polygon} The AABB
   */
  Polygon.prototype["getAABB"] = Polygon.prototype.getAABB = function() {
    var points = this["calcPoints"];
    var len = points.length;
    var xMin = points[0]["x"];
    var yMin = points[0]["y"];
    var xMax = points[0]["x"];
    var yMax = points[0]["y"];
    for (var i = 1; i < len; i++) {
      var point = points[i];
      if (point["x"] < xMin) {
        xMin = point["x"];
      }
      else if (point["x"] > xMax) {
        xMax = point["x"];
      }
      if (point["y"] < yMin) {
        yMin = point["y"];
      }
      else if (point["y"] > yMax) {
        yMax = point["y"];
      }
    }
    return new Box(this["pos"].clone().add(new Vector(xMin, yMin)), xMax - xMin, yMax - yMin).toPolygon();
  };
  

  // ## Box
  //
  // Represents an axis-aligned box, with a width and height.


  // Create a new box, with the specified position, width, and height. If no position
  // is given, the position will be `(0,0)`. If no width or height are given, they will
  // be set to `0`.
  /**
   * @param {Vector=} pos A vector representing the bottom-left of the box (i.e. the smallest x and smallest y value).
   * @param {?number=} w The width of the box.
   * @param {?number=} h The height of the box.
   * @constructor
   */
  function Box(pos, w, h) {
    this['pos'] = pos || new Vector();
    this['w'] = w || 0;
    this['h'] = h || 0;
  }
  SAT['Box'] = Box;

  // Returns a polygon whose edges are the same as this box.
  /**
   * @return {Polygon} A new Polygon that represents this box.
   */
  Box.prototype['toPolygon'] = Box.prototype.toPolygon = function() {
    var pos = this['pos'];
    var w = this['w'];
    var h = this['h'];
    return new Polygon(new Vector(pos['x'], pos['y']), [
     new Vector(), new Vector(w, 0), 
     new Vector(w,h), new Vector(0,h)
    ]);
  };
  
  // ## Response
  //
  // An object representing the result of an intersection. Contains:
  //  - The two objects participating in the intersection
  //  - The vector representing the minimum change necessary to extract the first object
  //    from the second one (as well as a unit vector in that direction and the magnitude
  //    of the overlap)
  //  - Whether the first object is entirely inside the second, and vice versa.
  /**
   * @constructor
   */  
  function Response() {
    this['a'] = null;
    this['b'] = null;
    this['overlapN'] = new Vector();
    this['overlapV'] = new Vector();
    this.clear();
  }
  SAT['Response'] = Response;

  // Set some values of the response back to their defaults.  Call this between tests if
  // you are going to reuse a single Response object for multiple intersection tests (recommented
  // as it will avoid allcating extra memory)
  /**
   * @return {Response} This for chaining
   */
  Response.prototype['clear'] = Response.prototype.clear = function() {
    this['aInB'] = true;
    this['bInA'] = true;
    this['overlap'] = Number.MAX_VALUE;
    return this;
  };

  // ## Object Pools

  // A pool of `Vector` objects that are used in calculations to avoid
  // allocating memory.
  /**
   * @type {Array.<Vector>}
   */
  var T_VECTORS = [];
  for (var i = 0; i < 10; i++) { T_VECTORS.push(new Vector()); }
  
  // A pool of arrays of numbers used in calculations to avoid allocating
  // memory.
  /**
   * @type {Array.<Array.<number>>}
   */
  var T_ARRAYS = [];
  for (var i = 0; i < 5; i++) { T_ARRAYS.push([]); }

  // Temporary response used for polygon hit detection.
  /**
   * @type {Response}
   */
  var T_RESPONSE = new Response();

  // Unit square polygon used for polygon hit detection.
  /**
   * @type {Polygon}
   */
  var UNIT_SQUARE = new Box(new Vector(), 1, 1).toPolygon();

  // ## Helper Functions

  // Flattens the specified array of points onto a unit vector axis,
  // resulting in a one dimensional range of the minimum and
  // maximum value on that axis.
  /**
   * @param {Array.<Vector>} points The points to flatten.
   * @param {Vector} normal The unit vector axis to flatten on.
   * @param {Array.<number>} result An array.  After calling this function,
   *   result[0] will be the minimum value,
   *   result[1] will be the maximum value.
   */
  function flattenPointsOn(points, normal, result) {
    var min = Number.MAX_VALUE;
    var max = -Number.MAX_VALUE;
    var len = points.length;
    for (var i = 0; i < len; i++ ) {
      // The magnitude of the projection of the point onto the normal
      var dot = points[i].dot(normal);
      if (dot < min) { min = dot; }
      if (dot > max) { max = dot; }
    }
    result[0] = min; result[1] = max;
  }
  
  // Check whether two convex polygons are separated by the specified
  // axis (must be a unit vector).
  /**
   * @param {Vector} aPos The position of the first polygon.
   * @param {Vector} bPos The position of the second polygon.
   * @param {Array.<Vector>} aPoints The points in the first polygon.
   * @param {Array.<Vector>} bPoints The points in the second polygon.
   * @param {Vector} axis The axis (unit sized) to test against.  The points of both polygons
   *   will be projected onto this axis.
   * @param {Response=} response A Response object (optional) which will be populated
   *   if the axis is not a separating axis.
   * @return {boolean} true if it is a separating axis, false otherwise.  If false,
   *   and a response is passed in, information about how much overlap and
   *   the direction of the overlap will be populated.
   */
  function isSeparatingAxis(aPos, bPos, aPoints, bPoints, axis, response) {
    var rangeA = T_ARRAYS.pop();
    var rangeB = T_ARRAYS.pop();
    // The magnitude of the offset between the two polygons
    var offsetV = T_VECTORS.pop().copy(bPos).sub(aPos);
    var projectedOffset = offsetV.dot(axis);
    // Project the polygons onto the axis.
    flattenPointsOn(aPoints, axis, rangeA);
    flattenPointsOn(bPoints, axis, rangeB);
    // Move B's range to its position relative to A.
    rangeB[0] += projectedOffset;
    rangeB[1] += projectedOffset;
    // Check if there is a gap. If there is, this is a separating axis and we can stop
    if (rangeA[0] > rangeB[1] || rangeB[0] > rangeA[1]) {
      T_VECTORS.push(offsetV); 
      T_ARRAYS.push(rangeA); 
      T_ARRAYS.push(rangeB);
      return true;
    }
    // This is not a separating axis. If we're calculating a response, calculate the overlap.
    if (response) {
      var overlap = 0;
      // A starts further left than B
      if (rangeA[0] < rangeB[0]) {
        response['aInB'] = false;
        // A ends before B does. We have to pull A out of B
        if (rangeA[1] < rangeB[1]) { 
          overlap = rangeA[1] - rangeB[0];
          response['bInA'] = false;
        // B is fully inside A.  Pick the shortest way out.
        } else {
          var option1 = rangeA[1] - rangeB[0];
          var option2 = rangeB[1] - rangeA[0];
          overlap = option1 < option2 ? option1 : -option2;
        }
      // B starts further left than A
      } else {
        response['bInA'] = false;
        // B ends before A ends. We have to push A out of B
        if (rangeA[1] > rangeB[1]) { 
          overlap = rangeA[0] - rangeB[1];
          response['aInB'] = false;
        // A is fully inside B.  Pick the shortest way out.
        } else {
          var option1 = rangeA[1] - rangeB[0];
          var option2 = rangeB[1] - rangeA[0];
          overlap = option1 < option2 ? option1 : -option2;
        }
      }
      // If this is the smallest amount of overlap we've seen so far, set it as the minimum overlap.
      var absOverlap = Math.abs(overlap);
      if (absOverlap < response['overlap']) {
        response['overlap'] = absOverlap;
        response['overlapN'].copy(axis);
        if (overlap < 0) {
          response['overlapN'].reverse();
        }
      }      
    }
    T_VECTORS.push(offsetV); 
    T_ARRAYS.push(rangeA); 
    T_ARRAYS.push(rangeB);
    return false;
  }
  
  // Calculates which Voronoi region a point is on a line segment.
  // It is assumed that both the line and the point are relative to `(0,0)`
  //
  //            |       (0)      |
  //     (-1)  [S]--------------[E]  (1)
  //            |       (0)      |
  /**
   * @param {Vector} line The line segment.
   * @param {Vector} point The point.
   * @return  {number} LEFT_VORONOI_REGION (-1) if it is the left region,
   *          MIDDLE_VORONOI_REGION (0) if it is the middle region,
   *          RIGHT_VORONOI_REGION (1) if it is the right region.
   */
  function voronoiRegion(line, point) {
    var len2 = line.len2();
    var dp = point.dot(line);
    // If the point is beyond the start of the line, it is in the
    // left voronoi region.
    if (dp < 0) { return LEFT_VORONOI_REGION; }
    // If the point is beyond the end of the line, it is in the
    // right voronoi region.
    else if (dp > len2) { return RIGHT_VORONOI_REGION; }
    // Otherwise, it's in the middle one.
    else { return MIDDLE_VORONOI_REGION; }
  }
  // Constants for Voronoi regions
  /**
   * @const
   */
  var LEFT_VORONOI_REGION = -1;
  /**
   * @const
   */
  var MIDDLE_VORONOI_REGION = 0;
  /**
   * @const
   */
  var RIGHT_VORONOI_REGION = 1;
  
  // ## Collision Tests

  // Check if a point is inside a circle.
  /**
   * @param {Vector} p The point to test.
   * @param {Circle} c The circle to test.
   * @return {boolean} true if the point is inside the circle, false if it is not.
   */
  function pointInCircle(p, c) {
    var differenceV = T_VECTORS.pop().copy(p).sub(c['pos']);
    var radiusSq = c['r'] * c['r'];
    var distanceSq = differenceV.len2();
    T_VECTORS.push(differenceV);
    // If the distance between is smaller than the radius then the point is inside the circle.
    return distanceSq <= radiusSq;
  }
  SAT['pointInCircle'] = pointInCircle;

  // Check if a point is inside a convex polygon.
  /**
   * @param {Vector} p The point to test.
   * @param {Polygon} poly The polygon to test.
   * @return {boolean} true if the point is inside the polygon, false if it is not.
   */
  function pointInPolygon(p, poly) {
    UNIT_SQUARE['pos'].copy(p);
    T_RESPONSE.clear();
    var result = testPolygonPolygon(UNIT_SQUARE, poly, T_RESPONSE);
    if (result) {
      result = T_RESPONSE['aInB'];
    }
    return result;
  }
  SAT['pointInPolygon'] = pointInPolygon;

  // Check if two circles collide.
  /**
   * @param {Circle} a The first circle.
   * @param {Circle} b The second circle.
   * @param {Response=} response Response object (optional) that will be populated if
   *   the circles intersect.
   * @return {boolean} true if the circles intersect, false if they don't. 
   */
  function testCircleCircle(a, b, response) {
    // Check if the distance between the centers of the two
    // circles is greater than their combined radius.
    var differenceV = T_VECTORS.pop().copy(b['pos']).sub(a['pos']);
    var totalRadius = a['r'] + b['r'];
    var totalRadiusSq = totalRadius * totalRadius;
    var distanceSq = differenceV.len2();
    // If the distance is bigger than the combined radius, they don't intersect.
    if (distanceSq > totalRadiusSq) {
      T_VECTORS.push(differenceV);
      return false;
    }
    // They intersect.  If we're calculating a response, calculate the overlap.
    if (response) { 
      var dist = Math.sqrt(distanceSq);
      response['a'] = a;
      response['b'] = b;
      response['overlap'] = totalRadius - dist;
      response['overlapN'].copy(differenceV.normalize());
      response['overlapV'].copy(differenceV).scale(response['overlap']);
      response['aInB']= a['r'] <= b['r'] && dist <= b['r'] - a['r'];
      response['bInA'] = b['r'] <= a['r'] && dist <= a['r'] - b['r'];
    }
    T_VECTORS.push(differenceV);
    return true;
  }
  SAT['testCircleCircle'] = testCircleCircle;
  
  // Check if a polygon and a circle collide.
  /**
   * @param {Polygon} polygon The polygon.
   * @param {Circle} circle The circle.
   * @param {Response=} response Response object (optional) that will be populated if
   *   they interset.
   * @return {boolean} true if they intersect, false if they don't.
   */
  function testPolygonCircle(polygon, circle, response) {
    // Get the position of the circle relative to the polygon.
    var circlePos = T_VECTORS.pop().copy(circle['pos']).sub(polygon['pos']);
    var radius = circle['r'];
    var radius2 = radius * radius;
    var points = polygon['calcPoints'];
    var len = points.length;
    var edge = T_VECTORS.pop();
    var point = T_VECTORS.pop();
    
    // For each edge in the polygon:
    for (var i = 0; i < len; i++) {
      var next = i === len - 1 ? 0 : i + 1;
      var prev = i === 0 ? len - 1 : i - 1;
      var overlap = 0;
      var overlapN = null;
      
      // Get the edge.
      edge.copy(polygon['edges'][i]);
      // Calculate the center of the circle relative to the starting point of the edge.
      point.copy(circlePos).sub(points[i]);
      
      // If the distance between the center of the circle and the point
      // is bigger than the radius, the polygon is definitely not fully in
      // the circle.
      if (response && point.len2() > radius2) {
        response['aInB'] = false;
      }
      
      // Calculate which Voronoi region the center of the circle is in.
      var region = voronoiRegion(edge, point);
      // If it's the left region:
      if (region === LEFT_VORONOI_REGION) {
        // We need to make sure we're in the RIGHT_VORONOI_REGION of the previous edge.
        edge.copy(polygon['edges'][prev]);
        // Calculate the center of the circle relative the starting point of the previous edge
        var point2 = T_VECTORS.pop().copy(circlePos).sub(points[prev]);
        region = voronoiRegion(edge, point2);
        if (region === RIGHT_VORONOI_REGION) {
          // It's in the region we want.  Check if the circle intersects the point.
          var dist = point.len();
          if (dist > radius) {
            // No intersection
            T_VECTORS.push(circlePos); 
            T_VECTORS.push(edge);
            T_VECTORS.push(point); 
            T_VECTORS.push(point2);
            return false;
          } else if (response) {
            // It intersects, calculate the overlap.
            response['bInA'] = false;
            overlapN = point.normalize();
            overlap = radius - dist;
          }
        }
        T_VECTORS.push(point2);
      // If it's the right region:
      } else if (region === RIGHT_VORONOI_REGION) {
        // We need to make sure we're in the left region on the next edge
        edge.copy(polygon['edges'][next]);
        // Calculate the center of the circle relative to the starting point of the next edge.
        point.copy(circlePos).sub(points[next]);
        region = voronoiRegion(edge, point);
        if (region === LEFT_VORONOI_REGION) {
          // It's in the region we want.  Check if the circle intersects the point.
          var dist = point.len();
          if (dist > radius) {
            // No intersection
            T_VECTORS.push(circlePos); 
            T_VECTORS.push(edge); 
            T_VECTORS.push(point);
            return false;              
          } else if (response) {
            // It intersects, calculate the overlap.
            response['bInA'] = false;
            overlapN = point.normalize();
            overlap = radius - dist;
          }
        }
      // Otherwise, it's the middle region:
      } else {
        // Need to check if the circle is intersecting the edge,
        // Change the edge into its "edge normal".
        var normal = edge.perp().normalize();
        // Find the perpendicular distance between the center of the 
        // circle and the edge.
        var dist = point.dot(normal);
        var distAbs = Math.abs(dist);
        // If the circle is on the outside of the edge, there is no intersection.
        if (dist > 0 && distAbs > radius) {
          // No intersection
          T_VECTORS.push(circlePos); 
          T_VECTORS.push(normal); 
          T_VECTORS.push(point);
          return false;
        } else if (response) {
          // It intersects, calculate the overlap.
          overlapN = normal;
          overlap = radius - dist;
          // If the center of the circle is on the outside of the edge, or part of the
          // circle is on the outside, the circle is not fully inside the polygon.
          if (dist >= 0 || overlap < 2 * radius) {
            response['bInA'] = false;
          }
        }
      }
      
      // If this is the smallest overlap we've seen, keep it. 
      // (overlapN may be null if the circle was in the wrong Voronoi region).
      if (overlapN && response && Math.abs(overlap) < Math.abs(response['overlap'])) {
        response['overlap'] = overlap;
        response['overlapN'].copy(overlapN);
      }
    }
    
    // Calculate the final overlap vector - based on the smallest overlap.
    if (response) {
      response['a'] = polygon;
      response['b'] = circle;
      response['overlapV'].copy(response['overlapN']).scale(response['overlap']);
    }
    T_VECTORS.push(circlePos); 
    T_VECTORS.push(edge); 
    T_VECTORS.push(point);
    return true;
  }
  SAT['testPolygonCircle'] = testPolygonCircle;
  
  // Check if a circle and a polygon collide.
  //
  // **NOTE:** This is slightly less efficient than polygonCircle as it just
  // runs polygonCircle and reverses everything at the end.
  /**
   * @param {Circle} circle The circle.
   * @param {Polygon} polygon The polygon.
   * @param {Response=} response Response object (optional) that will be populated if
   *   they interset.
   * @return {boolean} true if they intersect, false if they don't.
   */
  function testCirclePolygon(circle, polygon, response) {
    // Test the polygon against the circle.
    var result = testPolygonCircle(polygon, circle, response);
    if (result && response) {
      // Swap A and B in the response.
      var a = response['a'];
      var aInB = response['aInB'];
      response['overlapN'].reverse();
      response['overlapV'].reverse();
      response['a'] = response['b'];
      response['b'] = a;
      response['aInB'] = response['bInA'];
      response['bInA'] = aInB;
    }
    return result;
  }
  SAT['testCirclePolygon'] = testCirclePolygon;
  
  // Checks whether polygons collide.
  /**
   * @param {Polygon} a The first polygon.
   * @param {Polygon} b The second polygon.
   * @param {Response=} response Response object (optional) that will be populated if
   *   they interset.
   * @return {boolean} true if they intersect, false if they don't.
   */
  function testPolygonPolygon(a, b, response) {
    var aPoints = a['calcPoints'];
    var aLen = aPoints.length;
    var bPoints = b['calcPoints'];
    var bLen = bPoints.length;
    // If any of the edge normals of A is a separating axis, no intersection.
    for (var i = 0; i < aLen; i++) {
      if (isSeparatingAxis(a['pos'], b['pos'], aPoints, bPoints, a['normals'][i], response)) {
        return false;
      }
    }
    // If any of the edge normals of B is a separating axis, no intersection.
    for (var i = 0;i < bLen; i++) {
      if (isSeparatingAxis(a['pos'], b['pos'], aPoints, bPoints, b['normals'][i], response)) {
        return false;
      }
    }
    // Since none of the edge normals of A or B are a separating axis, there is an intersection
    // and we've already calculated the smallest overlap (in isSeparatingAxis).  Calculate the
    // final overlap vector.
    if (response) {
      response['a'] = a;
      response['b'] = b;
      response['overlapV'].copy(response['overlapN']).scale(response['overlap']);
    }
    return true;
  }
  SAT['testPolygonPolygon'] = testPolygonPolygon;

  return SAT;
}));
},{}],12:[function(require,module,exports){
var nengi = require('../../nengi/nengi')
var CircleColliderComponent = require('./CircleColliderComponent')
var PhysicsComponent = require('./PhysicsComponent')

function Asteroid() {
	nengi.Entity.call(this)
	this.radius = nengi.MathEx.random(10, 60)
	this.addComponent('physics', new PhysicsComponent(this))
	this.addComponent('collider', new CircleColliderComponent(this))
	this.collider.tags = ['asteroid']
	this.collider.collideWithTags = ['player', 'projectile', 'asteroid']
	this.rotation = 0
	this.physics.frictionCoef = 0
	this.physics.mass = this.radius * 100
}

Asteroid.prototype = Object.create(nengi.Entity.prototype)
Asteroid.prototype.constructor = Asteroid


Asteroid.prototype.update = function(delta, tick, now) {
	//console.log('asteroid updating!', this.rotation)
	//this.rotation += this.rotationVelocity * delta
	//console.log('projectile movement calculation', this.vx, this.speed, delta)
	//this.x += this.heading.x * this.speed * delta
	//this.y += this.heading.y * this.speed * delta

	//this.physics.addExternalForce({x: 5000, y: 5000 })
	this.collider.radius = this.radius

	// crude bounds check	
	if (this.x > 5000 || this.x < -5000 || this.y > 5000 || this.y < -5000) {
		this.instance.removeEntity(this)
	}
	

}

Asteroid.prototype.launch = function(origin, forceVector) {
	//console.log('launch', origin, destination, velocityScalar)
	this.position = origin
	this.physics.addInternalForce(forceVector)
}



// specify which properties should be networked, and their value types
Asteroid.prototype.netSchema = nengi.createEntitySchema({
	'id': nengi.UInt16,
	'type': nengi.UInt8,
	'x': nengi.Int16,
	'y': nengi.Int16,
	'radius': nengi.UInt8
	//'isAlive': { binaryType: nengi.Boolean, interp: false },
	//'rotation': nengi.Rotation
}, {
	'x': { delta: true, binaryType: nengi.Int8 },
	'y': { delta: true, binaryType: nengi.Int8 }
})

module.exports = Asteroid


},{"../../nengi/nengi":50,"./CircleColliderComponent":14,"./PhysicsComponent":15}],13:[function(require,module,exports){
var nengi = require('../../nengi/nengi')
var SAT = require('../external/SAT')
var Vector2 = nengi.Vector2

function ChevronColliderComponent(gameObject, config) {
	nengi.Component.call(this, gameObject, config)
	//this.radius = 25
	this.tags = []
	this.collideWithTags = []
	this.colliderType = 'polygon'

	// default is tier 1
	var tier = 1
	this.shape = new SAT.Polygon(new SAT.Vector(0,0), [
		new SAT.Vector(0, -10 * tier),
		new SAT.Vector(14 * tier, 9 * tier),
		new SAT.Vector(-14 * tier, 9 * tier)
	])
}

ChevronColliderComponent.prototype = Object.create(nengi.Component.prototype)
ChevronColliderComponent.prototype.constructor = ChevronColliderComponent


ChevronColliderComponent.prototype.canCollideWith = function(collider) {
	for (var i = 0; i < collider.tags.length; i++) {
		for (var j = 0; j < this.collideWithTags.length; j++) {
			if (collider.tags[i] === this.collideWithTags[j]) {
				return true
			}
		}
	}
	return false
}

ChevronColliderComponent.prototype.adjustTier = function(tier) {
	this.shape = new SAT.Polygon(new SAT.Vector(0,0), [
		new SAT.Vector(0, -10 * tier),
		new SAT.Vector(14 * tier, 9 * tier),
		new SAT.Vector(-14 * tier, 9 * tier)
	])
}

ChevronColliderComponent.prototype.checkCollision = function(collider) {
	var response = new SAT.Response()
	var collided = false

	if (collider.colliderType === 'circle') {
		collided = SAT.testCirclePolygon(collider.shape, this.shape, response)
	} else if (collider.colliderType === 'polygon') {
		//console.log('ChevronColliderComponent checkCollison polygon')
		collided = SAT.testPolygonPolyon(collider.shape, this.shape, response)
	}

	if (collided) {
		//console.log('collision?', collided)
		//console.log('response', response)

		//uncolliding test
		//this.x += response.overlapV.x
		//this.y += response.overlapV.y
	}
	return collided

}


ChevronColliderComponent.prototype.update = function() {
	//console.log('this.x', this.x, this.gameObject.x)
	// keep the SAT collider in sync with the entity
	//this.radius = this.gameObject.radius
	this.shape.pos.x = this.gameObject.x
	this.shape.pos.y = this.gameObject.y
	this.shape.setAngle(this.gameObject.rotation)
	//console.log('poly', this.gameObject.x)

	//TODO: rotation

	//this.circle.r = this.radius
	//console.log('radiu updated', this.radius)
}
module.exports = ChevronColliderComponent
},{"../../nengi/nengi":50,"../external/SAT":11}],14:[function(require,module,exports){
var nengi = require('../../nengi/nengi')
var SAT = require('../external/SAT')
var Vector2 = nengi.Vector2

function CircleColliderComponent(gameObject, config) {
	nengi.Component.call(this, gameObject, config)
	this.radius = 25
	this.tags = []
	this.collideWithTags = []

	this.colliderType = 'circle'
	this.shape = new SAT.Circle(new SAT.Vector(0,0), this.radius)
}

CircleColliderComponent.prototype = Object.create(nengi.Component.prototype)
CircleColliderComponent.prototype.constructor = CircleColliderComponent


CircleColliderComponent.prototype.canCollideWith = function(collider) {
	for (var i = 0; i < collider.tags.length; i++) {
		for (var j = 0; j < this.collideWithTags.length; j++) {
			if (collider.tags[i] === this.collideWithTags[j]) {
				return true
			}
		}
	}
	return false
}

CircleColliderComponent.prototype.checkCollision = function(collider) {
	var response = new SAT.Response()
	var collided = false

	if (collider.colliderType === 'circle') {
		collided = SAT.testCircleCircle(collider.shape, this.shape, response)
	} else if (collider.colliderType === 'polygon') {
		collided = SAT.testPolygonCircle(collider.shape, this.shape, response)
	} else if (collider.colliderType === 'point') {
		collided = SAT.pointInCircle(collider, this.shape)
	}

	if (collided) {
		//console.log('collision?', collided)
		//console.log('response', response)

		//uncolliding test
		//this.x += response.overlapV.x
		//this.y += response.overlapV.y
	}
	return collided

}


CircleColliderComponent.prototype.update = function() {
	// keep the SAT collider in sync with the entity
	this.shape.pos.x = this.x
	this.shape.pos.y = this.y
	this.shape.r = this.radius

}
module.exports = CircleColliderComponent
},{"../../nengi/nengi":50,"../external/SAT":11}],15:[function(require,module,exports){
var nengi = require('../../nengi/nengi')

var Vector2 = nengi.Vector2

function PhysicsComponent(gameObject, config) {
	nengi.Component.call(this, gameObject, config)

	// force applied from external sources
	this.externalForce = new Vector2()

	// force applied from own entity (e.g. engine or movement abilities)
	this.internalForce = new Vector2()

	// velocity of this entity
	this.velocity = new Vector2()

	this.mass = 1000

	this.frictionCoef = 3
}

PhysicsComponent.prototype = Object.create(nengi.Component.prototype)
PhysicsComponent.prototype.constructor = PhysicsComponent


PhysicsComponent.prototype.initialize = function() {

}

PhysicsComponent.prototype.addInternalForce = function(force) {
	this.internalForce.add(force)
}

PhysicsComponent.prototype.addExternalForce = function(force) {
	//console.log('addExternalForce', force)
	this.externalForce.add(force)
}

PhysicsComponent.prototype.update = function(delta, tick, now) {

	var externalAcceleration = new Vector2(
		this.externalForce.x / this.mass,
		this.externalForce.y / this.mass
	)

	var internalAcceleration = new Vector2(
		this.internalForce.x / this.mass,
		this.internalForce.y / this.mass
	)

 	// artificial friction to make the controls nice
	this.velocity.x += (externalAcceleration.x - this.frictionCoef * this.velocity.x) * delta
	this.velocity.y += (externalAcceleration.y - this.frictionCoef * this.velocity.y) * delta

	this.velocity.x += (internalAcceleration.x - this.frictionCoef * this.velocity.x) * delta
	this.velocity.y += (internalAcceleration.y - this.frictionCoef * this.velocity.y) * delta

	this.position.add(this.velocity)

    // set forces to 0 
	this.externalForce.x = 0
	this.externalForce.y = 0
	this.internalForce.x = 0
	this.internalForce.y = 0
}

module.exports = PhysicsComponent
},{"../../nengi/nengi":50}],16:[function(require,module,exports){
var nengi = require('../../nengi/nengi')
var ChevronColliderComponent = require('./ChevronColliderComponent')


function Projectile() {
	nengi.Entity.call(this)
	
	//this.hp = 100

	this.target = null
	//this.addComponent('ai', new TestAI(this))
	//this.radius = 5//nengi.MathEx.random(20, 80)
	this.rotation = 0
	//this.rotationVelocity = nengi.MathEx.random(-10, 10)
	this.heading = new nengi.Vector2(0, 0)

	this.addComponent('collider', new ChevronColliderComponent(this))
	this.collider.tags = ['projectile']
	this.collider.collideWithTags = ['player', 'asteroid']
	
	this.speed = 0

	this.owner = null
	this.tier = 1
}

Projectile.prototype = Object.create(nengi.Entity.prototype)
Projectile.prototype.constructor = Projectile


Projectile.prototype.update = function(delta, tick, now) {
	//console.log('asteroid updating!', this.rotation)
	//this.rotation += this.rotationVelocity * delta
	//console.log('projectile movement calculation', this.vx, this.speed, delta)
	this.x += this.heading.x * this.speed * delta
	this.y += this.heading.y * this.speed * delta

	// crude bounds check
	if (this.x > 1000 || this.x < 0 || this.y > 1000 || this.y < 0) {
		this.instance.removeEntity(this)
	}

}

Projectile.prototype.launch = function(origin, destination, velocityScalar) {
	//console.log('launch', origin, destination, velocityScalar)
	this.position = origin
	this.destination = destination

	var dx = destination.x - this.x
	var dy = destination.y - this.y

	this.heading = new nengi.Vector2(dx, dy)
	this.heading.normalize()

	this.speed = velocityScalar

	// rotate the projectile to face the direction it is headed
	this.rotation = Math.atan2(-dy, -dx) + 90 * Math.PI/180
	this.collider.adjustTier(this.tier)
}

Projectile.prototype.getForce = function() {
	var force = new nengi.Vector2(
		this.heading.x * 150000 * (this.tier+1),
		this.heading.y * 150000 * (this.tier+1)
	)
	return force
}

// specify which properties should be networked, and their value types
Projectile.prototype.netSchema = nengi.createEntitySchema({
	'id': nengi.UInt16,
	'type': nengi.UInt8,
	'x': nengi.Int16,
	'y': nengi.Int16,
	'rotation': nengi.Rotation,
	'tier': nengi.UInt8
}, {
	'x': { delta: true, binaryType: nengi.Int8 },
	'y': { delta: true, binaryType: nengi.Int8 }
})

module.exports = Projectile


},{"../../nengi/nengi":50,"./ChevronColliderComponent":13}],17:[function(require,module,exports){
var CircleColliderComponent = require('./CircleColliderComponent')

function SumoRingBoundary() {
	this.collider = new CircleColliderComponent(this)
	this.x = 500
	this.y = 500
	this.radius = 350

	this.collider.shape.pos.x = this.x
	this.collider.shape.pos.y = this.y
	this.collider.shape.r = this.radius
}

SumoRingBoundary.prototype.isPointInside = function(point) {
	return this.collider.checkCollision({ x: point.x, y: point.y, colliderType: 'point'})
}

module.exports = SumoRingBoundary
},{"./CircleColliderComponent":14}],18:[function(require,module,exports){
var nengi = require('../../nengi/nengi')
var PhysicsComponent = require('./PhysicsComponent')
var CircleColliderComponent = require('./CircleColliderComponent')
var Projectile = require('./Projectile')

function SumoShip() {
	nengi.Entity.call(this)

	this.rotation = 0
	this.addComponent('physics', new PhysicsComponent(this))
	this.addComponent('collider', new CircleColliderComponent(this))
	this.collider.tags = ['player']
	this.collider.collideWithTags = ['asteroid', 'projectile', 'player']


	this.isAlive = true

	this.isWeaponCharging = false
	this.weaponChargeLevel = 1
	this.weaponChargeTimestamp = Date.now()
}

SumoShip.prototype = Object.create(nengi.Entity.prototype)
SumoShip.prototype.constructor = SumoShip

SumoShip.prototype.update = function(delta) {
	//console.log('SumoShip update', delta)
	if (this.isWeaponCharging) {
		var elapsed = Date.now() - this.weaponChargeTimestamp
		var tier = Math.floor(elapsed/1000) + 1
		if (tier > 3) { tier = 3 }

		this.weaponChargeLevel = tier
	} else {
		this.weaponChargeLevel = 1
	}
}

SumoShip.prototype.applyInput = function(input) {
	if (input.constructor.name === 'Move') {
		//console.log('move being applied')
		this.move(input)
	}
}

SumoShip.prototype.move = function(move) {
	if (!this.isAlive) return
	//console.log('move', this.physics.addExternalForce)
	if (move.W) {
		this.physics.addInternalForce({ x: 0, y: -15000 })
		//this.y -= 1 * this.speed
	}

	if (move.A) {
		this.physics.addInternalForce({ x: -15000, y: 0 })
		//this.x -= 1 * this.speed
	}

	if (move.S) {
		this.physics.addInternalForce({ x: 0, y: 15000 })
		//this.y += 1 * this.speed
	}

	if (move.D) {
		this.physics.addInternalForce({ x: 15000, y: 0 })
		//this.x += 1 * this.speed
	}

	var dx = this.x - move.mx
	var dy = this.y - move.my
	this.rotation = Math.atan2(dy, dx) + 90 * Math.PI/180
}

SumoShip.prototype.chargeWeapon = function(charge) {
	if (!this.isAlive) return
	// fires a shot instantly, and begins charging a larger shot
	var dx = this.x - charge.x
	var dy = this.y - charge.y
	this.rotation = Math.atan2(dy, dx) + 90 * Math.PI/180

	this.isWeaponCharging = true
	this.weaponChargeTimestamp = Date.now()

	var projectile = new Projectile()
	projectile.owner = this
	projectile.tier = 1
	projectile.launch(
		new nengi.Vector2(this.x, this.y),
		new nengi.Vector2(charge.x, charge.y),
		600
	)

	var kickback = new nengi.Vector2(dx, dy)
	kickback.normalize()

	this.physics.addExternalForce({ 
		x: kickback.x * 250000, 
		y: kickback.y * 250000 
	})

	this.instance.addEntity(projectile)
}

SumoShip.prototype.dischargeWeapon = function(charge) {
	if (!this.isAlive) return
	// how long have we been charging?
	var elapsed = Date.now() - this.weaponChargeTimestamp
	var tier = Math.floor(elapsed/1000) + 1
	if (tier > 3) { tier = 3 }

	// do not discharge unless charge reached tier 2 (at least 1 second of charging)
	if (tier < 2) {
		this.isWeaponCharging = false
		return
	}

	// a larger shot was released
	this.isWeaponCharging = false
	//this.weaponChargeLevel = 1
	var dx = this.x - charge.x
	var dy = this.y - charge.y
	this.rotation = Math.atan2(dy, dx) + 90 * Math.PI/180

	var projectile = new Projectile()
	projectile.owner = this

	projectile.tier = tier
	projectile.launch(
		new nengi.Vector2(this.x, this.y),
		new nengi.Vector2(charge.x, charge.y),
		600
	)

	var kickback = new nengi.Vector2(dx, dy)
	kickback.normalize()

	this.physics.addExternalForce({ 
		x: kickback.x * 250000 * tier, 
		y: kickback.y * 250000 * tier
	})

	//console.log('discharged', tier, elapsed)
	this.instance.addEntity(projectile)
}

// specify which properties should be networked, and their value types
SumoShip.prototype.netSchema = nengi.createEntitySchema({
	'id': nengi.UInt16,
	'type': nengi.UInt8,
	'x': nengi.Int16,
	'y': nengi.Int16,
	'isAlive': { binaryType: nengi.Boolean, interp: false },
	'rotation': nengi.Rotation,
	'weaponChargeLevel': { binaryType: nengi.UInt8, interp: false }
}, {
	'x': { delta: true, binaryType: nengi.Int8 },
	'y': { delta: true, binaryType: nengi.Int8 }
})

module.exports = SumoShip


},{"../../nengi/nengi":50,"./CircleColliderComponent":14,"./PhysicsComponent":15,"./Projectile":16}],19:[function(require,module,exports){
var nengi = require('../../../nengi/nengi')

function ChargeWeapon() {
	this.id = -1
	this.x = 0
	this.y = 0
}

ChargeWeapon.prototype.constructor = ChargeWeapon

ChargeWeapon.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8,
	'x': nengi.UInt16,
	'y': nengi.UInt16
})

module.exports = ChargeWeapon


},{"../../../nengi/nengi":50}],20:[function(require,module,exports){
var nengi = require('../../../nengi/nengi')

function DischargeWeapon() {
	this.id = -1
	this.x = 0
	this.y = 0
}

DischargeWeapon.prototype.constructor = DischargeWeapon

DischargeWeapon.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8,
	'x': nengi.UInt16,
	'y': nengi.UInt16
})

module.exports = DischargeWeapon


},{"../../../nengi/nengi":50}],21:[function(require,module,exports){
var nengi = require('../../../nengi/nengi')

function Move() {
	this.id = -1
	this.W = false
	this.A = false
	this.S = false
	this.D = false
	this.mx = 0
	this.my = 0
}

Move.prototype.constructor = Move

Move.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8,
	'W': nengi.Boolean,
	'A': nengi.Boolean,
	'S': nengi.Boolean,
	'D': nengi.Boolean,
	'mx': nengi.UInt16,
	'my': nengi.UInt16
})

module.exports = Move


},{"../../../nengi/nengi":50}],22:[function(require,module,exports){
var nengi = require('../../../nengi/nengi')

function UseThruster() {
	this.id = -1
}

UseThruster.prototype.constructor = UseThruster

UseThruster.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8
})

module.exports = UseThruster


},{"../../../nengi/nengi":50}],23:[function(require,module,exports){


function AsteroidView(entity) {
	PIXI.Container.call(this)
	this.id = entity.id
	this.x = entity.x
	this.y = entity.y
	this.radius = entity.radius
	// perhaps 'hull' isn't the most accurate term for the asteroid
	this.hull = new PIXI.Sprite.fromFrame('asteroid-v1.png')
	this.hull.anchor.x = this.hull.anchor.y = 0.5
	this.hull.scale.x = this.hull.scale.y = entity.radius/40

	this.addChild(this.hull)

	this.isFadingIn = true
	this.rotationVelocity = random(-40, 40)/10

	// DEBUG draw the collider
	var circle = new PIXI.Graphics()
	circle.lineStyle(1,0xff00ff,1)
	circle.drawCircle(0, 0, this.radius)
	circle.alpha = 0.3
	this.addChild(circle)
}

AsteroidView.prototype = Object.create(PIXI.Container.prototype)
AsteroidView.prototype.constructor = AsteroidView

var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}


AsteroidView.prototype.update = function(delta) {
	this.rotation += this.rotationVelocity * delta
}


module.exports = AsteroidView


},{}],24:[function(require,module,exports){


function ChevronView(entity) {
	PIXI.Container.call(this)
	this.id = entity.id
	this.x = entity.x
	this.y = entity.y
	this.rotation = entity.rotation
	this.scale.x = this.scale.y = entity.tier

	this.hull = new PIXI.Sprite.fromFrame('chevron.png')
	this.hull.anchor.x = this.hull.anchor.y = 0.5
	this.addChild(this.hull)
	this.alpha = random(20,50)/100	

	this.isFadingIn = true

	// DEBUG Draw the shape of the collider
	var triangle = new PIXI.Graphics()
	triangle.beginFill(0x00FF00)
	triangle.lineStyle(1,0xff00ff,1)
	triangle.moveTo(0, -10)
	triangle.lineTo(14, 9)
	triangle.lineTo(-14, 9)
	triangle.endFill()
	//this.addChild(triangle)
}

ChevronView.prototype = Object.create(PIXI.Container.prototype)
ChevronView.prototype.constructor = ChevronView

ChevronView.prototype.setup = function() {

}

Object.defineProperty(ChevronView.prototype, 'tier', {
	get: function() { return this.tier },
	set: function(value) { 
		this.tier = value 
		this.scale.x = this.scale.y = value
	}
})


var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
ChevronView.prototype.update = function() {

	if (this.isFadingIn) {
		this.alpha += 0.05
	} else {
		this.alpha -= 0.05
	}	

	if (this.alpha >= 0.5) {
		this.isFadingIn = false
	}

	if (this.alpha <= 0.2) {
		this.isFadingIn = true
	}
}


module.exports = ChevronView


},{}],25:[function(require,module,exports){
function SumoRingBoundaryView(x, y, radius) {
	PIXI.Graphics.call(this)
	this.x = x
	this.y = y
	this.lineStyle(3,0xff00ff,1)
	//this.beginFill(0xff00ff)
	this.drawCircle(0, 0, radius)
	var sprite = new PIXI.Sprite.fromFrame('stars1000.png')

	// TODO fix, this background doesnt really line up
	var tiling = new PIXI.TilingSprite(sprite.texture, 1000, 1000)
	tiling.x = -500
	tiling.y = -500
	this.addChild(tiling)
}

SumoRingBoundaryView.prototype = Object.create(PIXI.Graphics.prototype)
SumoRingBoundaryView.prototype.constructor = SumoRingBoundaryView

module.exports = SumoRingBoundaryView
},{}],26:[function(require,module,exports){
var ExplosionEffect = require('./effect/ExplosionEffect')

function SumoShipView(entity) {
	PIXI.Container.call(this)
	this.id = entity.id
	this.x = entity.x
	this.y = entity.y
	this.hull = new PIXI.Sprite.fromFrame('green-ship-lit.png')
	this.hull.anchor.x = this.hull.anchor.y = 0.5
	this.hull.y = -5 //offset the image a little bit, makes the rotation look better
	this.addChild(this.hull)
	this.explosionEffect = new ExplosionEffect()
	this.addChild(this.explosionEffect)

	this.isFadingIn = false
	this.weaponChargeLevel = 1

	this.isPrediction = false
	this.isMine = false

	
	this.isDeathAnimationPlaying = false
	//this.addChild(this.explosionEffect)

	// DEBUG! Draw a collision circle
	var circle = new PIXI.Graphics()
	circle.lineStyle(1,0xff00ff,1)
	circle.beginFill(0xff00ff)
	circle.drawCircle(0, 0, 25)
	circle.alpha = 0.1
	//this.addChild(circle)
}

SumoShipView.prototype = Object.create(PIXI.Container.prototype)
SumoShipView.prototype.constructor = SumoShipView


Object.defineProperty(SumoShipView.prototype, 'isAlive', {
	get: function() { return this._isAlive },
	set: function(value) { 
		
		this._isAlive = value
		if (!this._isAlive && !this.isDeathAnimationPlaying) {
			this.playDeathAnimation()
		}
		
	}
})


SumoShipView.prototype.playChargeLevel2Animation = function() {

}

SumoShipView.prototype.playDeathAnimation = function() {
	console.log('DEATH ANIMATION')
	//this.hull.tint = 0xFF0000
	this.isDeathAnimationPlaying = true
	this.explosionEffect.alpha = 1
	this.explosionEffect.scale.x = this.explosionEffect.scale.y = 0.33
	this.explosionEffect.begin()
}

SumoShipView.prototype.update = function(delta) {



	this.explosionEffect.update(60/1000)
	
	// no flickering
	if (this.weaponChargeLevel === 1) {
		this.hull.tint = 0xFFFFFF
		if (this.alpha < 1) {
			this.alpha += 0.05
		} else {
			this.alpha = 1.0
		}	
	}

	// slow flickering
	if (this.weaponChargeLevel === 2) {
		this.hull.tint = 0xFFFF88		
		if (this.isFadingIn) {
			this.alpha += 0.05
		} else {
			this.alpha -= 0.05
		}	

		if (this.alpha >= 1) {
			this.isFadingIn = false
		}

		if (this.alpha <= 0.5) {
			this.isFadingIn = true
		}
		
	}

	// fast flickering
	if (this.weaponChargeLevel === 3) {
		this.hull.tint = 0xFFFF44		
		if (this.isFadingIn) {
			this.alpha += 0.1
		} else {
			this.alpha -= 0.1
		}	

		if (this.alpha >= 1) {
			this.isFadingIn = false
		}

		if (this.alpha <= 0.5) {
			this.isFadingIn = true
		}
		
	}

	if (!this.isPrediction && this.isMine) {
		this.alpha = 0.25
	}
}


module.exports = SumoShipView


},{"./effect/ExplosionEffect":27}],27:[function(require,module,exports){
var nengi = require('../../../nengi/nengi')
var MathEx = nengi.MathEx

function ExplosionEffect() {
	PIXI.Container.call(this)
	this.yellowLayers = []
	this.redLayers = []
	this.smokeLayers = []

	this.hasExploded = false


	for (var i = 0; i < 3; i++) {
		var particle = new Particle(0, 0)
		this.addChild(particle)
		this.smokeLayers.push(particle)
	}
	for (var i = 0; i < 3; i++) {
		var particle = new Particle(0, 0)
		this.addChild(particle)
		this.yellowLayers.push(particle)
	}
	for (var i = 0; i < 3; i++) {
		var particle = new Particle(0, 0)
		this.addChild(particle)
		this.redLayers.push(particle)
	}

	this.startTimestamp = Date.now()
	this.configureYellow()
	this.configureRed()
	this.configureSmoke()

	this.alpha = 0

	/*
	this.sound = new Howl({
		urls:['sounds/full-explosion.ogg'],
		volume: 1.0
		//pos3d: [500, 500, -0.5]
	})
*/
}


ExplosionEffect.prototype = Object.create(PIXI.Container.prototype)
ExplosionEffect.prototype.constructor = ExplosionEffect


ExplosionEffect.prototype.configureYellow = function() {
	for (var i = 0; i < this.yellowLayers.length; i++) {
		var particle = this.yellowLayers[i]
		particle.x = 0
		particle.y = 0
		particle.timestamp = Date.now()
		particle.blendMode = PIXI.BLEND_MODES.ADD
		particle.tint = 0xffff22
		particle.fadeTime = 4000		
		particle.scale.x = particle.scale.y = MathEx.random(20,40)/100
		particle.alpha = 1		
		particle.vx = MathEx.random(-370, 370)
		particle.vy = MathEx.random(-370, 370)
		particle.rotationRate = MathEx.random(-15, 15)
	}
}

ExplosionEffect.prototype.configureRed = function() {
	for (var i = 0; i < this.redLayers.length; i++) {
		var particle = this.redLayers[i]
		particle.x = 0
		particle.y = 0
		particle.timestamp = Date.now()
		particle.blendMode = PIXI.BLEND_MODES.ADD
		particle.tint = 0xff2222
		particle.fadeTime = 4000		
		particle.scale.x = particle.scale.y = MathEx.random(20,40)/100
		particle.alpha = 1		
		particle.vx = MathEx.random(-370, 370)
		particle.vy = MathEx.random(-370, 370)
		particle.rotationRate = MathEx.random(-15, 15)
	}
}

ExplosionEffect.prototype.configureSmoke = function() {
	for (var i = 0; i < this.smokeLayers.length; i++) {
		var particle = this.smokeLayers[i]
		particle.x = 0
		particle.y = 0
		particle.timestamp = Date.now()
		//particle.blendMode = PIXI.blendModes.ADD
		particle.tint = 0x111111
		particle.fadeTime = 4000		
		particle.scale.x = particle.scale.y = MathEx.random(20,40)/100
		particle.alpha = 0.0	
		particle.vx = MathEx.random(-370, 370)
		particle.vy = MathEx.random(-370, 370)
		particle.rotationRate = MathEx.random(-15, 15)
	}
}

ExplosionEffect.prototype.reset = function() {
	
	this.hasExploded = false
	this.configureYellow()
	this.configureRed()
	this.configureSmoke()
	console.log('ExplosionEffect.reset invoked')
}

ExplosionEffect.prototype.begin = function() {
	this.startTimestamp = Date.now()
	this.configureYellow()
	this.configureRed()
	this.configureSmoke()
	//this.sound.play()
}

ExplosionEffect.prototype.update = function(delta) {

	var now = Date.now()

	for (var i = 0; i < this.yellowLayers.length; i++) {
		var particle = this.yellowLayers[i]
		particle.x += particle.vx * delta
		particle.y += particle.vy * delta
		particle.vx *= 0.005//1 - 122.25 * delta
		particle.vy *= 0.005//1 - 122.25 * delta
		particle.rotationRate *= 1 - 1.25 * delta
		particle.rotation += particle.rotationRate * delta
		var alpha = 1 - (now - particle.timestamp) / particle.fadeTime
		if (alpha <= 0) alpha = 0.001
		particle.alpha = alpha


		particle.vx *= alpha
		particle.vy *= alpha
		//console.log(alpha)
		particle.scale.x += 1.1 * alpha * delta
		particle.scale.y += 1.1 * alpha * delta		
	}

	for (var i = 0; i < this.redLayers.length; i++) {
		var particle = this.redLayers[i]
		particle.x += particle.vx * delta
		particle.y += particle.vy * delta
		particle.vx *= 0.005//1 - 122.25 * delta
		particle.vy *= 0.005//1 - 122.25 * delta
		particle.rotationRate *= 1 - 1.25 * delta
		particle.rotation += particle.rotationRate * delta
		var alpha = 1 - (now - particle.timestamp) / particle.fadeTime
		if (alpha <= 0) alpha = 0.001


		particle.alpha = alpha

		particle.vx *= alpha
		particle.vy *= alpha
		particle.scale.x += 1.1 * alpha * delta
		particle.scale.y += 1.1 * alpha * delta			
	}

	for (var i = 0; i < this.smokeLayers.length; i++) {
		var particle = this.smokeLayers[i]
		particle.x += particle.vx * delta
		particle.y += particle.vy * delta
		particle.vx *= 0.005//1 - 122.25 * delta
		particle.vy *= 0.005//1 - 122.25 * delta
		particle.rotationRate *= 1 - 1.25 * delta
		particle.rotation += particle.rotationRate * delta
		var alpha = 1 - (now - particle.timestamp) / particle.fadeTime
		if (alpha <= 0) alpha = 0.00
		//var realAlpha = 0
		if (now - particle.timestamp < 2000) {
			particle.alpha = (now - particle.timestamp) / 2000
			//console.log('this CASE')
		} else {
			var alpha0 = particle.alpha - (0.015 * delta)		

			if (particle.alpha0 < 0) {
				particle.alpha = 0.00
			} else {
				particle.alpha = alpha0
			}
		}
		
		particle.vx *= alpha
		particle.vy *= alpha
		particle.scale.x += 0.8 * alpha * delta
		particle.scale.y += 0.8 * alpha * delta	
		//console.log(alpha)	
	}
}




function Particle() {
	PIXI.Sprite.call(this, PIXI.Texture.fromFrame('compressed-cloud.png'))
	this.anchor.x = this.anchor.y = 0.5
	//console.log('new particle created')
	Particle.total++
}
Particle.constructor = Particle
Particle.prototype = Object.create(PIXI.Sprite.prototype)
Particle.total = 0

module.exports = ExplosionEffect
},{"../../../nengi/nengi":50}],28:[function(require,module,exports){
function Dictionary() {
	this._objects = {}
	this._iterable = []
}

Dictionary.prototype.get = function(id) {
	return this._objects[id]
}

Dictionary.prototype.forEach = function(iterator) {
	for (var i = 0; i < this._iterable.length; i++) {
		iterator(this._iterable[i])
	}
}

// a copy of the underlying array
Dictionary.prototype.toArray = function() {
    return this._iterable.slice()
}

Dictionary.prototype.add = function(obj) {
	this._objects[obj.id] = obj
    this._iterable.push(obj)
}

//in place filter: 
/*
var j = 0; 
for (var i = 0; i < arr.length; ++i) { 
	if (shouldKeep(arr[i])) { 
		arr[j++] = arr[i]; 
	} 
} arr.length = j;
shouldKeep is the filter, apply any change to the items
*/

//remove: 
// arr[i] = arr[arr.length-1];
// arr.pop();
Dictionary.prototype.remove = function(id) {
    var index = -1
    for (var i = 0; i < this._iterable.length; i++) {
        if (this._iterable[i].id === id) {
            index = i
            break
        }
    }
    if (index !== -1) {
        this._iterable.splice(index, 1)
    }

    delete this._objects[id]
}


module.exports = Dictionary
},{}],29:[function(require,module,exports){

/**
* MathEx, aka MathExtensions
* not to be confused with javascript's Math
* contains some normal game dev math
*  distance, vectors, ranges, lerps
*/

/**
* Scales a number from a source range to its value in a destination range
* e.g. n: 127.5 from range [0, 255] would become n: 0.0 in range [-1, 1]
* @method scale
* @param {Number} n The number to scale
* @param {Number} a The lower limit of the source range
* @param {Number} b The upper limit of the source range
* @param {Number} c The lower limit of the destination range
* @param {Number} d The lower limit of the destination range
* @return {Number} Returns a value for n relative to the destination range
*/
var scale = function(n, a, b, c, d) {
  return (d - c) * (n - a) / (b - a) + c
}
module.exports.scale = scale


module.exports.radiansToUInt8 = function(rad) {
  return Math.floor(scale(rad, -Math.PI, Math.PI, 0, 255) % 256)
}

module.exports.UInt8ToRadians = function(uint8) {
  return uint8 * ((2 * Math.PI) / 255)
}

module.exports.radiansToDegrees = function(rad) {
  return rad * (180 / Math.PI)
}

module.exports.degreesToRadians = function(degrees) {
  return degrees * (Math.PI / 180)
}

module.exports.degreesToUInt8 = function(degrees) {
  if (degrees > 360 || degrees < 0)
    throw('degreesToUInt8 overflow')
  return Math.round(scale(degrees, 0, 360, 0, 255))
}

module.exports.UInt8ToDegrees = function(uint8) {
  return Math.round(scale(uint8, 0, 255, 0, 360))
}

module.exports.dot = function(a1, a2, b1, b2) {
  return (a1*b1) + (a2*b2)
}

/*
* Distance between two points, pythagorean
* @method distance
* @param {Number} x1 X component of the first point
* @param {Number} y1 Y component of the first point
* @param {Number} x2 X component of the second point
* @param {Number} y2 Y component of the second point
* @return {Number} Returns distance
*/
module.exports.distance = function(x1, y1, x2, y2) {
    var x = x2 - x1
    var y = y2 - y1
    return Math.sqrt((x*x) + (y*y))
}

/*
* Turns two points into a vector
* @method vectorize
* @param {Number} x1 X component of the first point
* @param {Number} y1 Y component of the first point
* @param {Number} x2 X component of the second point
* @param {Number} y2 Y component of the second point
* @return {Object} Returns a vector with x and y components
*/
module.exports.vectorize = function(x1, y1, x2, y2) {
  return {
    x: x2 - x1,
    y: y2 - y1
  }
}

/*
* Calculates length of a vector ##not sure if this is actually used anywhere
* @method vectorLength
* @param {Object} vector A vector with an x and y component
* @return {Object} Returns a vector with x and y components
*/
module.exports.vectorLength = function(vector) {
  return Math.sqrt((vector.x * vector.x) + (vector.y * vector.y))
}


/*
* Calculates a unit vector (vector of length 1)
* @method normalizeVector
* @param {Object} vector A vector with an x and y component
* @return {Object} Returns a unit vector with x and y components
*/
var normalizeVector = function(vector) {
  if (vector.x === 0 & vector.y === 0) {
    return { x: 0, y: 0 }
  }

  var length = Math.sqrt((vector.x * vector.x) + (vector.y * vector.y))
  return {
    x: vector.x / length,
    y: vector.y / length
  }
}
module.exports.normalizeVector = normalizeVector

/*
* Liner interpolation
* e.g. a: 50, b: 100, portion: 0.5, result: 75 (75 is halfway from 50 to 100)
* @method lerp
* @param {Number} a The first number in the range
* @param {Number} b The second number in the range
* @param {Number} portion Partial distance usually between 0.0 and 1.0
* @return {Object} Returns a value at 'portion' distance between a and b
*/
var lerp = function(a, b, portion) {
  return a + ((b - a) * portion)
}
module.exports.lerp = lerp

/*
* Liner interpolation between two colors (for blending/gradients)
* @method lerpColor
* @param {Number} colorA The first color
* @param {Number} colorB The second color
* @param {Number} portion How far between the colors (0.0 is colorA, 1.0 is colorB)
* @return {Object} Returns a color blend between a and b
*/
module.exports.lerpColor = function(colorA, colorB, portion) {
    return {
        r: lerp(colorA.r, colorB.r, portion),
        g: lerp(colorA.g, colorB.g, portion),
        b: lerp(colorA.b, colorB.b, portion),
        a: lerp(colorA.a, colorB.a, portion)
    }
}


/*
* Random integer from min to max
* @method random
* @param {Integer} min The minimum of the random range
* @param {Integer} max The maximum of the random range
* @return {Integer} Returns a random integer within the specified range
*/
var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
module.exports.random = random

/*
* Random heading as a unit vector
*/
module.exports.randomDirection = function() {
  return normalizeVector({x: random(-1000,1000), y: random(-1000, 1000)})
}

/*
* Random float from min to max
* @method random
* @param {Number} min The minimum of the random range
* @param {Number} max The maximum of the random range
* @return {Number} Returns a random float within the specified range
*/
module.exports.randomFloat = function(min, max) {
  return Math.random() * (max - min) + min
}

/*
* Interpolates UInt8 (0-255) angles as a rotation around a circle, carefully
* wraps around 0 and 255 choosing the intuitive direction to turn
* @method interpolateRotationUInt8random
* @param {UInt8} a First angle as a byte
* @param {UInt8} b Second angle as a byte
* @param {Number} ratio Amount to interpolate (0 -> a, 1 -> b, 0.5 -> halfway)
* @return {Number} Returns the new angle
*/
module.exports.interpolateRotationUInt8 = function(a, b, ratio) {
  if (a < 63 && b > 191) {
    return interpRot = lerp(a + 255, b, ratio) - 255
  } else if (a > 191 && b < 63) {
    return interpRot = lerp(a, b + 255, ratio) - 255
  } else {
    return interpRot = lerp(a, b, ratio)
  }    
}

/*
* Interpolates radians as a rotation around a circle, carefully
* wraps around 0 and 255 choosing the intuitive direction to turn
* @method interpolateRotationUInt8random
* @param {UInt8} a First angle as a byte
* @param {UInt8} b Second angle as a byte
* @param {Number} ratio Amount to interpolate (0 -> a, 1 -> b, 0.5 -> halfway)
* @return {Number} Returns the new angle
*/
module.exports.interpolateRotation = function(a, b, ratio) {
//return interpRot = lerp(a, b, ratio)
  var PI = Math.PI
  var whole = 2 * PI
  var quarter = PI / 2
  var threeQuarters = 3* PI / 2

  if (a < quarter && b > threeQuarters) {
    return interpRot = lerp(a + whole, b, ratio) - whole
  } else if (a > threeQuarters && b < quarter) {
    return interpRot = lerp(a, b + whole, ratio) - whole
  } else {
    return interpRot = lerp(a, b, ratio)
  }    
}


module.exports.isApproximately = function(a, b, threshold) {
  return (a < b + threshold && a > b - threshold)
}

},{}],30:[function(require,module,exports){
var MessageType = {
	Ping: 8,
	Pong: 9,
	PartialGameObjects: 10,
	GameObjects: 11,
	GameObjectOptimizations: 12,
	GameEvents: 13,
	ForgetObjects: 14,
	Quadtrees: 15,
	PlayerInputs: 16,
	DirectMessages: 17
}

module.exports = MessageType
},{}],31:[function(require,module,exports){
(function (process){

/**
* Runs a specified main function @ specified fps
*/
var NodeLoop = {}


/*
* Provides a high resolution time (nanonseconds) converted to milliseconds.
* Unlike Date.now(), NodeLoop.time is not relative to any real point in time.
*/
var hrtimeMs = function() {
	var time = process.hrtime()
	return time[0] * 1000 + time[1] / 1000000
}

NodeLoop.previousTick = hrtimeMs()
NodeLoop.tickLengthMs = null
NodeLoop.tick = 0
NodeLoop.main = null

NodeLoop.setFPS = function(fps) {
	NodeLoop.tickLengthMs = 1000 / fps
}

NodeLoop.setMain = function(main) {
	NodeLoop.main = main
}

NodeLoop.begin = function() {
	if (!NodeLoop.main)
		throw 'ERROR: NodeLoop requires a main() function, use NodeLoop.setMain(fn)'
	if (!NodeLoop.tickLengthMs)
		throw 'ERROR: NodeLoop requires an fps, use NodeLoop.setFPS(num)'

	NodeLoop.loop()
}

NodeLoop.loop = function() {
	var now = hrtimeMs()
	if (NodeLoop.previousTick + NodeLoop.tickLengthMs <= now) {
		var delta = (now - NodeLoop.previousTick) / 1000
		NodeLoop.previousTick = now
		NodeLoop.tick++

		//var start = hrtimeMs() // uncomment to benchmark main
		NodeLoop.main(delta, NodeLoop.tick, now)
		//var stop = hrtimeMs()
		//console.log('update took', stop-start, 'ms')
	}

	if (delta * 1000 > NodeLoop.tickLengthMs + 1) {
		// technically, we're always lagging by a few nanoseconds only report 
		// lag of 1 ms or more
		// disable this in production unless you want a log full of entries like
		// "lagged by 1.8291680216789246 ms"
		console.log('lagged by', (delta * 1000) - (NodeLoop.tickLengthMs), 'ms')
	}

	// schedules the next iteration of the loop on the node.js event loop using
	// a hyrbid of setTimeout & setImmediate calls. Low CPU use while idle.
	if (hrtimeMs() - NodeLoop.previousTick < NodeLoop.tickLengthMs - 16) {
		// schedule 16+ milliseconds in the future
		// NOTE: we cannot just set a timeout for the perfect amount of time
		// because setTimeout is innacurate by up to 16 ms, so instead we use
		// setTimeout when we have more than 16 ms remaining until the next loop
		// The resolution of node timer is subject to changes per version and 
		// per operating system. Node v0.12.0+ may be able to get away with 1 ms
		setTimeout(NodeLoop.loop)
		// P.S. using setTimeout only will make iterations up to 16 ms late
	} else {
		// schedule for next node cycle (nanoseconds from now, pending cpu use)
		// NOTE: unless we're already late, NodeLoop.loop will likely cycle many 
		// times for ~16 milliseconds until it is time to run main() again
		setImmediate(NodeLoop.loop)
		// P.S. using setImmediate only will keep CPU @ 100% regardless of
		// whether node had much to do
	}
}

module.exports = NodeLoop
}).call(this,require('_process'))
},{"_process":5}],32:[function(require,module,exports){
/*
* Vector2, a 2D vector and useful maths
* adapated from THREE.js (MIT License)
* license attached: https://github.com/mrdoob/three.js/blob/master/LICENSE
*/
function Vector2 (x, y) {
	this.x = x || 0
	this.y = y || 0
}

Vector2.prototype.constructor = Vector2

Vector2.prototype.add = function(vector) {
	this.x += vector.x
	this.y += vector.y
}

Vector2.prototype.addScalar = function(scalar) {
	this.x += scalar
	this.y += scalar
}

Vector2.prototype.subtract = function(vector) {
	this.x -= vector.x
	this.y -= vector.y
}

Vector2.prototype.multiply = function(vector) {
	this.x *= vector.x
	this.y *= vector.y
}

Vector2.prototype.multiplyScalar = function(scalar) {
	this.x *= scalar
	this.y *= scalar
}

Vector2.prototype.divide = function(vector) {
	this.x /= vector.x
	this.y /= vector.y
}

Vector2.prototype.divideScalar = function(scalar) {
	this.x /= scalar
	this.y /= scalar
}

Vector2.prototype.dotProduct = function(vector) {
	return this.x * vector.x + this.y * vector.y
}

Vector2.prototype.lengthSquared = function() {
	return this.x * this.x + this.y * this.y
}

Object.defineProperty(Vector2.prototype, 'length', {
	get: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y)
	},
	set: function(value) {
		var oldLength = this.length
		if (oldLength !== 0 && value !== oldLength) {
			this.multiplyScalar(value/oldLength)
		}
	}
})

Vector2.prototype.normalize = function() {
	return this.divideScalar(this.length)
}

Vector2.prototype.lerp = function(vector, alpha) {
	this.x += (vector.x - this.x) * alpha
	this.y += (vector.y - this.y) * alpha
}

Vector2.prototype.equals = function(vector) {
	return (this.x === vector.x && this.y === vector.y)
}

//TODO obey object pool creation rules, no 'new'
Vector2.prototype.clone = function() {
	return new Vector2(this.x, this.y)
}

module.exports = Vector2

/*
// some THREE.js functionality of potential interest to include
THREE.Vector2.prototype = {
	copy: function ( v ) {

		this.x = v.x;
		this.y = v.y;

		return this;
	},

	addVectors: function ( a, b ) {

		this.x = a.x + b.x;
		this.y = a.y + b.y;

		return this;

	},
	subVectors: function ( a, b ) {

		this.x = a.x - b.x;
		this.y = a.y - b.y;

		return this;

	},

	distanceTo: function ( v ) {
		return Math.sqrt( this.distanceToSquared( v ) );
	},
	distanceToSquared: function ( v ) {
		var dx = this.x - v.x, dy = this.y - v.y;
		return dx * dx + dy * dy;
	},
};
*/
},{}],33:[function(require,module,exports){

var BinaryType = require('./BinaryType')
/*
*
*
*
*
*/
var Binary = {}


/*
* Relatively straight forward binary types
*/
Binary[BinaryType.Boolean] = {
    'min': 0,
    'max': 1,
    'bits': 1,
    'write': 'writeBoolean',
    'read': 'readBoolean',
    'interp': 'never'
}

Binary[BinaryType.Int4] = {
    'min': -8,
    'max': 7,
    'bits': 4,
    'write': 'writeInt4',
    'read': 'readInt4',
    'interp': 'lerp'
}

Binary[BinaryType.UInt4] = {
    'min': 0,
    'max': 15,
    'bits': 4,
    'write': 'writeUInt4',
    'read': 'readUInt4',
    'interp': 'lerp'
}

Binary[BinaryType.Int6] = {
    'min': -32,
    'max': 31,
    'bits': 6,
    'write': 'writeInt6',
    'read': 'readInt6',
    'interp': 'lerp'
}

Binary[BinaryType.UInt6] = {
    'min': 0,
    'max': 63,
    'bits': 6,
    'write': 'writeUInt6',
    'read': 'readUInt6',
    'interp': 'lerp'
}

Binary[BinaryType.Int8] = {
    'min': -128,
    'max': 127,
    'bits': 8,
    'write': 'writeInt8',
    'read': 'readInt8',
    'interp': 'lerp'
}

Binary[BinaryType.UInt8] = {
    'min': 0,
    'max': 255,
    'bits': 8,
    'write': 'writeUInt8',
    'read': 'readUInt8',
    'interp': 'lerp'
}

Binary[BinaryType.Int10] = {
    'min': -512,
    'max': 511,
    'bits': 10,
    'write': 'writeInt10',
    'read': 'readInt10',
    'interp': 'lerp'
}

Binary[BinaryType.UInt10] = {
    'min': 0,
    'max': 1023,
    'bits': 10,
    'write': 'writeUInt10',
    'read': 'readUInt10',
    'interp': 'lerp'
}

Binary[BinaryType.Int16] = {
    'min': -32768,
    'max': 32767,
    'bits': 16,
    'write': 'writeInt16',
    'read': 'readInt16',
    'interp': 'lerp'
}

Binary[BinaryType.UInt16] = {
    'min': 0,
    'max': 65535,
    'bits': 16,
    'write': 'writeUInt16',
    'read': 'readUInt16',
    'interp': 'lerp'
}

// ## untested/unused so far
Binary[BinaryType.Int32] = {
    'min': -2147483648,
    'max': 2147483647,
    'bits': 32,
    'write': 'writeInt32',
    'read': 'readInt32',
    'interp': 'lerp'
}

// ## untested/unused so far
Binary[BinaryType.UInt32] = {
    'min': 0,
    'max': 4294967295,
    'bits': 32,
    'write': 'writeUInt32',
    'read': 'readUInt32',
    'interp': 'lerp'
}

/*
* Specially customized binary types
*/

// denotes that the number is an id
Binary[BinaryType.EntityId] = {
    'min': 0,
    'max': 255,
    'bits': 8,
    'write': 'writeUInt8',
    'read': 'readUInt8',
    'interp': 'never'
}

// storing a rotation as a byte
// should be translated to radians or degrees before being used meaningfully
Binary[BinaryType.Rotation] = {
    'min': 0,
    'max': 255,
    'bits': 8,
    'write': 'writeUInt8',
    'read': 'readUInt8',
    'interp': 'byteRotationLerp'
}

Binary[BinaryType.String] = {
    // todo
}


module.exports = Binary
},{"./BinaryType":35}],34:[function(require,module,exports){

var Binary = require('./Binary')
var BinaryType = require('./BinaryType')
var BitBuffer = require('./BitBuffer')

function BinarySerializer() {
	
}

BinarySerializer.prototype.checkProxies = function(proxyA, proxyB) {
	var dirty = []
	for (var i = 0; i < proxyA.length; i++) {
		if (proxyA[i] !== proxyB[i])
		dirty.push(i)		
	}
	return dirty
}


BinarySerializer.prototype.serializeFulls = function(bytes, serialized, netSchema) {
    //console.log(serialized)
    var bitBuffer = new BitBuffer(bytes * 8)
    var offset = 0
    for (var i = 0; i < serialized.length; i++) {
        var propName = netSchema.keys[i]
        var propData = netSchema.properties[propName]
       // bTypes.push(propData.binaryType)
        //var bytes = Binary[propData.binaryType].bytes
        var value = serialized[i]
        if (typeof value === 'undefined')
            value = 0

        //console.log('bits', Binary[propData.binaryType].bits, '/', bytes * 8, offset)

        bitBuffer[Binary[propData.binaryType].write](offset, value, true)
        offset += Binary[propData.binaryType].bits
    }
    return bitBuffer//.toBuffer()
}




BinarySerializer.prototype.serializePartials = function(bytes, serialized, netSchema) {
    //console.log('bar', serialized)
    var bitBuffer = new BitBuffer(bytes * 8)
    var offset = 0
    for (var key in serialized) {
    	//console.log('bitBuffer writing', key)
        //buffer[Binary[BinaryType.UInt8].write](key, offset, true)
        //buffer.writeUInt8(key, offset, true)
        //console.log('buffer->',bitBuffer[Binary[BinaryType.UInt8].svReadFn](offset))
        bitBuffer[Binary[BinaryType.UInt8].write](offset, key)
        //console.log('bitview->',bitBuffer[Binary[BinaryType.UInt8].svReadFn](offset))
        offset += 8
        
        var propName = netSchema.keys[key]
        //console.log('o', offset, key, 'key:'+propName)
        var propData = netSchema.properties[propName]
        var value = serialized[key]
        //console.log(propName, value, propData)
       // console.log('bitview writing', value)
        bitBuffer[Binary[propData.binaryType].write](offset, Math.round(value))
        //console.log('bitview->',bitBuffer[Binary[propData.binaryType].svReadFn](offset))
        offset += Binary[propData.binaryType].bits
        //console.log('o', offset, propName+':'+value)

    }
    return bitBuffer//.toBuffer()
}


/*
* Creates a proxy of anything with a netSchema
*
* example:
*	id: 1           ->    0: 1
*	type: SomeType  ->    1: ? (0-255 representation of type)
*	x: 55           ->    2: 55
*	y: 102          ->    3: 102
* the exact keys, values, and relevant properties come from a netSchema
*
* @method proxify
* @param {GameObject} gameObject
* @return {Object}
*/
BinarySerializer.prototype.proxify = function(gameObject) {
	//var bytes = 0
	var proxy = []

	//console.log('PROXY', gameObject)

	var schema = gameObject.netSchema
	for (var i = 0; i < schema.keys.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[i]]

		if (prop.path) {
			var path = prop.path
			//bytes += Binary[prop.binaryType].bytes
			if (path.length === 1) {
				value = gameObject[path[0]]
			} else if (path.length === 2) {
				value = gameObject[path[0]][path[1]]
			} else if (path.length === 3) {
				value = gameObject[path[0]][path[1]][path[2]]
			}
		}

		proxy.push(value)
	}

	return proxy
}

BinarySerializer.prototype.proxifyPartials = function(keys, gameObject) {
	var result = []
	var schema = gameObject.netSchema

	//console.log(keys, gameObject.netSchema)

	for (var i = 0; i < keys.length; i++) {
		var key = keys[i]
		var propAlias = schema.keys[key]

		var value = null
		var prop = schema.properties[propAlias]
		console.log('checking',propAlias, schema.keys[i], prop )
		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				value = gameObject[path[0]]
			} else if (path.length === 2) {
				value = gameObject[path[0]][path[1]]
			} else if (path.length === 3) {
				value = gameObject[path[0]][path[1]][path[2]]
			}
		}

		result.push({ key: key, value: value })
	}
	return result
}

/*
* Deserializes and copies data into a gameObject
*
* @method deproxify
* @param {Object} serializedGameObject The serialized data
* @param {GameObject} gameObject the recipient of the serialized data
*/
BinarySerializer.prototype.deproxify = function(serializedGameObject, gameObject) {	
	var schema = gameObject.netSchema
	for (var i = 0; i < schema.keys.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[i]]

		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				gameObject[path[0]] = serializedGameObject[i]
			} else if (path.length === 2) {
				gameObject[path[0]][path[1]] = serializedGameObject[i]
			} else if (path.length === 3) {
				gameObject[path[0]][path[1]][path[2]] = serializedGameObject[i]
			}
		}
	}
}


// for deproxifying net messages
BinarySerializer.prototype.deproxify2 = function(serializedGameObject, gameObject) {
	//console.log('deproxify2', serializedGameObject)	
	var schema = gameObject.netSchema
	for (var i = 2; i < schema.keys.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[i]]
		//console.log('checking', prop,prop.path)

		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				//console.log('herehere', path[0], 'set to', serializedGameObject[i-1])
				gameObject[path[0]] = serializedGameObject[i-1]
			} else if (path.length === 2) {
				gameObject[path[0]][path[1]] = serializedGameObject[i-1]
			} else if (path.length === 3) {
				gameObject[path[0]][path[1]][path[2]] = serializedGameObject[i-1]
			}
		}
	}
}

BinarySerializer.prototype.foo = function(serializedGameObject, gameObject) {
	var schema = gameObject.netSchema

	for (var i = 0; i < serializedGameObject.length; i++) {
		var value = null
		var prop = schema.properties[schema.keys[serializedGameObject[i].key]]

		console.log('prop', prop)
		
		if (prop.path) {
			var path = prop.path
			if (path.length === 1) {
				gameObject[path[0]] = serializedGameObject[i].value
			} else if (path.length === 2) {
				gameObject[path[0]][path[1]] = serializedGameObject[i].value
			} else if (path.length === 3) {
				gameObject[path[0]][path[1]][path[2]] = serializedGameObject[i].value
			}
		}
	}
}

/*
* Creates a netSchema based on the properties and types listed in config. Only
* the properties listed in this schema will be networked from server to client.
* Any other properties are considered private to the server. IMMUTABLE
*
* example config (object):
*  'id': BinaryType.UInt16
*  'type': BinaryType.UInt8
*  'x': BinaryType.Int16
*  'y': BinaryType.Int16
*  'physics.mass' : BinaryType.UInt16
*
* example resulting netSchema (array):
*   0: { key: 0, binaryType: 4, path: ['id'] }
*   1: { key: 1, binaryType: 1, path: ['type'] }
*   2: { key: 2, binaryType: 3, path: ['x'] }
*   3: { key: 3, binaryType: 3, path: ['y'] }
*   4: { key: 4, binaryType: 4, path: ['physics', 'mass'] }
*
* the netSchema can be used to serialize gameObject data for transmission
* 'type' represents a BinaryType (see BinaryType defintion)
* 'key' is a 0-255 key for the property name
* 'path' is a path to a value if the value is nested within a gameComponent
* rather than being in the root gameObject
*
* @constructor NetSchema
* @param {Object} config
* @return {Object}
*/
 BinarySerializer.prototype.createEntitySchema = function(config, optimizations) {
	// the list of properties
	var schema = {}

	schema.properties = {}
	schema.totalBits = 0
	// the byte representation of each property, e.g. 'id' is 0, 'type' is 1
	schema.keys = []
	var index = 0
	for (var prop in config) {

		var binaryType = ''
		var interp = true
		if (typeof config[prop] === 'object') {
			//console.log('prop was object', config[prop])
			binaryType = config[prop].binaryType
			interp = config[prop].interp
		} else {
			//console.log('prop was not object', config[prop], typeof config[prop])
			binaryType = config[prop]
		}
		// RULE: first property must be the gameObject id
		// RULE: second property must be the gameObject type
		if (index === 0 && prop !== 'id') {
			throw 'netSchema first property must be id'
		} else if (index === 1 && prop !== 'type') {
			throw 'netSchema second property must be type'
		}


		schema.properties[prop] = { key: index, binaryType: binaryType, interp: interp }
		schema.totalBits += Binary[binaryType].bits
		schema.keys.push(prop)
		// if the property was not in the root gameObject...
		if (prop.indexOf('.') !== -1) {
			// then create an array called path with the property path
			// e.g. 'physics.mass' becomes path['physics', 'mass']
			schema.properties[prop].path = prop.split('.')
			if (schema.properties[prop].path.length > 3) {
				throw new Exception('createSchema error: can only create schemas of gameObjects with 3 or fewer layers of nested components, e.g. gameObject.foo.bar.baz')
			}
		} else {
			schema.properties[prop].path = [prop]
		}
		index++
	}
	if (index > 255) {
		throw 'netSchema cannot be created with more than 255 properties'
	}

	if (typeof optimizations !== 'undefined') {
		schema.hasOptimizations = true		
		schema.optimizations = []

		for (var prop in optimizations) {
			var opt = optimizations[prop]
			opt.prop = prop
			schema.optimizations.push(optimizations[prop])
		}
		/*
		schema.optimization.properties = {}
		schema.optimization.keys =[]

		for (var prop in optimization) {
			var index = schema.properties[prop].key
			schema.optimization.properties[prop] =  { key: index, binaryType: optimization[prop] }
			schema.optimization.keys.push(prop)
		}
		*/
		//schema.keys[optimization]
	} else {
		schema.hasOptimizations = false
	}
	// return an immutable schema
	//console.log('SCHEMA', schema)
	return Object.freeze(schema)
}

 BinarySerializer.prototype.createSchema = function(config) {
	// the list of properties
	var schema = {}
	schema.properties = {}
	schema.totalBits = 0
	// the byte representation of each property, e.g. 'id' is 0, 'type' is 1
	schema.keys = []
	var index = 0
	for (var prop in config) {
		schema.properties[prop] = { key: index, binaryType: config[prop] }
		schema.totalBits += Binary[config[prop]].bits
		schema.keys.push(prop)
		// if the property was not in the root gameObject...
		if (prop.indexOf('.') !== -1) {
			// then create an array called path with the property path
			// e.g. 'physics.mass' becomes path['physics', 'mass']
			schema.properties[prop].path = prop.split('.')
			if (schema.properties[prop].path.length > 3) {
				throw new Exception('createSchema error: can only create schemas of gameObjects with 3 or fewer layers of nested components, e.g. gameObject.foo.bar.baz')
			}
		} else {
			schema.properties[prop].path = [prop]
		}
		index++
	}
	if (index > 255) {
		throw 'netSchema cannot be created with more than 255 properties'
	}
	schema.hasOptimizations = false

	// return an immutable schema
	//console.log('SCHEMA', schema)
	return Object.freeze(schema)
}

var singleton = new BinarySerializer()
module.exports = singleton
},{"./Binary":33,"./BinaryType":35,"./BitBuffer":36}],35:[function(require,module,exports){


var BinaryType = {
	// the 'value' of these enums is irrelevant.
	// usage:   something.binaryType = BinaryType.Int16
	// for convenience these are aliased in nengi's root e.g. nengi.UInt16
	Null: 0, // never used
	Boolean: 1,
	Int2: 2,
	UInt2: 3,
	Int3: 4,
	UInt3: 5,
	Int4: 6,
	UInt4: 7,
	Int6: 33,
	UInt6: 34,	
	Int8: 8,
	UInt8: 9,
	Int10: 10,
	UInt10: 11,
	Int12: 12,
	UInt12: 13,
	Int16: 14,
	UInt16: 15,
	Int32: 16,
	UInt32: 17,
	Float32: 18, // untested
	Float64: 19, // untested
	String: 20, // untested

	// Customized even more...

	EntityId: 21,
	Rotation: 22
}



module.exports = BinaryType
},{}],36:[function(require,module,exports){
(function (Buffer){
(function (root) {


var BitBuffer = function(sourceOrLength) {
	this.bitLength = null // length in bits (can be less than the bytes/8)
	this.byteLength = null // length in bytes (atleast enough to hold the bits)
	this.byteArray = null // Uint8Array holding the underlying bits and bytes

	//console.log('BitBuffer ctor', sourceOrLength, typeof sourceOrLength)
	if (typeof sourceOrLength === 'number') {
		// create a bitBuffer with *length* bits
		this.bitLength = sourceOrLength
		this.byteLength = Math.ceil(sourceOrLength / 8)
		if (typeof Buffer !== 'undefined') {
			this.byteArray = new Buffer(this.byteLength)
		} else {
			this.byteArray = new Uint8Array(this.byteLength)
		}
		
	} else if (sourceOrLength instanceof ArrayBuffer) {	
		// create a bitBuffer from an ArrayBuffer (Uint8Array, etc)
		this.bitLength = sourceOrLength.byteLength * 8
		this.byteLength = sourceOrLength.byteLength
		this.byteArray = new Uint8Array(sourceOrLength)
	} else if (typeof Buffer !== 'undefined' && sourceOrLength instanceof Buffer) {
		// create a bitBuffer from a node Buffer
		this.bitLength = sourceOrLength.length * 8
		this.byteLength = sourceOrLength.length
		this.byteArray = new Uint8Array(sourceOrLength)
	} else {
		throw 'Unable to create BitBuffer, expected length (in bits), ArrayBuffer, or Buffer'
	}	
}

// Used to massage fp values so we can operate on them
// at the bit level.
BitBuffer._scratch = new DataView(new ArrayBuffer(8));

BitBuffer.concat = function(bitViews) {
	var bitLength = 0
	for (var i = 0; i < bitViews.length; i++) {	
		bitLength += bitViews[i].bitLength	
	}
	var bitView = new BitBuffer(new Buffer(Math.ceil(bitLength/8)))
	var offset = 0
	for (var i = 0; i < bitViews.length; i++) {
		for (var j = 0; j < bitViews[i].bitLength; j++) {
			bitView._setBit(bitViews[i]._getBit(j), offset)
			offset++
		}
	}
	return bitView
}

BitBuffer.prototype.toBuffer = function() {
	return this.byteArray //new Buffer(this.byteArray, this.byteLength)
}


/*
Object.defineProperty(BitBuffer.prototype, 'buffer', {
	get: function () { return this._buffer; },
	enumerable: true,
	configurable: false
});
*/

/*
Object.defineProperty(BitBuffer.prototype, 'byteLength', {
	get: function () { return this.byteArray.length; },
	enumerable: true,
	configurable: false
});
*/

BitBuffer.prototype._getBit = function (offset) {
	return this.byteArray[offset >> 3] >> (offset & 7) & 0x1;
};

BitBuffer.prototype._setBit = function (on, offset) {
	if (on) {
		this.byteArray[offset >> 3] |= 1 << (offset & 7);
	} else {
		this.byteArray[offset >> 3] &= ~(1 << (offset & 7));
	}
};

BitBuffer.prototype.getBits = function (offset, bits, signed) {
	var available = (this.byteArray.length * 8 - offset);

	if (bits > available) {
		throw new Error('Cannot get ' + bits + ' bit(s) from offset ' + offset + ', ' + available + ' available');
	}

	var value = 0;
	for (var i = 0; i < bits;) {
		var read;

		// Read an entire byte if we can.
		if ((bits - i) >= 8 && ((offset & 7) === 0)) {
			value |= (this.byteArray[offset >> 3] << i);
			read = 8;
		} else {
			value |= (this._getBit(offset) << i);
			read = 1;
		}

		offset += read;
		i += read;
	}

	if (signed) {
		// If we're not working with a full 32 bits, check the
		// imaginary MSB for this bit count and convert to a
		// valid 32-bit signed value if set.
		if (bits !== 32 && value & (1 << (bits - 1))) {
			value |= -1 ^ ((1 << bits) - 1);
		}

		return value;
	}

	return value >>> 0;
};

BitBuffer.prototype.setBits = function (value, offset, bits) {
	var available = (this.byteArray.length * 8 - offset);

	if (bits > available) {
		throw new Error('Cannot set ' + bits + ' bit(s) from offset ' + offset + ', ' + available + ' available');
	}

	for (var i = 0; i < bits;) {
		var wrote;

		// Write an entire byte if we can.
		if ((bits - i) >= 8 && ((offset & 7) === 0)) {
			this.byteArray[offset >> 3] = value & 0xFF;
			wrote = 8;
		} else {
			this._setBit(value & 0x1, offset);
			wrote = 1;
		}

		value = (value >> wrote);

		offset += wrote;
		i += wrote;
	}
};

// true, false
BitBuffer.prototype.readBoolean = function (offset) {
	return this.getBits(offset, 1, false) !== 0;
};

// -4 to 3
BitBuffer.prototype.readInt3 = function (offset) {
	return this.getBits(offset, 3, true);
};
// 0 to 7
BitBuffer.prototype.readUInt3 = function (offset) {
	return this.getBits(offset, 3, false);
};
// -8 to 7
BitBuffer.prototype.readInt4 = function (offset) {
	return this.getBits(offset, 4, true);
};
// 0 to 15
BitBuffer.prototype.readUInt4 = function (offset) {
	return this.getBits(offset, 4, false);
};
// -32 to 31
BitBuffer.prototype.readInt6 = function (offset) {
	return this.getBits(offset, 6, true);
};
// 0 to 63
BitBuffer.prototype.readUInt6 = function (offset) {
	return this.getBits(offset, 6, false);
};
// -128 to 127
BitBuffer.prototype.readInt8 = function (offset) {
	return this.getBits(offset, 8, true);
};
// 0 to 255
BitBuffer.prototype.readUInt8 = function (offset) {
	return this.getBits(offset, 8, false);
};
// -512 to 511
BitBuffer.prototype.readInt10 = function (offset) {
	return this.getBits(offset, 10, true);
};
// 0 to 1023
BitBuffer.prototype.readUInt10 = function (offset) {
	return this.getBits(offset, 10, false);
};
// -2048 to 2047
BitBuffer.prototype.readInt12 = function (offset) {
	return this.getBits(offset, 12, true);
};
// 0 to 4095
BitBuffer.prototype.readUInt12 = function (offset) {
	return this.getBits(offset, 12, false);
};
// -32768 to 32767
BitBuffer.prototype.readInt16 = function (offset) {
	return this.getBits(offset, 16, true);
};
// 0 to 65535
BitBuffer.prototype.readUInt16 = function (offset) {
	return this.getBits(offset, 16, false);
};
// -2147483648 to 2147483647
BitBuffer.prototype.readInt32 = function (offset) {
	return this.getBits(offset, 32, true);
};
// 0 to 4294967295
BitBuffer.prototype.readUInt32 = function (offset) {
	return this.getBits(offset, 32, false);
};
BitBuffer.prototype.readFloat32 = function (offset) {
	BitBuffer._scratch.setUint32(0, this.readUInt32(offset));
	return BitBuffer._scratch.getFloat32(0);
};
BitBuffer.prototype.readFloat64 = function (offset) {
	BitBuffer._scratch.setUint32(0, this.readUInt32(offset));
	// DataView offset is in bytes.
	BitBuffer._scratch.setUint32(4, this.readUInt32(offset+32));
	return BitBuffer._scratch.getFloat64(0);
};

BitBuffer.prototype.writeBoolean = function (value, offset) {
	this.setBits(value ? 1 : 0, offset, 1);
};
BitBuffer.prototype.writeInt3  =
BitBuffer.prototype.writeUInt3 = function (value, offset) {
	this.setBits(value, offset, 3);
};
BitBuffer.prototype.writeInt4  =
BitBuffer.prototype.writeUInt4 = function (value, offset) {
	this.setBits(value, offset, 4);
};
BitBuffer.prototype.writeInt6  =
BitBuffer.prototype.writeUInt6 = function (value, offset) {
	this.setBits(value, offset, 6);
};
BitBuffer.prototype.writeInt8  =
BitBuffer.prototype.writeUInt8 = function (value, offset) {
	this.setBits(value, offset, 8);
};
BitBuffer.prototype.writeInt10  =
BitBuffer.prototype.writeUInt10 = function (value, offset) {
	this.setBits(value, offset, 10);
};
BitBuffer.prototype.writeInt12  =
BitBuffer.prototype.writeUInt12 = function (value, offset) {
	this.setBits(value, offset, 12);
};
BitBuffer.prototype.writeInt16  =
BitBuffer.prototype.writeUInt16 = function (value, offset) {
	this.setBits(value, offset, 16);
};
BitBuffer.prototype.writeInt32  =
BitBuffer.prototype.writeUInt32 = function (value, offset) {
	this.setBits(value, offset, 32);
};
BitBuffer.prototype.writeFloat32 = function (value, offset) {
	BitBuffer._scratch.setFloat32(0, value);
	this.setBits(BitBuffer._scratch.getUint32(0), offset, 32);
};
BitBuffer.prototype.writeFloat64 = function (value, offset) {
	BitBuffer._scratch.setFloat64(0, value);
	this.setBits(BitBuffer._scratch.getUint32(0), offset, 32);
	this.setBits(BitBuffer._scratch.getUint32(4), offset+32, 32);
};

/**********************************************************
 *
 * BitStream
 *
 * Small wrapper for a BitBuffer to maintain your position,
 * as well as to handle reading / writing of string data
 * to the underlying buffer.
 *
 **********************************************************/
var reader = function (name, size) {
	return function () {
		var val = this.byteArray[name](this._index);
		this._index += size;
		return val;
	};
};

var writer = function (name, size) {
	return function (value) {
		this.byteArray[name](this._index, value);
		this._index += size;
	};
};

function readASCIIString(stream, bytes) {
	var i = 0;
	var chars = [];
	var append = true;

	// Read while we still have space available, or until we've
	// hit the fixed byte length passed in.
	while (!bytes || (bytes && i < bytes)) {
		var c = stream.readUInt8();

		// Stop appending chars once we hit 0x00
		if (c === 0x00) {
			append = false;

			// If we don't have a fixed length to read, break out now.
			if (!bytes) {
				break;
			}
		}

		if (append) {
			chars.push(c);
		}

		i++;
	}

	// Convert char code array back to string.
	return chars.map(function (x) {
		return String.fromCharCode(x);
	}).join('');
}

function writeASCIIString(stream, string, bytes) {
	var length = bytes || string.length + 1;  // + 1 for NULL

	for (var i = 0; i < length; i++) {
		stream.writeUInt8(i < string.length ? string.charCodeAt(i) : 0x00);
	}
}

var BitStream = function (source, byteOffset, byteLength) {
	var isBuffer = source instanceof ArrayBuffer ||
		(typeof Buffer !== 'undefined' && source instanceof Buffer);

	if (!(source instanceof BitBuffer) && !isBuffer) {
		throw new Error('Must specify a valid BitBuffer, ArrayBuffer or Buffer');
	}

	if (isBuffer) {
		this.byteArray = new BitBuffer(source, byteOffset, byteLength);
	} else {
		this.byteArray = source;
	}

	this._index = 0;
};

Object.defineProperty(BitStream.prototype, 'byteIndex', {
	// Ceil the returned value, over compensating for the amount of
	// bits written to the stream.
	get: function () { return Math.ceil(this._index / 8); },
	set: function (val) { this._index = val * 8; },
	enumerable: true,
	configurable: true
});

Object.defineProperty(BitStream.prototype, 'buffer', {
	get: function () { return this.byteArray.buffer; },
	enumerable: true,
	configurable: false
});

Object.defineProperty(BitStream.prototype, 'view', {
	get: function () { return this.byteArray; },
	enumerable: true,
	configurable: false
});

BitStream.prototype.getBits = function (bits, signed) {
	var val = this.byteArray.getBits(this._index, bits, signed);
	this._index += bits;
	return val;
};

BitStream.prototype.setBits = function (value, bits) {
	this.byteArray.setBits(this._index, value, bits);
	this._index += bits;
};

BitStream.prototype.readInt8 = reader('getInt8', 8);
BitStream.prototype.readUInt8 = reader('getUInt8', 8);
BitStream.prototype.readInt16 = reader('getInt16', 16);
BitStream.prototype.readUInt16 = reader('getUInt16', 16);
BitStream.prototype.readInt32 = reader('getInt32', 32);
BitStream.prototype.readUInt32 = reader('getUInt32', 32);
BitStream.prototype.readFloat32 = reader('getFloat32', 32);
BitStream.prototype.readFloat64 = reader('getFloat64', 64);

BitStream.prototype.writeInt8 = writer('setInt8', 8);
BitStream.prototype.writeUInt8 = writer('setUInt8', 8);
BitStream.prototype.writeInt16 = writer('setInt16', 16);
BitStream.prototype.writeUInt16 = writer('setUInt16', 16);
BitStream.prototype.writeInt32 = writer('setInt32', 32);
BitStream.prototype.writeUInt32 = writer('setUInt32', 32);
BitStream.prototype.writeFloat32 = writer('setFloat32', 32);
BitStream.prototype.writeFloat64 = writer('setFloat64', 64);

BitStream.prototype.readASCIIString = function (bytes) {
	return readASCIIString(this, bytes);
};

BitStream.prototype.writeASCIIString = function (string, bytes) {
	writeASCIIString(this, string, bytes);
};

// AMD / RequireJS
if (typeof define !== 'undefined' && define.amd) {
	define(function () {
		return {
			BitBuffer: BitBuffer,
			BitStream: BitStream
		};
	});
}
// Node.js
else if (typeof module !== 'undefined' && module.exports) {
	module.exports = BitBuffer
}

}(this));
}).call(this,require("buffer").Buffer)
},{"buffer":1}],37:[function(require,module,exports){

var Registrar = require('./Registrar')
var GameStateReader = require('../network/GameStateReader')
var PlayerInputNetworker = require('../network/PlayerInputNetworker')
var BitBuffer = require('../binary/BitBuffer')
var BinarySerializer = require('../binary/BinarySerializer')
var EntityInterpolator = require('./EntityInterpolator')
var EventEmitter = require('../external/EventEmitter')
var InputFrame = require('./InputFrame')

/**
* Clientside (browser) entry point for nengi
*
*/

var latency = 0// add fake lag with this number
function withSimulatedLatency(fn) {
	setTimeout(fn, latency)
}

function Application(common) {
	EventEmitter.call(this)

	
	// a counter for game state snapshots sent FROM the server
	this.snapshotTick = 0
	// a counter for input state snapshots sent TO the server
	this.inputTick = 0

	// inputs to be sent
	this.inputBuffer = []

	this.websocket = null
	this.registrar = new Registrar(common)
	this.prediction = new Prediction(this)
	this.common = common
	this.gameStateReader = new GameStateReader(this.registrar, common)
	this.playerInputNetworker = new PlayerInputNetworker(common)
	this.entityInterpolator = new EntityInterpolator(this, this.registrar, common)
}

Application.prototype = Object.create(EventEmitter.prototype)
Application.prototype.constructor = Application


function Prediction(app) {
	this.app = app
	this.context = null
}

Prediction.prototype.createPredictionContext = function(id, acceptsInputs) {
	// TODO n_key isnt set, setup n_type and n_typename or something like that
	//var constructor = this.common.entityConstructors.getConstructor(serverEntity.n_type)

	//var clientEntity = new constructor()
	//clientEntity.x = serverEntity.x
	//clientEntity.y = serverEntity.y
	console.log('createPredictionContext', id, acceptsInputs)
	this.context = new PredictionContext()
	this.context.id = id
	this.context.acceptsInputs = acceptsInputs

	console.log('PREDICTION CONTEXT', this.context)
	//throw('k')
}

function PredictionContext() {
	this.id = -1
	this.isBound = false
	this.acceptsInputs = false
	this.serverEntity = null
	this.predictedEntity = null
	this.lastConfirmedInput = -1
	this.pending = []
}

PredictionContext.prototype.constructor = PredictionContext

PredictionContext.prototype.resolve = function(app) {
	//console.log('resolve')
	var obj = app.registrar.getGameObjectProxy(this.id, app.registrar.mostRecentTick)
	if (obj) {
		console.log('OBJ FOUND', obj)

		var constructor = app.common.entityConstructors.getConstructor(obj[1])
		//var serverEntity = 

		this.serverEntity = new constructor()
		BinarySerializer.deproxify(obj, this.serverEntity)

		this.predictedEntity = new constructor()
		BinarySerializer.deproxify(obj, this.predictedEntity)

		var original = this.predictedEntity.update

		
		this.predictedEntity.update = function(delta) {			
			original.apply(this, [delta])
			//console.log('!!', this.rotation)


		}
		

		this.isBound = true
	}
}
	

Prediction.prototype.update = function(inputs, delta) {
	var ctx = this.context
	if (!ctx) return

	if (inputs.length < 1) return 


	//ctx.pending[inputs[0].tick] = inputs
	ctx.pending.push(inputs)



	//console.log('inputs', inputs)
	//console.log('CONFIRM', this.app.registrar.lastConfirmedTick)

	var num = this.app.registrar.lastConfirmedTick
	var num2 = inputs[0].tick

	//console.log('diff', num, num2)

	var diff = 0

	if (num > num2) {
		diff = num2 + 256 - num
	} else {
		diff = num2 - num
	}

	while (ctx.pending.length > diff) {
		ctx.pending.shift()
	}


	if (ctx && !ctx.isBound) {
		//console.log('unbound')
		ctx.resolve(this.app)
		//console.log(ctx)
		
	} else if (ctx && ctx.isBound) {
		//console.log('bound, upating') 
		var obj = this.app.registrar.getGameObjectProxy(ctx.id, this.app.registrar.mostRecentTick)
		//console.log()

		if (!obj) return


		ctx.serverEntity.x = obj[2]
		ctx.serverEntity.y = obj[3]

		ctx.predictedEntity.x = ctx.serverEntity.x
		ctx.predictedEntity.y = ctx.serverEntity.y
		ctx.predictedEntity.rotation = ctx.serverEntity.rotation

		var debugString = 0
		/*
		for (var tick in ctx.pending) {
			//console.log('INPUT TICK', tick)
			var inputs = ctx.pending[tick]
			debugString++ //+= tick + ' '

			for (var i = 0; i < inputs.length; i++) {
				ctx.predictedEntity.applyInput(inputs[i])
			}
			ctx.predictedEntity._update(1/1000)

		}*/

		for (var i = 0; i < ctx.pending.length; i++) {
			var inputs = ctx.pending[i]

			for (var j = 0; j < inputs.length; j++) {
				ctx.predictedEntity.applyInput(inputs[j])
			}
			ctx.predictedEntity._update(0.001)
		}

		//console.log('debug', debugString)
		this.app.emit('prediction', ctx.predictedEntity)
	}


}





Application.prototype.connect = function() {
	var self = this
	this.websocket = new WebSocket('ws://' + this.common.config.IP + ':' + this.common.config.PORT, this.common.config.PROTOCOL)
	this.websocket.binaryType = 'arraybuffer'	

	this.websocket.onopen = function() {
		console.log('WebSocket connection open')
	}

	this.websocket.onerror = function(err) {
		console.log('WebSocket error', err)
	}

	this.websocket.onclose = function() {
		//throw('stop')
	}

	this.websocket.onmessage = function(message) {
		if (message.data instanceof ArrayBuffer) {		
			withSimulatedLatency(function() {
				self.snapshotTick++
				self.registrar.begin(self.snapshotTick)
				self.gameStateReader.readBinary(message.data, self.websocket)
			})
		} else if (typeof message.data === 'string') {
			console.log('received json from server, ignoring')
		} else {
			console.log('unknown websocket data type')
		}
	}
	this.emit('connect')

}

Application.prototype.disconnect = function() {
	//TODO
}

Application.prototype.send = function(input) {
	if (this.inputBuffer.length === 0) {
		var inputFrame = new InputFrame()
		inputFrame.tick = this.inputTick
		this.inputBuffer.push(inputFrame)
	}
	this.inputBuffer.push(input)
}

Application.prototype.sendInputs = function(playerInputs) {
	// TODO: calculate the real size of the buffer instead of just picking a big enough number
	var bitBuffer = new BitBuffer(256)
	var offset = 0
	var self = this
	if (this.websocket && this.websocket.readyState === 1) {
		for (var i = 0; i < playerInputs.length; i++) {
			var input = playerInputs[i]
			input.type = this.common.inputConstructors.getConstructorKeyByName(input.constructor.name)
		}

		offset = this.playerInputNetworker.write(bitBuffer, offset, playerInputs)			
		withSimulatedLatency(function() {self.websocket.send(bitBuffer.byteArray)})
	}
}


Application.prototype.update = function(delta) {
	//console.log(this.inputTick, this.snapshotTick)
	this.inputTick++
	if (this.inputTick > 255) {
		this.inputTick = 0
	}
	this.entityInterpolator.update(delta)

	this.sendInputs(this.inputBuffer)
	this.prediction.update(this.inputBuffer, delta)

	this.inputBuffer = []
}

module.exports = Application
},{"../binary/BinarySerializer":34,"../binary/BitBuffer":36,"../external/EventEmitter":44,"../network/GameStateReader":51,"../network/PlayerInputNetworker":54,"./EntityInterpolator":38,"./InputFrame":40,"./Registrar":42}],38:[function(require,module,exports){
var MathEx = require('../MathEx')
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

function EntityInterpolator(emitter, registrar, common) {
	this.emitter = emitter
	this.registrar = registrar
	this.common = common
	this.lastProcessed = 0
	this.isEnabled = true


}

/**
* Finds two snapshots on either side of renderTime
* there are a few common outcomes of this procedure:
*  1) there are no shapshots yet, because the client has just connected
*  2) there is snapshotA but no snapshotB because the client experienced a
*     lag spike (consider extrapolation)
*  3) both snapshotA and snapshotB exist, so interpolation will be possible
*
* @param {Registrar} registrar The source of snapshot data
* @param {Integer} renderTime A time in milliseconds in the recent past
* @return {Object} Returns { snapshotA: snapshotA, snapshotB: snapshotB }
*/
EntityInterpolator.prototype.findSnapshots = function(renderTime) {
	var snapshotA = null
	var snapshotB = null

	for (var i = 0; i < 10; i++) {
		// begin looking 0 snapshots ago (most recent)
		var snapshotId = this.registrar.mostRecentTick - i

		// wrap id from 0-255
		if (snapshotId < 0) {
			//snapshotId = 256 + snapshotId
		}
		var temp = this.registrar.getSnapshot(snapshotId)
		// does this snapshop qualify? (it must be older than renderTime)
		// it is possible for no snapshotA to exist if a client is freshly 
		// connected
		if (!snapshotA && temp && temp.timestamp < renderTime) {
			// we have found snapshotA
			snapshotA = temp

			// look for snapshotB, the snapshot that followed snapshotA
			var snapshotId2 = snapshotA.tick + 1
			// wrap id from 0-255
			if (snapshotId2 < 0) {
				//snapshotId2 = 256 + snapshotId2
			}
			if (snapshotId2 > 255) {
				//snapshotId2 = snapshotId2 - 256
			}

			var temp2 = this.registrar.getSnapshot(snapshotId2)
			// due to lag and whatshot it is possible to have a snapshotA but
			// no snapshotB, check if snapshotB exists
			if (temp2 && temp2.timestamp > renderTime) {
				snapshotB = temp2
				// end the for loop if snapshots A & B have been found already
				continue 
			}
		}
	}

	return { snapshotA: snapshotA, snapshotB: snapshotB }
}

EntityInterpolator.prototype.handleCreatesAndDeletes = function(snapshot) {


	//this.lastProcessed = snapshot.tick


	// createEntity
	for (var i = 0; i < snapshot.newly.length; i++) {
		var id = snapshot.newly[i]

		// look up the object's constructor and return a populated instance
		var proxy = snapshot.getGameObjectProxy(id)	
		var entity = this.registrar.createEntity(proxy)
		//console.log('emitting createEntity')
		this.emitter.emit('createEntity', entity)		
	}


	// deleteEntity
	for (var i = 0; i < snapshot.forgets.length; i++) {
		var id = snapshot.forgets[i]
		this.emitter.emit('deleteEntity', id)		
	}


}


EntityInterpolator.prototype.createEvents = function(snapshot) {
	// createEvent
	for (var i = 0; i < snapshot.events.length; i++) {
		//console.log('trying to create event', snapshot.gameEventProxies[i])
		//var id = snapshot.gameEventProxies[i]
		var proxy = snapshot.events[i]//snapshot.gameEventProxiesRef[id]
	
		var ctor = this.common.gameEventConstructors.getConstructor(proxy[0])
		var newEvent = new ctor()
		//newEvent.id = proxy[0]
		newEvent.n_type = ctor.name
		//newEvent.x = proxy[2]
		//newEvent.y = proxy[3]
		newEvent.targetId = proxy[3]

		this.emitter.emit('createEvent', newEvent)
	}
}

EntityInterpolator.prototype.createMessages = function(snapshot) {
	// createEvent
	for (var i = 0; i < snapshot.messages.length; i++) {
		var proxy = snapshot.messages[i]
	
		var ctor = this.common.messageConstructors.getConstructor(proxy[0])
		var message = new ctor()
		//newEvent.id = proxy[0]
		message.n_type = ctor.name
		//newEvent.x = proxy[2]
		//newEvent.y = proxy[3]
		//console.log('deproxify', proxy, message.netSchema)
		BinarySerializer.deproxify2(proxy, message)
		//console.log('resulting message', message)

		//message.targetId = proxy[3]

		this.emitter.emit('createMessage', message)
	}
}


/**
* Interpolates a position between snapshotA and snapshotB for every entity based
* on renderTime
* Returns nothing, emits 'updateEntity' { id: id, x: interpX, y: interpY }
*
* @param {Object} snapshotA The snapshot of entities before renderTime
* @param {Object} snapshotB The snapshot of entities after renderTime
* @param {Integer} renderTime A time in milliseconds inbetween snapshots A and B
* @emit {Object} updateEntity Emits 'updateEntity', { 
*	id: {Integer} id The id of the entity interpolated, 
*   x: {Number} x A new x position, 
*   y: {Number} Y A new y position
* }
*/
EntityInterpolator.prototype.interpolateSnapshots = function(snapshotA, snapshotB, renderTime) {
	// length in milliseconds of the time between snapshotA and snapshotB
	// this number is usually exactly the length of a tick, but can vary minorly
	var total = snapshotB.timestamp - snapshotA.timestamp

	// how long our target renderTime occured after snapshotA in milliseconds
	var portion = renderTime - snapshotA.timestamp
	
	// relative distance between snapshots represented by our renderTime (e.g.
    // 0.5 would mean halfway between snapshotA and snapshotB)
	var ratio = portion / total

	for (var i = 0; i < snapshotA.gameObjectProxies.length; i++) {
		var id = snapshotA.gameObjectProxies[i][0]
		var entityA = snapshotA.getGameObjectProxy(id)
		var entityB = snapshotB.getGameObjectProxy(id)
		
		// can only interpolate for entities that exist in both snapshots, 
		// this will exclude entities freshly created or deleted 
		if (entityA && entityB) {
			// lerp each position

			var netSchema = this.common.entityConstructors.getNetSchema(entityA[1])
			var interpResult = this.interpolateEntity(entityA, entityB, ratio)
			//console.log('interpResult', interpResult)
		    this.emitter.emit('updateEntity', id, interpResult)
		}
	}
}


EntityInterpolator.prototype.interpolateEntity = function(stateA, stateB, ratio) {
	var netSchema = this.common.entityConstructors.getNetSchema(stateA[1])


	var interpolatedArr = []

	//var interpolated = {}
	//interpolated['id'] = stateA[0]

	// skip i = 0 (id), skip i = 1 (type)
	for (var i = 2; i < netSchema.keys.length; i++) {


		// the name of the property
		var key = netSchema.keys[i]
		var propertyData = netSchema.properties[key]



		// values of the property at two positions in time
		var valueA = stateA[i]
		var valueB = stateB[i]

		// for the properties that skip interpolation
		var interpEnabledForThisProperty = propertyData.interp
		if (!interpEnabledForThisProperty) {
			interpValue = valueA
			interpolatedArr.push({ property: key, value: interpValue })
			// skip the rest of this iteration
			continue
		}
		//console.log('key', key, valueA)
		//console.log('interpolating', key, valueA, valueB)
		// skip interpolation if the values are the same
		if (valueA === valueB) {
			// skip the rest of this iteration
			continue
		}

		var binaryTypeSpec = Binary[propertyData.binaryType]
		//console.log('binaryTypeSpec', binaryTypeSpec)

		var interpValue = valueA

		if (binaryTypeSpec.interp && binaryTypeSpec.interp === 'lerp') {
			interpValue = MathEx.lerp(valueA, valueB, ratio)
		}

		if (binaryTypeSpec.interp && binaryTypeSpec.interp === 'byteRotationLerp') {

			interpValue = MathEx.interpolateRotation(valueA, valueB, ratio)
			//console.log('interp rotation', valueA, valueB, ratio, 'result', interpValue)
		}


		interpolatedArr.push({ property: key, value: interpValue })


	}
	return interpolatedArr
}

EntityInterpolator.prototype.update = function() {
	if (!this.isEnabled) {
		this.skipInterpolation()
		return
	}

	var now = Date.now()
	var renderTime = now - 100

	var snapshots = this.findSnapshots(renderTime)

	var snapshotA = snapshots.snapshotA
	var snapshotB = snapshots.snapshotB

	// if there was at least a snapshotA, emit all of its create/delete events
	if (snapshotA) {
		//console.log('processing snapshotA', snapshotA.tick, this.lastProcessed)
		if (snapshotA.tick === this.lastProcessed) {
			return
		} else if (snapshotA.tick > this.lastProcessed) {
			var diff = snapshotA.tick - this.lastProcessed
			//console.log('idff', diff)
			if (diff === 1) {

				if (!snapshotA.isLoaded) {
					this.handleCreatesAndDeletes(this.registrar.snapshots[snapshotA.tick])

					
					snapshotA.isLoaded = true
				}

				//this.lastProcessed = snapshotA.tick
			} else {
				//console.log('BEHIND', diff, 'catch up!')

				for (var i = diff-1; i >= 0; i--) {
					var tick = snapshotA.tick - i
					//console.log('catching up on', tick)
					if (!this.registrar.snapshots[tick].isLoaded) {
						this.handleCreatesAndDeletes(this.registrar.snapshots[tick])
						this.registrar.snapshots[tick].isLoaded = true
						this.lastProcessed = tick
					}

				}
				//console.log('CAUGHT UP')
			}
			

			/*
			for (var i = diff; i > 0; i++) {
				var tick = this.lastProcessed - i




				console.log('processing', tick)
				if (this.registrar.snapshots[tick])
				this.handleCreatesAndDeletes(this.registrar.snapshots[tick])
			}
			console.log('CAUGHT UP')
			*/
		} else {

		}


	}

	// if there both snapshots exist, it is interpolation time!
	if (snapshotA && snapshotB) {
		this.interpolateSnapshots(snapshotA, snapshotB, renderTime)
	} else {
		//console.log('could not interpolate')
		// there is where extrapolation code could go
	}

	var snapshot = this.registrar.snapshots[this.registrar.mostRecentTick]
	if (snapshot && !snapshot.loaded) {
		this.createEvents(snapshot)
		this.createMessages(snapshot)
		snapshot.loaded = true
	}
}


EntityInterpolator.prototype.skipInterpolation = function() {
	var snapshot = this.registrar.snapshots[this.registrar.mostRecentTick]

	if (snapshot) {
		this.handleCreatesAndDeletes(snapshot)
		this.createEvents(snapshot)
		this.createMessages(snapshot)
		snapshot.loaded = true

		for (var i = 0; i < snapshot.gameObjectProxies.length; i++) {
			var proxy = snapshot.gameObjectProxies[i]

			this.emitter.emit('updateEntity', {
				id: proxy[0],
				x: proxy[2],
				y: proxy[3]
			})
		}
	}
}

module.exports = EntityInterpolator
},{"../MathEx":29,"../binary/Binary":33,"../binary/BinarySerializer":34,"../binary/BinaryType":35}],39:[function(require,module,exports){
var createSchema = require('../binary/BinarySerializer').createSchema
var BinaryType = require('../binary/BinaryType')
var Message = require('../game/Message')

// should this be in a differnet folder?
function IdentityMessage() {
	Message.call(this)
	this.identity = -1
}

IdentityMessage.prototype = Object.create(Message.prototype)
IdentityMessage.prototype.constructor = IdentityMessage

// specify which properties should be networked, and their value types
IdentityMessage.prototype.netSchema = createSchema({
	'id': BinaryType.UInt16, // this type should come from the config
	'type': BinaryType.UInt8,
	'identity': BinaryType.EntityId
})

module.exports = IdentityMessage


},{"../binary/BinarySerializer":34,"../binary/BinaryType":35,"../game/Message":49}],40:[function(require,module,exports){
var createSchema = require('../binary/BinarySerializer').createSchema
var BinaryType = require('../binary/BinaryType')

function InputFrame() {
	this.id = -1
	this.tick = -1
}

InputFrame.prototype.constructor = InputFrame

// specify which properties should be networked, and their value types
InputFrame.prototype.netSchema = createSchema({
	'id': BinaryType.UInt8,
	'type': BinaryType.UInt8,
	'tick': BinaryType.UInt8
})

module.exports = InputFrame


},{"../binary/BinarySerializer":34,"../binary/BinaryType":35}],41:[function(require,module,exports){
var createSchema = require('../binary/BinarySerializer').createSchema
var BinaryType = require('../binary/BinaryType')
var Message = require('../game/Message')

// should this be in a differnet folder?
function PingMessage() {
	Message.call(this)
	this.ping = -1
}

PingMessage.prototype = Object.create(Message.prototype)
PingMessage.prototype.constructor = PingMessage

// specify which properties should be networked, and their value types
PingMessage.prototype.netSchema = createSchema({
	'id': BinaryType.UInt16, // this type should come from the config
	'type': BinaryType.UInt8,
	'ping': BinaryType.UInt16
})

module.exports = PingMessage


},{"../binary/BinarySerializer":34,"../binary/BinaryType":35,"../game/Message":49}],42:[function(require,module,exports){
var Snapshot = require('./Snapshot')
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')
/*
* A record of game state sent from the server
*/
function Registrar(common) {
	this.common = common
	this.snapshots = {}
	this.mostRecentTick = -1
	this.lastConfirmedTick = -1
	this.ping = 0



    this.entities = []
    this.entitiesRef = {}

    this.gameEvents = []
    this.gameEventsRef = {}
    //this.newGameEvents = []

    this.directMessages = []
    this.directMessagesRef = {}

	// debug representation of serverside quadtrees+aabbs
	this.aabbs = []
}


Registrar.prototype.createEntity = function(proxy) {
	var ctor = this.common.entityConstructors.getConstructor(proxy[1])
	var entity = new ctor()
	entity.id = proxy[0]
	entity.n_type = proxy[1]
	entity.n_constuctorName = ctor.name
	entity.x = proxy[2]
	entity.y = proxy[3]

	BinarySerializer.deproxify(proxy, entity)

    entity.instance = this
    this.entities.push(entity)
    this.entitiesRef[entity.id] = entity
    return entity
}





Registrar.prototype.addMessage = function(proxy) {
	this.snapshots[this.mostRecentTick].addMessage(proxy)
}

Registrar.prototype.addGameObjectProxy = function(proxy) {
	console.log('add whole proxy')
	this.snapshots[this.mostRecentTick].addGameObjectProxy(proxy)
}

Registrar.prototype.addGameEventProxy = function(proxy) {
	//console.log('add event')
	this.snapshots[this.mostRecentTick].addGameEventProxy(proxy)
}

Registrar.prototype.updateGameObjectProxy = function(id, prop, value) {
	this.snapshots[this.mostRecentTick].updateGameObjectProxy(id, prop, value)
}

Registrar.prototype.updateProxySimple = function(id, index, value) {
	this.snapshots[this.mostRecentTick].updateProxySimple(id, index, value)
}

Registrar.prototype.updateProxyDeltaSimple = function(id, prop, value) {
	this.snapshots[this.mostRecentTick].updateProxyDeltaSimple(id, prop, value)
}

Registrar.prototype.updateGameObjectProxyDelta = function(id, prop, value) {
	this.snapshots[this.mostRecentTick].updateGameObjectProxyDelta(id, prop, value)
}

Registrar.prototype.forget = function(id) {
	this.snapshots[this.mostRecentTick].forget(id)
}

Registrar.prototype.getGameObjectProxy = function(id, tick) {

	if (this.snapshots[tick]) {
		var snapshot = this.snapshots[tick]

		if (snapshot.gameObjectsProxiesRef[id]) 
			return snapshot.gameObjectsProxiesRef[id]
	}
	return null
}



Registrar.prototype.getGameEventProxy = function(id, tick) {
	if (this.snapshots[tick]) {
		var snapshot = this.snapshots[tick]

		if (snapshot.gameEventProxiesRef[id]) 
			return snapshot.gameEventProxiesRef[id]
	}
	return null
}


Registrar.prototype.getRecentGameEvents = function() {
	if (this.snapshots[this.mostRecentTick]) {
		var snapshot = this.snapshots[this.mostRecentTick] 
		return snapshot.gameEventProxies
	}
	return null
}

Registrar.prototype.getSnapshot = function(tick) {
	if (this.snapshots[tick]) {
		return this.snapshots[tick]
	}
	return null
}

Registrar.prototype.markNew = function(id) {
	this.snapshots[this.mostRecentTick].markNew(id)
}

Registrar.prototype.begin = function(tick) {
	this.mostRecentTick = tick
	var now = Date.now()

	var tickToClone = tick - 1
	if (tickToClone === -1) {
		tickToClone = 255
	}

	if (this.snapshots[tickToClone]) {
		var previous = this.snapshots[tickToClone]
		var clone = previous.clone()
		this.snapshots[tick] = clone
	} else {
		this.snapshots[tick] = new Snapshot()
	}
	
	this.snapshots[tick].tick = tick
	this.snapshots[tick].timestamp = now
}

Registrar.prototype.setLastConfirmedTick = function(lastConfirmedTick) {
	this.snapshots[this.mostRecentTick].confirmed = lastConfirmedTick
	this.lastConfirmedTick = lastConfirmedTick
}

module.exports = Registrar
},{"../binary/Binary":33,"../binary/BinarySerializer":34,"../binary/BinaryType":35,"./Snapshot":43}],43:[function(require,module,exports){
//var Shared = require('../core/Shared')

function Snapshot() {
	this.tick = -1
	this.confirmed = -1
	this.gameObjectsProxiesRef = {}
	this.gameObjectProxies = []
	
	//this.eventsRef = {}
	this.events = []

	this.messages = []
	//this.messagesRef = []

	this.newly = []
	this.forgets = []
	//this.events = {}

	this.loaded = false
}

Snapshot.prototype.clone = function() {
	var clonedSnapshop = new Snapshot()
	clonedSnapshop.tick = this.tick
	for (var i = 0; i < this.gameObjectProxies.length; i++) {
		var proxy = this.gameObjectProxies[i]
		var clonedProxy = proxy.slice()
        clonedSnapshop.addGameObjectProxy(clonedProxy)
	}
	return clonedSnapshop
}

Snapshot.prototype.forget = function(id) {
	//console.log('forgetting', id)
	this.forgets.push(id)


	delete this.gameObjectsProxiesRef[id]

	var index = -1
	for (var i = 0; i < this.gameObjectProxies.length; i++) {
		if (this.gameObjectProxies[i][0] === id) {
			index = i
			break
		}
	}

	if (index !== -1) {
		this.gameObjectProxies.splice(index, 1)
	}
}

Snapshot.prototype.addMessage = function(proxy) {
	this.messages.push(proxy)	
}


Snapshot.prototype.addGameObjectProxy = function(proxy) {
	var id = proxy[0]
	if (!this.gameObjectsProxiesRef[id]) {
		this.gameObjectsProxiesRef[id] = proxy
		this.gameObjectProxies.push(proxy)
	}
}

Snapshot.prototype.addGameEventProxy = function(proxy) {
	this.events.push(proxy)
}

Snapshot.prototype.markNew = function(id) {
	this.newly.push(id)
}

/*
Snapshot.prototype.updateGameObjectProxy = function(id, prop, value) {
	var netSchema = Shared.gameObjectCtors.getNetSchema(this.gameObjectsProxiesRef[id][1])
	var index = netSchema.properties[prop].key
	this.gameObjectsProxiesRef[id][index] = value
}
*/

Snapshot.prototype.updateProxySimple = function(id, index, value) {
	this.gameObjectsProxiesRef[id][index] = value
}

Snapshot.prototype.updateProxyDeltaSimple = function(id, index, value) {
	//console.log('updateProxySimple', id, index, value, this.tick, this.gameObjectsProxiesRef)
	this.gameObjectsProxiesRef[id][index] -= value
}

/*
Snapshot.prototype.updateGameObjectProxyDelta = function(id, prop, deltaValue) {
	var netSchema = Shared.gameObjectCtors.getNetSchema(this.gameObjectsProxiesRef[id][1])
	var index = netSchema.properties[prop].key
	this.gameObjectsProxiesRef[id][index] -= deltaValue
}
*/

Snapshot.prototype.getGameObjectProxy = function(id) {
	return this.gameObjectsProxiesRef[id]
}

module.exports = Snapshot
},{}],44:[function(require,module,exports){
'use strict';

/*
The MIT License (MIT)

Copyright (c) 2014 Arnout Kazemier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

https://github.com/primus/eventemitter3
*/

//
// We store our EE objects in a plain object whose properties are event names.
// If `Object.create(null)` is not supported we prefix the event names with a
// `~` to make sure that the built-in object properties are not overridden or
// used as an attack vector.
// We also assume that `Object.create(null)` is available when the event name
// is an ES6 Symbol.
//
var prefix = typeof Object.create !== 'function' ? '~' : false;

/**
 * Representation of a single EventEmitter function.
 *
 * @param {Function} fn Event handler to be called.
 * @param {Mixed} context Context for function execution.
 * @param {Boolean} [once=false] Only emit once
 * @api private
 */
function EE(fn, context, once) {
  this.fn = fn;
  this.context = context;
  this.once = once || false;
}

/**
 * Minimal EventEmitter interface that is molded against the Node.js
 * EventEmitter interface.
 *
 * @constructor
 * @api public
 */
function EventEmitter() { /* Nothing to set */ }

/**
 * Holds the assigned EventEmitters by name.
 *
 * @type {Object}
 * @private
 */
EventEmitter.prototype._events = undefined;

/**
 * Return a list of assigned event listeners.
 *
 * @param {String} event The events that should be listed.
 * @param {Boolean} exists We only need to know if there are listeners.
 * @returns {Array|Boolean}
 * @api public
 */
EventEmitter.prototype.listeners = function listeners(event, exists) {
  var evt = prefix ? prefix + event : event
    , available = this._events && this._events[evt];

  if (exists) return !!available;
  if (!available) return [];
  if (available.fn) return [available.fn];

  for (var i = 0, l = available.length, ee = new Array(l); i < l; i++) {
    ee[i] = available[i].fn;
  }

  return ee;
};

/**
 * Emit an event to all registered event listeners.
 *
 * @param {String} event The name of the event.
 * @returns {Boolean} Indication if we've emitted an event.
 * @api public
 */
EventEmitter.prototype.emit = function emit(event, a1, a2, a3, a4, a5) {
  var evt = prefix ? prefix + event : event;

  if (!this._events || !this._events[evt]) return false;

  var listeners = this._events[evt]
    , len = arguments.length
    , args
    , i;

  if ('function' === typeof listeners.fn) {
    if (listeners.once) this.removeListener(event, listeners.fn, undefined, true);

    switch (len) {
      case 1: return listeners.fn.call(listeners.context), true;
      case 2: return listeners.fn.call(listeners.context, a1), true;
      case 3: return listeners.fn.call(listeners.context, a1, a2), true;
      case 4: return listeners.fn.call(listeners.context, a1, a2, a3), true;
      case 5: return listeners.fn.call(listeners.context, a1, a2, a3, a4), true;
      case 6: return listeners.fn.call(listeners.context, a1, a2, a3, a4, a5), true;
    }

    for (i = 1, args = new Array(len -1); i < len; i++) {
      args[i - 1] = arguments[i];
    }

    listeners.fn.apply(listeners.context, args);
  } else {
    var length = listeners.length
      , j;

    for (i = 0; i < length; i++) {
      if (listeners[i].once) this.removeListener(event, listeners[i].fn, undefined, true);

      switch (len) {
        case 1: listeners[i].fn.call(listeners[i].context); break;
        case 2: listeners[i].fn.call(listeners[i].context, a1); break;
        case 3: listeners[i].fn.call(listeners[i].context, a1, a2); break;
        default:
          if (!args) for (j = 1, args = new Array(len -1); j < len; j++) {
            args[j - 1] = arguments[j];
          }

          listeners[i].fn.apply(listeners[i].context, args);
      }
    }
  }

  return true;
};

/**
 * Register a new EventListener for the given event.
 *
 * @param {String} event Name of the event.
 * @param {Function} fn Callback function.
 * @param {Mixed} [context=this] The context of the function.
 * @api public
 */
EventEmitter.prototype.on = function on(event, fn, context) {
  var listener = new EE(fn, context || this)
    , evt = prefix ? prefix + event : event;

  if (!this._events) this._events = prefix ? {} : Object.create(null);
  if (!this._events[evt]) this._events[evt] = listener;
  else {
    if (!this._events[evt].fn) this._events[evt].push(listener);
    else this._events[evt] = [
      this._events[evt], listener
    ];
  }

  return this;
};

/**
 * Add an EventListener that's only called once.
 *
 * @param {String} event Name of the event.
 * @param {Function} fn Callback function.
 * @param {Mixed} [context=this] The context of the function.
 * @api public
 */
EventEmitter.prototype.once = function once(event, fn, context) {
  var listener = new EE(fn, context || this, true)
    , evt = prefix ? prefix + event : event;

  if (!this._events) this._events = prefix ? {} : Object.create(null);
  if (!this._events[evt]) this._events[evt] = listener;
  else {
    if (!this._events[evt].fn) this._events[evt].push(listener);
    else this._events[evt] = [
      this._events[evt], listener
    ];
  }

  return this;
};

/**
 * Remove event listeners.
 *
 * @param {String} event The event we want to remove.
 * @param {Function} fn The listener that we need to find.
 * @param {Mixed} context Only remove listeners matching this context.
 * @param {Boolean} once Only remove once listeners.
 * @api public
 */
EventEmitter.prototype.removeListener = function removeListener(event, fn, context, once) {
  var evt = prefix ? prefix + event : event;

  if (!this._events || !this._events[evt]) return this;

  var listeners = this._events[evt]
    , events = [];

  if (fn) {
    if (listeners.fn) {
      if (
           listeners.fn !== fn
        || (once && !listeners.once)
        || (context && listeners.context !== context)
      ) {
        events.push(listeners);
      }
    } else {
      for (var i = 0, length = listeners.length; i < length; i++) {
        if (
             listeners[i].fn !== fn
          || (once && !listeners[i].once)
          || (context && listeners[i].context !== context)
        ) {
          events.push(listeners[i]);
        }
      }
    }
  }

  //
  // Reset the array, or remove it completely if we have no more listeners.
  //
  if (events.length) {
    this._events[evt] = events.length === 1 ? events[0] : events;
  } else {
    delete this._events[evt];
  }

  return this;
};

/**
 * Remove all listeners or only the listeners for the specified event.
 *
 * @param {String} event The event want to remove all listeners for.
 * @api public
 */
EventEmitter.prototype.removeAllListeners = function removeAllListeners(event) {
  if (!this._events) return this;

  if (event) delete this._events[prefix ? prefix + event : event];
  else this._events = prefix ? {} : Object.create(null);

  return this;
};

//
// Alias methods names because people roll like that.
//
EventEmitter.prototype.off = EventEmitter.prototype.removeListener;
EventEmitter.prototype.addListener = EventEmitter.prototype.on;

//
// This function doesn't apply anymore.
//
EventEmitter.prototype.setMaxListeners = function setMaxListeners() {
  return this;
};

//
// Expose the prefix.
//
EventEmitter.prefixed = prefix;

//
// Expose the module.
//
if ('undefined' !== typeof module) {
  module.exports = EventEmitter;
}
},{}],45:[function(require,module,exports){

function Component(gameObject, config) {
	this.gameObject = gameObject
	this.position = gameObject.position

	this.initialize()

	if (config) {
		this.configure(config)
	}
}

Component.prototype.constructor = Component


Component.prototype.setup = function(gameObject) {
	this.gameObject = gameObject
	this.position = gameObject.position
}


Component.prototype.initialize = function() {

}

/*
* Uses an object to set values within the gameObject
* Accepts x,y,rotation without an explicit position. An explicit position ref
* is allowed as well ( config.position )
* @method configure
* @param {Object} config Key-value-pair to copy into gameObject
*/
Component.prototype.configure = function(config) {
	for (var prop in config) {
		this[prop] = config[prop]
	}
}

/*
* Fetches a fellow component from the parent gameObject. Merely a shortcut for 
* access to other components within component scope.
* @method getComponent
* @return {Component} component
*/
Component.prototype.getComponent = function(name) {
	return this.gameObject.getComponent(name)
}

/*
* Shorthand access to gameObject.position.x via component.x
*/
Object.defineProperty(Component.prototype, 'x', {
	get: function() { return this.position.x },
	set: function(value) { this.position.x = value }
})

/*
* Shorthand access to gameObject.position.y via component.y
*/
Object.defineProperty(Component.prototype, 'y', {
	get: function() { return this.position.y },
	set: function(value) { this.position.y = value }
})

/*
* Shorthand access to gameObject.position.rotation via component.rotation
*/
Object.defineProperty(Component.prototype, 'rotation', {
	get: function() { return this.position.rotation },
	set: function(value) { this.position.rotation = value }
})

module.exports = Component
},{}],46:[function(require,module,exports){

function Constructors(ctors) {
	this.ctors = []
	this.ctorsRef = {}
	this.netSchemas = []
	this.register(ctors)

	this.enumValue = 0
	//Object.freeze(this)
}

Constructors.prototype.register = function(ctors) {
	for (var i = 0; i < ctors.length; i++) {
		var ctor = ctors[i]

		// new
		//ctor.prototype.type = this.enumValue++


		this.ctors.push(ctor)
		this.ctorsRef[ctor.name] = i
		var obj = new ctor()
		this.netSchemas.push(obj.netSchema)
	}
}

Constructors.prototype.getConstructor = function(key) {
	return this.ctors[key]
}

Constructors.prototype.getNetSchema = function(key) {
	return this.netSchemas[key]
}

Constructors.prototype.getConstructorKeyByName = function(ctorName) {
	return this.ctorsRef[ctorName]
}

module.exports = Constructors
},{}],47:[function(require,module,exports){
var Vector2 = require('../Vector2')
//var shared = require('../../core/Shared')

function Entity(config) {
	this.initialize()

	if (config) {
		this.configure(config)
	}


	//this.type = this.constructor.prototype.type
	//console.log('ENTITY TYPE', this.type, this.constructor.name)
}

Entity.prototype.constructor = Entity

Entity.prototype.initialize = function() {
	this.id = null

	this.position = new Vector2(0, 0)

	this.state = null

	/* components for iteration */
	this.components = []
	/* components are also added to the gameObject directly as properties
	* e.g.
	* this.bodyCollider = colliderComponent
	*/


}

/*
* Uses an object to set values within the gameObject
* Accepts x,y without an explicit position. An explicit position ref
* is allowed as well ( pass in config.position )
* @method configure
* @param {Object} config Key-value-pair to copy into gameObject
*/
Entity.prototype.configure = function(config) {
	for (var prop in config) {		
		if (prop === 'x' || prop === 'y') {
			// adding: x, y 
			this.position[prop] = config[prop]
		} else if (prop === 'position') {
			// adding: predefined position
			this['position'] = config['position']
		} else {
			// adding: component
			this.addComponent(config[prop])
		}
	}
}

/*
* Adds a component to the gameObject. The gameObject may only have one component
* with a given name. To add two of the same type of component to the gameObject 
* one of the components must be renamed, e.g. collider.name = 'specialCollider'
* @method addComponent
* @param {GameComponent} component
*/
Entity.prototype.addComponent = function(name, component) {
	if (!this[name]) {
		this[name] = component
		this.components.push(component)
	} else {
		throw 'Cannot add same component to gameObject twice.'
	}
}

/*
* Removes a component from the gameObject by name
* @method removeComponent
* @param {String} name
*/
Entity.prototype.removeComponent = function(name) {
	if (this[component.name]) {
		var removeIndex = -1
		for (var i = 0; i < this.components.length; i++) {
			if (this.components[i].name === component.name) {
				removeIndex = i
			}
		}
		if (removeIndex !== -1) {
			this.components.splice(removeIndex, 1)
			delete this[component.name]
		} else {
			throw 'Failed to remove component.'
		}
	}
}

/*
* Returns a component by name
* @method getComponent
* @param {String} name
* @return {GameComponent}
*/
Entity.prototype.getComponent = function(name) {
	if (this[name]) {
		return this[name]
	} else {
		throw 'Could not get component.'
	}
}
/* not implemented nor used: changeComponent */

/*
* Shorthand access to gameObject.position.x via gameObject.x
*/
Object.defineProperty(Entity.prototype, 'x', {
	get: function() { return this.position.x },
	set: function(value) { this.position.x = value }
})

/*
* Shorthand access to gameObject.position.y via gameObject.y
*/
Object.defineProperty(Entity.prototype, 'y', {
	get: function() { return this.position.y },
	set: function(value) { this.position.y = value }
})

/*
* Shorthand access to gameObject.position.rotation via gameObject.rotation
*/
Object.defineProperty(Entity.prototype, 'rotation', {
	get: function() { return this.position.rotation },
	set: function(value) { this.position.rotation = value }
})

Entity.prototype._update = function(delta, tick, now) {
	if (this.update) {
		this.update(delta, tick, now)
	}
	for (var i = 0; i < this.components.length; i++) {
		var component = this.components[i]
		if (component.update) {
			component.update(delta, tick, now)
		}
	}
}

module.exports = Entity
},{"../Vector2":32}],48:[function(require,module,exports){
var Vector2 = require('../Vector2')


function Event(now) {
	this.initialize(now)

	//if (config) {
	//	this.configure(config)
	//}
}

Event.prototype.constructor = Event

Event.prototype.initialize = function(now) {
	this.created = now
	this.position = new Vector2(0, 0)
	this.state = null
}

/*
* Shorthand access to gameEvent.position.x via gameEvent.x
*/
Object.defineProperty(Event.prototype, 'x', {
	get: function() { return this.position.x },
	set: function(value) { this.position.x = value }
})

/*
* Shorthand access to gameEvent.position.y via gameEvent.y
*/
Object.defineProperty(Event.prototype, 'y', {
	get: function() { return this.position.y },
	set: function(value) { this.position.y = value }
})

Event.prototype.update = function(delta, tick, now) {
	// to be overriden by descendents
}

module.exports = Event
},{"../Vector2":32}],49:[function(require,module,exports){


function Message() {
	
}

Message.prototype.constructor = Message

module.exports = Message
},{}],50:[function(require,module,exports){
/**
* Please see the NENGI END USER LICENSE available in the LICENSE.txt file
* By downloading or using this software or related content you are agreeing to 
* be bounds by the terms of the NENGI END USER LICENSE Agreement. If you do not 
* or cannot agree to the terms of the Agreement please do not download or use 
* this software.
*/
var nengi = {}

//nengi.config = require('./config')
/* Rather than add nengi to all of the classes, they are just wired together 
* here. This saves the trouble of typing nengi.something from within the lib
* itself. External usage should always be nengi.something */

var BinaryType = require('./binary/BinaryType')
// shortcuts for less typing
nengi.Boolean   = BinaryType.Boolean
nengi.Int2      = BinaryType.Int2
nengi.UInt2     = BinaryType.UInt2
nengi.Int3      = BinaryType.Int3
nengi.UInt3     = BinaryType.UInt3
nengi.Int4      = BinaryType.Int4
nengi.UInt4     = BinaryType.UInt4
nengi.Int6      = BinaryType.Int6
nengi.UInt6     = BinaryType.UInt6
nengi.Int8      = BinaryType.Int8
nengi.UInt8     = BinaryType.UInt8
nengi.Int10     = BinaryType.Int10
nengi.UInt10    = BinaryType.UInt10
nengi.Int12     = BinaryType.Int12
nengi.UInt12    = BinaryType.UInt12
nengi.Int16     = BinaryType.Int16
nengi.UInt16    = BinaryType.UInt16
nengi.Int32     = BinaryType.Int32
nengi.UInt32    = BinaryType.UInt32
nengi.String    = BinaryType.String
nengi.EntityId  = BinaryType.EntityId
nengi.Rotation  = BinaryType.Rotation

//nengi.ID_BINARYTYPE = nengi.config.ID_BINARYTYPE
//nengi.TYPE_BINARYTYPE = nengi.config.TYPE_BINARYTYPE

nengi.Constructors = require('./game/Constructors')

nengi.MessageType = require('./MessageType')

nengi.Entity = require('./game/Entity')
nengi.Component = require('./game/Component')
nengi.Event = require('./game/Event')
nengi.Message = require('./game/Message')
nengi.Binary = require('./binary/Binary')
nengi.createEntitySchema = require('./binary/BinarySerializer').createEntitySchema
nengi.createSchema = require('./binary/BinarySerializer').createSchema
//nengi.NetSchema = require('./NetSchema')

nengi.Vector2 = require('./Vector2')
nengi.MathEx = require('./MathEx')

nengi.IdentityMessage = require('./client/IdentityMessage')
nengi.PingMessage = require('./client/PingMessage')
nengi.InputFrame = require('./client/InputFrame')

//nengi.Historian = require('./Historian')
//nengi.Quadtree = require('./Quadtree')
//nengi.AABB = require('./AABB')

//nengi.NetObject = require('./NetObject')
//nengi.NetObjectCache = require('./NetObjectCache')

nengi.Instance = require('./server/Instance')
nengi.Client = require('./server/Client')

//nengi.IdentityEvent = require('./network/IdentityEvent')

nengi.Application = require('./client/Application')




// hacky way of excluding node.js specific code from the being built into the
// browser version of nengi
if (typeof window === 'undefined') {
    nengi.NodeLoop = require('./NodeLoop')
}


module.exports = nengi
},{"./MathEx":29,"./MessageType":30,"./NodeLoop":31,"./Vector2":32,"./binary/Binary":33,"./binary/BinarySerializer":34,"./binary/BinaryType":35,"./client/Application":37,"./client/IdentityMessage":39,"./client/InputFrame":40,"./client/PingMessage":41,"./game/Component":45,"./game/Constructors":46,"./game/Entity":47,"./game/Event":48,"./game/Message":49,"./server/Client":64,"./server/Instance":69}],51:[function(require,module,exports){

var BitBuffer = require('../binary/BitBuffer')
var MessageType = require('../MessageType')
var EntityNetworker = require('./entity/EntityNetworker')
var ForgetObjectNetworker = require('./entity/ForgetObjectNetworker')
var MicroOptimizationNetworker = require('./entity/MicroOptimizationNetworker')
var PartialGameObjectNetworker = require('./entity/PartialGameObjectNetworker')
var GameEventNetworker = require('./event/GameEventNetworker')
var DirectMessageNetworker = require('./message/DirectMessageNetworker')
var QuadtreeAABBNetworker = require('./debug/QuadtreeAABBNetworker')
var PingCheckNetworker = require('./PingCheckNetworker')

var Snapshot = require('../client/Snapshot')

function GameStateReader(registrar, common) {
	this.registrar = registrar
	this.common = common

	this.quadtreeAABBNetworker = new QuadtreeAABBNetworker(common)
	this.entityNetworker = new EntityNetworker(common)
	this.gameEventNetworker = new GameEventNetworker(common)
	this.forgetObjectNetworker = new ForgetObjectNetworker(common)
	this.microOptimizationNetworker = new MicroOptimizationNetworker(common)
	this.partialGameObjectNetworker = new PartialGameObjectNetworker(common)
	this.directMessageNetworker = new DirectMessageNetworker(common)
	this.pingCheckNetworker = new PingCheckNetworker(common)
}

GameStateReader.prototype.readBinary = function(data, websocket) {
	var registrar = this.registrar
	var bitBuffer = new BitBuffer(data)
	var offset = 0

	//console.log('DATA', data)

	var sv_lastProcessedTick = bitBuffer.readUInt8(offset)
	offset += 8

	registrar.setLastConfirmedTick(sv_lastProcessedTick)

	//var snapshot = new Snapshot()
	//snapshot.confirmed = sv_lastProcessedTick

	if (bitBuffer.bitLength >= 8) {
		var msgType = bitBuffer.readUInt8(offset)
	}

	while (offset < bitBuffer.bitLength && bitBuffer.bitLength - offset >= 8) {		
		var msgType = bitBuffer.readUInt8(offset)
		offset += 8

		if (msgType === MessageType.Quadtrees) {
			offset = this.quadtreeAABBNetworker.read(bitBuffer, offset, registrar)
		}
		
		if (msgType === MessageType.Ping) {
			offset = this.pingCheckNetworker.readPingAndReplyWithPong(bitBuffer, offset, websocket, registrar)
		}
		
		if (msgType === MessageType.DirectMessages) {
			offset = this.directMessageNetworker.read(bitBuffer, offset, null, registrar)
		}		

		if (msgType === MessageType.ForgetObjects) {
			offset = this.forgetObjectNetworker.read(bitBuffer, offset, registrar)
		}

		if (msgType === MessageType.GameObjects) {
			offset = this.entityNetworker.read(bitBuffer, offset, registrar)
		}

		if (msgType === MessageType.GameEvents) {
			offset = this.gameEventNetworker.read(bitBuffer, offset, null, registrar)
		}

		if (msgType === MessageType.GameObjectOptimizations) {
			offset = this.microOptimizationNetworker.read(bitBuffer, offset, registrar)
		}

		if (msgType === MessageType.PartialGameObjects) {
			offset = this.partialGameObjectNetworker.read(bitBuffer, offset, registrar)
		}	
	}
}

module.exports = GameStateReader
},{"../MessageType":30,"../binary/BitBuffer":36,"../client/Snapshot":43,"./PingCheckNetworker":53,"./debug/QuadtreeAABBNetworker":55,"./entity/EntityNetworker":56,"./entity/ForgetObjectNetworker":57,"./entity/MicroOptimizationNetworker":58,"./entity/PartialGameObjectNetworker":59,"./event/GameEventNetworker":60,"./message/DirectMessageNetworker":61}],52:[function(require,module,exports){
var BitBuffer = require('../binary/BitBuffer')
var Binary = require('../binary/Binary')
var EntityNetworker = require('./entity/EntityNetworker')
var PartialGameObjectNetworker = require('./entity/PartialGameObjectNetworker')
var MicroOptimizationNetworker = require('./entity/MicroOptimizationNetworker')
var ForgetObjectNetworker = require('./entity/ForgetObjectNetworker')
var GameEventNetworker = require('./event/GameEventNetworker')
var QuadtreeAABBNetworker = require('./debug/QuadtreeAABBNetworker')
var PingCheckNetworker = require('./PingCheckNetworker')
var DirectMessageNetworker = require('./message/DirectMessageNetworker')

function GameStateWriter(netObjectCache, common) {
	this.netObjectCache = netObjectCache
    this.entityNetworker = new EntityNetworker(common)
    this.partialEntityNetworker = new PartialGameObjectNetworker(common)
    this.microOptimizationNetworker = new MicroOptimizationNetworker(common)
    this.forgetObjectNetworker = new ForgetObjectNetworker(common)
    this.gameEventNetworker = new GameEventNetworker(common)
    this.quadtreeAABBNetworker = new QuadtreeAABBNetworker(common)
    this.pingCheckNetworker = new PingCheckNetworker(common)
    this.directMessageNetworker = new DirectMessageNetworker(common)
    this.debugShowQuadtree = false
}


/**
* Creates a buffer containing a snapshot of the game state
*
* @method
* @param {Client} client The client receiving this data
* @param {Object} visibilty Visibility changes for this client
* @param {Integer} tick The tick of the game simulation
* @param {Array} aabbs An array of AABBs showing the server side quadtrees for debug purposes
* @returns {Buffer}
*/
GameStateWriter.prototype.writeBuffer = function(client, visibility, tick, aabbs) {
	var forget = visibility.noLongerVisible
    var whole = visibility.newlyVisible
    var opt = []
    var partial = []

    // using visibility data sort the entity/event data into 4 categories:
    //  forget: ids that the this client should delete
    //  whole: whole game objects to send to this client
    //  opt: microoptmized changes to game objects already tracked by the client
    //  partial: small changes to game objects already tracked yb the client
    this.chooseOptOrPartial(client, visibility, whole, opt, partial, tick)

    // events are simply sent in their entirety, note that visible events are
    // events that have positional significant (occured near the player)
    var events = visibility.events

    var messageIds = []
    for (var i = 0; i < client.messages.length; i++) {
        messageIds.push(client.messages[i].id)
    }
    client.messages = []


    var isTimeToCheckPing = client.needsPingCheck()

    var arr = []
    // build and send network data
    var bitBuffers = []

    /* COUNTING THE BUFFER LENGTH IN BITS! */
    var quadtreeBits = 0
    if (this.debugShowQuadtree) {
    	quadtreeBits = this.quadtreeAABBNetworker.countBits(aabbs)  
    }

    var pingCheckBits = (isTimeToCheckPing) 
        ? this.pingCheckNetworker.countBits() : 0
      

    var forgetIdsBits = this.forgetObjectNetworker.countBits(
        visibility.noLongerVisible
    )

    var wholeProxyBits = this.entityNetworker.countBits(
        visibility.newlyVisible, 
        tick, 
        this.netObjectCache
    )

    var gameEventBits = this.gameEventNetworker.countBits(
        events, 
        tick,
        this.netObjectCache
    )

    
    var directMessageBits = this.directMessageNetworker.countBits(
        messageIds,
        tick,
        this.netObjectCache
    )


    var optimizedMovementBits = this.microOptimizationNetworker.countBits(
        opt, //visibility.stillVisible, 
        tick, 
        client,
        this.netObjectCache
    )

    var partialProxyBits = this.partialEntityNetworker.countBits(
        partial, 
        tick, 
        this.netObjectCache
    )

    var offset = 0

    // create a bitBuffer whose length is the bit total
    var bitBuffer = new BitBuffer(
        8 +
        pingCheckBits +
        directMessageBits +
        quadtreeBits +
        forgetIdsBits +
        wholeProxyBits +
        gameEventBits +
        optimizedMovementBits +
        partialProxyBits
    )
    
    
    
    // for debug purposes, log visibilty / bit data 
    /*
    console.log(
        'f>>', forgetIdsBits, 
        'w>>', wholeProxyBits, 
        'p>>', partialProxyBits, 
        'o>>', optimizedMovementBits,
        'total:', forgetIdsBits + wholeProxyBits + partialProxyBits + optimizedMovementBits,
        'forget', visibility.noLongerVisible.length, 
        'new', visibility.newlyVisible.length, 
        'still', visibility.stillVisible.length,
        'part', partial.length,
        'opt', opt.length
    )
*/
    
    
    /* ACTUAL WRITING TO THE BUFFER */
    //console.log('client.lastInputProcessed', client.lastInputProcessed)
    bitBuffer.writeUInt8(client.lastInputProcessed, offset)
    offset += 8

    if (this.debugShowQuadtree) {
	    offset = this.quadtreeAABBNetworker.write(bitBuffer, offset, aabbs)
	}

    
    if (isTimeToCheckPing) {
        offset = this.pingCheckNetworker.write(
            bitBuffer,
            offset,
            client
        )
        client.pingCheckSent()
    }

    


    offset = this.forgetObjectNetworker.write(
        bitBuffer, 
        offset, 
        visibility.noLongerVisible, 
        client
    )

    offset = this.entityNetworker.write(
        bitBuffer, 
        offset, 
        visibility.newlyVisible, 
        tick, 
        client,
        this.netObjectCache
    )

    offset = this.directMessageNetworker.write( 
        bitBuffer,
        offset,
        messageIds,
        tick,
        client,
        this.netObjectCache
    )
    
    offset = this.gameEventNetworker.write(
        bitBuffer,
        offset,
        events,
        tick,
        client,
        this.netObjectCache
    )       

    offset = this.microOptimizationNetworker.write(
        bitBuffer, 
        offset, 
        opt, //visibility.stillVisible,
        tick, 
        client,
        this.netObjectCache
    )

    offset = this.partialEntityNetworker.write(
        bitBuffer, 
        offset, 
        partial, 
        tick, 
        client,
        this.netObjectCache
    )


    //console.log('new', visibility.newlyVisible, 'cont', visibility.stillVisible, 'gone', visibility.noLongerVisible)
    return bitBuffer.toBuffer()
}

GameStateWriter.prototype.processDiffs = function(proxy, cachedProxy, netSchema) {
    var diffs = []
    // start at 2, skipping id and type (which are always the same)
    for (var j = 2; j < proxy.length; j++) {
        var propName = netSchema.keys[j]
        var propData = netSchema.properties[propName]
 
        var value = proxy[j]
        //if (typeof value === 'undefined')
        //    value = 0


        var realValue = proxy[netSchema.properties[propName].key]
        var cachedValue = cachedProxy[netSchema.properties[propName].key]

        var deltaValue = Math.round(cachedValue - realValue)

        if (deltaValue !== 0) {
            //console.log(propName, 'changed', deltaValue)

            diffs.push(propName)
        }
    }
    return diffs
}

GameStateWriter.prototype.countOptimizations = function(proxy, cachedProxy, netSchema) {
    var optimizableCount = 0
    var length = netSchema.optimizations.length
    for (var j = 0; j < length; j++) {
        var optimization = netSchema.optimizations[j]
        var binary = Binary[optimization.binaryType]
        var realValue = proxy[netSchema.properties[optimization.prop].key]
        var cachedValue = cachedProxy[netSchema.properties[optimization.prop].key]

        var deltaValue = Math.round(cachedValue - realValue)

        if (deltaValue >= binary.min && deltaValue <= binary.max) {
            // this particular prop can be optimized
            optimizableCount++
        }
    }
    return optimizableCount
}

GameStateWriter.prototype.partialDiffs = function(optimizableCount, diffs, length, netSchema, opt, id) {
    var remainingDiffs = []
    //if (noChangeCount !== length) {
    if (optimizableCount === length) {
        // all optimizations were valid for this gameObject
        for (var j = 0; j < diffs.length; j++) {                             
            var prop = diffs[j]

            // scan through the list of diffs, and remove any diff
            // that was covered by an optimization
            var wasOpt = false
            for (var k = 0; k < length; k++) {
                var optimization = netSchema.optimizations[k]
                if (optimization.prop === prop) {
                    wasOpt = true
                }
            }

            // the remaining diffs are any changes that were not
            // already part of an optimization
            if (!wasOpt) {
                remainingDiffs.push(prop)
            }
        }
        opt.push(id)
    } else {
        //console.log('nothing changed for gameobject', id)
        remainingDiffs = diffs
    }
    return remainingDiffs
}

/**
* Sorts entities into categories that affect the type of update sent by nengi
* This function and partialDiffs constitute the core algorithm which attempts to
* optimize the representation of an entity. The gist of the algo is:
*  microoptimizations are evaluated first, if an entity's changes only occured to
*    the properties that are 'microoptimized', then the microoptimization
*    representation is chosen, and passed out via opt
*  partial representation of an entity's state are evalulated second, and consist
*    of any properties that are not microoptimized
*
*  It is possible for this analysis to produce both a microoptimized and a partial
*  representation for an object, such as would commonly occur if an entity experienced
*  changes in x,y, and hp (x,y would be microoptimized, hp would be sent seperate)
*   
* @method
*
*/
GameStateWriter.prototype.chooseOptOrPartial = function(client, visibility, whole, opt, partial, tick) {
    for (var i = 0; i < visibility.stillVisible.length; i++) {
        var id = visibility.stillVisible[i]
        // seralized state of the object
        var netObject = this.netObjectCache.getEntity(id, tick)
        // last known state of this particular object known to the client      
        var cachedObject = client.clientState.gameObjects[netObject.id]
        // schema
        var netSchema = netObject.netSchema

        if (netSchema.hasOptimizations) {
            //console.log('herr')
            //var optimizableCount = 0
            var noChangeCount = 0
            var length = netSchema.optimizations.length

            var proxy = netObject.proxy

            var diffs = this.processDiffs(proxy, cachedObject, netSchema)
            var optimizableCount = this.countOptimizations(proxy, cachedObject, netSchema)


            var remainingDiffs = this.partialDiffs(optimizableCount, diffs, length, netSchema, opt, id)

            if (remainingDiffs.length > 0) {
                // if any differences remain that were not part of an opt                                        //}
                partial.push({id:id, diffs: remainingDiffs})

            }

            //console.log('still different', remainingDiffs)
        }
    }
}

module.exports = GameStateWriter
},{"../binary/Binary":33,"../binary/BitBuffer":36,"./PingCheckNetworker":53,"./debug/QuadtreeAABBNetworker":55,"./entity/EntityNetworker":56,"./entity/ForgetObjectNetworker":57,"./entity/MicroOptimizationNetworker":58,"./entity/PartialGameObjectNetworker":59,"./event/GameEventNetworker":60,"./message/DirectMessageNetworker":61}],53:[function(require,module,exports){

var MessageType = require('../MessageType')
var MathEx = require('../MathEx')
var BitBuffer = require('../binary/BitBuffer')
/**
* Writes the ids of objects to forget outgoing network buffer (these are objects
* that have died or moved too far away to still be relevant to a client/player)
*/
function PingCheckNetworker(common) {
    this.common = common
}

PingCheckNetworker.prototype.countBits = function() {
    var bits = 0
    // msgtype
    bits += 8
    // the random pingVerificationNumber
    bits += 32
    // the client's average ping
    bits += 16
    return bits
}


/**
* writes a ping check into buffer, records timestamp and verfication into client
*/
PingCheckNetworker.prototype.write = function(bitBuffer, offset, client) {
    //console.log('write ping check')
    bitBuffer.writeUInt8(MessageType.Ping, offset)
    offset += 8
    // write the number of whole proxies to be sent

    // the actual random 32-bit Integer is not particularly important
    // all that matters is that the client can't spoof its ping, so any random
    // number would probably suffice
    var random = MathEx.random(0, 2147483647)
    bitBuffer.writeUInt32(random, offset)
    offset += 32

    bitBuffer.writeUInt16(client.ping, offset)
    offset += 16

   
    // in truth the ping isn't sent yet, but it'll probaby be sent within the
    // same millisecond as this code
    client.pingTimestamp = Date.now()
    client.pingVerificationNumber = random
    return offset
}


/**
* Clientside reading of ping message, replies immediately
*
*/
PingCheckNetworker.prototype.readPingAndReplyWithPong = function(bitBuffer, offset, websocket, registrar) {
    
   
    var pingVerificationNumber = bitBuffer.readUInt32(offset)
    offset += 32
    var pingResult = bitBuffer.readUInt16(offset)
    offset += 16

    registrar.ping = pingResult
    //console.log('readPingAndReplyWithPong', pingVerificationNumber)
    var responseBuffer = new BitBuffer(56)
    responseBuffer.writeUInt8(MessageType.Pong, 0)
    responseBuffer.writeUInt32(pingVerificationNumber, 8)

    if (websocket && websocket.readyState === 1) { 
        //console.log('WEBSOCKET')        
        websocket.send(responseBuffer.byteArray)
    }

    return offset
}

/**
* Serverside reading of pong, performs ping calculation and stores in client
*/
PingCheckNetworker.prototype.readPong = function(bitBuffer, offset, client) {
    //console.log('readPong')
    var pingVerificationNumber = bitBuffer.readUInt32(offset)
    offset += 32

    if (pingVerificationNumber === client.pingVerificationNumber) {
        var diff = Date.now() - client.pingTimestamp
        //client.ping = diff
        client.setPingResult(diff)
       // console.log('ping calculated:', client.ping)
    } else {
        //console.log('client attempted to spoof ping/pong')
        client.ping = 0
    }
    return offset 

}

module.exports = PingCheckNetworker
},{"../MathEx":29,"../MessageType":30,"../binary/BitBuffer":36}],54:[function(require,module,exports){
var Binary = require('../binary/Binary')

var MessageType = require('../MessageType')

function PlayerInputNetworker(common) {
	this.common = common
}

PlayerInputNetworker.prototype.write = function(bitBuffer, offset, playerInputs) {
	
    var length = playerInputs.length

    bitBuffer.writeUInt8(MessageType.PlayerInputs, offset)
    offset += 8

    bitBuffer.writeUInt8(length, offset)
    offset += 8

	for (var j = 0; j < playerInputs.length; j++) {

		var playerInput = playerInputs[j]
		var netSchema = playerInput.netSchema

		for (var i = 0; i < netSchema.keys.length; i++) {
		    var propName = netSchema.keys[i]
		    var propData = netSchema.properties[propName]
		    var value = playerInput[propName]

		    //console.log('writing', propName, value)
	        bitBuffer[Binary[propData.binaryType].write](value, offset)
        	offset += Binary[propData.binaryType].bits
		}
	}
	return offset
}

PlayerInputNetworker.prototype.read = function(bitBuffer, offset, inputSnapshot) {
	//console.log('PlayerInputNetworker.prototype.read')
	var length = bitBuffer.readUInt8(offset)
	offset += 8
	//console.log('length', length)
	for (var i = 0; i < length; i++) {
		var serialized = []

        var id = bitBuffer.readUInt8(offset)
        offset += 8
        var type = bitBuffer.readUInt8(offset)
        offset += 8

        //console.log('reading input', id, type)
		var netSchema = this.common.inputConstructors.getNetSchema(type)
		var ctor = this.common.inputConstructors.getConstructor(type)

		var playerInput = new ctor()
		playerInput.id = id
		playerInput.type = type

        for (var j = 2; j < netSchema.keys.length; j++) {
            var propName = netSchema.keys[j]
            var propData = netSchema.properties[propName]

            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            playerInput[propName] = value
        }
        //client.instance.emit('playerInput', client, playerInput)
        //client.queueInputFrame(playerInput)

        if (ctor.name === 'InputFrame') {
        	inputSnapshot.tick = playerInput.tick

        	//console.log('INPUTFRAME', playerInput)
        } else {
        	//console.log('REGULARINPUT', playerInput)
        }

        inputSnapshot.add(playerInput)
	}
}

module.exports = PlayerInputNetworker
},{"../MessageType":30,"../binary/Binary":33}],55:[function(require,module,exports){
//var config = require('../../config')
var Binary = require('../../binary/Binary')
var MessageType = require('../../MessageType')

/**
* For debug purposes only!
* Writes quadtree aabbs to outgoing network buffer (bandwidth intensive!)
*/
function QuadtreeAABBNetworker() {

}

QuadtreeAABBNetworker.prototype.countBits = function(aabbs) {
    return (aabbs.length * 4 * 32) + 8 + 32
}

QuadtreeAABBNetworker.prototype.write = function (bitBuffer, offset, aabbs) {
    var count = aabbs.length

    bitBuffer.writeUInt8(MessageType.Quadtrees, offset)
    offset += 8
    bitBuffer.writeUInt32(count, offset)
    offset += 32

    for (var i = 0; i < count; i++) {
        offset = aabbToMessage(bitBuffer, offset, aabbs[i])
    }
    return offset
}

function aabbToMessage(bitBuffer, offset, aabb) {
    bitBuffer.writeInt32(aabb.x, offset)
    offset += 32
    bitBuffer.writeInt32(aabb.y, offset)
    offset += 32
    bitBuffer.writeInt32(aabb.halfWidth, offset)
    offset += 32
    bitBuffer.writeInt32(aabb.halfHeight, offset)
    offset += 32

    return offset
}


QuadtreeAABBNetworker.prototype.read = function(bitBuffer, offset, registrar) {
    var count = bitBuffer.readInt32(offset)
    offset += 32

    var aabbs = []
    for (var i = 0; i < count; i++) {
        var aabb = {}
        aabb.x = bitBuffer.readInt32(offset)
        offset += 32
        aabb.y = bitBuffer.readInt32(offset)
        offset += 32
        aabb.halfWidth = bitBuffer.readInt32(offset)
        offset += 32
        aabb.halfHeight = bitBuffer.readInt32(offset)
        offset += 32
        aabbs.push(aabb)
    }

    registrar.aabbs = aabbs
    return offset
    
}

module.exports = QuadtreeAABBNetworker
},{"../../MessageType":30,"../../binary/Binary":33}],56:[function(require,module,exports){
var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')
var MessageType = require('../../MessageType')
var MathEx = require('../../MathEx')
//var Shared = require('../../core/Shared')

/**
* Writes whole game objects to the outgoing network buffer
*/
function EntityNetworker(common) {
    this.common = common
}

EntityNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    var length = ids.length 
    for (var i = 0; i < length; i++) {

        var netObject = netObjectCache.getEntity(ids[i], tick)
        for (var j = 0; j < netObject.proxy.length; j++) {
            var propName = netObject.netSchema.keys[j]

            if (propName === 'id') {
                bits += this.common.config.ID_BINARYTYPE.bits
            } else {
                var propData = netObject.netSchema.properties[propName]
                bits += Binary[propData.binaryType].bits
            }
        }

    }

    return bits
}


/**
* Writes proxy data (a shallow copy) of game objects to the bitBuffer. The game 
* object's data is taken from the netObjectCache at the specified tick. After
* the data is written to the buffer it is also copied to each client.clientState
*
* @method
* @param {BitBuffer} bitBuffer - BitBuffer to write to
* @param {Integer} offset - position within the BitBuffer to begin writing
* @param {Array} ids - the ids of the gameObjects whose data will be written
* @param {Integer} tick - the step of the game simulation being represented
* @param {Client} client - the client to whom this data will be sent
* @param {NetObjectCache} netObjectCache - a cache of proxy data
* @returns {Integer} Returns the offset incremented by however many bits were written 
*
*/
EntityNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

    var length = ids.length

    // write the message type for GameObjects
    bitBuffer.writeUInt8(MessageType.GameObjects, offset)
    offset += 8

    // write the number of game objects to be transmitted
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) {
       
        // get the game object's data from the netObjectCache @ tick
        var netObject = netObjectCache.getEntity(ids[i], tick) 
        // write the game object (well, its proxy, a shallow copy) to the buffer           
        offset = this.writeWholeProxy(bitBuffer, offset, netObject.proxy, netObject.netSchema, client)        

        // copy the proxy
        var newProxy = []
        for (var j = 0; j < netObject.proxy.length; j++) {
            newProxy.push(netObject.proxy[j])
        }
        // store a copy of the proxy in the clientState
        client.clientState.gameObjects[ids[i]] = newProxy
    }    
    return offset
}


/**
* Writes a single game object in proxy form to the bitBuffer.
*
* @method
* @param {BitBuffer} bitBuffer - BitBuffer to write to
* @param {Integer} offset - position within the BitBuffer to begin writing
* @param {Array} proxy - the game object data as an array
* @param {NetSchema} netSchema - the game object's network schema
* @param {Client} client - the client to whom this game object will be sent
* @returns {Integer} Returns the offset incremented by however many bits were written 
*/
EntityNetworker.prototype.writeWholeProxy = function(bitBuffer, offset, proxy, netSchema, client) {

    // proxy is fundamentally an array of numbers that represents a shallow
    // copy of a game object. At this point we write the proxy to the bitBuffer
   // console.log('writeWholeProxy', proxy)
    for (var i = 0; i < proxy.length; i++) {
        // property name corresponding to this position in the array
        var propName = netSchema.keys[i]
        // network/binary information for this property
        var propData = netSchema.properties[propName]

 
        var value = proxy[i]
        if (typeof value === 'undefined')
            value = 0

        if (propData.binaryType === BinaryType.Rotation) {
            
            value = MathEx.radiansToUInt8(value)
            //console.log('WRITING A ROTATION', value)
        }
        
        if (propName === 'id'){
            // special case if we're writing an id to the buffer
            // id is aliased, to optmize binary size and provide security
            var alias = client.createAliasId(value)
            // write the id to the buffer and increment the offset
            bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
            offset += this.common.config.ID_BINARYTYPE.bits
        } else {
            // normal case, writing data other than an id
            // write the value to the buffer
            bitBuffer[Binary[propData.binaryType].write](value, offset)
            offset += Binary[propData.binaryType].bits
        }

    }
    return offset
}


 EntityNetworker.prototype.read = function(bitBuffer, offset, registrar) {
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    for (var j = 0; j < count; j++) {
        var serialized = []

        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)
        offset += this.common.config.ID_BINARYTYPE.bits
        serialized.push(id)

        var type = bitBuffer.readUInt8(offset)
        offset += 8
        serialized.push(type)

        //var constructor = this.common.gameObjectCtors.getConstructor(type)
        //var newObject = new constructor()
        //console.log('type', type)         

        var netSchema = this.common.entityConstructors.getNetSchema(type)//newObject.netSchema
        for (var i = 2; i < netSchema.keys.length; i++) {
            var propName = netSchema.keys[i]
            //console.log(propName)
            var propData = netSchema.properties[propName]


            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            if (propData.binaryType === BinaryType.Rotation) {
                
                value = MathEx.UInt8ToRadians(value)
                ///console.log('WRITING A ROTATION', value)
            }

            serialized.push(value)
        }
        //console.log(serialized)
        registrar.addGameObjectProxy(serialized)
        registrar.markNew(id)
    }

    return offset
}

module.exports = EntityNetworker
},{"../../MathEx":29,"../../MessageType":30,"../../binary/Binary":33,"../../binary/BinaryType":35}],57:[function(require,module,exports){

var MessageType = require('../../MessageType')
/**
* Writes the ids of objects to forget outgoing network buffer (these are objects
* that have died or moved too far away to still be relevant to a client/player)
*/
function ForgetObjectNetworker(common) {
    this.common = common
}

ForgetObjectNetworker.prototype.countBits = function(ids) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    for (var i = 0; i < ids.length; i++) {
        bits += this.common.config.ID_BINARYTYPE.bits
    }
    return bits
}


ForgetObjectNetworker.prototype.write = function(bitBuffer, offset, ids, client) {
    if (ids.length === 0) return offset

    var length = ids.length
    bitBuffer.writeUInt8(MessageType.ForgetObjects, offset)
    offset += 8
    // write the number of whole proxies to be sent
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    //console.log('ids', ids)
    for (var i = 0; i < length; i++) {
        //if (client.isKnownGameObjectId(ids[i])) {
        var alias = client.getAliasId(ids[i])
        bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
        //console.log('hey, forget about', alias)
        offset += this.common.config.ID_BINARYTYPE.bits
        client.releaseAliasId(alias)
        delete client.clientState.gameObjects[ids[i]]
        //}
    }
    return offset
}


ForgetObjectNetworker.prototype.read = function(bitBuffer, offset, registrar) {

    var count = bitBuffer.readUInt8(offset)
    offset += 8
    //console.log(count, 'readForgets')

    for (var j = 0; j < count; j++) {
        var serialized = []
        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)
        offset += this.common.config.ID_BINARYTYPE.bits
        registrar.forget(id)
    }

    return offset
}

module.exports = ForgetObjectNetworker
},{"../../MessageType":30}],58:[function(require,module,exports){
var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')
var MessageType = require('../../MessageType')

function MicroOptimizationNetworker(common) {
	this.common = common
}

MicroOptimizationNetworker.prototype.countBits = function(ids, tick, clients, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8    

    for (var i = 0; i < ids.length; i++) {
        var netObject = netObjectCache.getEntity(ids[i], tick)
        bits += this.common.config.ID_BINARYTYPE.bits

        for (var j = 0; j < netObject.netSchema.optimizations.length; j++) {
            var optimization = netObject.netSchema.optimizations[j]
            bits += Binary[optimization.binaryType].bits
        }   
    }
    return bits 
}

MicroOptimizationNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

    var length = ids.length

    bitBuffer.writeUInt8(MessageType.GameObjectOptimizations, offset)
    offset += 8

    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) { 
        var netObject = netObjectCache.getEntity(ids[i], tick)
        //console.log('id', ids[i])
        offset = this.writeOptimizedMovement(bitBuffer, offset, netObject, netObject.netSchema, client)
        //console.log('speed', this.getGameObjectById(netObject.id).ai.speed)

    }
    return offset
}

MicroOptimizationNetworker.prototype.writeOptimizedMovement = function(bitBuffer, offset, netObject, netSchema, client) {
    var alias = client.getAliasId(netObject.id)
    bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
    offset += this.common.config.ID_BINARYTYPE.bits

    var cachedObject = client.clientState.gameObjects[netObject.id]

    for (var i = 0; i < netSchema.optimizations.length; i++) {
        var optimization = netSchema.optimizations[i]
        var value = 0
        if (optimization.delta) {
            // delta! calculate delta
            var prop = optimization.prop
            var realValue = netObject.proxy[netSchema.properties[prop].key]
            var cachedValue = cachedObject[netSchema.properties[prop].key]
            var deltaValue = Math.round(cachedValue - realValue)
            // update cache
            cachedObject[netSchema.properties[prop].key] -= deltaValue
            // update client
            bitBuffer[Binary[optimization.binaryType].write](deltaValue, offset)
            offset += Binary[optimization.binaryType].bits
        } else {
            // no delta, just write the value
            var prop = optimization.prop
            var realValue = netObject.proxy[netSchema.properties[prop].key]
            var value = Math.round(realValue)
            // update cache
            cachedObject[netSchema.properties[prop].key] -= value
            // update client
            bitBuffer[Binary[optimization.binaryType].write](value, offset)
            offset += Binary[optimization.binaryType].bits
        }
    }

    return offset
}


MicroOptimizationNetworker.prototype.read = function(bitBuffer, offset, registrar) {
    // TODO: this is hardcoded for x,y movement, make it generic
    var go = null
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    //console.log(count, 'readOptimizedMovementProxies')
    for (var j = 0; j < count; j++) {
        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)
        offset += this.common.config.ID_BINARYTYPE.bits
        var dx = bitBuffer[Binary[BinaryType.Int8].read](offset)
        offset += 8
        var dy = bitBuffer[Binary[BinaryType.Int8].read](offset)
        offset += 8
        //console.log('id', id, 'dx', dx, 'dy', dy)

        registrar.updateProxyDeltaSimple(id, 2, dx)
        registrar.updateProxyDeltaSimple(id, 3, dy)
        //registrar.updateGameObjectProxyDelta(id, 'x', dx)
        //registrar.updateGameObjectProxyDelta(id, 'y', dy)
    }
    return offset
}

module.exports = MicroOptimizationNetworker
},{"../../MessageType":30,"../../binary/Binary":33,"../../binary/BinaryType":35}],59:[function(require,module,exports){

var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')
var MessageType = require('../../MessageType')
var MathEx = require('../../MathEx')

/**
* Writes partial game objects to the outgoing network buffer
*/
function PartialGameObjectNetworker(common) {
    this.common = common

}

PartialGameObjectNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    //bits += 8

    for (var i = 0; i < ids.length; i++) {

        var id = ids[i].id
        var diffs = ids[i].diffs

        //if (client.isKnownGameObjectId(ids[i])) {
        var netObject = netObjectCache.getEntity(id, tick)

        for (var j = 0; j < diffs.length; j++) {
            // object id
            bits += this.common.config.ID_BINARYTYPE.bits
            //var key = netObject.partial[j].propIndex
            //var value = netObject.partial[j].propValue
            //var propName = netObject.netSchema.keys[key]
            var propData = netObject.netSchema.properties[diffs[j]]

            //if (propName === 'x' || propName === 'y') {
                //continue
            //}
            // propIndex
            bits += 8  
            // propValue
            bits += Binary[propData.binaryType].bits
        }
        //}
    }
    //console.log('pTally', bits)
    return bits
}

PartialGameObjectNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset



    bitBuffer.writeUInt8(MessageType.PartialGameObjects, offset)
    offset += 8
    // write the number of whole proxies to be sent

    var length = 0
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i].id
        var netObject = netObjectCache.getEntity(id, tick)
        length += netObject.partial.length
    }

    //bitBuffer.writeUInt8(length, offset)
    //offset += 8

    for (var i = 0; i < ids.length; i++) {
        var id = ids[i].id
        //bitBuffer.writeUInt16(offset, ids[i])
       // offset += 16
        var netObject = netObjectCache.getEntity(id, tick)
        offset = this.writePartialProxy(bitBuffer, offset, netObject, ids[i].diffs, client)
    

       //client.clientState.gameObjects[ids[i]] = netObject.proxy
    }
    return offset
}

PartialGameObjectNetworker.prototype.writePartialProxy = function(bitBuffer, offset, netObject, diffs, client) {

    var aliasId = client.getAliasId(netObject.id)
    //console.log('partial id', aliasId)
    var cachedObject = client.clientState.gameObjects[netObject.id]

    for (var i = 0; i < diffs.length; i++) {

        
        // write id
        bitBuffer[this.common.config.ID_BINARYTYPE.write](aliasId, offset)        
        offset += this.common.config.ID_BINARYTYPE.bits
        //var key = netObject.partial[i].propIndex
        //var value = netObject.partial[i].propValue
        var propName = diffs[i]
        var propData = netObject.netSchema.properties[propName]
        var key = propData.key
        var value = netObject.proxy[key]

        if (propData.binaryType === BinaryType.Rotation) {
            //console.log('WRITING A ROTATION')
            value = MathEx.radiansToUInt8(value)
        }


        // write the property key (e.g. the key for 'x', 'y', etc)
        bitBuffer.writeUInt8(key, offset)
        offset += 8  
        // write the actual value
        bitBuffer[Binary[propData.binaryType].write](Math.round(value), offset)
        offset += Binary[propData.binaryType].bits


        cachedObject[key] = Math.round(value)

       // console.log(netObject.id, key, Math.round(value))
    }
    
   return offset
}


PartialGameObjectNetworker.prototype.read = function(bitBuffer, offset, registrar) {  
   

    var count = 0

    while (offset < bitBuffer.bitLength && bitBuffer.bitLength - offset >= this.common.config.ID_BINARYTYPE.bits) {
        var id = bitBuffer[this.common.config.ID_BINARYTYPE.read](offset)

        var proxyObject = registrar.getGameObjectProxy(id, registrar.mostRecentTick)
        var netSchema = this.common.entityConstructors.getNetSchema(proxyObject[1])

        //console.log('netSchema found', netSchema)
       // var netSchema = 
        //console.log('partial id', id)
        offset += this.common.config.ID_BINARYTYPE.bits
        var key = bitBuffer.readUInt8(offset)
        //console.log('key', key)
        offset += 8
        var propName = netSchema.keys[key]      
        var propData = netSchema.properties[propName]

        var value = bitBuffer[Binary[propData.binaryType].read](offset)
        //console.log('value', value)
        offset += Binary[propData.binaryType].bits
        //console.log('p', id, key, value)

        if (propData.binaryType === BinaryType.Rotation) {
            //console.log('WRITING A ROTATION')
            value = MathEx.UInt8ToRadians(value)
        }

        registrar.updateProxySimple(id, key, value)
        //registrar.updateGameObjectProxy(id, propName, value)

        count++
    }

    return offset
}

module.exports = PartialGameObjectNetworker
},{"../../MathEx":29,"../../MessageType":30,"../../binary/Binary":33,"../../binary/BinaryType":35}],60:[function(require,module,exports){

var Binary = require('../../binary/Binary')
var MessageType = require('../../MessageType')
function GameEventNetworker(common) {
    this.common = common
}

GameEventNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    for (var i = 0; i < ids.length; i++) {
        var id = ids[i]
        //bits += 8
        var netObject = netObjectCache.getEvent(ids[i], tick)
        for (var j = 0; j < netObject.proxy.length; j++) {
            var propName = netObject.netSchema.keys[j]

            // skip id and type
            if (propName === 'id') {
                //bits += this.common.config.ID_BINARYTYPE.bits
            } else {
                var propData = netObject.netSchema.properties[propName]
                bits += Binary[propData.binaryType].bits
            }
        }
    }
    return bits
}


GameEventNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

    //console.log('writeGameEvents', ids.length)
    //console.log('WP')
    var length = ids.length //(ids.length > client.maxVisibleObjects) ? client.maxVisibleObjects : ids.length
    //console.log('writing', 66, '@', offset)
    bitBuffer.writeUInt8(MessageType.GameEvents, offset)
    offset += 8
    // write the number of whole proxies to be sent
    //console.log('writing', length, '@', offset)
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) {
        //if (client.isKnownGameObjectId(ids[i])) {
        var netObject = netObjectCache.getEvent(ids[i], tick)
            //console.log('id', ids[i])
        offset = writeGameEvent(bitBuffer, offset, netObject.proxy, netObject.netSchema, client)
        

        var newProxy = []
        for (var j = 0; j < netObject.proxy.length; j++) {
            newProxy.push(netObject.proxy[j])
        }

        //client.clientState.gameObjects[ids[i]] = newProxy// netObject.proxy
        //}
    }    
    return offset
}

var writeGameEvent = function(bitBuffer, offset, proxy, netSchema, client) {
   // console.log(proxy)
    var debugOutput = []

    // writing type
    bitBuffer[this.common.config.ID_BINARYTYPE.write](0, offset)
    debugOutput.push(0)
    offset += this.common.config.ID_BINARYTYPE.bits

    for (var i = 2; i < proxy.length; i++) {
        var propName = netSchema.keys[i]
        var propData = netSchema.properties[propName]      

        var value = proxy[i]
        if (typeof value === 'undefined')
            value = 0

        if (propName === 'id') {
            //var alias = client.createAliasId(value)
            //var alias = client.createAliasId(value)
            //bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
            //offset += this.common.config.ID_BINARYTYPE.bits
        } else {
            bitBuffer[Binary[propData.binaryType].write](value, offset)
            offset += Binary[propData.binaryType].bits

            debugOutput.push(value)
        }
    }
   // console.log('writeGameEvent', debugOutput, proxy)

    return offset
}

GameEventNetworker.prototype.read = function(bitBuffer, offset, netSchema, registrar) {
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    console.log('read events:', count)
    for (var j = 0; j < count; j++) {
        var serialized = []

        //var id = bitBuffer[nengi.ID_BINARYTYPE.read](offset)
        //console.log('read16 bits from', offset, 'result', id)
        //offset += nengi.ID_BINARYTYPE.bits
        //serialized.push(id)

        var type = bitBuffer.readUInt8(offset)
        offset += 8


        var constructor = this.shared.eventConstructors.getConstructor(type)
        var newEvent = new constructor()

        serialized.push(type)
        var netSchema = newEvent.netSchema


        for (var i = 2; i < netSchema.keys.length; i++) {
            var propName = netSchema.keys[i]
            //console.log(propName)
            var propData = netSchema.properties[propName]

            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            serialized.push(value)
        }
        // TODO register this data
        registrar.addGameEventProxy(serialized)

    }
    return offset
}

module.exports = GameEventNetworker
},{"../../MessageType":30,"../../binary/Binary":33}],61:[function(require,module,exports){
var Binary = require('../../binary/Binary')
var BinaryType = require('../../binary/BinaryType')

var MessageType = require('../../MessageType')

function DirectMessageNetworker(common) {
	this.common = common
}

DirectMessageNetworker.prototype.countBits = function(ids, tick, netObjectCache) {
    if (ids.length === 0) return 0

    var bits = 0
    // msgtype
    bits += 8
    // length
    bits += 8

    for (var i = 0; i < ids.length; i++) {
        var id = ids[i]
        //bits += 8
        var netObject = netObjectCache.getMessage(ids[i], tick)
        for (var j = 0; j < netObject.proxy.length; j++) {
            var propName = netObject.netSchema.keys[j]

            // skip id 
            if (propName === 'id') {
                //bits += this.common.config.ID_BINARYTYPE.bits
            } else {
                var propData = netObject.netSchema.properties[propName]
                bits += Binary[propData.binaryType].bits
            }
        }
    }
    return bits
}


DirectMessageNetworker.prototype.write = function(bitBuffer, offset, ids, tick, client, netObjectCache) {
    if (ids.length === 0) return offset

 
    //console.log('writeGameEvents', ids.length)
    //console.log('WP')
    var length = ids.length //(ids.length > client.maxVisibleObjects) ? client.maxVisibleObjects : ids.length
    //console.log('writing', 66, '@', offset)
    bitBuffer.writeUInt8(MessageType.DirectMessages, offset)
    offset += 8
    // write the number of whole proxies to be sent
    //console.log('writing', length, '@', offset)
    bitBuffer.writeUInt8(length, offset)
    offset += 8

    for (var i = 0; i < length; i++) {
        //if (client.isKnownGameObjectId(ids[i])) {
        var netObject = netObjectCache.getMessage(ids[i], tick)
            //console.log('id', ids[i])
        offset = this.writeGameEvent(bitBuffer, offset, netObject.proxy, netObject.netSchema, client)
        

        var newProxy = []
        for (var j = 0; j < netObject.proxy.length; j++) {
            newProxy.push(netObject.proxy[j])
        }

        //client.clientState.gameObjects[ids[i]] = newProxy// netObject.proxy
        //}
    }    
    return offset
}

DirectMessageNetworker.prototype.writeGameEvent = function(bitBuffer, offset, proxy, netSchema, client) {
   // console.log(proxy)
    var debugOutput = []

    // writing type
    bitBuffer[this.common.config.ID_BINARYTYPE.write](0, offset)
    debugOutput.push(0)
    offset += this.common.config.ID_BINARYTYPE.bits

    for (var i = 2; i < proxy.length; i++) {
        var propName = netSchema.keys[i]
        var propData = netSchema.properties[propName]   



        var value = proxy[i]

        if (propData.binaryType === BinaryType.EntityId) {
            value = client.getAliasId(value)
            console.log('changed to', value)
        }

        if (typeof value === 'undefined')
            value = 0

        if (propName === 'id') {
            //var alias = client.createAliasId(value)
            //var alias = client.createAliasId(value)
            //bitBuffer[this.common.config.ID_BINARYTYPE.write](alias, offset)
            //offset += this.common.config.ID_BINARYTYPE.bits
        } else {
            bitBuffer[Binary[propData.binaryType].write](value, offset)
            offset += Binary[propData.binaryType].bits

            debugOutput.push(value)
        }
    }
  // console.log('writeMessage', debugOutput, proxy)

    return offset
}



DirectMessageNetworker.prototype.read = function(bitBuffer, offset, netSchema, registrar) {
    var count = bitBuffer.readUInt8(offset)
    offset += 8

    for (var j = 0; j < count; j++) {
        var serialized = []

        //var id = bitBuffer[nengi.ID_BINARYTYPE.read](offset)
        //console.log('read16 bits from', offset, 'result', id)
        //offset += nengi.ID_BINARYTYPE.bits
        //serialized.push(id)

        var type = bitBuffer.readUInt8(offset)
        offset += 8


        var constructor = this.common.messageConstructors.getConstructor(type)
        var newEvent = new constructor()

        serialized.push(type)
        var netSchema = newEvent.netSchema


        for (var i = 2; i < netSchema.keys.length; i++) {
            var propName = netSchema.keys[i]
            //console.log(propName)
            var propData = netSchema.properties[propName]

            var value = bitBuffer[Binary[propData.binaryType].read](offset)
            offset += Binary[propData.binaryType].bits

            serialized.push(value)
        }
        // TODO register this data
        registrar.addMessage(serialized)

    }
    return offset
}



module.exports = DirectMessageNetworker
},{"../../MessageType":30,"../../binary/Binary":33,"../../binary/BinaryType":35}],62:[function(require,module,exports){

var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
// axis-aligned bounding box
/* PARAMS: (Point)center, (Point)half */
function AABB(x, y, halfWidth, halfHeight) {
	this.initialize(x, y, halfWidth, halfHeight)
}

AABB.pool = []
AABB.instanceCount = 0
AABB.inUseCount = 0
AABB.logStats = function() {
	console.log(
		'AABBs', 
		'total', AABB.instanceCount, 
		'avail', AABB.instanceCount - AABB.inUseCount, 
		'in-use', AABB.inUseCount
	)
}

AABB.create = function(x, y, halfWidth, halfHeight) {
	//return new AABB(x, y, halfWidth, halfHeight)
	var instance

	if (AABB.pool.length === 0) {        
		instance = new AABB(x, y, halfWidth, halfHeight)
		AABB.instanceCount++
    } else {
        instance = AABB.pool.pop()
        instance.initialize(x, y, halfWidth, halfHeight)        
    }

    AABB.inUseCount++
	return instance
}

AABB.prototype.release = function() {
	AABB.pool.push(this)
	AABB.inUseCount--
}

AABB.prototype.initialize = function(x, y, halfWidth, halfHeight) {
	this.x = x
	this.y = y
	this.halfWidth = halfWidth
	this.halfHeight = halfHeight
}

AABB.prototype.containsPoint = function(x, y) {
	// inclusive on the lower bound, exclusivive on the upper bound for quadtree
	// compatibilty, so that a point exactly on a line belongs to only one node
	return (x >= this.x - this.halfWidth 
		&& y >= this.y - this.halfHeight
		&& x < this.x + this.halfWidth
		&& y < this.y + this.halfHeight)
}

AABB.prototype.intersects = function(aabb) {
	return (Math.abs(this.x - aabb.x) * 2 < (this.halfWidth  * 2 + aabb.halfWidth  * 2))
		&& (Math.abs(this.y - aabb.y) * 2 < (this.halfHeight * 2 + aabb.halfHeight * 2))
}

AABB.prototype.clone = function() {
	return AABB.create(this.x, this.y, this.halfWidth, this.halfHeight)
}


/*
// only used for debugging quadtrees across a network
AABB.prototype.netSchema = BinarySerializer.createSchema({
	'x': BinaryType.Int32,
	'y': BinaryType.Int32,
	'halfWidth': BinaryType.Int32,
	'halfHeight': BinaryType.Int32
})
*/

module.exports = AABB
},{"../binary/BinarySerializer":34,"../binary/BinaryType":35}],63:[function(require,module,exports){


function BandwidthMonitor(config) {
	this.bytesSent = 0
	this.recentTimestamp = 0
	this.started = 0
	this.bytesSentRecently = 0
	this.bytesReceived = 0
	this.sampleWindow = 5000 // milliseconds 

	this.outgoingBytesPerSecondTotal = 0
	this.outgoingBytesPerSecondRecent = 0
	this.config = config

	//this.incomingBytesPerSecondTotal = 0
	//this.incomingBytesPerSecondRecent = 0
}

BandwidthMonitor.prototype.constructor = BandwidthMonitor

BandwidthMonitor.prototype.update = function(delta, tick, now) {

	if (this.recentTimestamp === 0) {
		this.recentTimestamp = now
		this.started = now
	}
	this.recalculate(now)

	if (now > this.recentTimestamp + this.sampleWindow) {
		console.log(
			'recent:', bytesToString(this.outgoingBytesPerSecondRecent),
			'total:', bytesToString(this.outgoingBytesPerSecondTotal)
		)

		this.bytesSentRecently = 0
		this.recentTimestamp = now
	}
}

BandwidthMonitor.prototype.addOutgoing = function(bytes) {
	this.bytesSent += bytes
	this.bytesSentRecently += bytes
}

BandwidthMonitor.prototype.recalculate = function(now) {
	var totalTime = (now - this.started)/1000

	this.outgoingBytesPerSecondRecent = this.bytesSentRecently / (this.sampleWindow / 1000)
	this.outgoingBytesPerSecondTotal = this.bytesSent / totalTime
}

var bytesToString = function(bytes) {
	// following the base 10 rules for bytes (1000 instead of 1024 bytes in a kB)
	if (bytes < 1000) {
		return bytes.toFixed(2) + ' B/s'
	} else if (bytes >= 1000000) {
		return (bytes/1000000).toFixed(2) + ' MB/s'
	} else if (bytes >= 1000) {
		return (bytes/1000).toFixed(2)+ ' KB/s'
	}
}

module.exports = BandwidthMonitor
},{}],64:[function(require,module,exports){
var AABB = require('./AABB')
var IdAliases = require('./IdAliases')
var ClientState = require('./ClientState')
//var EventEmitter = require('./external/EventEmitter')

/*
* (Server-side) holds information about a connected client, and what the client
* can see
*/
function Client(common) {
	//EventEmitter.call(this)
	this.id = null
	// <WebSocketServerConnection>
	this.connection = null
	this.common = common

	this.entity = null
	// <AABB> visible area
	this.view = AABB.create(250, 250, 900, 900)
	this.clientState = new ClientState()
	this.interp = 100
	this.tickRate = this.common.config.DRAW_RATE

	// <Array> gameObjectIds visible last time updatedateVisibilty was called
	this.previousVisibleIds = []
	this.maxVisibleObjects = this.common.config.ID_BINARYTYPE.max
	// <IdAliases> provides alias identifers for game objects from 0-255
	this.idAliases = new IdAliases(this.maxVisibleObjects)

	this.messages = []

	// instance to which this client is connected
	this.instance = null

	this.totalVisible = 0

	this.lastInputProcessed = -1

	this.inputFrameBuffer = []


	this.lastPingCheckTimestamp = Date.now()
	this.ping = 0
	this.pingResults = []
}

//Client.prototype = Object.create(EventEmitter.prototype)
Client.prototype.constructor = Client

Client.prototype.setPingResult = function(ping) {
	this.pingResults.push(ping)
	while (this.pingResults.length > 5) {
		this.pingResults.shift()
	}

	var tempTotal = 0
	for (var i = 0; i < this.pingResults.length; i++) {
		tempTotal += this.pingResults[i]
	}
	this.ping = tempTotal / this.pingResults.length

	if (this.ping > 1024) {
		this.ping = 1024
	}
	//console.log('AVERAGE PING', this.ping)
}

Client.prototype.needsPingCheck = function() {
	return Date.now() - this.lastPingCheckTimestamp > this.common.config.PING_INTERVAL
}

Client.prototype.pingCheckSent = function() {
	this.lastPingCheckTimestamp = Date.now()
}


/*
* The client uses a system of Alias Ids, shortened Ids that map to full game 
* objects e.g. gameObject 120390 could map to an alias of 15, allowing for less 
* data to be sent, as well as providing some security because the client does 
* not know the true ids of the game objects it can see
*/
Client.prototype.getAliasId = function(gameObjectId) {
	//console.log('getAliasId', gameObjectId, this.idAliases.getAliasId(gameObjectId))
	return this.idAliases.getAliasId(gameObjectId)	
}

Client.prototype.createAliasId = function(gameObjectId) {

	return this.idAliases.registerId(gameObjectId)
}

Client.prototype.releaseAliasId = function(aliasId) {

	return this.idAliases.unregisterId(aliasId)
}


Client.prototype.queueMessage = function(message) {
	this.messages.push(message)
}

Client.prototype.queueInputFrame = function(inputFrame) {
	this.inputFrameBuffer.push(inputFrame)
}



/*
* Updates the visibility category of gameObjects
* gameObjects may be one in one of three categories
*   newlyVisible: gameObject has just become visible, client needs its full data
*   stillVisible: gameObject was already visible, client needs only changes updated
*   noLongerVisible: gameObject has just left visibility, client should remove it
*/
Client.prototype.categorizeEntityVisibility = function(quadtree) {

	var stuffs = quadtree.selectObjectAndEventIds(this.view)
	var allVisibleIds = stuffs.objectIds//quadtree.selectIds(this.view)

	var visibleIds = []
	var max = allVisibleIds.length
	if (allVisibleIds.length > this.maxVisibleObjects) {
		max = this.maxVisibleObjects
	}
	for (var i = 0; i < max; i++) {
		visibleIds.push(allVisibleIds[i])
	}

	//console.log('TOTAL VISIBLE', visibleIds.length, '/', allVisibleIds.length, 'max:', this.maxVisibleObjects)
	//console.log('VISIBLE', visibleIds)
	//console.log('PREVIOUS',this.previousVisibleIds)

	var categories = arrayDiffs(visibleIds, this.previousVisibleIds)
	// update the visible ids, for the next call
	this.previousVisibleIds = visibleIds

	this.noLongerVisible = categories.right
	this.newlyVisible = categories.left
	this.stillVisible = categories.both

	//this.noLongerVisibleCache = this.noLongerVisible

	//console.log(this.noLongerVisible)





	var ret = { 
		noLongerVisible: this.noLongerVisible, 
		newlyVisible: this.newlyVisible, 
		stillVisible: this.stillVisible,
		events: stuffs.eventIds
	}

	
	return ret
}


Client.prototype.skipVisibilityCheck = function() {
	return { 
		noLongerVisible: [], 
		newlyVisible: [],
		stillVisible: this.previousVisibleIds
	}
}

function compareNumbers(a, b) {
  return a - b
}

function arrayDiffs(a, b) {
	a.sort(compareNumbers)
	b.sort(compareNumbers)

	//console.log('A', a)
	//console.log('B', b)

	var left = []
	var both = []
	var right = []

	var i = 0
	var j = 0

	while (i < a.length && j < b.length) {
	    if (a[i] < b[j]) {
        	left.push(a[i])
	        ++i
	    } else if (b[j] < a[i]) {
	        right.push(b[j])
	        ++j
	    } else {
	        both.push(a[i])
	        ++i
	        ++j
	    }
	}
	while (i < a.length) {
	   left.push(a[i])
	    ++i
	}
	while (j < b.length) {
	    right.push(b[j])
	    ++j
	}

	return { left: left, both: both, right: right }
}

module.exports = Client
},{"./AABB":62,"./ClientState":65,"./IdAliases":67}],65:[function(require,module,exports){
/*
* Serverside record of a client's state
*/
function ClientState() {
	// <Object> key: gameObject.id, value: gameObject
	this.gameObjects = {}
}

ClientState.prototype.constructor = ClientState

module.exports = ClientState
},{}],66:[function(require,module,exports){
var AABB = require('./AABB')
var Quadtree = require('./Quadtree')
var LagCompensator = require('./LagCompensator')
/** 
Stores snapshots of the gameObject quadtree, offers the ability to see where
entities were in the recent past

@class Historian
@constructor
*/
function Historian(config) {
	this.history = {}
  this.mostRecentTick = -1
  this.lagCompensator = new LagCompensator(this, config)
}


/**
Returns a historical Quadtree

@method getTimesliceQuadtree
@param {Integer} tick The simulation step
@return {Object} Returns a historical Quadtree
*/
Historian.prototype.getTimesliceQuadtree = function(tick) {
  if (this.history[tick]) {
    return this.history[tick]
  } else {
    return Quadtree.create(AABB.create(0,0,500,500))
  }
}


/**
* enum private to Historian
*/
var ProxyType = {
  GameObject: 1,
  GameEvent: 2
}

/**
For any given tick of the simulation, creates and saves a quadtree holding all
of the entities. Given that all collisions involve these quadtrees, the 
Historian is required to have recored at least one tick before any collisions 
may occur.

@method recordHistory
@param {Integer} tick The simulation step
@param {Array} gameObjects Array of gameObjects to insert
@param {Array} gameEvents Array of gameEvents to insert
@param {AABB} boundary An AABB of the maximum area of the quadtree
*/
Historian.prototype.recordHistory = function(tick, gameObjects, gameEvents, boundary) {
  var quadtree = Quadtree.create(boundary.clone())
  for (var i = 0; i < gameObjects.length; i++) {
    var gameObject = gameObjects[i]
    quadtree.insert({
      id: gameObject.id,
      x: gameObject.x, 
      y: gameObject.y,  
      type: ProxyType.GameObject
    })
  }

  for (var i = 0; i < gameEvents.length; i++) {
    var gameEvent = gameEvents[i]
    quadtree.insert({
      id: gameEvent.id,
      x: gameEvent.x, 
      y: gameEvent.y,    
      type: ProxyType.GameEvent
    })
  }

  this.history[tick] = quadtree

  if (tick > this.mostRecentTick) {
    this.mostRecentTick = tick
  }

  //Quadtree.logStats()
  //AABB.logStats()
  if (this.history[tick-20]) {
    this.history[tick-20].release()
    delete this.history[tick-20]
  }
}

Historian.prototype.getRecentState = function(view) {
  var quadtree = this.getTimesliceQuadtree(this.mostRecentTick)
  quadtree.selectIds(view)


}

Historian.prototype.getRecentEvents = function() {
  var quadtree = this.getTimesliceQuadtree(this.mostRecentTick)
}

Historian.prototype.getRecentQuadtree = function() {
  return this.getTimesliceQuadtree(this.mostRecentTick)
}

module.exports = Historian
},{"./AABB":62,"./LagCompensator":70,"./Quadtree":75}],67:[function(require,module,exports){
var IdPool = require('./IdPool')

/**
* IdAliases provides bi-directional dictionary-like access between aliasIds and
* actual gameObjectIds. Internally IdAliases uses an IdPool. Using an alias for
* an id allows for smaller numbers (e.g. gameObject.id 12039490 could map to 
* aliasId 12). This allows for a level of security, as the cilent only knows
* alias ids, and does not know the actual ids. This also allows for optimization
* of the data type used to represent an id (e.g. maxAliasId of 255 is 8 bits, a 
* max of 65535 is 16 bits, etc).
*
* @class IdAliases
* @constructor
* @param {Integer} maxAliasId The upper end of the desired alias id range
*/
function IdAliases(maxAliasId) {
	// key: gameObjectId, value: aliasId
	this.gameObjectIds = {}
	// key: aliasId, value: gameObjectId
	this.aliasIds = {}
	// generator for the alias ids 
	this.aliasIdPool = new IdPool(maxAliasId)
}


/**
* Returns the gameObject id for a given alias id.
*
* @method getGameObjectId
* @param {Integer} aliasId 
* @return {Integer} The real gameObjectId, or -1 if nothing found
*/
IdAliases.prototype.getGameObjectId = function(aliasId) {
	//return aliasId
	
	var gameObjectId = this.aliasIds[aliasId]
	if (typeof gameObjectId !== 'undefined') {
		return gameObjectId
	} else {
		return -1
		//throw new Error('No gameObject id for alias ' + aliasId)
	}
}


/**
* Returns the alias id for a given gameObject id.
*
* @method getAliasId
* @param {Integer} gameObjectId
* @return {Integer} The aliasId for the gameObjectId, or -1 if nothing found
*/
IdAliases.prototype.getAliasId = function(gameObjectId) {
	//return gameObjectId
	
	var aliasId = this.gameObjectIds[gameObjectId]
	if (typeof aliasId !== 'undefined') {
		return aliasId
	} else {
		return -1
		//throw new Error('No alias id for gameObject ' + gameObjectId)
	}	
}


/**
* Registers a gameObjectId, assigning it an aliasId
*
* @method registerId
* @param {Integer} gameObjectId
* @return {Integer} The aliasId for the gameObjectId
*/
IdAliases.prototype.registerId = function(gameObjectId) {
	//return gameObjectId
	
	var aliasId = this.aliasIdPool.nextId()

	if (aliasId !== -1) {
		this.gameObjectIds[gameObjectId] = aliasId
		this.aliasIds[aliasId] = gameObjectId
	} else {
		console.log('no alias available for', gameObjectId)
	}
	return aliasId	
}


/**
* Unregisters a aliasId, freeing this aliasId for future use
*
* @method unregisterId
* @param {Integer} aliasId
*/
IdAliases.prototype.unregisterId = function(aliasId) {
	//return aliasId
	
	var gameObjectId = this.getGameObjectId(aliasId)
	delete this.gameObjectIds[gameObjectId]
	delete this.aliasIds[aliasId]
	this.aliasIdPool.releaseId(aliasId)	
}

module.exports = IdAliases
},{"./IdPool":68}],68:[function(require,module,exports){
/**
* IdPool provides a set of continuous integer (e.g. 0-255) that can be borrowed
* from the pool and returned to the pool.
*
* @class IdPool
* @constructor
* @param {Integer} count The (max) size of the IdPool
*/
function IdPool(count) {
	this.ids = []
	for (var i = 0; i < count; i++) {
		this.ids.push(i)
	}
}


/**
* Obtains an availity id from the pool. Will return -1 if no ids are available.
*
* @method nextId
* @return {Integer} An available id
*/
IdPool.prototype.nextId = function() {
	if (this.ids.length > 1) {
		var id = this.ids.shift()
		return id
	} else {
		console.log('IdPool overflow')
		return -1
		//throw new Error('IdPool overflow. No ids left to distribute.')
	}	
}

/**
* Returns an id to the pool
*
* @method releaseId
* @param {Integer} id
*/
IdPool.prototype.releaseId = function(id) {
	this.ids.push(id)
}

module.exports = IdPool
},{}],69:[function(require,module,exports){
var Historian = require('./Historian')
var AABB = require('./AABB')
var NetObjectCache = require('./NetObjectCache')
var BandwidthMonitor = require('./BandwidthMonitor')
//var BinaryType = require('./BinaryType')
var EventEmitter = require('../external/EventEmitter')
var Dictionary = require('../Dictionary')

var QuadtreeAABBNetworker = require('../network/debug/QuadtreeAABBNetworker')
var GameStateWriter = require('../network/GameStateWriter')

var PingMessage = require('../client/PingMessage')

function Instance(common) {
    EventEmitter.call(this)
	this.boundary = AABB.create(0, 0, 10000, 10000)
	this.historian = new Historian(common.config)

    //this.entities = new Dictionary()
    this.entities = []
    this.entitiesRef = {}

    // positional, most events related to the game
    // these events are added to the quadtree like entitys, and are selected
    // based on vicinity
    // e.g. fired a laser, healed someone, gaining a level (if everyone can see)
    this.gameEvents = []
    this.gameEventsRef = {}
    this.newGameEvents = []

    // private, for a player only
    // per player queue
    // e.g. a private message to a player, achievement unlocked (if private)
    // format { clientId, gameEvent }
    this.directMessages = []
    this.directMessagesRef = {}

    // global, for everyone
    // queue, cleared at end of tick
    // e.g. messages for all players, server shutting down, boss killed,
    this.instanceEvents = []
    this.instanceMessages = []  

    //this.messages = new Dictionary()
    this.messages = []
    this.messagesRef = []

	this.netObjectCache = new NetObjectCache(this)
    this.gameStateWriter = new GameStateWriter(this.netObjectCache, common)
    this.bandwidthMonitor = new BandwidthMonitor(common.config)

    this.common = common
    //console.log('CTORS', this.common)

   // this.clientsRef = {}
	//this.clients = []

    this.clients = []
    this.clientsRef = {}

    //this.clients = new Dictionary()
    this.inputSnapshots = []
}

Instance.prototype = Object.create(EventEmitter.prototype)
Instance.prototype.constructor = Instance

Instance.prototype.addInputSnapshot = function(inputSnapshot) {
    this.inputSnapshots.push(inputSnapshot)
}

var eventId = 0
Instance.prototype.addGameEvent = function(gameEvent) {
    //gameEvent.instance = this // TODO do events use this?
    gameEvent.id = eventId++
    this.gameEvents.push(gameEvent)
    this.newGameEvents.push(gameEvent)
    this.gameEventsRef[gameEvent.id] = gameEvent
    //console.log('EVENT-->', gameEvent)
   // this.entitysRef[entity.id] = entity
}

Instance.prototype.createEntity = function(type, config) {
    //TODO
    //instantiate by type
    //copy config into object
    //this.addEntity(entity)
}

var messageId = 0
Instance.prototype.addMessage = function(message, client) {
    //gameMessage
    console.log('message added to instance', message, message.constructor.name)
    message.id = messageId++
    message.type = this.common.messageConstructors.getConstructorKeyByName(message.constructor.name)
    this.messages.push(message)
    this.messagesRef[message.id] = message
    client.queueMessage(message)
}

var clientId = 0
Instance.prototype.addClient = function(client) {
    client.id = clientId++
    client.instance = this
    this.clients.push(client)
    this.clientsRef[client.id] = client
}
Instance.prototype.removeClient = function(client) {
    var id = client.id
    var index = -1
    for (var i = 0; i < this.clients.length; i++) {
        if (this.clients[i].id === id) {
            index = i
            break
        }
    }
    if (index !== -1) {
        this.clients.splice(index, 1)
    }
    delete this.clientsRef[id]
}

var id = 0
Instance.prototype.addEntity = function(entity) {
    entity.instance = this
    entity.id = id++

    // TODO consider decoupling the constructor code from this area
    entity.type = this.common.entityConstructors.getConstructorKeyByName(entity.constructor.name)
    

    //this.entities.add(entity)

    this.entities.push(entity)
    this.entitiesRef[entity.id] = entity
    //console.log(entity)
}

Instance.prototype.getEntity = function(id) {
   // return this.entities.get(id)
    return this.entitiesRef[id]
}

Instance.prototype.getMessage = function(id) {
    return this.messagesRef[id]
}

Instance.prototype.getEventObjectById = function(id) {
    return this.gameEventsRef[id]
}

// unused & untested
Instance.prototype.removeEntity = function(entity) {

    //return this.entities.remove(id)

    var id = entity.id
    var index = -1
    for (var i = 0; i < this.entities.length; i++) {
        if (this.entities[i].id === id) {
            index = i
            break
        }
    }
    if (index !== -1) {
        this.entities.splice(index, 1)
    }
    //this.entitiesRef[id].instance = null
    delete this.entitiesRef[id]
    
}


Instance.prototype.addPrivateEvent = function(client, evt) {
    //this.privateEvents.push({ clientId: clientId, gameEvent: ev})

    client.directMessages.push(evt)
}



Instance.prototype.n_update = function(delta, tick, now) {

    //console.log('instance', 'c', this.clients.length, 'e', this.entities.length)
    if (this.update) {
        this.update(delta, tick, now)
    }
    this.bandwidthMonitor.update(delta, tick, now)


    while (this.inputSnapshots.length > 0) {
        var inputSnapshot = this.inputSnapshots.shift()

        while (inputSnapshot.inputs.length > 0) {
            var input = inputSnapshot.inputs.shift()
            inputSnapshot.client.lastInputProcessed = inputSnapshot.tick
            this.emit('inputContext', { 
                tick: inputSnapshot.tick, 
                client: inputSnapshot.client, 
                input: input 
            })
        }
        
    }
    

    // update all entitys

	for (var i = 0; i < this.entities.length; i++) {
		var entity = this.entities[i]
		entity._update(delta, tick, now)				
	}
    

    //this.entities.forEach(function(entity) {
    //    entity._update(delta, tick, now)
    //})

    for (var i = 0; i < this.gameEvents.length; i++) {
        var gameEvent = this.gameEvents[i]
        gameEvent._update(delta, tick, now)
    }


    //console.log('adding events to history:', this.newGameEvents.length)
    this.historian.recordHistory(
        tick, 
        this.entities,
        this.newGameEvents, 
        this.boundary
    )
    this.newGameEvents = []

    // get most recent entity positions for the entire instance
	var quadtree = this.historian.getRecentQuadtree()

    var aabbs = quadtree.getAllAABBs()
    var self = this
	for (var m = 0; m < this.clients.length; m++) {
		var client = this.clients[m]

        //console.log('client ping', client.ping)
        //var pingMessage = new PingMessage()
        //pingMessage.ping = client.ping
       // this.addMessage(pingMessage, client)

        // determine visibility of client
        //console.log('attempting to send to client', client)

		var visibility = null

        //var temp = m+tick
        //console.log(j, 'j+tick', j+tick)
        //if (temp % 1 === 0) {
        visibility = client.categorizeEntityVisibility(quadtree)

        var buffer = self.gameStateWriter.writeBuffer(client, visibility, tick, aabbs)
        //console.log('+=== ===+')
        self.bandwidthMonitor.addOutgoing(buffer.length)

        //console.log('sending', finalBuffer.length, 'to', client.id)
        //var test = new BitBuffer(8)
        //test.writeUInt8(0, 15)
        //if (finalBuffer.length > 0)

        

        client.connection.sendBytes(buffer)

        //console.log(client.clientState)

        //client.flush()

       // console.log('SENT', bitBuffer.readUInt8(0))
    }
}



module.exports = Instance
},{"../Dictionary":28,"../client/PingMessage":41,"../external/EventEmitter":44,"../network/GameStateWriter":52,"../network/debug/QuadtreeAABBNetworker":55,"./AABB":62,"./BandwidthMonitor":63,"./Historian":66,"./NetObjectCache":74}],70:[function(require,module,exports){
var MathEx = require('../MathEx')

// TODO: consider just merging this to historian
function LagCompensator(historian, config) {
	this.historian = historian
	this.config = config
}

/**
* Looks into the past of area, returning the past positions of all entities therein.
*
* @param area {AABB} Axis-aligned bounding box specifying the area
* @param latencyMs {Integer} Latency of client in milliseconds
* @param interpolationMs {Integer} Length of the interpolation window in the client in millseconds
* @param clientTickRate {Integer} Tick rate, aka fps of the client
* @return {Array} An array of past position objects [{ id: Integer, x: Number, y: Number },etc.]
*/
// TODO: client should have its own latency, interp, tickrate variables,
// which could simplify these params to area, currentTick, client
// or perhaps only client, currenTick, and assume area as the client.viewArea
LagCompensator.prototype.compensateArea = function(client, area, tick) {
	var currentTick = tick
	var latencyMs = client.ping // or shoudl this be ping/2?
	var interpolationMs = 100 //client.interp
	var clientTickRate = client.tickRate

	var TICKS_PER_SECOND = this.config.UPDATE_RATE

	// how far in the past the player's entities are relative to the server in milliseconds
	// e.g. a player with 250 ping and default 100 ms interpolation, running at 60 fps 
	// has an approximate total delay of 366 ms
	var delay = latencyMs + interpolationMs + (1000 / clientTickRate)

	//console.log('compensating with delay', delay)

	//console.log('compensation delay', delay)
	// how long a tick is on the server in milliseconds
	// e.g. a server at 20 fps will have a ticklength of 50 ms
	var tickLength = 1000 / TICKS_PER_SECOND

	// how many ticks ago to rewind the server's entities
	// e.g. 366/50 means we need to look 7 ticks into the past to get entity positions
	var ticksAgo = Math.floor(delay / tickLength)

	// how far between one tick and the next are we?
	// e.g. 366/50 is actually 7.32, the tick portion is 0.32, 
	// roughly 1/3 the way from tick 7 to tick 8
	// TODO: while this seems to produce good results (perhaps because it is *close*), 
	// is itactually right? maybe 1.0 - 0.32 is more accurate. because we woudl be lerping
	// from 8 to 7, not 7 to 8...
	var tickPortion = (delay % tickLength) / tickLength

	// two timeslices from the historian that flank the time in the past we are compensating to
	// e.g. if our delay requires us to go back in time 7.32 ticks, then
	// timesliceA is the positional data from 8 ticks ago
	// timesliceB is the positional data from 7 ticks ago
	// and we intend to interpolate postions 0.32 of the way between these postions
	// TODO: or should we interpolate 1 - 0.32 = 0.68, in this example
	var timesliceA = this.historian.getTimesliceQuadtree(currentTick - ticksAgo - 1)
	var timesliceB = this.historian.getTimesliceQuadtree(currentTick - ticksAgo)

	// get all the NPCs from the quadtree in the relevant area for both time slices
	// NOTE: fast moving objects might have existed in one slice but not another, if
	// these still need compensated, then one must look in a larger area
	var positionsA = []
	timesliceA.queryRange(area, positionsA)
	var positionsB = []
	timesliceB.queryRange(area, positionsB)

	// copy the entity position data into a dictionary instead of an array
	// TODO: evaluate if this could be more elegant
	var entitiesA = {}
	var entitiesB = {}
	for (var i = 0; i < positionsA.length; i++) {
	    entitiesA[positionsA[i].id] = positionsA[i]
	}
	for (var i = 0; i < positionsB.length; i++) {
	    entitiesB[positionsB[i].id] = positionsB[i]
	}

	var pastPositions = []

	for (var entityId in entitiesA) {
        // if it appears in both timeslices...
        if (entitiesB[entityId]) {

            // lerp between the position in timesliceA and timesliceB
            var entityA = entitiesA[entityId]
            var entityB = entitiesB[entityId]

            // calculate the x, y position the object would've been in, making the
            // assumption that it moved linearly between these past two positions
            var x = MathEx.lerp(entityA.x, entityB.x, tickPortion)
            var y = MathEx.lerp(entityA.y, entityB.y, tickPortion)
            
            var pastPosition = {
            	id: entityId,
            	x: x,
            	y: y
            }
            pastPositions.push(pastPosition)
		}
    }
    return pastPositions
}

module.exports = LagCompensator
},{"../MathEx":29}],71:[function(require,module,exports){
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

/*
* Holds a gameObject's networked values
*/
function NetEntity() {
    this.id = null

    this.tickSerialized = null
    // <Object> serialized object (via a netSchema)
    this.proxy = null
    // <Array> props changed since last tick, use to create partial
    this.dirty = []
    // <Buffer> full data for the game object as a buffer
    this.full = null
    // byte count of the full data
    this.fullBytes = 0
    // <Buffer> partial data for the game object, prop changes since last tick
    this.partial = null
    // byte count of the partial data
    this.partialBytes = 0

    this.dx = 0
    this.dy = 0
    this.dxr = 0
    this.dyr = 0

    this.netSchema = null
}

NetEntity.prototype.constructor = NetEntity
//todo
NetEntity.prototype.copy = function() {
    var netObject = new NetEntity()
    netObject.id = this.id
    netObject.tickSerialized = this.tickSerialized
    netObject.proxy = this.proxy.copy()
    netObject.dirty = this.dirty.slice()

    netObject.full = this.full
    netObject.fullBytes = this.fullBytes

    netObject.partial = this.partial
    netObject.partialBytes = this.partialBytes

    netObject.dx = this.dx
    netObject.dy = this.dy
    netObject.netSchema = this.netSchema
}

/*
* Serializes a gameObject and stores it in this netObject
* sets values for proxy, full, fullBytes
*/
NetEntity.prototype.serialize = function(gameObject, tick) {
    var netSchema = gameObject.netSchema
    this.id = gameObject.id
    this.proxy = BinarySerializer.proxify(gameObject)
    this.netSchema = gameObject.netSchema
    this.tickSerialized = tick
}

/*
* Reserializes a gameObject, tracking the differences
* sets values for proxy, full, fullbytes, partial, partialBytes
*/
NetEntity.prototype.reserialize = function(gameObject, tick) {
    var netSchema = gameObject.netSchema


    var oldX = this.proxy[netSchema.properties['x'].key]
    var oldY = this.proxy[netSchema.properties['y'].key]
    //console.log('old', oldX, oldY, netSchema.properties)

    var proxy = BinarySerializer.proxify(gameObject)
    

    // gameObject already cached, find diffs between the new state and old state
    var diffs = []
    for (var j = 0; j < this.proxy.length; j++) {
        if (this.proxy[j] !== proxy[j]) {
            diffs.push(j)
        }
    }

    if (diffs.length > 0) {
        var diffArr = []

        for (var j = 0; j < diffs.length; j++) {
            var index = diffs[j]
            var propName = netSchema.keys[index]
            var prop = netSchema.properties[propName]
            var value = this.proxy[index]
            diffArr.push({ propIndex: index, propValue: value })

            if (propName === 'x') {
                this.dx = this.proxy[index] - proxy[index]
                this.dxr = (this.proxy[index] - proxy[index]) % 1
            } else if (propName === 'y') {
                this.dy = this.proxy[index] - proxy[index]
                this.dyr = (this.proxy[index] - proxy[index]) % 1
            }
        }
        this.partial = diffArr
    }
    //console.log('dxr', this.dxr, 'dyr', this.dyr)
    this.dirty = diffs
    this.proxy = proxy
    this.netSchema = gameObject.netSchema
    this.tickSerialized = tick    
}



module.exports = NetEntity
},{"../binary/Binary":33,"../binary/BinarySerializer":34,"../binary/BinaryType":35}],72:[function(require,module,exports){
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

/*
* Holds a gameEvent's networked values
*/
function NetEvent() {
    this.id = null

    this.tickSerialized = null
    // <Object> serialized object (via a netSchema)
    this.proxy = null
    // <Buffer> full data for the game object as a buffer
    this.full = null
    // byte count of the full data
    this.fullBytes = 0
    // <Buffer> partial data for the game object, prop changes since last tick

    this.netSchema = null
}

NetEvent.prototype.constructor = NetEvent

//todo
NetEvent.prototype.copy = function() {
    var netObject = new NetEvent()
    netObject.id = this.id
    netObject.tickSerialized = this.tickSerialized
    netObject.proxy = this.proxy.copy()
    netObject.full = this.full
    netObject.fullBytes = this.fullBytes
}

/*
* Serializes a gameEvent and stores it in this netObject
* sets values for proxy, full, fullBytes
*/
NetEvent.prototype.serialize = function(gameEvent, tick) {

    //console.log('NetEvent.serialize', gameEvent, tick)
    //console.log('schema', gameEvent.netSchema)
    var netSchema = gameEvent.netSchema
    this.id = gameEvent.id
    this.proxy = BinarySerializer.proxify(gameEvent)
    this.netSchema = gameEvent.netSchema
    this.tickSerialized = tick
}


module.exports = NetEvent
},{"../binary/Binary":33,"../binary/BinarySerializer":34,"../binary/BinaryType":35}],73:[function(require,module,exports){
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')

/*
* Holds a message's networked values
*/
function NetMessage() {
    this.id = null

    this.tickSerialized = null
    // <Object> serialized object (via a netSchema)
    this.proxy = null
    // <Buffer> full data for the game object as a buffer
    this.full = null
    // byte count of the full data
    this.fullBytes = 0
    // <Buffer> partial data for the game object, prop changes since last tick

    this.netSchema = null
}

NetMessage.prototype.constructor = NetMessage


NetMessage.prototype.copy = function() {
    var netObject = new NetMessage()
    netObject.id = this.id
    netObject.tickSerialized = this.tickSerialized
    netObject.proxy = this.proxy.copy()
    netObject.full = this.full
    netObject.fullBytes = this.fullBytes
}

/*
* Serializes a message and stores it in this netObject
* sets values for proxy, full, fullBytes
*/
NetMessage.prototype.serialize = function(message, tick) {
    var netSchema = message.netSchema
    this.id = message.id
    this.proxy = BinarySerializer.proxify(message)
    this.netSchema = message.netSchema
    this.tickSerialized = tick
}


module.exports = NetMessage
},{"../binary/Binary":33,"../binary/BinarySerializer":34,"../binary/BinaryType":35}],74:[function(require,module,exports){
var BinarySerializer = require('../binary/BinarySerializer')
var BinaryType = require('../binary/BinaryType')
var Binary = require('../binary/Binary')
var NetEntity = require('./NetEntity')
var NetEvent = require('./NetEvent')
var NetMessage = require('./NetMessage')

/*
* Holds NetObjects
*/
function NetObjectCache(instance) {
    // cached netproxy of entities
    this.netEntities = {}
    // cached netproxy of positional events
    this.netEvents = {}
    // cached netproxy of messages
    this.netMessages = {}
    this.instance = instance
}

/*
* Returns a netEntity (a representation of a entity, see: NetObject)
* if this object was not already in the cache, it is created and cached
* if this object was already in the cache, it is reserialized
* reserialization checks the object for changes and updates the netEntity
* objects will only undergo [re]serialization once per tick, subsequent calls to
* get() will return a cached copy
*/
NetObjectCache.prototype.getEntity = function(id, tick) {
	var entity = this.instance.getEntity(id)

	// update the cache, if needed
    if (!this.netEntities[id]) {
    	// not in the cache? serialize and add it
    	var netEntity = new NetEntity()
    	netEntity.serialize(entity, tick)
    	this.netEntities[id] = netEntity
    	return netEntity
    } else {
    	// already in cache? reserialize (only once per tick)
    	var netEntity = this.netEntities[id]
    	if (netEntity.tickSerialized !== tick) {
    		netEntity.reserialize(entity, tick)
    	} 
    	// return cached copy
    	return netEntity
    }
}


NetObjectCache.prototype.getEvent = function(id, tick) {
    var gameEvent = this.instance.getEventObjectById(id)

    // update the cache, if needed
    if (!this.netEvents[id]) {
        // not in the cache? serialize and add it
        var netEvent = new NetEvent()
        netEvent.serialize(gameEvent, tick)

        //console.log('seralized netevent', netEvent)
        this.netEvents[id] = netEvent
        return netEvent
    } else {
        // return cached copy
        var netEvent = this.netEvents[id]
        return netEvent
    }   
}

NetObjectCache.prototype.getMessage = function(id, tick) {
    var message = this.instance.getMessage(id)

    // update the cache, if needed
    if (!this.netMessages[id]) {
        // not in the cache? serialize and add it
        var netMessage = new NetMessage()
        netMessage.serialize(message, tick)
        this.netMessages[id] = netMessage
        return netMessage
    } else {
        // return cached copy
        var netMessage = this.netMessages[id]
        return netMessage
    }   
}


module.exports = NetObjectCache
},{"../binary/Binary":33,"../binary/BinarySerializer":34,"../binary/BinaryType":35,"./NetEntity":71,"./NetEvent":72,"./NetMessage":73}],75:[function(require,module,exports){
var AABB = require('./AABB')

var minimumNodeSize = 60

// quadtree
function Quadtree(aabb) {
	this.initialize(aabb)
}

Quadtree.pool = []
Quadtree.instanceCount = 0
Quadtree.inUseCount = 0
Quadtree.logStats = function() {
	console.log(
		'Quadtrees', 
		'total', Quadtree.instanceCount, 
		'avail', Quadtree.instanceCount - Quadtree.inUseCount, 
		'in-use', Quadtree.inUseCount
	)
}

Quadtree.create = function(aabb) {
	//return new Quadtree(aabb)
	var instance

	if (Quadtree.pool.length === 0) {
		instance = new Quadtree(aabb)
        Quadtree.instanceCount++        
    } else {
        instance = Quadtree.pool.pop()
        instance.initialize(aabb)
    }
    
    Quadtree.inUseCount++
	return instance
}


/*
* Converts a quadtree into an array of aabbs, DEBUG USE
*/
Quadtree.prototype._getAllAABBs = function(aabbs) {

	aabbs.push(this.aabb)

	if (!this.nw) return aabbs

	aabbs.concat(this.nw._getAllAABBs(aabbs))
	aabbs.concat(this.ne._getAllAABBs(aabbs))
	aabbs.concat(this.sw._getAllAABBs(aabbs))
	aabbs.concat(this.se._getAllAABBs(aabbs))
	
	return aabbs
}

Quadtree.prototype.getAllAABBs = function() {
	var aabbs = []
	return this._getAllAABBs(aabbs)
}



Quadtree.prototype.release = function() {	
	Quadtree.pool.push(this)
	Quadtree.inUseCount--

	this.aabb.release()
	this.aabb = null	

	if (this.nw) this.nw.release()
	if (this.ne) this.ne.release()
	if (this.sw) this.sw.release()
	if (this.se) this.se.release()
}

Quadtree.prototype.initialize = function(aabb) {
	// boundaries
	this.aabb = aabb
	// max points this node can hold; 4 for leaf, 0 for branch
	this.capacity = 4
	// points contained in this node
	this.points = [] 

	// sub quadtree nodes
	this.nw = null
	this.ne = null
	this.sw = null
	this.se = null
}


/* Insert a point! Quadtree will subdivide accordingly */
Quadtree.prototype.insert = function(gameObject) {
	// point not within this node's bounds? return immediately
	if (!this.aabb.containsPoint(gameObject.x, gameObject.y)) return false

	// if node is not too small nor too full add the point to this node and return
	if (this.points.length < this.capacity || this.aabb.halfWidth * 2 < minimumNodeSize) {
		this.points.push(gameObject)
		return true
	}	

	// too crowded? split this node into 4 nodes
	if (!this.nw) this.subdivide()

	// recursively add this point to whichever subnode qualifies
	if (this.nw.insert(gameObject)) return true
	if (this.ne.insert(gameObject)) return true
	if (this.sw.insert(gameObject)) return true
	if (this.se.insert(gameObject)) return true

	return false // unreachable code, in theory
}

Quadtree.prototype.subdivide = function() {
	this.nw = Quadtree.create(
		AABB.create(
			this.aabb.x - this.aabb.halfWidth * 0.5, 
			this.aabb.y - this.aabb.halfHeight * 0.5, 
			this.aabb.halfWidth * 0.5,
			this.aabb.halfHeight * 0.5
		)
	)
	this.nw.parent = this

	this.ne = Quadtree.create(
		AABB.create( 
			this.aabb.x + this.aabb.halfWidth * 0.5,
			this.aabb.y - this.aabb.halfHeight * 0.5, 
			this.aabb.halfWidth * 0.5,
			this.aabb.halfHeight * 0.5
		)
	)
	this.ne.parent = this

	this.sw = Quadtree.create(
		AABB.create( 
			this.aabb.x - this.aabb.halfHeight * 0.5,
			this.aabb.y + this.aabb.halfHeight * 0.5, 
			this.aabb.halfWidth * 0.5,
			this.aabb.halfHeight * 0.5
		)
	)
	this.sw.parent = this

	this.se = Quadtree.create(
		AABB.create( 
			this.aabb.x + this.aabb.halfWidth * 0.5,
			this.aabb.y + this.aabb.halfHeight * 0.5, 
			this.aabb.halfWidth * 0.5,
			this.aabb.halfHeight * 0.5
		)
	)
	this.se.parent = this

	// transfer points from this node to the appropriate subnodes	
	for (var i = 0; i < this.points.length; i++) {
		this.nw.insert(this.points[i])
		this.ne.insert(this.points[i])
		this.sw.insert(this.points[i])
		this.se.insert(this.points[i])
	}

	// remove the points from this node now that they've been transfered
	this.points = []
	// this is now a branch, not a leaf, and cannot have points added to it
	this.capacity = 0
}

Quadtree.prototype.queryRange = function(aabb, pointsInRange) {
	if (!this.aabb.intersects(aabb)) return pointsInRange

	for (var i = 0; i < this.points.length; i++) {
		if (aabb.containsPoint(this.points[i].x, this.points[i].y)) {
			pointsInRange.push(this.points[i])
		}
	}

	if (!this.nw) return pointsInRange

	pointsInRange.concat(this.nw.queryRange(aabb, pointsInRange))
	pointsInRange.concat(this.ne.queryRange(aabb, pointsInRange))
	pointsInRange.concat(this.sw.queryRange(aabb, pointsInRange))
	pointsInRange.concat(this.se.queryRange(aabb, pointsInRange))
	
	return pointsInRange
}

Quadtree.prototype.select = function(aabb) {
	var gameObjects = []
	this.queryRange(aabb, gameObjects)
	return gameObjects
}

// limit is optional
Quadtree.prototype.selectIds = function(aabb, limit) {
	var gameObjects = this.select(aabb)
	var ids = []

	var max = gameObjects.length

	if (typeof limit !== 'undefined')
		max = (gameObjects.length > limit) ? limit : gameObjects.length

	for (var i = 0; i < max; i++) {
		ids.push(gameObjects[i].id)
	}
	return ids
}

Quadtree.prototype.selectObjectAndEventIds = function(aabb, limit) {
	var gameObjects = this.select(aabb)
	var objectIds = []
	var eventIds = []

	var max = gameObjects.length

	if (typeof limit !== 'undefined')
		max = (gameObjects.length > limit) ? limit : gameObjects.length

	for (var i = 0; i < max; i++) {
		if (gameObjects[i].type === 1) {
			objectIds.push(gameObjects[i].id)
		} else if (gameObjects[i].type === 2) {
			eventIds.push(gameObjects[i].id)
		}
		
	}
	return { objectIds: objectIds, eventIds: eventIds }
}


module.exports = Quadtree

},{"./AABB":62}]},{},[6]);
