
var nengi = require('../nengi/nengi')
var config = require('./config')

module.exports = {
	config: config,

	entityConstructors: new nengi.Constructors([
		require('./model/SumoShip'), 
		require('./model/Asteroid'),
		require('./model/Projectile')
	]),

	eventConstructors: new nengi.Constructors([
		//require('./model/MouseOverEvent') // not used
	]),

	messageConstructors: new nengi.Constructors([
		nengi.IdentityMessage // leave this line
	]),

	inputConstructors: new nengi.Constructors([
		nengi.InputFrame, // leave this line
		require('./model/input/Move'),
		require('./model/input/ChargeWeapon'),
		require('./model/input/DischargeWeapon'),
		require('./model/input/UseThruster'),
	])

}

