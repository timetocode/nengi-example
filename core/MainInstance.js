var nengi = require('../nengi/nengi')
var Asteroid = require('./model/Asteroid')
var SumoShip = require('./model/SumoShip')
var Projectile = require('./model/Projectile')
var SumoRingBoundary = require('./model/SumoRingBoundary')
var AsteroidStorm = require('./model/AsteroidStorm')
var IdentityMessage = nengi.IdentityMessage
var common = require('./common')


function MainInstance(common) {
	nengi.Instance.call(this, common)
	this.setup()
	this.sumoRingBoundary = new SumoRingBoundary()

	this.asteroidStormTimestamp = Date.now()
	this.asteroidStormInterval = 6000

	this.asteroidStorm = new AsteroidStorm(this, this.sumoRingBoundary)
	this.asteroidStorm.begin()

	this.debugCounter = 0
}

MainInstance.prototype = Object.create(nengi.Instance.prototype)
MainInstance.prototype.constructor = MainInstance


MainInstance.prototype.destroyShipAndBeginRespawn = function(client) {
	client.entity.isAlive = false
	client.deathTimestamp = Date.now()

	var self = this
	setTimeout(function() {
		self.removeEntity(client.entity)
		// RESPAWN!
		self.spawnSumoShip(client)
	}, 3000)
}

MainInstance.prototype.spawnSumoShip = function(client) {
	var sumoShip = new SumoShip()
	sumoShip.x = 500//nengi.MathEx.random(0, 10000)
	sumoShip.y = 500//nengi.MathEx.random(0, 10000)
	this.addEntity(sumoShip)

	

	var self = this
	//setTimeout(function() {
		var identityMessage = new IdentityMessage()
		identityMessage.identity = sumoShip.id
		self.addMessage(identityMessage, client)
	//}, 100)
	

	// store a reference to sumoShip in this client
	client.entity = sumoShip
	this.debugCounter++

	if (this.debugCounter < 5) {
		sumoShip.x = 500
		sumoShip.y = 500
	}
}

MainInstance.prototype.setup = function() {	


	var asteroid = new Asteroid()
	asteroid.x = asteroid.y = 200
	asteroid.physics.addExternalForce({x: 50000, y: 50000 })
	this.addEntity(asteroid)

	var self = this
	// setup a sumoShip spawn on 'connect'
	this.on('connect', function(client) {
		console.log('connected to instance')
		self.addClient(client)
		self.spawnSumoShip(client)
		console.log('info', 'c.id', client.id, 'e.id', client.entity.id, client.entity.x, client.entity.y)
	})

	this.on('disconnect', function(client) {
		self.removeEntity(client.entity)
		self.removeClient(client)
	})

	// setup movement on 'playerInput'
	this.on('inputContext', function(inputContext) {
        // move the client's entity per the input
        //console.log('handler', inputContext.input)
        var entity = inputContext.client.entity
        // TODO use input.type or input.n_type instead of input.constructor.name
        if (inputContext.input.constructor.name === 'Move') {
        	entity.move(inputContext.input)
        } else if (inputContext.input.constructor.name === 'ChargeWeapon') {
        	entity.chargeWeapon(inputContext.input)
        } else if (inputContext.input.constructor.name === 'DischargeWeapon') {
        	entity.dischargeWeapon(inputContext.input)
        }
        
    })

	// tell the asteroid storm to begin 10 seconds after the previous one ended
    this.on('AsteroidStormEnded', function() {
    	console.log('STORM OVER, resetting')
    	setTimeout(function() {
    		console.log('BEGINING STORM AGAIN')
    		self.asteroidStorm.begin()
    	}, 20000)
    })
}



MainInstance.prototype.update = function(delta, tick, now) {
	// TODO: have a specific place for components/entities that are not networked
	// but still need their update called
	this.asteroidStorm.update(delta, tick, now)


	// all of the below could just be wrapped into a component and then called
	// via someComponent.update(delta)

	// collisions between projectiles and asteroids
	// TODO (maybe): assign projectiles to the client that fired them and 
	// implement lag compensation for them as well
	for (var i = 0; i < this.entities.length; i++) {
		var entity = this.entities[i]
		if (entity.constructor.name === 'Projectile') {
			for (var j = 0; j < this.entities.length; j++) {
				var otherEntity = this.entities[j]

				if (otherEntity.constructor.name === 'Asteroid') {

					var collided = entity.collider.checkCollision(otherEntity.collider)
	
					if (collided) {
						otherEntity.physics.addExternalForce(entity.getForce())
					}
				}
			}
		} else if (entity.constructor.name === 'Asteroid') {
			for (var j = 0; j < this.entities.length; j++) {
				var otherEntity = this.entities[j]

				if (otherEntity.constructor.name === 'Asteroid') {

						var dx = otherEntity.x - entity.x
						var dy = otherEntity.y - entity.y

					var collided = entity.collider.checkCollision(otherEntity.collider)
	
					if (collided) {
						otherEntity.physics.addExternalForce({ x: dx * 5000, y: dy * 5000 })
					}
				}
			}
		}
	}

	// checks for each player 
	// 1) sumoship out of sumoring
    // 2) lag compensated sumoship hit by projectile or asteroid
	for (var i = 0; i < this.clients.length; i++) {
		var client = this.clients[i]
		var entity = client.entity

		client.view.x = entity.x
		client.view.y = entity.y


		// destroy sumoships that go outside of the sumo ring
		if (entity) {
			if (!this.sumoRingBoundary.isPointInside({ x: client.entity.x, y: client.entity.y })) {
				if (client.entity.isAlive) {
					this.destroyShipAndBeginRespawn(client)
				}
				
			}
		}

		var past = this.historian.lagCompensator.compensateArea(
			client, 
			client.view, 
			tick
		)

		for (var k = 0; k < past.length; k++) {
			var entityProxy = past[k]
			var nearbyEntity = this.getEntity(entityProxy.id)



			if (entityProxy.id != client.entity.id && nearbyEntity) {

				if (nearbyEntity.owner && nearbyEntity.owner.id === entity.id) {
					//console.log('was my own')
					//continue
				}

				/*
				console.log(
					nearbyEntity.constructor.name,
					'ping',
					client.ping,
					'past', 
					Math.floor(entityProxy.x), 
					Math.floor(entityProxy.y), 
					'now', 
					Math.floor(nearbyEntity.x), 
					Math.floor(nearbyEntity.y)
				)
				*/
	
			
			
				var temp = { 
					x: nearbyEntity.collider.shape.pos.x,
					y: nearbyEntity.collider.shape.pos.y
				}

				nearbyEntity.collider.shape.pos.x = entityProxy.x
				nearbyEntity.collider.shape.pos.y = entityProxy.y
				

				if (entity.collider.canCollideWith(nearbyEntity.collider)) {
					var collided = entity.collider.checkCollision(nearbyEntity.collider)

					if (collided) {
						var dx = entityProxy.x - client.entity.x
						var dy = entityProxy.y - client.entity.y
					
						
						

						if (nearbyEntity.constructor.name === 'Projectile') {
							//nearbyEntity.physics.addExternalForce({ x: dx * 5000, y: dy * 5000 })
							client.entity.physics.addExternalForce({ x: -dx * 5000, y: -dy * 5000 })
						} else if (nearbyEntity.constructor.name === 'SumoShip') {						
							nearbyEntity.physics.addExternalForce({ x: dx * 500, y: dy * 500 })
						} else  if (nearbyEntity.constructor.name === 'Asteroid' || nearbyEntity.constructor.name === 'Projectile') {						
							nearbyEntity.physics.addExternalForce({ x: dx * 5000, y: dy * 5000 })
							client.entity.physics.addExternalForce({ x: -dx * 5000, y: -dy * 5000 })
						} else {
							//client.entity.physics.addExternalForce({ x: -dx * 5000, y: -dy * 5000 })
						}
					}
				}

				
				nearbyEntity.collider.shape.pos.x = temp.x
				nearbyEntity.collider.shape.pos.y = temp.y
				
				
			}
		}		
	}
}

module.exports = MainInstance