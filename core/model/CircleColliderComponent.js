var nengi = require('../../nengi/nengi')
var SAT = require('../external/SAT')
var Vector2 = nengi.Vector2

function CircleColliderComponent(gameObject, config) {
	nengi.Component.call(this, gameObject, config)
	this.radius = 25
	this.tags = []
	this.collideWithTags = []

	this.colliderType = 'circle'
	this.shape = new SAT.Circle(new SAT.Vector(0,0), this.radius)
}

CircleColliderComponent.prototype = Object.create(nengi.Component.prototype)
CircleColliderComponent.prototype.constructor = CircleColliderComponent


CircleColliderComponent.prototype.canCollideWith = function(collider) {
	for (var i = 0; i < collider.tags.length; i++) {
		for (var j = 0; j < this.collideWithTags.length; j++) {
			if (collider.tags[i] === this.collideWithTags[j]) {
				return true
			}
		}
	}
	return false
}

CircleColliderComponent.prototype.checkCollision = function(collider) {
	var response = new SAT.Response()
	var collided = false

	if (collider.colliderType === 'circle') {
		collided = SAT.testCircleCircle(collider.shape, this.shape, response)
	} else if (collider.colliderType === 'polygon') {
		collided = SAT.testPolygonCircle(collider.shape, this.shape, response)
	} else if (collider.colliderType === 'point') {
		collided = SAT.pointInCircle(collider, this.shape)
	}

	if (collided) {
		//console.log('collision?', collided)
		//console.log('response', response)

		//uncolliding test
		//this.x += response.overlapV.x
		//this.y += response.overlapV.y
	}
	return collided

}


CircleColliderComponent.prototype.update = function() {
	// keep the SAT collider in sync with the entity
	this.shape.pos.x = this.x
	this.shape.pos.y = this.y
	this.shape.r = this.radius

}
module.exports = CircleColliderComponent