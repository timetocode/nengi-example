var nengi = require('../../nengi/nengi')
var CircleColliderComponent = require('./CircleColliderComponent')
var PhysicsComponent = require('./PhysicsComponent')

function Asteroid() {
	nengi.Entity.call(this)
	this.radius = nengi.MathEx.random(10, 60)
	this.addComponent('physics', new PhysicsComponent(this))
	this.addComponent('collider', new CircleColliderComponent(this))
	this.collider.tags = ['asteroid']
	this.collider.collideWithTags = ['player', 'projectile', 'asteroid']
	this.rotation = 0
	this.physics.frictionCoef = 0
	this.physics.mass = this.radius * 100
}

Asteroid.prototype = Object.create(nengi.Entity.prototype)
Asteroid.prototype.constructor = Asteroid


Asteroid.prototype.update = function(delta, tick, now) {
	//console.log('asteroid updating!', this.rotation)
	//this.rotation += this.rotationVelocity * delta
	//console.log('projectile movement calculation', this.vx, this.speed, delta)
	//this.x += this.heading.x * this.speed * delta
	//this.y += this.heading.y * this.speed * delta

	//this.physics.addExternalForce({x: 5000, y: 5000 })
	this.collider.radius = this.radius

	// crude bounds check	
	if (this.x > 5000 || this.x < -5000 || this.y > 5000 || this.y < -5000) {
		this.instance.removeEntity(this)
	}
	

}

Asteroid.prototype.launch = function(origin, forceVector) {
	//console.log('launch', origin, destination, velocityScalar)
	this.position = origin
	this.physics.addInternalForce(forceVector)
}



// specify which properties should be networked, and their value types
Asteroid.prototype.netSchema = nengi.createEntitySchema({
	'id': nengi.UInt16,
	'type': nengi.UInt8,
	'x': nengi.Int16,
	'y': nengi.Int16,
	'radius': nengi.UInt8
	//'isAlive': { binaryType: nengi.Boolean, interp: false },
	//'rotation': nengi.Rotation
}, {
	'x': { delta: true, binaryType: nengi.Int8 },
	'y': { delta: true, binaryType: nengi.Int8 }
})

module.exports = Asteroid

