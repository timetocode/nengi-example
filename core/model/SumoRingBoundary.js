var CircleColliderComponent = require('./CircleColliderComponent')

function SumoRingBoundary() {
	this.collider = new CircleColliderComponent(this)
	this.x = 500
	this.y = 500
	this.radius = 350

	this.collider.shape.pos.x = this.x
	this.collider.shape.pos.y = this.y
	this.collider.shape.r = this.radius
}

SumoRingBoundary.prototype.isPointInside = function(point) {
	return this.collider.checkCollision({ x: point.x, y: point.y, colliderType: 'point'})
}

module.exports = SumoRingBoundary