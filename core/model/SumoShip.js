var nengi = require('../../nengi/nengi')
var PhysicsComponent = require('./PhysicsComponent')
var CircleColliderComponent = require('./CircleColliderComponent')
var Projectile = require('./Projectile')

function SumoShip() {
	nengi.Entity.call(this)

	this.rotation = 0
	this.addComponent('physics', new PhysicsComponent(this))
	this.addComponent('collider', new CircleColliderComponent(this))
	this.collider.tags = ['player']
	this.collider.collideWithTags = ['asteroid', 'projectile', 'player']


	this.isAlive = true

	this.isWeaponCharging = false
	this.weaponChargeLevel = 1
	this.weaponChargeTimestamp = Date.now()
}

SumoShip.prototype = Object.create(nengi.Entity.prototype)
SumoShip.prototype.constructor = SumoShip

SumoShip.prototype.update = function(delta) {
	//console.log('SumoShip update', delta)
	if (this.isWeaponCharging) {
		var elapsed = Date.now() - this.weaponChargeTimestamp
		var tier = Math.floor(elapsed/1000) + 1
		if (tier > 3) { tier = 3 }

		this.weaponChargeLevel = tier
	} else {
		this.weaponChargeLevel = 1
	}
}

SumoShip.prototype.applyInput = function(input) {
	if (input.constructor.name === 'Move') {
		//console.log('move being applied')
		this.move(input)
	}
}

SumoShip.prototype.move = function(move) {
	if (!this.isAlive) return
	//console.log('move', this.physics.addExternalForce)
	if (move.W) {
		this.physics.addInternalForce({ x: 0, y: -15000 })
		//this.y -= 1 * this.speed
	}

	if (move.A) {
		this.physics.addInternalForce({ x: -15000, y: 0 })
		//this.x -= 1 * this.speed
	}

	if (move.S) {
		this.physics.addInternalForce({ x: 0, y: 15000 })
		//this.y += 1 * this.speed
	}

	if (move.D) {
		this.physics.addInternalForce({ x: 15000, y: 0 })
		//this.x += 1 * this.speed
	}

	var dx = this.x - move.mx
	var dy = this.y - move.my
	this.rotation = Math.atan2(dy, dx) + 90 * Math.PI/180
}

SumoShip.prototype.chargeWeapon = function(charge) {
	if (!this.isAlive) return
	// fires a shot instantly, and begins charging a larger shot
	var dx = this.x - charge.x
	var dy = this.y - charge.y
	this.rotation = Math.atan2(dy, dx) + 90 * Math.PI/180

	this.isWeaponCharging = true
	this.weaponChargeTimestamp = Date.now()

	var projectile = new Projectile()
	projectile.owner = this
	projectile.tier = 1
	projectile.launch(
		new nengi.Vector2(this.x, this.y),
		new nengi.Vector2(charge.x, charge.y),
		600
	)

	var kickback = new nengi.Vector2(dx, dy)
	kickback.normalize()

	this.physics.addExternalForce({ 
		x: kickback.x * 250000, 
		y: kickback.y * 250000 
	})

	this.instance.addEntity(projectile)
}

SumoShip.prototype.dischargeWeapon = function(charge) {
	if (!this.isAlive) return
	// how long have we been charging?
	var elapsed = Date.now() - this.weaponChargeTimestamp
	var tier = Math.floor(elapsed/1000) + 1
	if (tier > 3) { tier = 3 }

	// do not discharge unless charge reached tier 2 (at least 1 second of charging)
	if (tier < 2) {
		this.isWeaponCharging = false
		return
	}

	// a larger shot was released
	this.isWeaponCharging = false
	//this.weaponChargeLevel = 1
	var dx = this.x - charge.x
	var dy = this.y - charge.y
	this.rotation = Math.atan2(dy, dx) + 90 * Math.PI/180

	var projectile = new Projectile()
	projectile.owner = this

	projectile.tier = tier
	projectile.launch(
		new nengi.Vector2(this.x, this.y),
		new nengi.Vector2(charge.x, charge.y),
		600
	)

	var kickback = new nengi.Vector2(dx, dy)
	kickback.normalize()

	this.physics.addExternalForce({ 
		x: kickback.x * 250000 * tier, 
		y: kickback.y * 250000 * tier
	})

	//console.log('discharged', tier, elapsed)
	this.instance.addEntity(projectile)
}

// specify which properties should be networked, and their value types
SumoShip.prototype.netSchema = nengi.createEntitySchema({
	'id': nengi.UInt16,
	'type': nengi.UInt8,
	'x': nengi.Int16,
	'y': nengi.Int16,
	'isAlive': { binaryType: nengi.Boolean, interp: false },
	'rotation': nengi.Rotation,
	'weaponChargeLevel': { binaryType: nengi.UInt8, interp: false }
}, {
	'x': { delta: true, binaryType: nengi.Int8 },
	'y': { delta: true, binaryType: nengi.Int8 }
})

module.exports = SumoShip

