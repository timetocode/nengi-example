var nengi = require('../../nengi/nengi')
var Asteroid = require('./Asteroid')

function AsteroidStorm(instance, sumoRing) {	
	this.instance = instance
	this.sumoRing = sumoRing

	this.isActive = false

	// intensity of the storm, builds to a climax and then remits
	this.intensity = 0
	this.intensityIncreaseInterval = 1000
	this.intensityIncreaseTimestamp = Date.now()
	this.isIntensifying = true

	// the dominant direction the asteroids travel
	this.target = new nengi.Vector2(0, 0)
	// where the asteroids come from
	this.origin = new nengi.Vector2(0, 0)
}

// begins a new storm from a random position
AsteroidStorm.prototype.begin = function() {
	// a random angle in radians
	var theta = nengi.MathEx.degreesToRadians(nengi.MathEx.random(0, 360))

	// a random position outside of the sumo ring, per the above angle
	this.origin.x = (2000 * Math.cos(theta)) + this.sumoRing.x
	this.origin.y = (2000 * Math.sin(theta)) + this.sumoRing.y

	// aim the the shower towards the sumo ring
	this.target.x = this.sumoRing.x
	this.target.y = this.sumoRing.y

	this.intensity = 1
	this.isIntensifying = true
	this.isActive = true
}

AsteroidStorm.prototype.update = function(delta, tick, now) {
	if (!this.isActive) return

	var now = Date.now()

	if (now - this.intensityIncreaseTimestamp > this.intensityIncreaseInterval) {
		this.intensity += (this.isIntensifying) ? 2 : -2
		this.intensityIncreaseTimestamp = now
		//console.log('AsteroidStorm intensity', this.intensity)
	}

	if (this.intensity > 20) {
		this.isIntensifying = false
	}

	if (this.intensity <= 0) {
		console.log('AsteroidStorm finished!')
		this.instance.emit('AsteroidStormEnded')
		this.isActive = false
	}


	if (nengi.MathEx.random(0, 100) > 100 - this.intensity) {
		//console.log('spawning asteroid', this.origin, this.direction)

		var origin = new nengi.Vector2(
			this.origin.x + nengi.MathEx.random(-500, 500), 
			this.origin.y + nengi.MathEx.random(-500, 500)
		)

		var target = new nengi.Vector2(
			this.target.x + nengi.MathEx.random(-300, 300), 
			this.target.y + nengi.MathEx.random(-300, 300)
		)

		var dx = target.x - origin.x
		var dy = target.y - origin.y

		var direction = new nengi.Vector2(dx, dy)
		direction.normalize()
		direction.multiplyScalar(nengi.MathEx.random(1000000, 2500000))


		var asteroid = new Asteroid()
		asteroid.x = origin.x
		asteroid.y = origin.y
		asteroid.physics.addExternalForce(direction)//{x: direction.x * 50000, y: direction.y * 50000})

		this.instance.addEntity(asteroid)
	}
}


module.exports = AsteroidStorm