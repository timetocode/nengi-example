var nengi = require('../../nengi/nengi')

var Vector2 = nengi.Vector2

function PhysicsComponent(gameObject, config) {
	nengi.Component.call(this, gameObject, config)

	// force applied from external sources
	this.externalForce = new Vector2()

	// force applied from own entity (e.g. engine or movement abilities)
	this.internalForce = new Vector2()

	// velocity of this entity
	this.velocity = new Vector2()

	this.mass = 1000

	this.frictionCoef = 3
}

PhysicsComponent.prototype = Object.create(nengi.Component.prototype)
PhysicsComponent.prototype.constructor = PhysicsComponent


PhysicsComponent.prototype.initialize = function() {

}

PhysicsComponent.prototype.addInternalForce = function(force) {
	this.internalForce.add(force)
}

PhysicsComponent.prototype.addExternalForce = function(force) {
	//console.log('addExternalForce', force)
	this.externalForce.add(force)
}

PhysicsComponent.prototype.update = function(delta, tick, now) {

	var externalAcceleration = new Vector2(
		this.externalForce.x / this.mass,
		this.externalForce.y / this.mass
	)

	var internalAcceleration = new Vector2(
		this.internalForce.x / this.mass,
		this.internalForce.y / this.mass
	)

 	// artificial friction to make the controls nice
	this.velocity.x += (externalAcceleration.x - this.frictionCoef * this.velocity.x) * delta
	this.velocity.y += (externalAcceleration.y - this.frictionCoef * this.velocity.y) * delta

	this.velocity.x += (internalAcceleration.x - this.frictionCoef * this.velocity.x) * delta
	this.velocity.y += (internalAcceleration.y - this.frictionCoef * this.velocity.y) * delta

	this.position.add(this.velocity)

    // set forces to 0 
	this.externalForce.x = 0
	this.externalForce.y = 0
	this.internalForce.x = 0
	this.internalForce.y = 0
}

module.exports = PhysicsComponent