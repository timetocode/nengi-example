var nengi = require('../../../nengi/nengi')

function UseThruster() {
	this.id = -1
}

UseThruster.prototype.constructor = UseThruster

UseThruster.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8
})

module.exports = UseThruster

