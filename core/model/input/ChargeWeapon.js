var nengi = require('../../../nengi/nengi')

function ChargeWeapon() {
	this.id = -1
	this.x = 0
	this.y = 0
}

ChargeWeapon.prototype.constructor = ChargeWeapon

ChargeWeapon.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8,
	'x': nengi.UInt16,
	'y': nengi.UInt16
})

module.exports = ChargeWeapon

