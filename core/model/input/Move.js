var nengi = require('../../../nengi/nengi')

function Move() {
	this.id = -1
	this.W = false
	this.A = false
	this.S = false
	this.D = false
	this.mx = 0
	this.my = 0
}

Move.prototype.constructor = Move

Move.prototype.netSchema = nengi.createSchema({
	'id': nengi.UInt8,
	'type': nengi.UInt8,
	'W': nengi.Boolean,
	'A': nengi.Boolean,
	'S': nengi.Boolean,
	'D': nengi.Boolean,
	'mx': nengi.UInt16,
	'my': nengi.UInt16
})

module.exports = Move

