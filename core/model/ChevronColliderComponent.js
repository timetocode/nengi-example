var nengi = require('../../nengi/nengi')
var SAT = require('../external/SAT')
var Vector2 = nengi.Vector2

function ChevronColliderComponent(gameObject, config) {
	nengi.Component.call(this, gameObject, config)
	//this.radius = 25
	this.tags = []
	this.collideWithTags = []
	this.colliderType = 'polygon'

	// default is tier 1
	var tier = 1
	this.shape = new SAT.Polygon(new SAT.Vector(0,0), [
		new SAT.Vector(0, -10 * tier),
		new SAT.Vector(14 * tier, 9 * tier),
		new SAT.Vector(-14 * tier, 9 * tier)
	])
}

ChevronColliderComponent.prototype = Object.create(nengi.Component.prototype)
ChevronColliderComponent.prototype.constructor = ChevronColliderComponent


ChevronColliderComponent.prototype.canCollideWith = function(collider) {
	for (var i = 0; i < collider.tags.length; i++) {
		for (var j = 0; j < this.collideWithTags.length; j++) {
			if (collider.tags[i] === this.collideWithTags[j]) {
				return true
			}
		}
	}
	return false
}

ChevronColliderComponent.prototype.adjustTier = function(tier) {
	this.shape = new SAT.Polygon(new SAT.Vector(0,0), [
		new SAT.Vector(0, -10 * tier),
		new SAT.Vector(14 * tier, 9 * tier),
		new SAT.Vector(-14 * tier, 9 * tier)
	])
}

ChevronColliderComponent.prototype.checkCollision = function(collider) {
	var response = new SAT.Response()
	var collided = false

	if (collider.colliderType === 'circle') {
		collided = SAT.testCirclePolygon(collider.shape, this.shape, response)
	} else if (collider.colliderType === 'polygon') {
		//console.log('ChevronColliderComponent checkCollison polygon')
		collided = SAT.testPolygonPolyon(collider.shape, this.shape, response)
	}

	if (collided) {
		//console.log('collision?', collided)
		//console.log('response', response)

		//uncolliding test
		//this.x += response.overlapV.x
		//this.y += response.overlapV.y
	}
	return collided

}


ChevronColliderComponent.prototype.update = function() {
	//console.log('this.x', this.x, this.gameObject.x)
	// keep the SAT collider in sync with the entity
	//this.radius = this.gameObject.radius
	this.shape.pos.x = this.gameObject.x
	this.shape.pos.y = this.gameObject.y
	this.shape.setAngle(this.gameObject.rotation)
	//console.log('poly', this.gameObject.x)

	//TODO: rotation

	//this.circle.r = this.radius
	//console.log('radiu updated', this.radius)
}
module.exports = ChevronColliderComponent