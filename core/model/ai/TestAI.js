var nengi = require('../../../nengi/nengi')

function TestAI(gameObject, config) {
	nengi.Component.call(this, gameObject, config)
}

TestAI.prototype = Object.create(nengi.Component.prototype)
TestAI.prototype.constructor = TestAI

var min = 0
var max = 500

TestAI.prototype.initialize = function() {
	this.destination = {
		x: nengi.MathEx.random(min, max),
		y: nengi.MathEx.random(min, max)
	}


	this.speed = nengi.MathEx.random(20,60)
	this.xpos = true
	this.ypos = true
}

TestAI.prototype.update = function(delta, tick, now) {
	//console.log(delta, tick, now)
	if (this.xpos)
		this.x += this.speed * delta
	else
		this.x -= this.speed * delta

	if (this.ypos)
		this.y += this.speed * delta
	else
		this.y -= this.speed * delta

	if (this.x >= max) {
		this.xpos = false
	} else if (this.x <= min) {
		this.xpos = true
	}

	if (this.y >= max) {
		this.ypos = false
	} else if (this.y <= min) {
		this.ypos = true
	}

	if (nengi.MathEx.random(1,15) === 5)
		this.gameObject.test = nengi.MathEx.random(8,15)
	//console.log(this.gameObject.test)

}

module.exports = TestAI