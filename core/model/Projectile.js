var nengi = require('../../nengi/nengi')
var ChevronColliderComponent = require('./ChevronColliderComponent')


function Projectile() {
	nengi.Entity.call(this)
	
	//this.hp = 100

	this.target = null
	//this.addComponent('ai', new TestAI(this))
	//this.radius = 5//nengi.MathEx.random(20, 80)
	this.rotation = 0
	//this.rotationVelocity = nengi.MathEx.random(-10, 10)
	this.heading = new nengi.Vector2(0, 0)

	this.addComponent('collider', new ChevronColliderComponent(this))
	this.collider.tags = ['projectile']
	this.collider.collideWithTags = ['player', 'asteroid']
	
	this.speed = 0

	this.owner = null
	this.tier = 1
}

Projectile.prototype = Object.create(nengi.Entity.prototype)
Projectile.prototype.constructor = Projectile


Projectile.prototype.update = function(delta, tick, now) {
	//console.log('asteroid updating!', this.rotation)
	//this.rotation += this.rotationVelocity * delta
	//console.log('projectile movement calculation', this.vx, this.speed, delta)
	this.x += this.heading.x * this.speed * delta
	this.y += this.heading.y * this.speed * delta

	// crude bounds check
	if (this.x > 1000 || this.x < 0 || this.y > 1000 || this.y < 0) {
		this.instance.removeEntity(this)
	}

}

Projectile.prototype.launch = function(origin, destination, velocityScalar) {
	//console.log('launch', origin, destination, velocityScalar)
	this.position = origin
	this.destination = destination

	var dx = destination.x - this.x
	var dy = destination.y - this.y

	this.heading = new nengi.Vector2(dx, dy)
	this.heading.normalize()

	this.speed = velocityScalar

	// rotate the projectile to face the direction it is headed
	this.rotation = Math.atan2(-dy, -dx) + 90 * Math.PI/180
	this.collider.adjustTier(this.tier)
}

Projectile.prototype.getForce = function() {
	var force = new nengi.Vector2(
		this.heading.x * 150000 * (this.tier+1),
		this.heading.y * 150000 * (this.tier+1)
	)
	return force
}

// specify which properties should be networked, and their value types
Projectile.prototype.netSchema = nengi.createEntitySchema({
	'id': nengi.UInt16,
	'type': nengi.UInt8,
	'x': nengi.Int16,
	'y': nengi.Int16,
	'rotation': nengi.Rotation,
	'tier': nengi.UInt8
}, {
	'x': { delta: true, binaryType: nengi.Int8 },
	'y': { delta: true, binaryType: nengi.Int8 }
})

module.exports = Projectile

