

function ChevronView(entity) {
	PIXI.Container.call(this)
	this.id = entity.id
	this.x = entity.x
	this.y = entity.y
	this.rotation = entity.rotation
	this.scale.x = this.scale.y = entity.tier

	this.hull = new PIXI.Sprite.fromFrame('chevron.png')
	this.hull.anchor.x = this.hull.anchor.y = 0.5
	this.addChild(this.hull)
	this.alpha = random(20,50)/100	

	this.isFadingIn = true

	// DEBUG Draw the shape of the collider
	var triangle = new PIXI.Graphics()
	triangle.beginFill(0x00FF00)
	triangle.lineStyle(1,0xff00ff,1)
	triangle.moveTo(0, -10)
	triangle.lineTo(14, 9)
	triangle.lineTo(-14, 9)
	triangle.endFill()
	//this.addChild(triangle)
}

ChevronView.prototype = Object.create(PIXI.Container.prototype)
ChevronView.prototype.constructor = ChevronView

ChevronView.prototype.setup = function() {

}

Object.defineProperty(ChevronView.prototype, 'tier', {
	get: function() { return this.tier },
	set: function(value) { 
		this.tier = value 
		this.scale.x = this.scale.y = value
	}
})


var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
ChevronView.prototype.update = function() {

	if (this.isFadingIn) {
		this.alpha += 0.05
	} else {
		this.alpha -= 0.05
	}	

	if (this.alpha >= 0.5) {
		this.isFadingIn = false
	}

	if (this.alpha <= 0.2) {
		this.isFadingIn = true
	}
}


module.exports = ChevronView

