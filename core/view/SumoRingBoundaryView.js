function SumoRingBoundaryView(x, y, radius) {
	PIXI.Graphics.call(this)
	this.x = x
	this.y = y
	this.lineStyle(3,0xff00ff,1)
	//this.beginFill(0xff00ff)
	this.drawCircle(0, 0, radius)
	var sprite = new PIXI.Sprite.fromFrame('stars1000.png')

	// TODO fix, this background doesnt really line up
	var tiling = new PIXI.TilingSprite(sprite.texture, 1000, 1000)
	tiling.x = -500
	tiling.y = -500
	this.addChild(tiling)
}

SumoRingBoundaryView.prototype = Object.create(PIXI.Graphics.prototype)
SumoRingBoundaryView.prototype.constructor = SumoRingBoundaryView

module.exports = SumoRingBoundaryView