var nengi = require('../../../nengi/nengi')
var MathEx = nengi.MathEx

function ExplosionEffect() {
	PIXI.Container.call(this)
	this.yellowLayers = []
	this.redLayers = []
	this.smokeLayers = []

	this.hasExploded = false


	for (var i = 0; i < 3; i++) {
		var particle = new Particle(0, 0)
		this.addChild(particle)
		this.smokeLayers.push(particle)
	}
	for (var i = 0; i < 3; i++) {
		var particle = new Particle(0, 0)
		this.addChild(particle)
		this.yellowLayers.push(particle)
	}
	for (var i = 0; i < 3; i++) {
		var particle = new Particle(0, 0)
		this.addChild(particle)
		this.redLayers.push(particle)
	}

	this.startTimestamp = Date.now()
	this.configureYellow()
	this.configureRed()
	this.configureSmoke()

	this.alpha = 0

	/*
	this.sound = new Howl({
		urls:['sounds/full-explosion.ogg'],
		volume: 1.0
		//pos3d: [500, 500, -0.5]
	})
*/
}


ExplosionEffect.prototype = Object.create(PIXI.Container.prototype)
ExplosionEffect.prototype.constructor = ExplosionEffect


ExplosionEffect.prototype.configureYellow = function() {
	for (var i = 0; i < this.yellowLayers.length; i++) {
		var particle = this.yellowLayers[i]
		particle.x = 0
		particle.y = 0
		particle.timestamp = Date.now()
		particle.blendMode = PIXI.BLEND_MODES.ADD
		particle.tint = 0xffff22
		particle.fadeTime = 4000		
		particle.scale.x = particle.scale.y = MathEx.random(20,40)/100
		particle.alpha = 1		
		particle.vx = MathEx.random(-370, 370)
		particle.vy = MathEx.random(-370, 370)
		particle.rotationRate = MathEx.random(-15, 15)
	}
}

ExplosionEffect.prototype.configureRed = function() {
	for (var i = 0; i < this.redLayers.length; i++) {
		var particle = this.redLayers[i]
		particle.x = 0
		particle.y = 0
		particle.timestamp = Date.now()
		particle.blendMode = PIXI.BLEND_MODES.ADD
		particle.tint = 0xff2222
		particle.fadeTime = 4000		
		particle.scale.x = particle.scale.y = MathEx.random(20,40)/100
		particle.alpha = 1		
		particle.vx = MathEx.random(-370, 370)
		particle.vy = MathEx.random(-370, 370)
		particle.rotationRate = MathEx.random(-15, 15)
	}
}

ExplosionEffect.prototype.configureSmoke = function() {
	for (var i = 0; i < this.smokeLayers.length; i++) {
		var particle = this.smokeLayers[i]
		particle.x = 0
		particle.y = 0
		particle.timestamp = Date.now()
		//particle.blendMode = PIXI.blendModes.ADD
		particle.tint = 0x111111
		particle.fadeTime = 4000		
		particle.scale.x = particle.scale.y = MathEx.random(20,40)/100
		particle.alpha = 0.0	
		particle.vx = MathEx.random(-370, 370)
		particle.vy = MathEx.random(-370, 370)
		particle.rotationRate = MathEx.random(-15, 15)
	}
}

ExplosionEffect.prototype.reset = function() {
	
	this.hasExploded = false
	this.configureYellow()
	this.configureRed()
	this.configureSmoke()
	console.log('ExplosionEffect.reset invoked')
}

ExplosionEffect.prototype.begin = function() {
	this.startTimestamp = Date.now()
	this.configureYellow()
	this.configureRed()
	this.configureSmoke()
	//this.sound.play()
}

ExplosionEffect.prototype.update = function(delta) {

	var now = Date.now()

	for (var i = 0; i < this.yellowLayers.length; i++) {
		var particle = this.yellowLayers[i]
		particle.x += particle.vx * delta
		particle.y += particle.vy * delta
		particle.vx *= 0.005//1 - 122.25 * delta
		particle.vy *= 0.005//1 - 122.25 * delta
		particle.rotationRate *= 1 - 1.25 * delta
		particle.rotation += particle.rotationRate * delta
		var alpha = 1 - (now - particle.timestamp) / particle.fadeTime
		if (alpha <= 0) alpha = 0.001
		particle.alpha = alpha


		particle.vx *= alpha
		particle.vy *= alpha
		//console.log(alpha)
		particle.scale.x += 1.1 * alpha * delta
		particle.scale.y += 1.1 * alpha * delta		
	}

	for (var i = 0; i < this.redLayers.length; i++) {
		var particle = this.redLayers[i]
		particle.x += particle.vx * delta
		particle.y += particle.vy * delta
		particle.vx *= 0.005//1 - 122.25 * delta
		particle.vy *= 0.005//1 - 122.25 * delta
		particle.rotationRate *= 1 - 1.25 * delta
		particle.rotation += particle.rotationRate * delta
		var alpha = 1 - (now - particle.timestamp) / particle.fadeTime
		if (alpha <= 0) alpha = 0.001


		particle.alpha = alpha

		particle.vx *= alpha
		particle.vy *= alpha
		particle.scale.x += 1.1 * alpha * delta
		particle.scale.y += 1.1 * alpha * delta			
	}

	for (var i = 0; i < this.smokeLayers.length; i++) {
		var particle = this.smokeLayers[i]
		particle.x += particle.vx * delta
		particle.y += particle.vy * delta
		particle.vx *= 0.005//1 - 122.25 * delta
		particle.vy *= 0.005//1 - 122.25 * delta
		particle.rotationRate *= 1 - 1.25 * delta
		particle.rotation += particle.rotationRate * delta
		var alpha = 1 - (now - particle.timestamp) / particle.fadeTime
		if (alpha <= 0) alpha = 0.00
		//var realAlpha = 0
		if (now - particle.timestamp < 2000) {
			particle.alpha = (now - particle.timestamp) / 2000
			//console.log('this CASE')
		} else {
			var alpha0 = particle.alpha - (0.015 * delta)		

			if (particle.alpha0 < 0) {
				particle.alpha = 0.00
			} else {
				particle.alpha = alpha0
			}
		}
		
		particle.vx *= alpha
		particle.vy *= alpha
		particle.scale.x += 0.8 * alpha * delta
		particle.scale.y += 0.8 * alpha * delta	
		//console.log(alpha)	
	}
}




function Particle() {
	PIXI.Sprite.call(this, PIXI.Texture.fromFrame('compressed-cloud.png'))
	this.anchor.x = this.anchor.y = 0.5
	//console.log('new particle created')
	Particle.total++
}
Particle.constructor = Particle
Particle.prototype = Object.create(PIXI.Sprite.prototype)
Particle.total = 0

module.exports = ExplosionEffect