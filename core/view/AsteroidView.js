

function AsteroidView(entity) {
	PIXI.Container.call(this)
	this.id = entity.id
	this.x = entity.x
	this.y = entity.y
	this.radius = entity.radius
	// perhaps 'hull' isn't the most accurate term for the asteroid
	this.hull = new PIXI.Sprite.fromFrame('asteroid-v1.png')
	this.hull.anchor.x = this.hull.anchor.y = 0.5
	this.hull.scale.x = this.hull.scale.y = entity.radius/40

	this.addChild(this.hull)

	this.isFadingIn = true
	this.rotationVelocity = random(-40, 40)/10

	// DEBUG draw the collider
	var circle = new PIXI.Graphics()
	circle.lineStyle(1,0xff00ff,1)
	circle.drawCircle(0, 0, this.radius)
	circle.alpha = 0.3
	this.addChild(circle)
}

AsteroidView.prototype = Object.create(PIXI.Container.prototype)
AsteroidView.prototype.constructor = AsteroidView

var random = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}


AsteroidView.prototype.update = function(delta) {
	this.rotation += this.rotationVelocity * delta
}


module.exports = AsteroidView

