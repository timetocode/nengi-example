var ExplosionEffect = require('./effect/ExplosionEffect')

function SumoShipView(entity) {
	PIXI.Container.call(this)
	this.id = entity.id
	this.x = entity.x
	this.y = entity.y
	this.hull = new PIXI.Sprite.fromFrame('green-ship-lit.png')
	this.hull.anchor.x = this.hull.anchor.y = 0.5
	this.hull.y = -5 //offset the image a little bit, makes the rotation look better
	this.addChild(this.hull)
	this.explosionEffect = new ExplosionEffect()
	this.addChild(this.explosionEffect)

	this.isFadingIn = false
	this.weaponChargeLevel = 1

	this.isPrediction = false
	this.isMine = false

	
	this.isDeathAnimationPlaying = false
	//this.addChild(this.explosionEffect)

	// DEBUG! Draw a collision circle
	var circle = new PIXI.Graphics()
	circle.lineStyle(1,0xff00ff,1)
	circle.beginFill(0xff00ff)
	circle.drawCircle(0, 0, 25)
	circle.alpha = 0.1
	//this.addChild(circle)
}

SumoShipView.prototype = Object.create(PIXI.Container.prototype)
SumoShipView.prototype.constructor = SumoShipView


Object.defineProperty(SumoShipView.prototype, 'isAlive', {
	get: function() { return this._isAlive },
	set: function(value) { 
		
		this._isAlive = value
		if (!this._isAlive && !this.isDeathAnimationPlaying) {
			this.playDeathAnimation()
		}
		
	}
})


SumoShipView.prototype.playChargeLevel2Animation = function() {

}

SumoShipView.prototype.playDeathAnimation = function() {
	console.log('DEATH ANIMATION')
	//this.hull.tint = 0xFF0000
	this.isDeathAnimationPlaying = true
	this.explosionEffect.alpha = 1
	this.explosionEffect.scale.x = this.explosionEffect.scale.y = 0.33
	this.explosionEffect.begin()
}

SumoShipView.prototype.update = function(delta) {



	this.explosionEffect.update(60/1000)
	
	// no flickering
	if (this.weaponChargeLevel === 1) {
		this.hull.tint = 0xFFFFFF
		if (this.alpha < 1) {
			this.alpha += 0.05
		} else {
			this.alpha = 1.0
		}	
	}

	// slow flickering
	if (this.weaponChargeLevel === 2) {
		this.hull.tint = 0xFFFF88		
		if (this.isFadingIn) {
			this.alpha += 0.05
		} else {
			this.alpha -= 0.05
		}	

		if (this.alpha >= 1) {
			this.isFadingIn = false
		}

		if (this.alpha <= 0.5) {
			this.isFadingIn = true
		}
		
	}

	// fast flickering
	if (this.weaponChargeLevel === 3) {
		this.hull.tint = 0xFFFF44		
		if (this.isFadingIn) {
			this.alpha += 0.1
		} else {
			this.alpha -= 0.1
		}	

		if (this.alpha >= 1) {
			this.isFadingIn = false
		}

		if (this.alpha <= 0.5) {
			this.isFadingIn = true
		}
		
	}

	if (!this.isPrediction && this.isMine) {
		this.alpha = 0.25
	}
}


module.exports = SumoShipView

