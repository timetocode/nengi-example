var BinaryType = require('../nengi/binary/BinaryType')
var Binary = require('../nengi/binary/Binary')

module.exports = {
	// the tick rate of the game simulation
	UPDATE_RATE: 20,

	// the tick rate of rendering and input processing on client
	DRAW_RATE: 60,

	// server ip
	IP: '127.0.0.1',

	// server port
	PORT: '8080',
	
	// protocol name for websocket connection handshake
	PROTOCOL: 'nengi-protocol',

	// the number of bits used to represent 'ids' in this app
	ID_BINARYTYPE: Binary[BinaryType.UInt8],

	// the number of bits used to represent 'types' in this app
	TYPE_BINARYTYPE: Binary[BinaryType.UInt8],

	// time between ping/pong messages, in milliseconds
	// this won't occur faster than 1000/UPDATE_RATE (aka every tick)
	PING_INTERVAL: 500,

	OPTIMIZE_XY: true, // not used
	version: 123456789.0 // not used
}
