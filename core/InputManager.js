
/**
* Stores keystrokes for any given tick of the client engine
* During any given tick, a key is counted as either havng been 
* pressed for the entire tick, or not pressed at all -- there is 
* no such thing as pressing a key for a portion of a tick. This
* allows for a deterministic simulation.
* @class InputManager
* @constructor
*/

var EventEmitter = require('../nengi/external/EventEmitter')
function InputManager() {
  EventEmitter.call(this)
  this.buffer = {} 
  // current tick # of the client engine
  this.currentTick = 0 
  // mouse position
  this.mx = 0
  this.my = 0
  this.mouseDown = false
  this.Space = false
  this.camera = null
  this.lastSent = -1
  this.unsentIds = []
  this.cursor = null
  this.display = null

  this.mouseDownTimestamp = Date.now()
}

InputManager.prototype = Object.create(EventEmitter.prototype)
InputManager.prototype.constructor = InputManager

InputManager.prototype.readUnsent = function() {
  var unsent = []
  while (this.unsentIds.length > 0) {
    var id = this.unsentIds.pop()
    unsent.unshift(this.getInput(id))
  }
  return unsent
}

InputManager.prototype.addAttackCommand = function(id) {
  console.log('attack command', id)
  this.buffer[this.currentTick].attackId = id

  ee.emit('attack', id)
}

InputManager.prototype.addMoveCommand = function(x, y) {
  //console.log('move command [raw]', x, y)


  //var relativeX = this.display.convertToRelativeX(x)
  //var relativeY = this.display.convertToRelativeY(y)

  

  //ee.emit('move', { x: relativeX, y: relativeY })

  //ee.emit('mov')
}


//hackz
InputManager.prototype.draw = function(delta, tick) {
  this.update(delta, tick)
}

/**
* Creates a new tick for the InputManager
* All keystrokes are set to false for self tick, unless
* the key is still being held down
* @method update
* @param {Integer} tick A tick number from the client engine
*/
InputManager.prototype.update = function(delta, tick) {
  //console.log('input tick', tick)
  this.currentTick = tick

  var input
  if (!this.buffer[tick]) {
    //console.log('created new object')
    this.buffer[tick] = {}
  } else {
    //console.log('recycling existing object') 
  }

  input = this.buffer[tick]
  // TODO: set any key states to their default state
 // input.confirmedByServer = false
  //input.predictedX = null
  //input.predictedY = null

  input.tick = tick



  // predicted yet? change to true after applying input locally
  input.isPredicted = false
  //input.sent = false
  input.move = null
  input.attackId = null

  input.releaseStrength = 0

  // on any given frame W A S D may be registered at most one time
  input.W = false
  input.A = false
  input.S = false
  input.D = false
  
  input.One = false
  input.Two = false
  input.Three = false
  input.Four = false
  input.Five = false

  input.Space = false

  input.mouseDown = false

  if (this.mouseDown) { 
    input.mouseDown = true 
    //input.mx = this.mx
    //input.my = this.my
  }

  input.mx = this.mx
  input.my = this.my

    if (this.mouseDown) {
      //this.addMoveCommand(this.mx, this.my)
      input.mouseClickDown = { x: this.mx, y: this.my }
    }


  //input.attack = false
  //input.attackX = null
  //input.attackY = null
  //input.One = false

  // W A S D are still held down from a previous frame
  if (this.W) { input.W = true }
  if (this.A) { input.A = true }
  if (this.S) { input.S = true }
  if (this.D) { input.D = true }

  if (this.One) { input.One = true }
  if (this.Two) { input.Two = true }
  if (this.Three) { input.Three = true }
  if (this.Four) { input.Four = true }
  if (this.Five) { input.Five = true }

  if (this.Space) { input.Space = true }



  this.unsentIds.push(tick)
  //console.log(input)

  // Old inputs are never deleted! They are overwritten via the tick cycle 0-99
  //this.deleteInput(tick-30)
}

/**
* Returns the inputs for any given tick
* @method getInput
* @param {Integer} tick
*/
InputManager.prototype.getInput = function(tick) {
  return this.buffer[tick]
}

/**
* Deletes the inputs for any given tick, for example after they've been confirmed
* by the server
* @method deleteInput
* @param {Integer} tick
*/
InputManager.prototype.deleteInput = function(tick) {
  delete this.buffer[tick]
}

/**
* Begins listening for W A S D keystrokes
* @method beginCapture
* @param {DOselfElement} canvas The canvas or DOselfElement which will listen for keystrokes
*/
InputManager.prototype.beginCapture = function(canvas) {
  var self = this

  document.addEventListener('keydown', function(event) {
    //if($('#input').is(':focus') || $('#name').is(':focus')) return
    // keep track of which keys are up/down
    // and register any movement immediately even if the 
    // player lets go of the key
   //console.log('keydown:' + event.keyCode)


    // W A S D
    if (event.keyCode === 87 || event.keyCode === 38) { 
      self.W = true
      self.buffer[self.currentTick].W = true
    } 
    if (event.keyCode === 65 || event.keyCode === 37) { 
      self.A = true
      self.buffer[self.currentTick].A = true
    }
    if (event.keyCode === 83 || event.keyCode === 40) { 
      self.S = true 
      self.buffer[self.currentTick].S = true
    }
    if (event.keyCode === 68 || event.keyCode === 39) { 
      self.D = true
      self.buffer[self.currentTick].D = true
    }

    // 1 through 5
    if (event.keyCode === 49) {
      self.One = true
      self.buffer[self.currentTick].One = true
    }
    if (event.keyCode === 50) {
      self.Two = true
      self.buffer[self.currentTick].Two = true
    }
    if (event.keyCode === 51) {
      self.Three = true
      self.buffer[self.currentTick].Three = true
    }
    if (event.keyCode === 52) {
      self.Four = true
      self.buffer[self.currentTick].Four = true
    }
    if (event.keyCode === 53) {
      self.Five = true
      self.buffer[self.currentTick].Five = true
    }

    // spacebar
    if (event.keyCode === 32) {
      self.Space = true
      self.buffer[self.currentTick].Space = true
    }

    //if (event.keyCode === 49) { 
      //self.D = true
    //  self.buffer[self.currentTick].One = true
    //} 
  })

  document.addEventListener('keyup', function(event) {
    // don't capture game input if user is chatting / changing their name
    //if($('#input').is(':focus') || $('#name').is(':focus')) return

    // W or up arrow
    if (event.keyCode === 87 || event.keyCode === 38) { self.W = false }
    // A or left arrow
    if (event.keyCode === 65 || event.keyCode === 37) { self.A = false }
    // S or down arrow
    if (event.keyCode === 83 || event.keyCode === 40) { self.S = false }
    // D or right arrow
    if (event.keyCode === 68 || event.keyCode === 39) { self.D = false }

    // 1 through 5
    if (event.keyCode === 49) { self.One = false }
    if (event.keyCode === 50) { self.Two = false }
    if (event.keyCode === 51) { self.Three = false }
    if (event.keyCode === 52) { self.Four = false }
    if (event.keyCode === 53) { self.Five = false }

    // spacebar
    if (event.keyCode === 32) { self.Space = false }
  })

  document.addEventListener('mousemove', function(e) {
    if (e.offsetX !== undefined && e.offsetY !== undefined) { 
      self.mx = e.offsetX
      self.my = e.offsetY
    } else {
      self.mx = e.layerX
      self.my = e.layerY
    }

    if (self.cursor) {
      self.cursor.x = self.mx
      self.cursor.y = self.my
    }



    //console.log(self.mx, self.my)
  })

  // disable right click context menu
  /*
  canvas.oncontextmenu = function(e) {
    e.preventDefault()
  }
  */


  document.addEventListener('mousedown', function(e) {
  
    var rightclick
    if (!e) var e = window.event
    if (e.which) rightclick = (e.which == 3)
    else if (e.button) rightclick = (e.button == 2)

    self.buffer[self.currentTick].mouseDown = true
    self.mouseDown = true
    self.buffer[self.currentTick].mx = self.mx
    self.buffer[self.currentTick].my = self.my

    console.log('mouse clicked', 
      self.buffer[self.currentTick].mx,
      self.buffer[self.currentTick].my
    )

    if (rightclick) {
      console.log('right click')
     // event.preventDefault()
      //
    } else {
      //if (self.cursor.state === 1) {
      self.buffer[self.currentTick].mouseClickDown = { x: self.mx, y: self.my }
        //..self.addMoveCommand(self.mx, self.my)
      self.mouseDownTimestamp = Date.now()

      self.emit('mouseDown', self.mx, self.my)
      //}
    }
  })

  document.addEventListener('mouseup', function(event) {
    self.mouseDown = false
    self.emit('mouseUp', self.mx, self.my)

    //self.buffer[self.currentTick].releaseStrength = Date.now() - self.mouseDownTimestamp
  })

  // EXPANDABLE: could listen for other keys or mouse actions
}

module.exports = InputManager
