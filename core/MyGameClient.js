var nengi = require('../nengi/nengi')

var Move = require('./model/input/Move')
var ChargeWeapon = require('./model/input/ChargeWeapon')
var DischargeWeapon = require('./model/input/DischargeWeapon')
var SumoShipView = require('./view/SumoShipView')
var SumoShip = require('./model/SumoShip')
var ChevronView = require('./view/ChevronView')
var AsteroidView = require('./view/AsteroidView')
var SumoRingBoundary = require('./model/SumoRingBoundary')
var SumoRingBoundaryView = require('./view/SumoRingBoundaryView')
var InputManager = require('./InputManager')
var common = require('./common')


function MyGameClient() {
	// TODO: make these things clientside components
	this.app = new nengi.Application(common)
	this.inputManager = new InputManager()

	this.queuedAttacks = []
	// even the pixi stuff could be a component
	this.stage = null
	this.background = null
	this.foreground = null
	this.renderer = null

	this.graphics = []
	this.graphicsRef = {}

	this.meGraphic = null
}


var predictionContext = {
	id: -1,
	last: -1
}


MyGameClient.prototype.update = function(delta) {
	this.app.update(delta)



	//console.log('ping', this.app.registrar.ping)

	// clientside objects running update (usually to run their own rendering stuff)
	for (var i = 0; i < this.graphics.length; i++) {

		var obj = this.graphics[i]
		if (typeof obj.update !== 'undefined') {
			obj.update(delta)
		}
	}

	// send all of the inputs that were buffered during the last frame
	var unsent = this.inputManager.readUnsent()
	// any movement (all buffered by inputManager)
	for (var i = 0; i < unsent.length; i++) {
		var rawInput = unsent[i]

		var move = new Move()
		move.W = rawInput.W
		move.A = rawInput.A
		move.S = rawInput.S
		move.D = rawInput.D
		move.mx = rawInput.mx
		move.my = rawInput.my
		//console.log(move)
		this.app.send(move)

		if (predictionContext.id > -1) {
			predictionContext.last = this.app.registrar.lastConfirmedTick
			//console.log(predictionContext.last)
			predictionContext.model.move(move)

			//console.log(predictionContext.model.physics.velocity, predictionContext.model.position)

			//console.log('PHYS?', predictionContext.model.physics)
			//console.log(predictionContext.model)
			predictionContext.model.physics.update(delta)
			//console.log(predictionContext.model.position)
			predictionContext.cl.x = predictionContext.model.x
			predictionContext.cl.y = predictionContext.model.y

			//predictionContext.cl.x++


		}
	}



	this.renderer.render(this.stage)
	this.inputManager.update()


}

MyGameClient.prototype.setup = function() {
	//app.entityInterpolator.isEnabled = false
	var self = this

	// setup the pixi stuff
	this.stage = new PIXI.Container()
	this.stage.scale.x = this.stage.scale.y = 1
	this.background = new PIXI.Container()
	this.foreground = new PIXI.Container()
	this.stage.addChild(this.background)
	this.stage.addChild(this.foreground)

	this.renderer = PIXI.autoDetectRenderer(
		window.innerWidth,
		window.innerHeight,
		{ antialiasing: false, transparent: false, resolution: 1 }
	)
	document.body.appendChild(this.renderer.view)

	setTimeout(function() {
		// setup the sumo ring and background (no backgound yet)
		var sumoRing = new SumoRingBoundary()
		var sumoRingView = new SumoRingBoundaryView(
			sumoRing.x, 
			sumoRing.y, 
			sumoRing.radius
		)
		self.background.addChild(sumoRingView)
	}, 1000)


	// connects nengi
	this.app.connect()

	// input bindings...
	this.inputManager.on('mouseUp', function(x, y) {
		var discharge = new DischargeWeapon()
		discharge.x = x
		discharge.y = y
		self.app.send(discharge)
	})

	this.inputManager.on('mouseDown', function(x, y) {
		var charge = new ChargeWeapon()
		charge.x = x
		charge.y = y
		self.app.send(charge)
	})

	this.inputManager.beginCapture()

	/*
	this.app.on('connect', function() {		
	})
	*/

	// receive nengi messages
	this.app.on('createMessage', function(message) {
		// Doesn't really matter yet, but will be used for prediction later


		console.log('message', message)
		if (message.n_type === 'IdentityMessage') {
			//return

				
				//self.myId = message.identity
				//predictionContext.id = message.identity

				//var auth = self.graphicsRef[message.identity]
				//predictionContext.sv = auth

				self.app.prediction.createPredictionContext(message.identity, true)

				setTimeout(function() {
					self.graphicsRef[message.identity].isMine = true
				}, 1000)
				

				/*
				var model = new SumoShip()
				predictionContext.model = model
				model.x = auth.x
				model.y = auth.y

				console.log('ID', message.identity, auth)

				predictionContext.cl = new SumoShipView(predictionContext.sv)
				self.foreground.addChild(predictionContext.cl)
				*/

			//var graphics = n	
		}
	})

	this.app.on('prediction', function(entity) {
		//console.log('prediction', entity)

		if (!self.meGraphic) {
			self.meGraphic = new SumoShipView(entity)
			self.foreground.addChild(self.meGraphic)
		} else {
			self.meGraphic.x = entity.x
			self.meGraphic.y = entity.y
			self.meGraphic.rotation = entity.rotation + (Math.PI / 180) * 180
			self.meGraphic.isPrediction = true
			self.meGraphic.hull.tint = 0xFFFF88
			self.meGraphic.update(60/1000)
		}
	})


	// receive nengi entities
	this.app.on('createEntity', function(entity) {
		// TODO: clean this up
		//console.log('createEntity', entity)
		var graphics = null
		if (entity.constructor.name === 'SumoShip') {
			graphics = new SumoShipView(entity)			
		} else if (entity.constructor.name === 'Projectile') {
			graphics = new ChevronView(entity)
		} else if (entity.constructor.name === 'Asteroid') {
			graphics = new AsteroidView(entity)

		}
		graphics.originalEntity = entity

		

		self.foreground.addChild(graphics)
		self.graphics.push(graphics)
		self.graphicsRef[entity.id] = graphics
		console.log('createEntity', entity.id)		
	})

	// receive nengi entity updates
	this.app.on('updateEntity', function(id, changes) {
		//console.log('update received for', id, changes)
		var graphics = self.graphicsRef[id]
		//TODO: make a helper method that copies changes into a ViewEntity
		for (var i = 0; i < changes.length; i++) {
			var change = changes[i]
			graphics[change.property] = change.value
		}
	})

	// receive nengi entity deletes
	this.app.on('deleteEntity', function(id) {
		var graphics = self.graphicsRef[id]
		self.foreground.removeChild(graphics)

	    var index = -1
	    for (var i = 0; i < self.graphics.length; i++) {
	        if (self.graphics[i].id === id) {
	            index = i
	            break
	        }
	    }
	    if (index !== -1) {
	        self.graphics.splice(index, 1)
	    }
	    delete self.graphicsRef[id]
	})

}

module.exports = MyGameClient