var http = require('http')
var url = require('url')
var path = require('path')
var fs = require('fs')

// are these really needed? browsers are pretty good at guessing nowadays
var mimeTypes = {
    'html': 'text/html',
    'jpeg': 'image/jpeg',
    'jpg': 'image/jpeg',
    'png': 'image/png',
    'js': 'text/javascript',
    'css': 'text/css'
}

function HTTPServer() {

}

/* returns an instance of an http server */
HTTPServer.createServer = function() {
    return http.createServer(function(req, res) {

        // serve from the ../client/public/ folder
        var filePath = '../client/public' + req.url
        if (filePath === '../client/public/') {
            filePath = '../client/public/index.html'
        }

        var fileExtension = path.extname(filePath)
        var contentType = mimeTypes[fileExtension]

        fs.readFile(filePath, function(error, content) {
            if (!content) {
                res.end('aaah')
            }
            if (error) {
                if(error.code == 'ENOENT'){                   
                    res.writeHead(200, { 'Content-Type': contentType })
                    res.end()
                    return             
                }
                else {
                    res.writeHead(500)
                    res.end('unknown error: ' + error.code)
                    return
                }
            }
            else {
                console.log('serving')
                res.writeHead(200, { 'Content-Type': contentType })
                res.end(content, 'utf-8')
                return
            }
        })
    })
}

module.exports = HTTPServer