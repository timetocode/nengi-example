// TO RUN A PROFILER:
// downgrade to node v0.10.0
// install webkit-devtools-agent (after making sure node -v is 0.10.0)
// uncomment the next two lines of code! (look up the profiler for a tutorial)
//var agent = require('webkit-devtools-agent');
//agent.start()




var HTTPServer = require('./HTTPServer')
var WebSocketServer = require('websocket').server
var nengi = require('../nengi/nengi')
var ServersideReader = require('../nengi/network/ServersideReader')
var common = require('../core/common')
var config = common.config

console.log('common', common)

//console.log(config)
var serversideReader = new ServersideReader(common)
var server = HTTPServer.createServer()
server.listen(config.PORT, function() {
    console.log((new Date()) + ' Server is listening on port ' + config.PORT)
})

// start the websocket server
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
})

function originIsAllowed(origin) {
    // accepts all connections! could implement a banlist here instead
    return true
}

//var clients = []
var id = 0

/*
* This demo uses a single instance whose game logic is contained in MainInstance
*/
var MainInstance = require('../core/MainInstance')
var mainInstance = new MainInstance(common)
    
wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      request.reject()
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.')
      return
    }
    
    var connection = request.accept(config.PROTOCOL, request.origin)

    var client = new nengi.Client(common)
    client.connection = connection
    client.instance = mainInstance
    //clients.push(client)

    //client.instance.acceptConnection(client)
    client.instance.emit('connect', client)

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            // ignore
            //console.log('Received Message: ' + message.utf8Data)
            //connection.sendUTF(message.utf8Data)
        } else if (message.type === 'binary') {
            serversideReader.read(message.binaryData, client)
        }
    })
    
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.')

        client.instance.emit('disconnect', client)
    })
})

/*
* Run the main loop, in this case just calling update on mainInstance
*/
var main = function(delta, tick, now) {
    mainInstance.n_update(delta, tick, now)
}

nengi.NodeLoop.setFPS(config.UPDATE_RATE)
nengi.NodeLoop.setMain(main)
nengi.NodeLoop.begin()
